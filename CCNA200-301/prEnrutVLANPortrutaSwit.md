# Práctica de laboratorio CPT Enrutamiento VLAN con Puerto de Ruta de Switch

![alt text](image-183.png)

## Pasos previos

- Se deben configurar las VLAN tal como se indican en la figura.
- Se deben configurar las IP/Máscaras en las interfaces.

## Configurar el Switch

### Paso 1

```switch
sw1> enable
sw1# configure terminal
sw1(config)# ip routing
sw1(config)# end

sw1# show ip route

sw1# configure terminal
sw1(config)# interface vlan 10
sw1(config-if)# ip address 10.1.10.1 255.255.255.0
sw1(config-if)# interface vlan 20
sw1(config-if)# ip address 10.1.20.1 255.255.255.0
sw1(config-if)# end

sw1# show ip route
```

### Paso 2

```switch
sw1> enable
sw1# configure terminal
sw1(config)# interface gigabitethernet 0/1
sw1(config-if)# no shutdown
sw1(config-if)# no switchport   <!-El puerto se convierte en route-port-!>
sw1(config-if)# ip address 10.1.30.1 255.255.255.0
sw1(config-if)# exit
sw1(config)# ip route 0.0.0.0 0.0.0.0 10.1.30.2
sw1(config)# end

```

## Pruebas

1. Desde PC0 hacer `ping 10.1.10.1` puerta de enlace.
2. Desde PC0 en VLAN-10 hacer `ping 10.20.2` PC1 en VLAN-20.

## Configurar el Router

```router
R1> enable
R1# configure terminal
R1(config)# interface gigabitethernet 0/0
R1(config-if)# ip address 10.1.30.2 255.255.255.0
R1(config-if)# no shutdown
R1(config-if)# exit
R1(config)# end

R1# show ip route

R1# configure terminal
R1(config)# ip route 10.0.0.0 255.0.0.0 10.1.30.1
R1(config)# exit

R1# ping 10.1.10.2
R1# ping 10.1.20.2

R1# configure terminal
R1(config)# interface loopback 0        <!-creamos un bucle invertido de la interfaz-!>
R1(config-if)# ip address 8.8.8.8 255.255.255.0
R1(config-if)# exit
```
