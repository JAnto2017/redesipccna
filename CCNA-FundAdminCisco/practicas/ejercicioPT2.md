# PRÁCTICA PT2 PROTOCOLO DINÁMICA OSPF

Configurar los routers para que exista convergencia.

![alt text](image-25.png)

## R1

```R1
    ...
    (config)# router ospf 1
    (config-router)# network 192.168.1.0 0.0.0.255 area 0

    # show ip ospf neigbor
    # show ip route
```

## R2

```R2
    # show ip route
    # show ip ospf neighbor
    # show ip protocols
    ...
    (config)# router ospf 1
    (config-router)# network 2.2.2.2 0.0.0.0 area 0

    # show ip protocols

```

### R3

```R3
    # show ip route
    # show ip ospf neighbor
    # show ip protocols
    ...
    (config)# 
```

### R4

```R4
    ...
    # show ip protocols
    # show ip ospf neighbors
    # show running-config

    (config)# router ospf 1
    (config-router)# do show ip interface brief
    (config-router)# network 10.10.10.1 0.0.0.3 area 0
    (config-router)# network 10.10.10.14 0.0.0.3 area 0
    (config-router)# network 4.4.4.4 0.0.0.0 area 0
```

### R5

```R5
    ...
    (config)# interface serial 0/0/1
    (config-if)# ip address 10.10.10.9 255.255.255.252

    # debug ip ospf events

    (config)# interface serial 0/0/0
    (config-if)# ip ospf hello-interval 3
    (config-if)# ip ospf dead-interval 12
```

### EDGE

```EDGE
    ...
    # show running-config

    (config)# router ospf 1
    (config-router)# default-information originate
```
