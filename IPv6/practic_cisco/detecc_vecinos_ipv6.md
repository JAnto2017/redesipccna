 Packet Tracer - Detección de vecinos IPv6
Tabla de direccionamiento

![alt text](image-4.png)

Objetivos

Parte 1: Red local de detección de vecinos IPv6

Parte 2: Red remota de detección de vecinos IPv6
Aspectos básicos

Para que un dispositivo se comunique con otro dispositivo, se debe conocer la dirección MAC del destino. Con IPv6, un proceso denominado Neighbor Discovery (Descubrimiento de vecino) que utiliza el protocolo NDP o ND es responsable de determinar la dirección MAC de destino. Recopilará información de PDU en modo de simulación para comprender mejor el proceso. No hay puntuación de Packet Tracer para esta actividad.
Instrucciones
Parte 1: Red local de descubrimiento de vecinos IPv6

En esta parte, obtendrá la dirección MAC de un dispositivo de destino en la misma red.
Paso 1: Revise el router para ver si hay algún vecino que haya descubierto.

a.     Haga clic en el Router RTA. Seleccione la pestaña CLI e ingrese el comando show ipv6 neighbors desde el modo EXEC privilegiado. Si se muestran entradas, elimínelas con el comando clear ipv6 neighbors.

b.     Haga clic en PCA1, seleccione la pestaña Escritorio y haga clic en el icono Símbolo del sistema.
Paso 2: Cambie al modo de simulación para capturar eventos.

a.     Haga clic en el botón de Simulación en la esquina inferior derecha de la ventana Topología de Packet Tracer.

b.     Haga clic en el botón Show All/None (Mostrar todos / ninguno) en el panel de simulación hasta que los Filtros de la lista de eventos - Eventos visibles (Event List Filters – Visible Events) muestren Ninguno (None).

c.     Hagan clic en Edit Filters (Editar filtros). Seleccione la pestaña IPv6 en la parte superior y marque las casillas para ICMPv6 y NDP . Cierre la ventana Editar filtros de ACL. Ahora, los filtros de la lista de eventos muestran ICMPv6 y NDP.

d.     Desde el símbolo del sistema en PCA1, ejecute el comando ping –n 1 2001:db8:acad:1::b. Esto iniciará el proceso de hacer ping PCA2.

e.     Haga clic en Reproducir (manos libres) en los CONTROLES DE REPRODUCCIÓN, fuera del panel de simulación. Si se le solicita, haga clic en Ver eventos anteriores en la ventana Buffer Full - Packet Tracer. Debe tener aproximadamente 12 entradas en la ventana.
Pregunta:

¿Por qué están presentes PDU ND?
Área de respuesta

Para enviar paquetes de ping ICMPv6 a PCA2, PCA1 necesita conocer la dirección MAC del destino. IPv6 ND solicita esta información en la red.

 f.      Haga clic en el cuadrado de la columna Tipo para el primer evento, que debería ser ICMPv6.
Pregunta:

Debido a que el mensaje comienza con este evento, solo hay un PDU de salida. Debajo de la pestaña Modelo OSI, ¿cuál es el tipo de mensaje listado para ICMPv6?
Área de respuesta

Tipo de mensaje de eco ICMPv6: 128

 Observe que no hay direccionamiento de capa 2. Haga clic en el botón Next Layer >> (Siguiete capa) para obtener una explicación sobre el proceso ND (Neighbor Discovery).

g.     Haga clic en el cuadrado situado junto al siguiente evento en el Panel de simulación. Debe estar en el dispositivo PCA1 y el tipo debe ser NDP.
Preguntas:

¿Qué cambió en el direccionamiento de Capa 3?
Área de respuesta

La dirección de destino ahora es una dirección de multidifusión IPv6 de FF02::1:FF00:B

 ¿Qué direcciones de capa 2 se muestran?
Área de respuesta

La dirección de origen es PCA1 MAC - 0001.427E.E8ED y la dirección de destino MAC es 3333.FF00.000B

 Cuando un host no conoce la dirección MAC del destino, IPv6 Neighbor Discovery utiliza una dirección MAC de multidifusión especial como dirección de destino de capa 2.

h.     Vuelva a la lista de eventos. Seleccione el primer evento NDP en el SwitchA.
Pregunta:

¿Hay alguna diferencia entre las capas de entrada y salida para la capa 2?
Área de respuesta

No. El interruptor no altera la información de la capa 2, solo reenvía la trama.

 i.       Seleccione el primer evento NDP en PCA2 . Haga clic en la ficha de Detalles de la PDU saliente.
Pregunta:

¿Qué direcciones se muestran para lo siguiente?

Nota: Las direcciones en los campos pueden estar ajustadas, ajuste el tamaño de la ventana de la PDU para que la información de la dirección sea más fácil de leer.

Ethernet II DEST ADDR:
Área de respuesta

0001.427E.E8ED

 Ethernet II SRC ADDR:
Área de respuesta

0040.0B02:.243E

IPv6 SRC IP:
Área de respuesta

2001:db8:acad:1::b

IPv6 DST IP:
Área de respuesta

2001:db8:acad:1::a

 j.       Seleccione el primer evento NDP en RTA . ¿Por qué no hay capas de salida?
Área de respuesta

La dirección IPv6 no coincide con la dirección del enrutador, por lo que descarta el paquete.

 k.     Haga clic en el botón Next Layer >> (siguiente capa) hasta el final y lea los pasos del 4 hasta el 7 para obtener más información.

l.       Haga clic en el siguiente evento ICMPv6 en PCA1 .
Pregunta:

¿PCA1 ahora tiene toda la información necesaria para comunicarse con PCA2?
Área de respuesta

Sí, ahora conoce tanto la dirección IPv6 de destino como la dirección MAC de destino de PCA2.

 m.    Haga clic en el último evento ICMPv6 en PCA1 . Observe que esta es la última comunicación listada.
Pregunta:

¿Cuál es el tipo de mensaje de eco ICMPv6?
Área de respuesta

El tipo de mensaje de eco ICMPv6 es 129, una respuesta de eco.

 n.     Haga clic en Reset Simulation (Restablecer simulación) en el panel de simulación. Desde el símbolo del sistema de PCA1, repita el ping a PCA2. (Sugerencia: debería poder presionar la flecha hacia arriba para devolver el comando anterior.)

o.     Haga clic en Reproducir (manos libres) para completar el proceso de ping. Haga clic en Ver eventos anteriores (View Previous Events) si se le solicita.
Pregunta:

¿Por qué no hubo eventos NDP?
Área de respuesta

PCA1 ya conoce la dirección MAC de PCA2, por lo que no necesita usar Neighbor Discovery.

 Parte 2: Detección de Vecinos IPv6 de una Red remota

En la Parte 2 de esta actividad, realizará pasos similares a los de la parte anterior, excepto que en este caso, el host de destino está en otra LAN. Observe cómo el proceso de descubrimiento de vecinos difiere del proceso que observó en la parte anterior. Preste mucha atención a algunos de los pasos de direccionamiento adicionales que tienen lugar cuando un dispositivo se comunica con un dispositivo que está en una red diferente.
Paso 1: Capturar eventos para comunicación remota.

a.     Haga clic en el botón Reset Simulation (Restablecer simulación) para borrar los eventos anteriores. Muestre y borre cualquier entrada en la tabla de dispositivos vecinos IPv6 como se hizo en la parte anterior.

b.     Verifique que solo ICMPv6 y NDP aparezcan en los filtros de la lista de eventos: Eventos visibles.

c.     Desde el símbolo del sistema en PCA1, emita el comando ping –n 1 2001:db8:acad:2::a para hacer ping al host PCB1.

d.     Haga clic en Reproducir (manos libres) en los CONTROLES DE REPRODUCCIÓN, fuera del panel de simulación. Si se le solicita, haga clic en Ver eventos anteriores en la ventana Buffer Full - Packet Tracer.

e.     Cuando finalice el proceso de ping, haga clic en el cuadrado de la columna Tipo para el primer evento, que debe ser ICMPv6. Dado que el mensaje comienza con este evento, sólo hay una PDU saliente. Observe que le falta la información de Capa 2 como en el escenario anterior.

f.      Haga clic en el primer evento NDP en el dispositivo PCA1 .
Pregunta:

¿Qué dirección se utiliza para la IP Src en la PDU entrante?
Área de respuesta

La dirección Link Local (local del enlace) para PCA1 – fe80::201:42ff:fe7e:e8ed

 La detección de vecinos IPv6 determinará el próximo destino para reenviar el mensaje ICMPv6.

g.     Haga clic en el segundo evento ICMPv6 para PCA1. PCA1 ahora tiene suficiente información para crear una solicitud de eco ICMPv6.
Pregunta:

¿Qué dirección MAC se utiliza para el MAC de destino?
Área de respuesta

0001.961d.6301, la dirección MAC de G0/0/0 de RTA 

 h.     Haga clic en el siguiente evento ICMPv6 en el dispositivo RTA. Observe que la PDU saliente de RTA carece de la dirección de capa 2 de destino. Esto significa que RTA una vez más tiene que realizar un descubrimiento de vecinos para la interfaz que tiene la red 2001:db8:acad:2:: porque no conoce las direcciones MAC de los dispositivos en la LAN de G0/0/1.

i.       Vaya al primer evento ICMPv6 para el dispositivo PCB1.
Pregunta:

¿Qué falta en la información saliente de Capa 2?
Área de respuesta

La dirección MAC de destino debe determinarse para la dirección de destino IPv6.

 j.       Los siguientes eventos NDP están asociando las direcciones IPv6 restantes hacia las direcciones MAC. Los eventos NDP anteriores asociaron direcciones MAC con direcciones locales de enlace.

k.     Vaya al último conjunto de eventos ICMPv6 y observe que se han aprendido todas las direcciones. Ahora se conoce la información requerida, por lo que PCB1 puede enviar mensajes de respuesta de eco a PCA1.

l.       Haga clic en el botón Reset Simulation (Restablecer simulación) en el Panel de simulación. Desde el símbolo del sistema de PCA1, repita el comando para hacer ping a PCB1.

m.    Haga clic en Reproducir (manos libres) para completar el proceso de ping. Haga clic en Ver eventos anteriores si se le solicita en la ventana Búfer lleno.
Pregunta:

¿Hubo algún evento de NDP?
Área de respuesta

No.

 n.     Haga clic en el único evento PCB1 en la nueva lista.
Preguntas:

¿A qué corresponde la dirección MAC de destino?
Área de respuesta

La interfaz del router G0/0/1 en RTA

 ¿Por qué PCB1 utiliza la dirección MAC de la interfaz del router para crear sus PDU ICMP?
Área de respuesta

Dado que el dispositivo de destino está en otra red, PCB1 dirige la PDU a la interfaz de puerta de enlace predeterminada MAC. RTA determinará cómo abordar la PDU en la Capa 2 para enviarla a su destino.

 Paso 2: Examine las salidas del router.

a.     Vuelva al modo Realtime.

b.     Haga clic en RTA y seleccione la pestaña CLI. En la consola del router, ingrese el comando show ipv6 neighbors.
Preguntas:

¿Cuántas direcciones aparecen en la lista?
Área de respuesta

4 — Unidifusión global IPv6 y enlace direcciones locales y direcciones MAC para PCA1 y PCB1

 ¿Con qué dispositivos están asociadas estas direcciones?
Área de respuesta

PCA1 y PCB1

 ¿Hay alguna entrada para PCA2 en la lista? Explique.
Área de respuesta

PCA2 aún no se ha comunicado a través de la red.

 c.     Ping PCA2 desde el router.

d.     Emita el comando show ipv6 neighbors.
Pregunta:

¿Hay entradas para PCA2?
Área de respuesta

Sí, la dirección IPv6 y la dirección MAC para PCA2.

Preguntas de reflexión

1.     ¿Cuándo un dispositivo requiere el proceso de detección de vecinos IPv6?
Área de respuesta

Cuando no se conoce la dirección MAC de destino. Este proceso es similar a ARP con IPv4.

 2.     ¿Cómo ayuda un router a minimizar la cantidad de tráfico de detección de vecinos IPv6 en una red?
Área de respuesta

El router mantiene las tablas vecinas para que no tenga que iniciar ND para cada host de destino.

 3.     ¿Cómo minimiza IPv6 el impacto del proceso ND en los hosts de red?
Área de respuesta

Utiliza una dirección de multidifusión para que sólo un puñado de direcciones estén escuchando los mensajes de detección de vecinos. IPv6 crea una dirección MAC de destino de multidifusión especialmente diseñada que incluye una parte de la dirección del nodo.

 4.     ¿En qué difiere el proceso de detección de vecinos cuando un host de destino está en la misma LAN y cuando está en una LAN remota?
Área de respuesta

Cuando un host de destino está en el mismo segmento LAN, solo responde el dispositivo que coincide con la dirección IPv6 y los otros dispositivos descartan el paquete. Cuando el dispositivo es remoto, el dispositivo de puerta de enlace (normalmente un router) proporciona la dirección MAC de la interfaz en la interfaz local para el MAC de destino y, a continuación, busca la dirección MAC en la red remota. A continuación, el router colocará el par de direcciones IPv6/Mac que responde en la tabla de vecinos IPv6. (similar a una solicitud de ARP en IPv4)