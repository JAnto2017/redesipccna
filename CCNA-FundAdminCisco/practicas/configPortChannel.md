# Configuración de Port-Channel

![alt text](image-38.png "Configuración PortChannel")

La conexión entre los _switch_ se realiza mediante cable cruzado entre las interfaces _Fa-0/16_, _Fa-0/17_, _Fa-0/18_, _Fa-0/19_.

![alt text](image-39.png "Varias conexiones entre las interfaces")

## Switch S1

```S1
    ...
    S1(config)# interface range fastethernet 0/16 - fastEthernet 0/19
    S1(config-if-range)# channel-group 1 mode active

    S1(config)# interface port-channel 1
    S1(config-if)# description HACIA_S2
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 10
```

```S1
    ...
    S1(config)# interface port-channel 1
    S1(config-if)# switchport mode trunk
    S1(config-if)# switchport trunk allowed vlan 10,20
```

## Switch S2

```S2
    ...
    S2(config)# interface range fastEthernet 0/16 - fastEthernet 0/19
    S2(config-if-range)# channel-group 2 mode active

    S2# show run

    S2(config)# interface port-channel 2
    S2(config-if)# description HACIA_S1
    S2(config-if)# switchport mode access
    S2(config-if)# switchport access vlan 10

    S2# show ip int brief
```

```S2
    ...
    S2(config)# interface port-channel 2
    S2(config-if)# switchport mode trunk
    S2(config-if)# switchport trunk allowed vlan 10,20
```
