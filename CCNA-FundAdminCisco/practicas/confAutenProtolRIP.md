# PRÁCTICA AUTENTICACIÓN DEL PROTOCOLO RIP

![alt text](image-76.png "Topología practica protección protocolo RIP")

## Router R1

```router
    ...
    R1(config)# router rip
    R1(config-router)# passive-interface fastEthernet 0/0

    /*** AUTENTICAR LA INFORMACIÓN ***/

    R1(config)# key chain CLAVE_RIP
    R1(config-keychain)# key 1                              //crear una clave
    R1(config-keychain-key)# key-string cisco123
```

## Router R2

```router
    ...
    R2(config)# router rip
    R2(config-router)# passive-interface fastEthernet 0/0
```

## Router EDGE

```router
    ...
    EDGE(config)# router rip
    EDGE(config-router)# passive-interface fastEthernet 0/0
```

- - -

## AUTENTICACIÓN EIGRP

## R1

```router
    ...
    /*** PROTECCIÓN CONTRA LOS ATAQUES USANDO EIGRP ***/
    R1(config)# key chain CLAVE_EIGRP                       //crear pool de claves
    R1(config-keychain)# key 1
    R1(config-keychain-key)# key-string proteigrp
    R1(config-keychain-key)# exit
    R1(config-keychain)# exit

    R1(config)# interface serial 0/0/0
    R1(config-if)# ip authentication mode eigrp 1 MD5
    R1(config-if)# ip authentication key-chain EIGRP 1 CLAVE_EIGRP

    R1(config)# interface serial 0/0/1
    R1(config-if)# ip authentication mode eigrp 1 MD5
    R1(config-if)# ip authentication key-chain EIGRP 1 CLAVE_EIGRP
```

## R2

```router
    ...
    R2(config)# key chain CLAVE_EIGRP
    R2(config-keychain)# key 1
    R2(config-keychain-key)# key-string proteigrp
    R2(config-keychain-key)# exit
    R2(config-keychain)# exit
    R2(config)# interface serial 0/0/0
    R2(config-if)# ip authentication mode eigrp 1 MD5
    R2(config-if)# ip authentication key-chain eigrp 1 CLAVE_EIGRP

    R2(config)# interface serial 0/0/1
    R2(config-if)# ip authentication mode eigrp 1 MD5
    R2(config-if)# ip authentication key-chain eigrp 1 CLAVE_EIGRP

    /*** ENCRIPTAR LOS DATOS ***/
    R2(config)# service password-encryption
```

## EDGE

```router
    ...
    EDGE(config)# key chain CLAVE_EIGRP
    EDGE(config-keychain)# key 1
    EDGE(config-keychain-key)# key-string proteigrp
    EDGE(config-keychain-key)# exit
    EDGE(config-keychain)# exit
    EDGE(config)# interface serial 0/0/0
    EDGE(config-if)# ip authentication mode eigrp 1 MD5
    EDGE(config-if)# ip authentication key-chain eigrp 1 CLAVE_EIGRP
    
    EDGE(config)# interface serial 0/0/1
    EDGE(config-if)# ip authentication mode eigrp 1 MD5
    EDGE(config-if)# ip authentication key-chain eigrp 1 CLAVE_EIGRP
```

- - -

## AUTENTICACIÓN OSPF

### R1 OSPF

```router
    ...
    R1(config)# interface serial 0/0/0
    R1(config-if)# ip ospf message-digest-key 1 MD5 CCNA_pwd
    R1(config-if)# ip ospf authentication message-digest
    R1(config-if)# exit

    R1(config)# interface serial 0/0/1
    R1(config-if)# ip ospf message-digest-key 1 MD5 CCNA_pwd
    R1(config-if)# ip ospf authentication message-digest
    R1(config-if)# exit

    R1(config)# router ospf 1
    R1(config-router)# area 0 authentication message-digest
```

## R2 OSPF

```router
    ...
    R2(config)# interface serial 0/0/0
    R2(config-if)# ip ospf message-digest-key 1 md5 CCNA_pwd
    R2(config-if)# ip ospf authentication message-digest
    R2(config-if)# exit

    R2(config)# interface serial 0/0/1
    R2(config-if)# ip ospf message-digest-key 1 md5 CCNA_pwd
    R2(config-if)# ip ospf authentication message-digest
    R2(config-if)# exit

    R2(config)# router ospf 1
    R2(config-router)# area 0 authentication message-digest
```

## R-EDGE

```router
    ...
    EDGE(config)# interface serial 0/0/0
    EDGE(config-if)# ip ospf message-digest-key 1 md5 CCNA_pwd  //la misma clave en la misma serial de dos router
    EDGE(config-if)# ip ospf authentication message-digest

    EDGE(config)# interface serial 0/0/1
    EDGE(config-if)# ip ospf message-digest-key 1 md5 CCNA_pwd  //la misma clave en la misma serial de dos router
    EDGE(config-if)# ip ospf authentication message-digest

    EDGE(config)# router ospf 1
    EDGE(config)# area 0 authentication message-digest
```

- - -

## COMANDO AUTO SECURE

```router
    EDGE> enable
    EDGE# auto secure   //ayuda habilitar seguridad en todos los servicios
```
