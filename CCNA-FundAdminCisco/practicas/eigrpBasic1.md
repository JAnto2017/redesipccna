# PRÁCTICA BÁSICA CONFIGURACIÓN DE EIGRP

![alt text](image-11.png)

Configuración del router R1:

```R1
    > enable
    # config terminal
    (config)# router eigrp 1    //el nº indica el sistema autónomo
    
    //añadimos las redes WAN para que crea las adyacencias con otros Routers
    (config-router)# network 172.16.0.0
    (config-router)# end

    //WILD CARD es la inversa de la máscara de subred
    # config terminal
    (config)# router eigrp 1
    (config-router)# no  network 172.16.0.0 //elimina ruta
    (config-router)# network 172.16.1.0 0.0.0.255   //WILD CARD inversa de /24

    (config-router)# network 172.16.3.0 0.0.0.3   //WILD CARD inversa de /30

    (config-router)# network 192.168.10.4 0.0.0.3   //WILD CARD
```

Configuración del router R2:

```R2
    > enable
    # config terminal
    (config)# router eigrp 1 

    (config-router)# network 172.16.0.0
    (config-router)# end

    # show ip protocols

    //WILD CARD
    # config terminal
    (config)# router eigrp 1 

    (config-router)# no network 172.16.0.0

    (config-router)# network 172.16.0.0 0.0.0.3         //WILD CARD
    (config-router)# no network 172.16.2.0 0.0.0.255    //WILD CARD
    (config-router)# no network 192.168.10.8 0.0.0.3    //WILD CARD
    (config-router)# no network 10.1.1.0 0.0.0.3        //WILD CARD
```

Configuración del router R3:

```R3
    > enable
    # config terminal
    (config)# router eigrp 1

    (config-router)# no network 192.168.10.8 0.0.0.3    //WILD CARD
    (config-router)# no network 192.168.1.0 0.0.0.255   //WILD CARD
    (config-router)# no network 192.168.10.4 0.0.0.3    //WILD CARD

    # show ip eigrp neighbors
```

Cambio de la métrica en el Router:

```R1
    > enable
    # config terminal
    (config)# router eigrp 1
    (config-router)# metric weights 8 1 1 1 1 1
```

Imagen con el valor de los **pesos métricos** en un Router tras ejecutar el comando `show ip protocols`.

![alt text](image-12.png)

Modificación del ancho de banda en un router. Es recomendable que se tenga el mismo valor en ambos extremos de la red.

```R2
    > enable
    # config terminal
    (config)# interface serial 0/0/0
    (config-if)# bandwidth 64

    # show ip eigrp topology

    # show ip eigrp topology 192.168.1.0
```

Crear una ruta por defecto en el router R2:

```R2
    > enable
    # config terminal
    (config)# router eigrp 1
    (config-router)# no network 10.1.1.0 0.0.0.3
    (config-router)# exit

    (config)# ip route 0.0.0.0 0.0.0.0 loopback 1
    (config)# router eigrp 1
    (config-router)# redistribute static
    (config-router)# exit

    # show ip protocols
```
