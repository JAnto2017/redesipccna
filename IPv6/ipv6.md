# Direccionamiento de red y solución de problemas básicos

- [Direccionamiento de red y solución de problemas básicos](#direccionamiento-de-red-y-solución-de-problemas-básicos)
  - [Tipos de direcciones IPv6](#tipos-de-direcciones-ipv6)
    - [Unidifusión, Multidifusión, cualquier Difusión](#unidifusión-multidifusión-cualquier-difusión)
    - [Longitud de prefijo IPv6](#longitud-de-prefijo-ipv6)
    - [Tipos de direcciones de unidifusión IPv6](#tipos-de-direcciones-de-unidifusión-ipv6)
    - [Nota sobre la dirección local única](#nota-sobre-la-dirección-local-única)
    - [GUA IPv6](#gua-ipv6)
    - [Estructura IPv6 GUA](#estructura-ipv6-gua)
      - [Prefijo de enrutamiento global](#prefijo-de-enrutamiento-global)
      - [ID de subred](#id-de-subred)
      - [ID de interfaz](#id-de-interfaz)
    - [IPv6 LLA](#ipv6-lla)
      - [Comunicaciones de enlace local de IPv6](#comunicaciones-de-enlace-local-de-ipv6)
    - [Compruebe su comprensión](#compruebe-su-comprensión)
      - [Pregunta 1](#pregunta-1)
      - [Pregunta 2](#pregunta-2)
      - [Pregunta 3](#pregunta-3)
      - [Pregunta 4](#pregunta-4)
      - [Pregunta 5](#pregunta-5)
  - [Configuración Estática GUA y LLA](#configuración-estática-gua-y-lla)
    - [Configuración de GUA estática en un host de Windows](#configuración-de-gua-estática-en-un-host-de-windows)
    - [Configuración Estática de una dirección de Unidifusión Local de enlace](#configuración-estática-de-una-dirección-de-unidifusión-local-de-enlace)
  - [Direccionamiento Dinámico para GUA IPv6](#direccionamiento-dinámico-para-gua-ipv6)
    - [Mensajes RS y RA](#mensajes-rs-y-ra)
    - [Método 1: SLAAC](#método-1-slaac)
    - [Método 2: SLAAC y DHCPv6 sin estado](#método-2-slaac-y-dhcpv6-sin-estado)
    - [Método 3: DHCPv6 con estado](#método-3-dhcpv6-con-estado)
    - [Proceso EUI-64 versus generado aleatoriamente](#proceso-eui-64-versus-generado-aleatoriamente)
    - [Proceso EUI-64](#proceso-eui-64)
    - [ID de interfaz generadas aleatoriamente](#id-de-interfaz-generadas-aleatoriamente)
    - [Verifica su comprensión - Direccionamiento dinámico para GUA IPv6](#verifica-su-comprensión---direccionamiento-dinámico-para-gua-ipv6)
      - [Pregunta 1 - Direccionamiento dinámico para GUA IPv6](#pregunta-1---direccionamiento-dinámico-para-gua-ipv6)
      - [Pregunta 2 - Direccionamiento dinámico para GUA IPv6](#pregunta-2---direccionamiento-dinámico-para-gua-ipv6)
      - [Pregunta 3 - Direccionamiento dinámico para GUA IPv6](#pregunta-3---direccionamiento-dinámico-para-gua-ipv6)
      - [Pregunta 4 - Direccionamiento dinámico para GUA IPv6](#pregunta-4---direccionamiento-dinámico-para-gua-ipv6)
      - [Pregunta 5 - Direccionamiento dinámico para GUA IPv6](#pregunta-5---direccionamiento-dinámico-para-gua-ipv6)
  - [Direccionamiento Dinámico para LLA de IPv6](#direccionamiento-dinámico-para-lla-de-ipv6)
    - [LLA dinámicos](#lla-dinámicos)
    - [LLA dinámicos en Windows](#lla-dinámicos-en-windows)
    - [LLA dinámicos en enrutadores Cisco](#lla-dinámicos-en-enrutadores-cisco)
    - [Verificar la configuración de la dirección IPv6](#verificar-la-configuración-de-la-dirección-ipv6)
  - [Direcciones Multicast de IPv6](#direcciones-multicast-de-ipv6)
    - [Direcciones IPv6 de multidifusión asignadas](#direcciones-ipv6-de-multidifusión-asignadas)
    - [Direcciones de multidifusión IPv6 bien conocidas](#direcciones-de-multidifusión-ipv6-bien-conocidas)
    - [Direcciones IPv6 de multidifusión de nodo solicitado](#direcciones-ipv6-de-multidifusión-de-nodo-solicitado)
  - [Resumen Direccionamiento IPv6](#resumen-direccionamiento-ipv6)
  - [Cuestionario de direcciones IPv6](#cuestionario-de-direcciones-ipv6)
    - [Pregunta 1 - direcciones IPv6](#pregunta-1---direcciones-ipv6)
    - [Pregunta 2 - direcciones IPv6](#pregunta-2---direcciones-ipv6)
    - [Pregunta 3 - direcciones IPv6](#pregunta-3---direcciones-ipv6)
    - [Pregunta 4 - direcciones IPv6](#pregunta-4---direcciones-ipv6)
    - [Pregunta 5 - direcciones IPv6](#pregunta-5---direcciones-ipv6)
    - [Pregunta 6 - direcciones IPv6](#pregunta-6---direcciones-ipv6)
    - [Pregunta 7 - direcciones IPv6](#pregunta-7---direcciones-ipv6)
    - [Pregunta 8 - direcciones IPv6](#pregunta-8---direcciones-ipv6)
    - [Pregunta 9 - direcciones IPv6](#pregunta-9---direcciones-ipv6)
    - [Pregunta 10 - direcciones IPv6](#pregunta-10---direcciones-ipv6)
    - [Pregunta 11 - direcciones IPv6](#pregunta-11---direcciones-ipv6)
    - [Pregunta 12 - direcciones IPv6](#pregunta-12---direcciones-ipv6)
  - [Descrubrimiento de vecinos IPv6](#descrubrimiento-de-vecinos-ipv6)
    - [Mensajes de descubrimiento de vecinos IPv6](#mensajes-de-descubrimiento-de-vecinos-ipv6)
    - [Descubrimiento de vecinos IPv6 - Resolución de direcciones](#descubrimiento-de-vecinos-ipv6---resolución-de-direcciones)
  - [Resumen descubrimiento de vecinos de IPv6](#resumen-descubrimiento-de-vecinos-de-ipv6)
    - [Operación de descubrimiento de vecinos](#operación-de-descubrimiento-de-vecinos)
  - [Cuestionario de Detección de vecinos IPv6](#cuestionario-de-detección-de-vecinos-ipv6)
    - [Pregunta 1 - detección de vecinos IPv6](#pregunta-1---detección-de-vecinos-ipv6)
    - [Pregunta 2 - detección de vecinos IPv6](#pregunta-2---detección-de-vecinos-ipv6)
    - [Pregunta 3 - detección de vecinos IPv6](#pregunta-3---detección-de-vecinos-ipv6)
    - [Pregunta 4 - detección de vecinos IPv6](#pregunta-4---detección-de-vecinos-ipv6)
    - [Pregunta 5 - detección de vecinos IPv6](#pregunta-5---detección-de-vecinos-ipv6)
    - [Pregunta 6 - detección de vecinos IPv6](#pregunta-6---detección-de-vecinos-ipv6)
    - [Pregunta 7 - detección de vecinos IPv6](#pregunta-7---detección-de-vecinos-ipv6)
    - [Pregunta 8 - detección de vecinos IPv6](#pregunta-8---detección-de-vecinos-ipv6)
    - [Pregunta 9 - detección de vecinos IPv6](#pregunta-9---detección-de-vecinos-ipv6)
    - [Pregunta 10 - detección de vecinos IPv6](#pregunta-10---detección-de-vecinos-ipv6)
    - [Pregunta 11 - detección de vecinos IPv6](#pregunta-11---detección-de-vecinos-ipv6)
    - [Pregunta 12 - detección de vecinos IPv6](#pregunta-12---detección-de-vecinos-ipv6)

---

## Tipos de direcciones IPv6

### Unidifusión, Multidifusión, cualquier Difusión

Al igual que con IPv4, existen diferentes tipos de direcciones IPv6. De hecho, existen tres categorías amplias de direcciones IPv6:

- **Difusión**. Una dirección de difusión IPv6 identifican de forma exclusiva una interfaz en un dispositivo con IPv6 habilitado.
- **Multidifusión**. Una dirección de multidifusión IPv6 se utiliza para enviar un único paquete IPv6 a varios destinos.
- **Anycast**. Una dirección IPv6 anycast es cualquier dirección IPv6 de difusión que se puede asignar a varios dispositivos. Los paquetes enviados a una dirección de difusión por proximidad se enrutan al dispositivo más cercano que tenga esa dirección. Las direcciones de difusión por proximidad exceden el ámbito de este curso.

A diferencia de IPv4, IPv6 no tiene una dirección de difusión. Sin embargo, existe una dirección IPv6 de multidifusión de todos los nodos que brinda básicamente el mismo resultado.

### Longitud de prefijo IPv6

El prefijo, o parte de red, de una dirección IPv4 se puede identificar mediante una máscara de subred decimal punteada o por longitud del prefijo (notación de barra). Por ejemplo, la dirección IPv4 192.168.1.10 con la máscara de subred decimal punteada 255.255.255.0 equivale a 192.168.1.10/24.

En IPv6 solo se le llama longitud del prefijo. IPv6 no utiliza la notación decimal punteada de máscara de subred. Al igual que IPv4, la longitud del prefijo se representa en notación de barra inclinada y se usa para indicar la porción de red de una dirección IPv6.

La longitud de prefijo puede ir de 0 a 128. La longitud recomendada del prefijo IPv6 para las LAN y la mayoría de los otros tipos de redes es /64, como se muestra en la figura.

![alt text](image.png)

El prefijo o la porción de red de la dirección tiene 64 bits de longitud, dejando otros 64 bits para la ID de interfaz (porción de host) de la dirección.

Se recomienda encarecidamente utilizar un ID de interfaz de 64 bits para la mayoría de las redes. Esto se debe a que la autoconfiguración de direcciones sin estado (SLAAC) utiliza 64 bits para el ID de la interfaz. También facilita la creación y gestión de subredes.

### Tipos de direcciones de unidifusión IPv6

Las direcciones IPv6 de difusión identifican de forma exclusiva una interfaz en un dispositivo con IPv6 habilitado. La interfaz a la que se le asigna esa dirección recibe un paquete enviado a una dirección de difusión. Como sucede con IPv4, las direcciones IPv6 de origen deben ser direcciones de difusión. Las direcciones IPv6 de destino pueden ser direcciones de difusión o de multidifusión. La figura muestra los diferentes tipos de direcciones de difusión IPv6.

![alt text](image-1.png)

A diferencia de los dispositivos IPv4 que tienen una sola dirección, las direcciones IPv6 suelen tener dos direcciones de difusión:

- Dirección de difusión global (**GUA**). Estas son similares a las direcciones IPv4 públicas. Estas son direcciones enrutables de Internet globalmente exclusivas. Las GUA pueden configurarse estáticamente o asignarse dinámicamente.
- Dirección local de enlace (**LLA**). Se requiere para cada dispositivo habilitado para IPv6. Los LLA se utilizan para comunicarse con otros dispositivos en el mismo enlace local. Con IPv6, el término enlace hace referencia a una subred. Las LLA se limitan a un único enlace. Su exclusividad se debe confirmar solo para ese enlace, ya que no se pueden enrutar más allá del enlace. En otras palabras, los enrutadores no reenvían paquetes con una dirección de origen o de destino de enlace local.

### Nota sobre la dirección local única

Las direcciones locales únicas (rango fc00::/7 a fdff::/7) aún no se implementan comúnmente. Por lo tanto, este módulo solo cubre la configuración GUA y LLA. Sin embargo, se pueden usar direcciones locales únicas para dirigir dispositivos a los que no se debe acceder desde el exterior, como servidores internos e impresoras.

Las direcciones locales únicas de IPv6 tienen cierta similitud con las direcciones privadas RFC 1918 para IPv4, pero existen diferencias significativas:

- Las direcciones locales únicas se utilizan para el direccionamiento local dentro de un sitio o entre una cantidad limitada de sitios.
- Se pueden utilizar direcciones locales únicas para dispositivos que nunca necesitarán acceder a otra red.
- Las direcciones locales únicas no se enrutan o traducen globalmente a una dirección IPv6 global.

Nota: Muchos sitios también utilizan la naturaleza privada de las direcciones RFC 1918 para intentar proteger u ocultar su red de posibles riesgos de seguridad. Sin embargo, este nunca fue el uso previsto de estas tecnologías, y el IETF siempre ha recomendado que los sitios tomen las precauciones de seguridad adecuadas en sus enrutadores con conexión a Internet.

### GUA IPv6

Las direcciones IPv6 de difusión globales (GUA) son globalmente únicas y enrutables en Internet IPv6. Estas direcciones son equivalentes a las direcciones IPv4 públicas. El Comité de Internet para la Asignación de Nombres y Números (ICANN), es el operador de la Autoridad de Asignación de Números de Internet (IANA), que asigna bloques de direcciones IPv6 a los cinco RIR. Actualmente, solo se están asignando GUAs con los primeros tres bits de 001 o 2000::/3 como se muestra en la figura.

La figura muestra el rango de valores para el primer sexteto donde el primer dígito hexadecimal para las GUA disponibles actualmente comienza con un 2 o un 3. Esto solo constituye un octavo del espacio total disponible de direcciones IPv6, sin incluir solamente una parte muy pequeña para otros tipos de direcciones de difusión y multidifusión.

Nota: la dirección 2001:0DB8::/32 se reservó para fines de documentación, incluido el uso en ejemplos.

![alt text](image-2.png)

La siguiente figura muestra la estructura y el rango de una GUA.
Dirección IPv6 con un prefijo de enrutamiento global /48 y un prefijo /64.

![alt text](image-3.png)

Un GUA tiene tres partes:

- Prefijo de enrutamiento global.
- ID de subred.
- ID de interfaz.

### Estructura IPv6 GUA

#### Prefijo de enrutamiento global

El prefijo de enrutamiento global es la porción de prefijo, o de red, de la dirección que asigna el proveedor (por ejemplo, un ISP) a un cliente o a un sitio. Por ejemplo, es común que los ISP asignen un prefijo de enrutamiento global /48 a sus clientes. El prefijo de enrutamiento global suele variar dependiendo de las políticas del ISP.

La figura anterior muestra un GUA que utiliza un prefijo de enrutamiento global /48. Los prefijos /48 son un prefijo de enrutamiento global común que se asigna y se utilizará en la mayoría de los ejemplos a lo largo de este curso.

Por ejemplo, la dirección IPv6 2001:db8:acad ::/48 tiene un prefijo de enrutamiento global que indica que los primeros 48 bits (3 sextetos) (2001:db8:acad) es cómo el ISP conoce este prefijo (red). Los dos puntos dobles (::) que siguen a la longitud del prefijo /48 significa que el resto de la dirección contiene todos los 0. El tamaño del prefijo de enrutamiento global determina el tamaño de la ID de subred.

#### ID de subred

El campo ID de subred es el área entre el prefijo de enrutamiento global y la ID de interfaz. A diferencia de IPv4, donde debe tomar prestado bits de la parte del host para crear subredes, IPv6 se diseñó teniendo en cuenta la subred. Las organizaciones utilizan la ID de subred para identificar una subred dentro de su ubicación. Cuanto mayor es la ID de subred, más subredes habrá disponibles.

Nota: Muchas organizaciones reciben un prefijo de enrutamiento global /32. El uso del prefijo /64 recomendado para crear una ID de interfaz de 64 bits deja una ID de subred de 32 bits. Esto significa que una organización con un prefijo de enrutamiento global /32 y un ID de subred de 32 bits tendrá 4.300 millones de subredes, cada una con 18 quintillonesde dispositivos por subred. ¡Son tantas subredes como direcciones IPv4 públicas!

La dirección IPv6 de la figura anterior tiene un prefijo de enrutamiento global /48, que es común entre muchas redes empresariales. Esto hace que sea especialmente fácil examinar las diferentes partes de la dirección. Usando una longitud de prefijo /64 típica, los primeros cuatro sextetos son para la porción de red de la dirección, y el cuarto sexteto indica la ID de subred. Los cuatro sextetos restantes son para la ID de interfaz.

#### ID de interfaz

La ID de interfaz IPv6 equivale a la porción de host de una dirección IPv4. Se utiliza el término ID de interfaz debido a que un único host puede tener varias interfaces, cada una con una o más direcciones IPv6. La figura muestra un ejemplo de la estructura de un GUA IPv6. Se recomienda encarecidamente que en la mayoría de los casos se utilicen subredes /64, lo que crea una ID de interfaz de 64 bits. Un ID de interfaz de 64 bits permite 18 quintillones de dispositivos o hosts por subred.

Una subred o prefijo /64 (prefijo de enrutamiento global + ID de subred) deja 64 bits para el ID de interfaz. Esto se recomienda para permitir que los dispositivos habilitados para SLAAC creen su propio ID de interfaz de 64 bits. También hace que el desarrollo de un plan de direccionamiento IPv6 sea sencillo y eficaz.

Nota: a diferencia de IPv4, en IPv6 se pueden asignar las direcciones de host compuestas solo por ceros y unos a un dispositivo. La dirección all-1s se puede utilizar porque las direcciones de difusión no se utilizan en IPv6. También se puede utilizar la dirección compuesta solo por ceros, pero se reserva como una dirección anycast de subred y enrutador, y se debe asignar solo a enrutadores.

### IPv6 LLA

Una dirección local de enlace IPv6 (LLA) permite que un dispositivo se comunique con otros dispositivos habilitados para IPv6 en el mismo enlace y solo en ese enlace (subred). Los paquetes con un LLA de origen o destino no se pueden enrutar más allá del enlace desde el que se originó el paquete.

La GUA no es un requisito. Sin embargo, cada interfaz de red habilitada para IPv6 debe tener una LLA.

Si un LLA no se configura manualmente en una interfaz, el dispositivo creará automáticamente el suyo sin comunicarse con un servidor DHCP. Los hosts con IPv6 habilitado crean un LLA de IPv6 incluso si el dispositivo no tiene asignada una dirección IPv6 de difusión global. Esto permite que los dispositivos con IPv6 habilitado se comuniquen con otros dispositivos con IPv6 habilitado en la misma subred. Esto incluye la comunicación con la puerta de enlace predeterminada (enrutador).

Las direcciones LLA IPv6 están en el rango fe80::/10. /10 indica que los primeros 10bit s son 1111 1110 10xx xxxx. El primer sexteto tiene un rango de 1111 1110 1000 0000 (fe80) a 1111 1110 1011 1111 (febf).

La figura muestra un ejemplo de comunicación utilizando LLA de IPv6. El PC es capaz de comunicarse directamente con la impresora utilizando las LLA.

#### Comunicaciones de enlace local de IPv6

![alt text](image-4.png)

La siguiente figura muestra algunos de los usos de las LLA IPv6.

![alt text](image-5.png)

1. Los enrutadores usan el LLA de los enrutadores vecinos para enviar actualizaciones de enrutamiento.
2. Los hosts usan el LLA de un enrutador local como puerta de enlace predeterminada.

Nota: Por lo general, es el LLA del enrutador, y no la GUA, que se usa como la puerta de enlace predeterminada para otros dispositivos en el enlace.

Hay dos maneras en que un dispositivo puede obtener una LLA:

- Estáticamente. Esto significa que el dispositivo se ha configurado manualmente.
- Dinámicamente. Esto significa que el dispositivo crea su propia ID de interfaz usando valores generados aleatoriamente o usando el Método de identificador único extendido (EUI), que usa la dirección de control de acceso a medios (MAC) del cliente junto con bits adicionales.

### Compruebe su comprensión

#### Pregunta 1

¿Cuál es la longitud de prefijo recomendada para la mayoría de las subredes IPv6?

- [ ] /48
- [ ] /32
- [ ] /128
- [x] /64

#### Pregunta 2

¿Qué parte de un GUA es asignada por el ISP?

- [ ] prefijo
- [ ] prefijo RIR
- [x] prefijo de enrutamiento global
- [ ] prefijo de enrutamiento global e ID de subred

#### Pregunta 3

¿Qué tipo de dirección de difusión IPv6 no se puede enrutar entre redes?

- [ ] dirección local única
- [X] LLA
- [ ] dirección IPv4 incrustada
- [ ] GUA

#### Pregunta 4

¿Verdadero o falso? El campo ID de subred de una GUA debe tomar prestados bits del ID de interfaz.

- [ ] Verdadero
- [X] Falso

#### Pregunta 5

¿Qué tipo de dirección IPv6 comienza con fe80?

- [ ] Ninguno. Una dirección IPv6 debe comenzar con 2001.
- [ ] dirección de multidifusión
- [X] LLA
- [ ] GUA

---

## Configuración Estática GUA y LLA

Las **GUA IPv6** son las mismas que las direcciones _IPv4 públicas_. **ELLA** son globalmente únicas y enrutables en Internet IPv6. Una _URL IPv6_ permite que dos dispositivos habilitados para IPv6 se comuniquen entre sí en el mismo vínculo (subred). Es fácil configurar estáticamente las **GUA** y las **LLA IPv6** en los enrutadores para ayudarle a crear una red IPv6.

La mayoría de los comandos de configuración de los comandos de configuración y verificación **IPv6 de Cisco IOS** son similares a sus equivalentes de IPv4. La única diferencia es el uso de _ipv6_ en lugar de _ip_ dentro de los comandos.

El comando de Cisco IOS para configurar una dirección IPv4 en una interfaz es `ip address subnet-mask`. El comando para configurar una **GUA IPv6** en una interfaz es `ipv6 address/prefix-length`. Observar que no hay espacio entre `ipv6-address` y `prefix-length`.

La configuración de ejemplo utiliza la topología y estas subredes IPv6:

- 2001:db8:acad:1::/64
- 2001:db8:acad:2::/64
- 2001:db8:acad:3::/64

![alt text](image-6.png)

El ejemplo muestra los comandos necesarios para configurar la GUA IPv6 en _GigabitEthernet 0/0/0_, _GigabitEthernet 0/0/1_ y la interfaz _Serial 0/1/0_ de R1.

```cisco
R1(config)# interface GigabitEthernet 0/0/0
R1(config-if)# ipv6 address 2001:db8:acad:1::1/64
R1(config-if)# no shutdown
R1(config-if)# exit

R1(config)# interface GigabitEthernet 0/0/1
R1(config-if)# ipv6 address 2001:db8:acad:2::1/64
R1(config-if)# no shutdown
R1(config-if)# exit

R1(config)# interface GigabitEthernet 0/1/0
R1(config-if)# ipv6 address 2001:db8:acad:3::1/64
R1(config-if)# no shutdown
R1(config-if)# exit
```

### Configuración de GUA estática en un host de Windows

Configurar la dirección IPv6 en un host de forma manual es similar a configurar una dirección IPv4.

Como se muestra en la figura, la dirección de puerta de enlace predeterminada configurada para PC1 es _2001:db8:acad:1::1_. Esta es la **GUA** de la interfaz R1 GigabitEthernet en la misma red. Alternativamente, la dirección de puerta de enlace predeterminada se puede configurar para que coincida con el **LLA** de la interfaz GigabitEthernet. El uso de la **LLA** del enrutador como dirección de puerta de enlace predeterminada se considera una práctica recomendada. Cualquiera de las dos configuraciones funciona.

![alt text](image-7.png)

Al igual que con IPv4, la configuración de direcciones estáticas en clientes no se extiende a entornos más grandes. Por este motivo, la mayoría de los administradores de redes en una red IPv6 habilitan la asignación dinámica de direcciones IPv6.

Hay dos formas en que un dispositivo puede obtener una **GUA IPv6** automáticamente:

- Configuración automática de direcciones sin estado (SLAAC)
- DHCPv6 con estado.

Cuando se usa **DHCPv6** o **SLAAC**, se especifica automáticamente la **LLA** del enrutador local como dirección de puerta de enlace predeterminada.

### Configuración Estática de una dirección de Unidifusión Local de enlace

Configurar la **LLA** manualmente permite crear una dirección reconocible y más fácil de recordar. Por lo general, solo es necesario crear **LLA** reconocibles en los enrutadores. Esto es beneficioso porque los **LLA** de enrutador se usan como direcciones de puerta de enlace predeterminadas y en el enrutamiento de mensajes publicitarios.

Las **LLAS** se pueden configurar manualmente mediante el comando _ipv6 address ipv6-de enlace local-address de enlace local_. Cuando una dirección comienza con este sexteto dentro del rango de `fe80` a `febf`, el parámetro de de enlace local debe seguir a la dirección.

La figura muestra un ejemplo de topología con LLA en cada interfaz:

![alt text](image-8.png)

El ejemplo muestra la configuración de un LLA en el router R1:

```cisco
R1(config)# interface GigabitEthernet 0/0/0
R1(config-if)# ipv6 address fe80::1:1 de enlace local
R1(config-if)# exit

R1(config)# interface GigabitEthernet 0/0/1
R1(config-if)# ipv6 address fe80::2:1 de enlace local
R1(config-if)# exit

R1(config)# interface GigabitEthernet 0/0/0
R1(config-if)# ipv6 address fe80::1:1 de enlace local
R1(config-if)# exit

R1(config)# interface serial 0/1/0
R1(config-if)# ipv6 address fe80::3:1 de enlace local
R1(config-if)# exit
```

Las **LLA** configuradas estáticamente se utilizan para hacerlas más fácilmente reconocibles como pertenecientes al enrutador R1. En este ejemplo, todas las interfaces del enrutador R1 se configuraron con un **LLA** que comienza con `fe80::n:1`.

Se podría configurar exactamente la misma **LLA** en cada enlace siempre que sea única en ese enlace. Esto se debe a que los **LLA** solo tienen que ser únicos en ese enlace. Sin embargo, la práctica común es crear un **LLA** diferente en cada interfaz del enrutador para facilitar la Identificarl enrutador y la interfaz específica.

## Direccionamiento Dinámico para GUA IPv6

### Mensajes RS y RA

La mayoría de los dispositivos obtienen sus GUA IPv6 de forma dinámica. En este tema se explica cómo funciona este proceso mediante mensajes de anuncio de enrutador (RA) y solicitud de enrutador (RS). Cuando comprenda la diferencia entre los tres Métodos que puede usar un anuncio de enrutador, así como el proceso EUI-64 para crear un ID de interfaz difiere de un proceso generado aleatoriamente, ¡habrá dado un gran salto en su experiencia en IPv6!

Para el GUA, un dispositivo obtiene la dirección dinámicamente a través de mensajes del Protocolo de mensajes de control de Internet versión 6 (ICMPv6). Los enrutadores IPv6 envían mensajes RA de ICMPv6 periódicamente, cada 200 segundos, a todos los dispositivos con IPv6 habilitado en la red. También se enviará un mensaje RA en respuesta a un host que envía un mensaje ICMPv6 RS, que es una solicitud de un mensaje RA. Ambos mensajes se muestran en la figura.

Mensajes de RS y RA de ICMPv6:

- Los hosts que solicitan información de direccionamiento envían mensajes RS a todos los enrutadores IPv6.
- RA messages are sent to all IPv6 nodes. Si se utiliza el Método 1 (solo SLAAC), el RA incluye el prefijo de red, la longitud del prefijo y la información sobre el puerta de enlace predeterminada.

![alt text](image-9.png)

Los mensajes RA están en interfaces Ethernet del Ruter IPv6. Para habilitar un Ruter como enrutador IPv6, se debe utilizar el comando de configuración global _ipv6 de difusión-enrutamiento_.

El mensaje ICMPv6 RA es una sugerencia para un dispositivo sobre cómo obtener una GUA IPv6. La decisión final depende del sistema operativo del dispositivo. El mensaje ICMPv6 RA incluye lo siguiente:

- _Prefijo de red y longitud del prefijo_. Esto le dice al dispositivo a qué red pertenece.
- _Dirección de puerta de enlace predeterminada_. Es un IPv6 **LLA**, la dirección IPv6 de origen del mensaje RA.
- _Direcciones DNS y de nombre de dominio_. Estas son las direcciones de los servidores DNS y nombre de dominio.

Existen tres Métodos para los mensajes de RA:

- **Método 1: SLAAC** - Tengo todo lo que necesita, incluido el prefijo, la longitud del prefijo y la dirección de la puerta de enlace predeterminada.
- **Método 2: SLAAC con un servidor DHCPv6 sin estado** - Aquí está mi información, pero necesita obtener otra información como direcciones DNS, de un servidor DHCPv6 sin estado.
- **Método 3: DHCPv6 con estado (sin SLAAC)** - Puedo darle su dirección de puerta de enlace predeterminada. Necesita pedir a un servidor DHCPv6 con estado para toda su otra información.

### Método 1: SLAAC

**SLAAC** es un Método que permite a un dispositivo crear su propio **GUA** sin los servicios de _DHCPv6_. Usando **SLAAC**, los dispositivos confían en los mensajes _ICMPv6 RA_ del enrutador local para obtener la información necesaria.

Por defecto, el mensaje _RA_ sugiere que el dispositivo receptor use la información en el mensaje _RA_ para crear su propia _GUA IPv6_ y toda la otra información necesaria. No se requieren los servicios de un servidor _DHCPv6_.

**SLAAC** no tiene estado, lo que significa que no hay un servidor central (por ejemplo, un servidor _DHCPv6_ con estado) que asigne _GUA_ y mantenga una lista de dispositivos y sus direcciones. Con **SLAAC**, el dispositivo cliente usa la información en el mensaje RA para crear su propia _GUA_. Como se muestra en la figura, las dos partes de la dirección se crean de la siguiente manera:

- **Prefijo** - se anuncia en el mensaje RA.
- **ID de interfaz** - utiliza el proceso EUI-64 o genera un número aleatorio de 64 bits, según el sistema operativo del dispositivo.

![alt text](image-10.png)

1. El router envía un mensaje RA con el prefijo para el enlace local.
2. El PC usa SLAAC para obtener un prefijo del mensaje RA y crea su propia ID de interfaz.

### Método 2: SLAAC y DHCPv6 sin estado

Se puede configurar una interfaz del Router para enviar un anuncio utilizando **SLAAC** y _DHCPv6_ sin estado. Con este Método, el mensaje RA sugiere que los dispositivos utilicen lo siguiente:

- SLAAC para crear su propio IPv6 GUA.
- La dirección de enlace local del enrutador, la dirección IPv6 de origen del RA para la dirección de la puerta de enlace predeterminada.
- Un servidor DHCPv6 sin estado, que obtendrá otra información como la dirección del servidor DNS y el nombre de dominio.

Un servidor _DHCPv6_ sin información de estado distribuye las direcciones del servidor _DNS_ y los nombres de dominio. No asigna **GUA**.

![alt text](image-11.png)

1. El PC envía un RS a todos los Ruters IPv6. Necesito información de direccionamiento.
2. El Ruter envía un mensaje RA a todos los nodos IPv6 con el **Método 2** (**SLAAC** y **DHCPv6**) especificado. Aquí está la información de su prefijo, longitud de prefijo y puerta de enlace predeterminada. Pero tendrá que obtener información DNS de un servidor DHCPv6.
3. El PC envía un mensaje de solicitud _DHCPv6_ a todos los servidores _DHCPv6_. Utilicé **SLAAC** para crear mi dirección _IPv6_ y obtener mi dirección de puerta de enlace predeterminada, pero necesito otra información de un servidor _DHCPv6_ sin estado.

### Método 3: DHCPv6 con estado

Una interfaz de enrutador se puede configurar para enviar una **RA** usando _DHCPv6_ con estado solamente.

_DHCPv6_ con información de estado es similar a _DHCP_ para _IPv4_. Un dispositivo puede recibir automáticamente su información de direccionamiento, incluida una **GUA**, la longitud del prefijo y las direcciones de los servidores DNS de un servidor _DHCPv6_ con estado.

Como se muestra en la figura, con este Método, el mensaje **RA** sugiere que los dispositivos usen lo siguiente:

- La dirección **LLA** del enrutador, que es la dirección _IPv6_ de origen del **RA**, para la dirección de la puerta de enlace predeterminada.
- Un servidor _DHCPv6_ con estado, para obtener una **GUA**, otra información como la dirección del servidor _DNS_ y el nombre de dominio.

![alt text](image-12.png)

1. El PC envía un RS a todos los enrutadores IPv6, "Necesito información de direccionamiento.
2. El Ruter envía un mensaje **RA** a todos los nodos _IPv6_ con el **Método 3** (DHCPv6 con estado) especificado, "_Soy su puerta de enlace predeterminada, pero debe pedirle a un servidor DHCPv6 con estado su dirección IPv6 y otra información de direccionamiento_."
3. El PC envía un mensaje de solicitud de _DHCPv6_ a todos los servidores _DHCPv6_, "_Recibí mi dirección de puerta de enlace predeterminada del mensaje RA, pero necesito una dirección IPv6 y toda otra información de direccionamiento de un servidor DHCPv6 con estado_."

Un servidor _DHCPv6_ con información de estado asigna y mantiene una lista de qué dispositivo recibe cuál dirección _IPv6_. _DHCP_ para _IPv4_ tiene información de estado.

La dirección de puerta de enlace predeterminada solo se puede obtener dinámicamente a partir del mensaje **RA**. El servidor _DHCPv6_ con información de estado o sin ella no brinda la dirección de puerta de enlace predeterminada.

### Proceso EUI-64 versus generado aleatoriamente

Cuando el mensaje **RA** es **SLAAC** o **SLAAC con DHCPv6 sin estado**, el cliente debe generar su propia ID de interfaz. El cliente conoce la parte del prefijo de la dirección del mensaje **RA**, pero debe crear su propia **ID** de interfaz. El **ID** de la interfaz se puede crear utilizando el proceso EUI-64 o un número de _64 bits_ generado aleatoriamente, como se muestra en la figura.

Creación dinámica de un ID de interfaz

![alt text](image-13.png)

1. El Router envía un mensaje RA.
2. El PC utiliza el prefijo del mensaje RA y utiliza EUI-64 o un número aleatorio de 64 bits para generar un ID de Interfaz.

### Proceso EUI-64

El IEEE definió el identificador único extendido (EUI) o proceso EUI-64 modificado. Este proceso utiliza la dirección MAC Ethernet de 48 bits de un cliente e inserta otros 16 bits en el medio de la dirección MAC de 48 bits para crear una ID de interfaz de 64 bits.

Las direcciones MAC de Ethernet, por lo general, se representan en formato hexadecimal y constan de dos partes:

- **Identificador único de organización (OUI)** - El OUI es un código de proveedor de 24-bits (6 dígitos hexadecimales) que asigna el IEEE.
- **Identificador de dispositivo** - El identificador de dispositivo es un valor único de 24-bits (6 dígitos hexadecimales) dentro de un OUI común.

Las ID de interfaz EUI-64 se representan en sistema binario y constan de tres partes:

- OUI de 24-bits de la dirección MAC del cliente, pero el 7th bit (bit universal/local, U/L) se invierte. Esto quiere decir que si el 7th bit es un 0, se transforma en un 1, y viceversa.
- El valor insertado de 16-bits **fffe** (en hexadecimal).
- Identificador de dispositivo de 24-bits de la dirección MAC del cliente.

El proceso EUI-64 se ilustra en la figura, utilizando la dirección MAC R1 GigabitEthernet de fc99:4775:cee0.

El proceso EUI-64 se ilustra en la figura, utilizando la dirección MAC R1 GigabitEthernet de fc99:4775:cee0.

![alt text](image-14.png)

- **Paso 1**: Dividir la dirección MAC entre el OUI y el identificador de dispositivo.
- **Paso 2**: Insertar el valor hexadecimal _fffe_, que en formato binario es: 1111 1111 1111 1110.
- **Paso 3**: Convertir los primeros dos valores hexadecimales del OUI a binario e invertir el bit U/L (7th bit) En este ejemplo, el 0 en el bit 7 se cambia a 1.
- El resultado es un ID de interfaz generado por EUI-64 de _fe99:47ff:fe75:cee0_.

El resultado de ejemplo del comando **ipconfig** muestra el GUA IPv6 que se crea dinámicamente mediante **SLAAC** y el proceso EUI-64. Una manera fácil de identificar que una dirección probablemente se creó utilizando EUI-64 es observando el **fffe** ubicado en el medio de la ID de la interfaz.

La ventaja de EUI-64 es que la dirección MAC de Ethernet se puede utilizar para determinar la ID de la interfaz. También permite que los administradores de redes rastreen fácilmente una dirección IPv6 a un terminal mediante la dirección MAC única. Sin embargo, esto ha causado preocupaciones de privacidad entre muchos usuarios que temían que sus paquetes pudieran rastrearse hasta el equipo físico real. Debido a estas preocupaciones, se puede usar una ID de interfaz generada aleatoriamente.

### ID de interfaz generadas aleatoriamente

Según el sistema operativo, un dispositivo puede utilizar una ID de interfaz generada aleatoriamente en lugar de utilizar la dirección MAC y el proceso EUI-64. Una vez establecida la ID de la interfaz, ya sea a través del proceso EUI-64 o mediante la generación aleatoria, se puede combinar con un prefijo IPv6 en el mensaje RA para crear una GUA, como se muestra en la figura.

Para garantizar la exclusividad de cualquier dirección de difusión de IPv6, el cliente puede usar un proceso denominado detección de direcciones duplicadas (DAD). Es similar a una solicitud de ARP para su propia dirección. Si no se obtiene una respuesta, la dirección es única.

### Verifica su comprensión - Direccionamiento dinámico para GUA IPv6

#### Pregunta 1 - Direccionamiento dinámico para GUA IPv6

_¿Verdadero o falso? Los hosts que solicitan información de direccionamiento envían mensajes RA a todos los enrutadores IPv6._

- [X] Falso.
- [ ] Verdadero.

#### Pregunta 2 - Direccionamiento dinámico para GUA IPv6

_¿Qué Método de direccionamiento dinámico para GUA es aquel en el que los dispositivos se basan únicamente en el contenido del mensaje RA para su información de direccionamiento?_

- [ ] Método 2: SLAAC y DHCPv6 sin estado.
- [X] Método 1: SLAAC.
- [ ] Método 3: DHCPv6 con estado.

#### Pregunta 3 - Direccionamiento dinámico para GUA IPv6

_¿Qué Método de direccionamiento dinámico para GUA es aquel en el que los dispositivos dependen únicamente de un servidor DHCPv6 para su información de direccionamiento?_

- [ ] Método 1: SLAAC
- [X] Método 3: DHCPv6 con estado.
- [ ] Método 2: SLAAC y DHCPv6 sin estado.

#### Pregunta 4 - Direccionamiento dinámico para GUA IPv6

_¿Qué Método de direccionamiento dinámico para GUA es aquel en el que los dispositivos obtienen su configuración IPv6 en un mensaje RA y solicitan información DNS de un servidor DHCPv6?_

- [ ] Método 1: SLAAC
- [ ] Método 3: DHCPv6 con estado
- [X] Método 2: SLAAC y DHCPv6 sin estado

#### Pregunta 5 - Direccionamiento dinámico para GUA IPv6

_¿Cuáles son los dos Métodos que un dispositivo puede usar para generar su propio ID de interfaz IPv6?_ (seleccionar dos)

- [ ] DHCPv6 con información de estado
- [X] generado aleatoriamente
- [ ] DHCPv6 sin información de estado
- [X] EUI-64
- [ ] SLAAC

---

## Direccionamiento Dinámico para LLA de IPv6

### LLA dinámicos

Todos los dispositivos IPv6 deben tener una **LLA** IPv6. Al igual que IPv6 **GUA**, también puede crear **LAs** dinámicamente. Independientemente de cómo cree las **LAs** (y las **GUA**), es importante que verifique toda la configuración de direcciones IPv6. En este tema se explica la verificación de configuración de **LLA** y IPv6 generadas dinámicamente.

La figura muestra que el **LLA** se crea dinámicamente usando el prefijo **fe80::/10** y la ID de interfaz usando el proceso **EUI-64**, o un número de 64 bits generado aleatoriamente.

![alt text](image-15.png)

### LLA dinámicos en Windows

Los sistemas operativos, como Windows, suelen utilizar el mismo Método tanto para una GUA creada por **SLAAC** como para una **LLA** asignada dinámicamente. Vea las áreas resaltadas en los siguientes ejemplos que se mostraron anteriormente.

ID de interfaz generada mediante EUI-64:

![alt text](image-16.png)

ID de interfaz de 64 bits generada aleatoriamente:

![alt text](image-17.png)

### LLA dinámicos en enrutadores Cisco

Los enrutadores Cisco crean automáticamente un LLA IPv6 cada vez que se asigna una GUA a la interfaz. De manera predeterminada, los enrutadores con Cisco IOS utilizan EUI-64 para generar la ID de interfaz para todas las direcciones LLA en las interfaces IPv6. Para las interfaces seriales, el enrutador utiliza la dirección MAC de una interfaz Ethernet. Recuerde que un LLA debe ser único solo en ese enlace o red. Sin embargo, una desventaja de usar el LLA asignado dinámicamente es su ID de interfaz larga, lo que dificulta identificar y recordar las direcciones asignadas. El ejemplo muestra la dirección MAC en la interfaz GigabitEthernet 0/0/0 del enrutador R1. Esta dirección se utiliza para crear dinámicamente el LLA en la misma interfaz, y también para la interfaz Serial 0/1/0.

Para que sea más fácil reconocer y recordar estas direcciones en los enrutadores, es común configurar estáticamente los LLA de IPv6 en los enrutadores.

### Verificar la configuración de la dirección IPv6

Ejemplo de topología:

![alt text](image-18.png)

El comando **show ipv6 interface brief** muestra todas las direcciones IPv6 configuradas en una interfaz. EUI-64 usa esta dirección MAC para generar la ID de interfaz para el LLA. Además, el comando **show ipv6 interface brief** muestra un resultado abreviado para cada una de las interfaces. El resultado [up/up] en la misma línea que la interfaz indica el estado de interfaz de la capa 1/capa2. Esto es lo mismo que las columnas Status (Estado) y Protocol (Protocolo) en el comando IPv4 equivalente.

Observe que cada interfaz tiene dos direcciones IPv6. La segunda dirección de cada interfaz es la GUA que se configuró. La primera dirección, la que comienza con fe80, es la dirección de difusión local de enlace para la interfaz. Recuerde que el LLA se agrega automáticamente a la interfaz cuando se asigna una GUA.

Además, observe que el R1 Serial 0/1/0 LLA es el mismo que su interfaz GigabitEthernet 0/0/0. Las interfaces seriales no tienen direcciones MAC de Ethernet, por lo que Cisco IOS usa la dirección MAC de la primera interfaz Ethernet disponible. Esto es posible porque las interfaces de enlace local solo deben ser únicas en ese enlace.

![alt text](image-19.png)

Como se muestra en el ejemplo, el comando **show ipv6 route** se puede usar para verificar que las redes IPv6 y las direcciones de interfaz IPv6 específicas se hayan instalado en la tabla de enrutamiento IPv6. El comando **show ipv6 route** muestra solamente redes IPv6, no redes IPv4.

Dentro de la tabla de la ruta, una C junto a una ruta indica que se trata de una red conectada directamente. Cuando la interfaz del enrutador está configurada con una GUA y está en el estado "up/up", el prefijo IPv6 y la longitud del prefijo se agregan a la tabla de enrutamiento IPv6 como una ruta conectada.

_Note_: La L indica una ruta local, la dirección IPv6 específica asignada a la interfaz. Esto no es una LLA. Los LLA no se incluyen en la tabla de enrutamiento del enrutador porque no son direcciones enrutables.

La GUA IPv6 configurada en la interfaz también se instala en la tabla de enrutamiento como una ruta local. La ruta local tiene un prefijo /128. La tabla de enrutamiento utiliza las rutas locales para procesar eficientemente los paquetes con una dirección de destino de la dirección de la interfaz del enrutador.

![alt text](image-20.png)

El comando **ping** para IPv6 es idéntico al comando que se utiliza con IPv4, excepto que se usa una dirección IPv6. Como se muestra en el ejemplo, el comando se usa para verificar la conectividad de Capa 3 entre R1 y PC1. Al hacer ping a un LLA desde un enrutador, Cisco IOS solicitará al usuario la interfaz de salida. Debido a que el LLA de destino puede estar en uno o más de sus enlaces o redes, el enrutador necesita saber a qué interfaz enviar el ping.

![alt text](image-21.png)

## Direcciones Multicast de IPv6

### Direcciones IPv6 de multidifusión asignadas

Las direcciones IPv6 de _multidifusión_ son similares a las direcciones IPv4 de _multidifusión_. Recuerde que las direcciones de _multidifusión_ se utilizan para enviar un único paquete a uno o más destinos (grupo de multidifusión). Las direcciones de _multidifusión_ IPv6 tienen el prefijo **ff00::/8**.

Las direcciones de _multidifusión_ solo pueden ser direcciones de destino y no direcciones de origen.

Existen dos tipos de direcciones IPv6 de _multidifusión_:

- Direcciones de multidifusión conocidas
- Direcciones de multidifusión de nodo solicitado.

### Direcciones de multidifusión IPv6 bien conocidas

Se asignan direcciones de _multidifusión_ IPv6 conocidas. Las direcciones de _multidifusión_ asignadas son direcciones de _multidifusión_ reservadas para grupos predefinidos de dispositivos. Una dirección de _multidifusión_ asignada es una única dirección que se utiliza para llegar a un grupo de dispositivos que ejecutan un protocolo o servicio común. Las direcciones de _multidifusión_ asignadas se utilizan en contexto con protocolos específicos, como DHCPv6.

Estos son dos grupos de _multidifusión_ asignados por IPv6 comunes:

- **ff02::1 Grupo multidifusión de todos los nodos** - Grupo multidifusión al que se unen todos los dispositivos con IPv6 habilitado. Los paquetes que se envían a este grupo son recibidos y procesados por todas las interfaces IPv6 en el enlace o en la red. Esto tiene el mismo efecto que una dirección de difusión en IPv4. En la ilustración, se muestra un ejemplo de comunicación mediante la dirección de multidifusión de todos los nodos. Un enrutador IPv6 envía mensajes RA ICMPv6 al grupo de multidifusión de todos los nodos.
- **ff02::2 Grupo multidifusión de todos los enrutadores** - Grupo multidifusión al que se unen todos los enrutadores con IPv6 habilitado. Un enrutador se convierte en un miembro de este grupo cuando se habilita como enrutador IPv6 mediante el comando de configuración global _ipv6 de difusión-enrutamiento_. Los paquetes que se envían a este grupo son recibidos y procesados por todos los enrutadores IPv6 en el enlace o en la red.

Multidifusión de todos los nodos IPv6: Mensaje RA.

![alt text](image-22.png)

Los dispositivos habilitados para IPv6 envían mensajes ICMPv6 RS a la dirección de multidifusión de todos los enrutadores. El mensaje RS solicita un mensaje RA del enrutador IPv6 para contribuir a la configuración de direcciones del dispositivo. El enrutador IPv6 responde con un mensaje RA, como se muestra.

### Direcciones IPv6 de multidifusión de nodo solicitado

Una dirección multidifusión de nodo solicitado es similar a una dirección multidifusión de todos los nodos. La ventaja de una dirección multidifusión de nodo solicitado es que se asigna a una dirección especial de multidifusión de Ethernet. Esto permite que la NIC Ethernet filtre el marco al examinar la dirección MAC de destino sin enviarla al proceso de IPv6 para ver si el dispositivo es el objetivo previsto del paquete IPv6.

![alt text](image-23.png)

---

## Resumen Direccionamiento IPv6

**Tipos de direcciones IPv6**.

Existen tres tipos de direcciones IPv6: de difusión, de multidifusión y de difusión por proximidad. IPv6 no utiliza la notación decimal punteada de máscara de subred. Al igual que IPv4, la longitud del prefijo se representa en notación de barra inclinada y se usa para indicar la porción de red de una dirección IPv6. Las direcciones IPv6 de difusión identifican de forma exclusiva una interfaz en un dispositivo con IPv6 habilitado. Las direcciones IPv6 suelen tener dos direcciones de difusión: GUA y LLA. Las direcciones locales únicas de IPv6 tienen los siguientes usos: se utilizan para direcciones locales dentro de un sitio o entre un número limitado de sitios, se pueden usar para dispositivos que nunca necesitarán acceder a otra red y no se enrutan o traducen globalmente a una dirección IPv6 global. Las direcciones IPv6 de difusión globales (GUA) son globalmente únicas y enrutables en Internet IPv6. Estas direcciones son equivalentes a las direcciones IPv4 públicas. Un GUA tiene tres partes: un prefijo de enrutamiento global, un Id. de subred y un Id. de interfaz. Una dirección local de enlace IPv6 (LLA) permite que un dispositivo se comunique con otros dispositivos habilitados para IPv6 en el mismo enlace y solo en ese enlace (subred). Los dispositivos pueden obtener un LLA de forma estática o dinámica.

**Configuración estática de GUA y LLA**.

El comando de Cisco IOS para configurar una dirección IPv4 en una interfaz es: ip address ip-address subnet-mask. Por el contrario, el comando para configurar una GUA IPv6 en una interfaz es: ipv6 address ipv6-address/prefix-length. Al igual que con IPv4, la configuración de direcciones estáticas en clientes no se extiende a entornos más grandes. Por este motivo, la mayoría de los administradores de redes en una red IPv6 habilitan la asignación dinámica de direcciones IPv6. Configurar la LLA manualmente permite crear una dirección reconocible y más fácil de recordar. Por lo general, solo es necesario crear LLA reconocibles en los enrutadores. Las LLA se pueden configurar manualmente mediante el comando ipv6 address ipv6-de enlace local-address de enlace local.

**Direccionamiento estática de GUA y LLA**.

Un dispositivo obtiene una GUA dinámicamente a través de mensajes ICMPv6. Los enrutadores IPv6 envían mensajes RA de ICMPv6 periódicamente, cada 200 segundos, a todos los dispositivos con IPv6 habilitado en la red. También se enviará un mensaje RA en respuesta a un host que envía un mensaje ICMPv6 RS, que es una solicitud de un mensaje RA. El mensaje RA ICMPv6 incluye: prefijo de red y longitud de prefijo, dirección de puerta de enlace predeterminada y direcciones DNS y nombre de dominio. Los mensajes RA tienen tres Métodos: SLAAC, SLAAC con un servidor DHCPv6 sin estado y DHCPv6 con estado (sin SLAAC). Con SLAAC, el dispositivo cliente utiliza la información del mensaje RA para crear su propio GUA porque el mensaje contiene el prefijo y el ID de interfaz. Con SLAAC con DHCPv6 sin estado, el mensaje RA sugiere que los dispositivos utilicen SLAAC para crear su propia GUA IPv6, utilizar la LLA del router como dirección de puerta de enlace predeterminada y utilizar un servidor DHCPv6 sin estado para obtener otra información necesaria.

Con DHCPv6 con estado, RA sugiere que los dispositivos utilicen el enrutador LLA como dirección de puerta de enlace predeterminada, y el servidor DHCPv6 con estado para obtener una GUA, una dirección de servidor DNS, nombre de dominio y toda la información necesaria. La ID de interfaz se puede crear utilizando el proceso EUI-64 o un número de 64 bits generado aleatoriamente. El proceso EUI utiliza la dirección MAC Ethernet de 48 bits del cliente e inserta otros 16 bits en el medio de la dirección MAC para crear una ID de interfaz de 64 bits. Dependiendo del sistema operativo, un dispositivo puede usar una ID de interfaz generada aleatoriamente.

**Direccionamiento dinámico para LLA de IPv6**.

Todos los dispositivos IPv6 deben tener una LLA IPv6. Una LLA se puede configurar manualmente o crear dinámicamente. Los sistemas operativos, como Windows, suelen utilizar el mismo Método tanto para una GUA creada por SLAAC como para una LLA asignada dinámicamente. Los enrutadores Cisco crean automáticamente un LLA IPv6 cada vez que se asigna una GUA a la interfaz. De manera predeterminada, los enrutadores Cisco IOS usan EUI-64 para generar la ID de interfaz para todos los LLA en las interfaces IPv6. Para las interfaces seriales, el router utiliza la dirección MAC de una interfaz Ethernet. Para que sea más fácil reconocer y recordar estas direcciones en los enrutadores, es común configurar estáticamente los LLA de IPv6 en los enrutadores. Para verificar la configuración de la dirección IPv6, utilice los siguientes tres comandos: show ipv6 interface brief, show ipv6 route y ping.

**Direcciones IPv6 de multidifusión**.

Existen dos tipos de direcciones multidifusión de IPv6: direcciones multidifusión conocidas y direcciones multidifusión de nodos solicitados. Las direcciones de multidifusión asignadas son direcciones de multidifusión reservadas para grupos predefinidos de dispositivos. Se asignan direcciones multidifusión conocidas. Dos grupos multidifusión asignados por IPv6 comunes son: ff02::1 grupo multidifusión de todos los nodos y ff02::2 grupo multidifusión de todos los enrutadores. Una dirección multidifusión de nodo solicitado es similar a una dirección multidifusión de todos los nodos. La ventaja de una dirección multidifusión de nodo solicitado es que se asigna a una dirección especial de multidifusión de Ethernet.

---

## Cuestionario de direcciones IPv6

### Pregunta 1 - direcciones IPv6

_¿Qué tipo de dirección IPv6 no se puede enrutar y se utiliza solo para la comunicación en una sola subred?_

- [ ] dirección de difusión global
- [X] dirección de enlace local
- [ ] dirección de bucle invertido
- [ ] dirección local única
- [ ] dirección no especificada

### Pregunta 2 - direcciones IPv6

_¿Cuáles son las tres partes de una dirección de difusión global IPv6?_ (Escoja tres opciones.)

- [ ] direccion de difusión
- [X] prefijo de enrutamiento global
- [ ] máscara de subred
- [X] ID de subred
- [X] ID de interfaz

### Pregunta 3 - direcciones IPv6

_¿Cuáles de los siguientes son dos tipos de direcciones IPv6 de unidifusión?_ (Elija dos opciones.)

- [ ] multidifusión
- [X] bucle invertido
- [X] enlace local
- [ ] anycast
- [ ] transmisión

### Pregunta 4 - direcciones IPv6

_Una empresa utiliza el Método SLAAC para configurar direcciones IPv6 para las estaciones de trabajo de los empleados. ¿Qué dirección utilizará un cliente como puerta de enlace predeterminada?_

- [ ] la dirección de multidifusión de todos los enrutadores
- [X] la dirección local de enlace de la interfaz del enrutador conectada a la red
- [X] la dirección local exclusiva de la interfaz del enrutador conectada a la red
- [X] la dirección de difusión global de la interfaz del enrutador conectada a la red

### Pregunta 5 - direcciones IPv6

_¿Qué son las ULA de IPv6?_

- [ ] un Método para traducir direcciones IPv6 en direcciones IPv4
- [X] un rango de direcciones IPv6 privadas que se utilizan para la comunicación dentro de un sitio local
- [ ] cualquier dispositivo que pueda ejecutar una pila de protocolos IPv6 e IPv4 simultáneamente
- [ ] protocolos de túnel que permiten que dos redes IPv6 se comuniquen a través de una red IPv4

### Pregunta 6 - direcciones IPv6

_¿Qué tipo de dirección no se admite en IPv6?_

- [ ] privada
- [ ] multidifusión
- [ ] difusión
- [X] transmisión

### Pregunta 7 - direcciones IPv6

_¿Qué servicio proporciona direccionamiento IPv6 dinámicas globales a las terminales sin utilizar un servidor que registre las direcciones IPv6 disponibles?_

- [ ] DHCPv6 con información de estado
- [X] SLAAC
- [ ] direccionamiento IPv6 estático
- [ ] DHCPv6 sin información de estado

### Pregunta 8 - direcciones IPv6

_Como mínimo, ¿cuál es la dirección que se requiere en las interfaces con IPv6 habilitado?_

- [x] enlace local
- [ ] local única
- [ ] local de sitio
- [ ] difusión global

### Pregunta 9 - direcciones IPv6

¿Qué tipo de dirección IPv6 hace referencia a cualquier dirección de difusión asignada a varios hosts?

- [ ] local única
- [ ] difusión global
- [ ] enlace local
- [X] anycast

### Pregunta 10 - direcciones IPv6

_¿Qué dirección se utiliza para derivar una ID de interfaz IPv6 mediante el Método EUI-64?_

- [ ] IPv4
- [X] MAC
- [ ] multidifusión
- [ ] Capa 4

### Pregunta 11 - direcciones IPv6

_¿Qué tipo de mensaje ICMPv6 contiene información de configuración de red para los clientes que realizan SLAAC?_

- [X] anuncio de enrutador
- [ ] solicitud de enrutador
- [ ] anuncio vecino
- [ ] solicitud de vecino

### Pregunta 12 - direcciones IPv6

¿Cuántos bits de una dirección IPv6 se utilizan para identificar la ID de la interfaz?

- [ ] 32
- [ ] 48
- [X] 64
- [ ] 128

---

## Descrubrimiento de vecinos IPv6

Si su red utiliza el protocolo de comunicaciones IPv6, el protocolo de detección de vecinos o ND es lo que necesita para hacer coincidir las direcciones IPv6 con las direcciones MAC. En este tema se explica cómo funciona ND.

### Mensajes de descubrimiento de vecinos IPv6

El protocolo de descubrimiento de vecinos de IPv6 se conoce a veces como ND o NDP. En este curso, nos referiremos a él como ND. ND proporciona servicios de resolución de direcciones, detección de enrutadores y redirección para IPv6 mediante ICMPv6. ICMPv6 ND utiliza cinco mensajes ICMPv6 para realizar estos servicios:

- Mensajes de solicitud de vecinos.
- Mensajes de anuncio de vecino.
- Mensajes de solicitud del enrutador.
- Mensajes de anuncio del enrutador.
- Mensaje de redirección.

Los mensajes de solicitud de vecino y anuncio de vecino se utilizan para la mensajería de dispositivo a dispositivo, como la resolución de direcciones (similar a ARP para IPv4). Los dispositivos incluyen tanto equipos host como enrutadores.

![alt text](image-24.png)

Los mensajes de solicitud de enrutador y anuncio de enrutador son para mensajes entre dispositivos y enrutadores. Normalmente, la detección de enrutadores se utiliza para la asignación dinámica de direcciones y la configuración automática de direcciones sin estado (SLAAC).

![alt text](image-25.png)

Nota: El quinto mensaje ICMPv6 ND es un mensaje de redirección que se utiliza para una mejor selección de siguiente salto.

### Descubrimiento de vecinos IPv6 - Resolución de direcciones

Al igual que ARP para IPv4, los dispositivos IPv6 utilizan IPv6 ND para determinar la dirección MAC de un dispositivo que tiene una dirección IPv6 conocida.

Los mensajes ICMPv6 Solicitud de vecino y Anuncio de vecino se utilizan para la resolución de la dirección MAC. Esto es similar a las solicitudes ARP y las respuestas ARP utilizadas por ARP para IPv4. Por ejemplo, supongamos que PC1 desea hacer ping a PC2 en la dirección IPv6 2001:db8:acad::11. Para determinar la dirección MAC de la dirección IPv6 conocida, PC1 envía un mensaje de solicitud de vecino ICMPv6 como se ilustra en la figura.

![alt text](image-26.png)

Los mensajes de solicitud de vecinos ICMPv6 se envían utilizando direcciones multidifusión Ethernet e IPv6 especiales. Esto permite que la NIC Ethernet del dispositivo receptor determine si el mensaje de solicitud de vecino es para sí mismo sin tener que enviarlo al sistema operativo para su procesamiento. PC2 responde a la solicitud con un mensaje ICMPv6 Anuncio vecino que incluye su dirección MAC.

---

## Resumen descubrimiento de vecinos de IPv6

### Operación de descubrimiento de vecinos

IPv6 no utiliza ARP, utiliza el protocolo ND para resolver direcciones MAC. ND proporciona servicios de resolución de direcciones, detección de enrutadores y redirección para IPv6 mediante ICMPv6. ICMPv6 ND utiliza cinco mensajes ICMPv6 para realizar estos servicios: solicitud de vecino, anuncio de vecino, solicitud de enrutador, anuncio de enrutador y redirección. Al igual que ARP para IPv4, los dispositivos IPv6 utilizan IPv6 ND para resolver la dirección MAC de un dispositivo en una dirección IPv6 conocida.

---

## Cuestionario de Detección de vecinos IPv6

### Pregunta 1 - detección de vecinos IPv6

_¿Qué dos mensajes ICMPv6 se utilizan durante el proceso de resolución de direcciones MAC Ethernet?_ (Elija dos opciones)

- [ ] solicitud de enrutador
- [ ] anuncio de enrutador
- [X] solicitud de vecino
- [X] anuncio de vecino
- [ ] solicitud echo

### Pregunta 2 - detección de vecinos IPv6

_¿Qué dos mensajes ICMPv6 se utilizan durante el proceso de resolución de direcciones MAC Ethernet?_ (Elija dos opciones)

- [ ] solicitud de enrutador
- [ ] anuncio de enrutador
- [X] solicitud de vecino
- [X] anuncio de vecino
- [ ] solicitud echo

### Pregunta 3 - detección de vecinos IPv6

_¿Qué tipo de transmisión de datos utiliza un host IPv6 para enviar un mensaje de solicitud de vecino?_

- [ ] unidifusión
- [ ] anycast
- [X] multidifusión
- [ ] transmisión

### Pregunta 4 - detección de vecinos IPv6

¿Qué tipo de trama de Ethernet utiliza un host IPv6 para enviar un mensaje en respuesta a una solicitud de resolución de dirección?

- [X] unidifusión
- [ ] anycast
- [ ] multidifusión
- [ ] transmisión

### Pregunta 5 - detección de vecinos IPv6

_¿Cuántos mensajes se utilizan en el protocolo de detección de vecinos ICMPv6?_

- [ ] 3
- [ ] 4
- [X] 5
- [ ] 6

### Pregunta 6 - detección de vecinos IPv6

_¿Qué protocolo proporciona mensajes para admitir la resolución de direcciones IPv6?_

- [ ] DNS
- [ ] ARP
- [X] ICMPv6
- [ ] DHCPv6

### Pregunta 7 - detección de vecinos IPv6

_¿Qué capa OSI proporciona la operación de detección de vecinos IPv6?_

- [X] red
- [ ] enlace de datos
- [ ] transporte
- [ ] aplicación

### Pregunta 8 - detección de vecinos IPv6

_¿Qué método emplearía un host habilitado para IPv6 que usa SLAAC para aprender la dirección de la puerta de enlace predeterminada?_

- [X] anuncios de enrutador que son recibidos del enrutador de enlace
- [ ] mensajes de solicitud de vecinos enviados a vecinos conectados de manera directa
- [ ] anuncios de vecinos recibidos de vecinos conectados de manera directa
- [ ] mensajes de solicitud de enrutador recibidos desde el enrutador de enlace

### Pregunta 9 - detección de vecinos IPv6

¿Qué mensaje debe enviar un host IPv6 en respuesta a una solicitud de su dirección MAC?

- [ ] mensaje de solicitud del enrutador
- [ ] mensaje de solicitud de vecino
- [ ] mensaje de anuncio del enrutador
- [X] mensaje de anuncio de vecino

### Pregunta 10 - detección de vecinos IPv6

_¿Qué enunciado describe la solicitud de vecino y los mensajes de anuncio de vecino utilizados en el protocolo de detección de vecino IPv6?_

- [ ] Se usan solo entre dos enrutadores.
- [ ] Se usan solo entre dos computadoras.
- [ ] Se utilizan para enviar mensajes a todos los dispositivos en la misma red.
- [X] Se utilizan para la mensajería de dispositivo a dispositivo y los dispositivos incluyen computadoras y enrutadores.

### Pregunta 11 - detección de vecinos IPv6

_El host IPv6 A está enviando un mensaje de solicitud de vecino al host IPv6 B en la misma red Ethernet. ¿Qué tipo de dirección se utiliza en el campo MAC de destino del encabezado de trama Ethernet?_

- [ ] dirección MAC unicast
- [ ] dirección MAC anycast
- [X] dirección MAC multicast
- [ ] dirección MAC de difusión

### Pregunta 12 - detección de vecinos IPv6

Un host IPv6 debe iniciar la comunicación con otro host IPv6. ¿Cuál es el primer paso que debe realizar el host de envío?

- [ ] Transmite un mensaje ARP a la red.
- [ ] Envía un mensaje de solicitud de vecino al host de destino.
- [X] Determinar si el host de destino está en la misma red IPv6.
- [ ] Envíe un mensaje de anuncio de vecino al host de destino.
