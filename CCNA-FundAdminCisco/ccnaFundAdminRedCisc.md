# CCNA - Fundamentos y Administración de Redes CISCO

- [CCNA - Fundamentos y Administración de Redes CISCO](#ccna---fundamentos-y-administración-de-redes-cisco)
  - [Introducción a las comunicaciones y dispositivos de red](#introducción-a-las-comunicaciones-y-dispositivos-de-red)
  - [Modelo de capas](#modelo-de-capas)
    - [Transmisión de Datos y Elementos de Red](#transmisión-de-datos-y-elementos-de-red)
    - [Tipos de Redes](#tipos-de-redes)
    - [Suite de Protocolos y Modelos de Red](#suite-de-protocolos-y-modelos-de-red)
      - [Modelo TCP/IP](#modelo-tcpip)
      - [Funcionalidad del Modelo TCP/IP](#funcionalidad-del-modelo-tcpip)
    - [Modelo OSI](#modelo-osi)
      - [Comparación del modelo OSI y del modelo TCP/IP](#comparación-del-modelo-osi-y-del-modelo-tcpip)
    - [Funcionalidad del Modelo OSI](#funcionalidad-del-modelo-osi)
  - [Protocolos de capas superiores](#protocolos-de-capas-superiores)
    - [Capas Superiores del Modelo OSI](#capas-superiores-del-modelo-osi)
    - [Modelo Cliente-Servidor y P2P](#modelo-cliente-servidor-y-p2p)
      - [Redes P2P (Peer To Peer)](#redes-p2p-peer-to-peer)
    - [DNS - Domain Name System](#dns---domain-name-system)
    - [HTTP - HTTPS \& WWW](#http---https--www)
    - [SMTP \& POP](#smtp--pop)
    - [FTP](#ftp)
    - [DHCP](#dhcp)
    - [Servicio Telnet](#servicio-telnet)
  - [Capa de Transporte](#capa-de-transporte)
    - [TCP \& UDP](#tcp--udp)
      - [Tipos de puetos](#tipos-de-puetos)
    - [Funcionalidad de TCP](#funcionalidad-de-tcp)
      - [Partes del mensaje TCP](#partes-del-mensaje-tcp)
      - [Otras Funciones de TCP](#otras-funciones-de-tcp)
      - [Servidor TCP](#servidor-tcp)
    - [Funcionalidad de UDP](#funcionalidad-de-udp)
  - [Capa de Red](#capa-de-red)
    - [Encabezado Paquete IPv4](#encabezado-paquete-ipv4)
    - [Encabezado Paquete IPv6](#encabezado-paquete-ipv6)
    - [Separación de Redes](#separación-de-redes)
    - [Conectividad fuera de la red local](#conectividad-fuera-de-la-red-local)
  - [Direccionamiento IPv4](#direccionamiento-ipv4)
    - [Fundamentos de Enrutamiento](#fundamentos-de-enrutamiento)
      - [Protocolos de Enrutamiento](#protocolos-de-enrutamiento)
      - [Enrutamiento Estático](#enrutamiento-estático)
      - [Enrutamiento Dinámico](#enrutamiento-dinámico)
    - [Direcciones IPv4 y Prefijos de Red](#direcciones-ipv4-y-prefijos-de-red)
      - [Prefijos de Red](#prefijos-de-red)
      - [Otros Prefijos de Red](#otros-prefijos-de-red)
    - [Ejemplo encontrar las direcciones de: red, broadcast y hosts](#ejemplo-encontrar-las-direcciones-de-red-broadcast-y-hosts)
      - [Ejemplo encontrar las direcciones de red, broadcast y host usando el método intuitivo](#ejemplo-encontrar-las-direcciones-de-red-broadcast-y-host-usando-el-método-intuitivo)
    - [Unicast, Broadcast, Multicast](#unicast-broadcast-multicast)
      - [Unicast](#unicast)
      - [Broadcast](#broadcast)
      - [Multicast](#multicast)
    - [Rango de direcciones IPv4](#rango-de-direcciones-ipv4)
      - [Direcciones IPv4 especiales](#direcciones-ipv4-especiales)
      - [Planificación del direccionamiento](#planificación-del-direccionamiento)
    - [Direccionamiento Estático, Dinámico e ISPs](#direccionamiento-estático-dinámico-e-isps)
    - [Máscara de Subred](#máscara-de-subred)
    - [Generación de subredes](#generación-de-subredes)
    - [El patrón de las subredes](#el-patrón-de-las-subredes)
    - [Método Saltos de Línea para crear Subredes](#método-saltos-de-línea-para-crear-subredes)
      - [Ejemplo práctico](#ejemplo-práctico)
      - [Ventajas del método de saltos de red](#ventajas-del-método-de-saltos-de-red)
      - [Consideraciones importantes](#consideraciones-importantes)
    - [Comprender la notación CIDR](#comprender-la-notación-cidr)
    - [Determinar la nueva máscara de subred](#determinar-la-nueva-máscara-de-subred)
    - [Calcular el salto de red](#calcular-el-salto-de-red)
    - [Crear las subredes](#crear-las-subredes)
    - [Determinar el rango de direcciones IP utilizables en cada subred](#determinar-el-rango-de-direcciones-ip-utilizables-en-cada-subred)
    - [VLSM](#vlsm)
    - [Pruebas Básicas de Capa de Red](#pruebas-básicas-de-capa-de-red)
    - [Método de Incremento de Subredes](#método-de-incremento-de-subredes)
      - [Concepto Clave](#concepto-clave)
      - [Pasos para aplicar el método de saltos de red](#pasos-para-aplicar-el-método-de-saltos-de-red)
        - [_Paso 1_: Determinar el número de subredes necesarias](#paso-1-determinar-el-número-de-subredes-necesarias)
        - [_Paso 2_: Calcular los bits de subred](#paso-2-calcular-los-bits-de-subred)
        - [_Paso 3_: Calcular la nueva máscara de subred](#paso-3-calcular-la-nueva-máscara-de-subred)
        - [_Paso 4_: Calcular el tamaño del salto (tamaño de cada subred)](#paso-4-calcular-el-tamaño-del-salto-tamaño-de-cada-subred)
        - [_Paso 5_: Determinar los rangos de direcciones de cada subred](#paso-5-determinar-los-rangos-de-direcciones-de-cada-subred)
        - [_Paso 6_: Identificar direcciones asignables](#paso-6-identificar-direcciones-asignables)
    - [Ejemplo completo](#ejemplo-completo)
  - [Capa Enlace de Datos](#capa-enlace-de-datos)
    - [PDU de Capa 2](#pdu-de-capa-2)
    - [Control de Acceso al Medio](#control-de-acceso-al-medio)
    - [Topologías Lógicas y físicas](#topologías-lógicas-y-físicas)
    - [Estructura de Trama](#estructura-de-trama)
    - [Protocolo Ethernet](#protocolo-ethernet)
    - [Protocolo PPP](#protocolo-ppp)
    - [Protocolo WLAN](#protocolo-wlan)
  - [Capa Física](#capa-física)
    - [Funciones de la Capa Física](#funciones-de-la-capa-física)
      - [Señalización](#señalización)
        - [Señalizaicón NRZ](#señalizaicón-nrz)
        - [Señalización Manchester](#señalización-manchester)
      - [Codificación: agrupación de bit](#codificación-agrupación-de-bit)
    - [Transferencia de datos](#transferencia-de-datos)
    - [Medios físicos de cobre - estructurados](#medios-físicos-de-cobre---estructurados)
      - [Montaje cable estructurado en conector RJ-45](#montaje-cable-estructurado-en-conector-rj-45)
    - [Selección de cable directo o cruzado en dispositivos](#selección-de-cable-directo-o-cruzado-en-dispositivos)
    - [Cable de Fibra Óptica](#cable-de-fibra-óptica)
    - [Medios Inalámbricos](#medios-inalámbricos)
    - [Conectores cableado en redes](#conectores-cableado-en-redes)
  - [Tecnología Ethernet](#tecnología-ethernet)
    - [Trama Ethernet](#trama-ethernet)
    - [MAC Address](#mac-address)
    - [Direccionamiento Ethernet](#direccionamiento-ethernet)
      - [Unicast Ethernet](#unicast-ethernet)
      - [Broadcast Ethernet](#broadcast-ethernet)
      - [Multicast Ethernet](#multicast-ethernet)
    - [Proceso CSMA/CD](#proceso-csmacd)
    - [Velocidades Ethernet](#velocidades-ethernet)
    - [Switch](#switch)
      - [Protocolo ARP](#protocolo-arp)
  - [Consideraciones para Conexiones](#consideraciones-para-conexiones)
    - [Factores del cableado](#factores-del-cableado)
      - [Conexión Directa y Cruzada](#conexión-directa-y-cruzada)
    - [Conexiones WAN](#conexiones-wan)
  - [IOS de CISCO](#ios-de-cisco)
    - [Comandos del IOS](#comandos-del-ios)
    - [Comandos de análisis](#comandos-de-análisis)
    - [Comandos para añadir contraseñas y mensajes](#comandos-para-añadir-contraseñas-y-mensajes)
    - [Administración de archivos de configuración](#administración-de-archivos-de-configuración)
    - [Configuración de interfaces](#configuración-de-interfaces)
    - [Líneas de base \& ARP](#líneas-de-base--arp)
    - [Prácticas. Configuraciones en Router](#prácticas-configuraciones-en-router)
  - [Direccionamiento IPv6](#direccionamiento-ipv6)
    - [Formato en IPv6](#formato-en-ipv6)
      - [Simplificación de las direcciones IPv6](#simplificación-de-las-direcciones-ipv6)
      - [Direcciones UNICAST - UNICAST GLOBAL (2000::/3)](#direcciones-unicast---unicast-global-20003)
      - [Direcciones MULTICAST (FF00::/8)](#direcciones-multicast-ff008)
      - [Direcciones ANYCAST](#direcciones-anycast)
      - [Direcciones LINK - LOCAL (FE80::/10)](#direcciones-link---local-fe8010)
      - [Direcciones LOCALES (FC00::/7)](#direcciones-locales-fc007)
      - [Direcciones ESPECIALES](#direcciones-especiales)
    - [Subnetting y Mecanismos de Transición](#subnetting-y-mecanismos-de-transición)
      - [Subnettig IPv6](#subnettig-ipv6)
      - [Mecanismos de Transición IPv6](#mecanismos-de-transición-ipv6)
  - [Configuraciones básicas](#configuraciones-básicas)
    - [Routers](#routers)
      - [Proceso de arranque del Router](#proceso-de-arranque-del-router)
      - [Interfaces de un Router](#interfaces-de-un-router)
      - [Routers y Capa de Red](#routers-y-capa-de-red)
      - [Configuraciones Básicas](#configuraciones-básicas-1)
      - [Tabla de enrutamiento](#tabla-de-enrutamiento)
      - [Enrutamiento Estático y Dinámico](#enrutamiento-estático-y-dinámico)
      - [Mejor ruta](#mejor-ruta)
    - [Prácticas configuración Router](#prácticas-configuración-router)
      - [Práctica de conectividad entre Routers](#práctica-de-conectividad-entre-routers)
  - [Ruteo Estático](#ruteo-estático)
    - [Rutas estáticas en el ejemplo](#rutas-estáticas-en-el-ejemplo)
    - [Protocolo CDP](#protocolo-cdp)
      - [Práctica I - Ruteo estático en IPv6](#práctica-i---ruteo-estático-en-ipv6)
      - [Práctica II - Ruteo estático en IPv6](#práctica-ii---ruteo-estático-en-ipv6)
      - [Práctica III - Ruteo estático en IPv6](#práctica-iii---ruteo-estático-en-ipv6)
  - [Introducción al ruteo dinámico](#introducción-al-ruteo-dinámico)
    - [Función y Propósito de los Protocolos Dinámicos](#función-y-propósito-de-los-protocolos-dinámicos)
    - [Tipos de Protocolos Dinámicos](#tipos-de-protocolos-dinámicos)
      - [Protocolo Vector Distancia y Estado de Enlace](#protocolo-vector-distancia-y-estado-de-enlace)
    - [VLSM en Protocolos Dinámicos y Convergencia](#vlsm-en-protocolos-dinámicos-y-convergencia)
      - [Convergencia](#convergencia)
    - [Métrica y Balance de Carga](#métrica-y-balance-de-carga)
    - [Distancia Administrativa](#distancia-administrativa)
  - [Protocolos dinámicos de Vector Distancia](#protocolos-dinámicos-de-vector-distancia)
    - [Vector Distancia](#vector-distancia)
    - [Características Vector Distancia](#características-vector-distancia)
    - [Actualización de Enrutamiento y Convergencia](#actualización-de-enrutamiento-y-convergencia)
    - [Temporizadores y Envío de Actualizaciones](#temporizadores-y-envío-de-actualizaciones)
    - [Actualizaciones periódicas](#actualizaciones-periódicas)
      - [Fluctuación de fase aleatoria](#fluctuación-de-fase-aleatoria)
    - [Routing Loops](#routing-loops)
    - [Cuenta a Infinito](#cuenta-a-infinito)
    - [Prevención de Loops con Temporizadores](#prevención-de-loops-con-temporizadores)
    - [Horizonte Dividido](#horizonte-dividido)
      - [Envenenamiento en Reversa](#envenenamiento-en-reversa)
      - [TTL del protocolo IP](#ttl-del-protocolo-ip)
  - [Protocolos RIP versión 1](#protocolos-rip-versión-1)
    - [Configuración RIPv1](#configuración-ripv1)
    - [Interfaces Pasivas y Debug RIP](#interfaces-pasivas-y-debug-rip)
    - [Desventajas de RIPv1](#desventajas-de-ripv1)
    - [Enruramiento Estático y RIP](#enruramiento-estático-y-rip)
  - [VLSM \& CIDR](#vlsm--cidr)
    - [Clases de Direcciones IPv4](#clases-de-direcciones-ipv4)
    - [Classless Inter-Domain Routing (CIDR)](#classless-inter-domain-routing-cidr)
    - [Variable Length Subnet Mask (VLSM)](#variable-length-subnet-mask-vlsm)
    - [Configuración Básica con IPv6](#configuración-básica-con-ipv6)
  - [RIP versión 2 y otras configuraciones](#rip-versión-2-y-otras-configuraciones)
    - [Interfaces Loopback](#interfaces-loopback)
    - [Rutas NULL y Redistribución de Ruta Estática](#rutas-null-y-redistribución-de-ruta-estática)
    - [RIPv2](#ripv2)
    - [Ejercicio PT](#ejercicio-pt)
    - [Ejercicio configuración RIPng para IPv6](#ejercicio-configuración-ripng-para-ipv6)
  - [Análisis de la tabla de enrutamiento](#análisis-de-la-tabla-de-enrutamiento)
  - [Protocolos dinámico EIGRP](#protocolos-dinámico-eigrp)
    - [Protocolo EIGRP](#protocolo-eigrp)
    - [Formato de mensajes EIGRP](#formato-de-mensajes-eigrp)
    - [Tipos de paquetes EIGRP](#tipos-de-paquetes-eigrp)
      - [Módulos Dependientes de Protocolo (PDM)](#módulos-dependientes-de-protocolo-pdm)
    - [Envíos de Saludos/Actualizaciones](#envíos-de-saludosactualizaciones)
    - [DUAL, Distancia Administrativa y Autenticación](#dual-distancia-administrativa-y-autenticación)
    - [Configuración Básica de EIGRP](#configuración-básica-de-eigrp)
    - [Práctica RIP - EIGRP - STATIC](#práctica-rip---eigrp---static)
    - [Práctica Conexión en Topología](#práctica-conexión-en-topología)
    - [Práctica configuración EIGRP para IPv6](#práctica-configuración-eigrp-para-ipv6)
  - [Protocolos dinámicos de estado de enlace](#protocolos-dinámicos-de-estado-de-enlace)
    - [Protocolos de Estado de Enlace](#protocolos-de-estado-de-enlace)
    - [Algoritmo SPF (EDSGER DIJKSTRA)](#algoritmo-spf-edsger-dijkstra)
    - [Proceso de Enrutamiento](#proceso-de-enrutamiento)
    - [Conocimiento de Redes Directamente Conectadas](#conocimiento-de-redes-directamente-conectadas)
    - [Descubrimiento de vecinos](#descubrimiento-de-vecinos)
    - [Construcción de LSPs](#construcción-de-lsps)
    - [Saturación de LSPs](#saturación-de-lsps)
    - [Base de Datos SPF](#base-de-datos-spf)
    - [Creación del árbol SPF](#creación-del-árbol-spf)
    - [Ventajas de los  protocolos SPF](#ventajas-de-los--protocolos-spf)
    - [Requerimientos y Consideraciones](#requerimientos-y-consideraciones)
  - [Protocolos dinámicos OSPF](#protocolos-dinámicos-ospf)
    - [Paquetes de OSPF](#paquetes-de-ospf)
    - [Protocolo de Saludo](#protocolo-de-saludo)
    - [Actualización y Algoritmo OSPF](#actualización-y-algoritmo-ospf)
    - [Distancia Administrativa de OSPF](#distancia-administrativa-de-ospf)
    - [Configuración Básica de OSPF](#configuración-básica-de-ospf)
    - [ID del Router OSPF](#id-del-router-ospf)
    - [Configuración de Loopback y Cambio de Router ID](#configuración-de-loopback-y-cambio-de-router-id)
      - [Comando Router-ID](#comando-router-id)
    - [Verificación de los procesos OSPF](#verificación-de-los-procesos-ospf)
    - [Métrica OSPF](#métrica-ospf)
    - [Análisis de la Métrica de OSPF](#análisis-de-la-métrica-de-ospf)
    - [Modificar el Coste mediante el Ancho de Banda](#modificar-el-coste-mediante-el-ancho-de-banda)
    - [Modificación Directa del Costo OSPF](#modificación-directa-del-costo-ospf)
    - [Topología de Accesos Múltiples](#topología-de-accesos-múltiples)
    - [Elección de DR \& BDR](#elección-de-dr--bdr)
    - [Modificación de Prioridad de OSPF](#modificación-de-prioridad-de-ospf)
    - [Redistribución de Rutas Estáticas y Rutas por Defecto](#redistribución-de-rutas-estáticas-y-rutas-por-defecto)
    - [Modificación del Ancho de Banda de Referencia](#modificación-del-ancho-de-banda-de-referencia)
    - [Modificación de Temporizadores](#modificación-de-temporizadores)
    - [Ejercicio PT 1](#ejercicio-pt-1)
    - [Ejercicio PT 2](#ejercicio-pt-2)
    - [Ejercicio PT 3](#ejercicio-pt-3)
  - [Introducción a redes conmutadas](#introducción-a-redes-conmutadas)
    - [Redes Jerárquicas](#redes-jerárquicas)
    - [Principios de Diseño en Redes Jerárquicas](#principios-de-diseño-en-redes-jerárquicas)
    - [Consideraciones para Switch](#consideraciones-para-switch)
    - [Características de los Switch](#características-de-los-switch)
  - [Funcionalidades de un Switch](#funcionalidades-de-un-switch)
    - [Elementos de Ethernet](#elementos-de-ethernet)
    - [Dominios de Broadcast \& Colisiones](#dominios-de-broadcast--colisiones)
      - [Latencia](#latencia)
      - [Segmentar una LAN](#segmentar-una-lan)
    - [Consideraciones en el Diseño de Redes LAN](#consideraciones-en-el-diseño-de-redes-lan)
    - [Métodos de conmutación de un Switch](#métodos-de-conmutación-de-un-switch)
    - [Conmutación Simétrica y Asimétrica](#conmutación-simétrica-y-asimétrica)
    - [Buffer de memoria](#buffer-de-memoria)
    - [Switch de Capa 2 \& Switch de Capa 3](#switch-de-capa-2--switch-de-capa-3)
      - [Switch de Capa 2](#switch-de-capa-2)
      - [Switch de Capa 3](#switch-de-capa-3)
    - [IOS del Switch](#ios-del-switch)
    - [Administración de un Switch por Interfaz Local](#administración-de-un-switch-por-interfaz-local)
    - [Secuencia de arranque de un Switch](#secuencia-de-arranque-de-un-switch)
    - [configuraciones Básicas de un Switch](#configuraciones-básicas-de-un-switch)
    - [Cambio de Velocidad en Interfaz](#cambio-de-velocidad-en-interfaz)
    - [Otros Comandos de Interés](#otros-comandos-de-interés)
    - [Telnet \& SSH](#telnet--ssh)
    - [Ataques a una Red LAN](#ataques-a-una-red-lan)
      - [Saturación de MAC](#saturación-de-mac)
      - [Suplantación de Identidad](#suplantación-de-identidad)
      - [Otros tipos de ataques](#otros-tipos-de-ataques)
    - [Uso de Herramientas de Seguridad](#uso-de-herramientas-de-seguridad)
    - [Seguridad de Puertos](#seguridad-de-puertos)
    - [Seguridad de Puertos - MAC Estática y Dinámica](#seguridad-de-puertos---mac-estática-y-dinámica)
    - [Seguridad de Puertos - MAC sin Modificación](#seguridad-de-puertos---mac-sin-modificación)
    - [Verificación Seguridad Puertos](#verificación-seguridad-puertos)
    - [Ejercicio Funcionalidades de un Switch](#ejercicio-funcionalidades-de-un-switch)
  - [VLANS](#vlans)
    - [Introducción a las VLAN](#introducción-a-las-vlan)
      - [Ventajas de Utilizar VLAN](#ventajas-de-utilizar-vlan)
      - [División de las VLAN](#división-de-las-vlan)
    - [Tipos de VLAN](#tipos-de-vlan)
      - [VLAN de Voz](#vlan-de-voz)
    - [Puertos del Switch](#puertos-del-switch)
    - [Dominios de Broadcast y Comunicación entre VLAN](#dominios-de-broadcast-y-comunicación-entre-vlan)
    - [Configuración Básica de VLAN](#configuración-básica-de-vlan)
    - [Enlaces Troncales](#enlaces-troncales)
      - [Etiquetado de trama 802.1Q](#etiquetado-de-trama-8021q)
    - [Modos de Configuración de Enlaces Troncales](#modos-de-configuración-de-enlaces-troncales)
    - [Configuración Básica en Enlaces Troncales](#configuración-básica-en-enlaces-troncales)
    - [Modos de Enlace Troncal](#modos-de-enlace-troncal)
    - [Problemas Comunes Enlaces Troncales](#problemas-comunes-enlaces-troncales)
    - [Práctica Problemas Comunes](#práctica-problemas-comunes)
  - [VTP](#vtp)
    - [Introducción al Protocolo Enlace Local de VLAN (VTP)](#introducción-al-protocolo-enlace-local-de-vlan-vtp)
    - [Información de Estado de VTP](#información-de-estado-de-vtp)
    - [Dominios de VTP](#dominios-de-vtp)
    - [Número de Revisión de Configuración](#número-de-revisión-de-configuración)
    - [Notificaciones de VTP](#notificaciones-de-vtp)
      - [Publicaciones de VTP](#publicaciones-de-vtp)
    - [Modos de VTP](#modos-de-vtp)
    - [Problemas Comunes en VTP](#problemas-comunes-en-vtp)
    - [Configuración Básica de VTP](#configuración-básica-de-vtp)
  - [STP](#stp)
    - [Redundancia en una Red Conmutada](#redundancia-en-una-red-conmutada)
    - [Inconveniente de la Redundancia](#inconveniente-de-la-redundancia)
    - [Bucles de Infraestructura](#bucles-de-infraestructura)
    - [Protocolo Spanning Tree (STP)](#protocolo-spanning-tree-stp)
    - [BID \& Costo de Ruta](#bid--costo-de-ruta)
    - [Coste de STP](#coste-de-stp)
    - [Proceso de BPDU](#proceso-de-bpdu)
    - [ID de Puente (BID)](#id-de-puente-bid)
    - [Prioriddad de Puente Raíz](#prioriddad-de-puente-raíz)
    - [Funciones de los Puertos](#funciones-de-los-puertos)
    - [Elección de Funciones de Puertos](#elección-de-funciones-de-puertos)
    - [Estados de los Puertos](#estados-de-los-puertos)
    - [Función PortFast](#función-portfast)
    - [Configuración de PortFast](#configuración-de-portfast)
    - [Convergencia de STP](#convergencia-de-stp)
    - [Convergencia - Asignación de Puertos Raíz](#convergencia---asignación-de-puertos-raíz)
    - [Verificación de STP](#verificación-de-stp)
    - [Cambio de Topología en STP](#cambio-de-topología-en-stp)
    - [Variantes de STP](#variantes-de-stp)
    - [PVST+](#pvst)
    - [Configuración de PVSTP](#configuración-de-pvstp)
    - [RSTP](#rstp)
    - [Puertos de Extremo](#puertos-de-extremo)
    - [Tipos de Enlace](#tipos-de-enlace)
    - [Estados y Funciones de Puertos en RSTP](#estados-y-funciones-de-puertos-en-rstp)
    - [Configuración de PVST](#configuración-de-pvst)
    - [Configuración de Port-Channel](#configuración-de-port-channel)
  - [Enrutamiento entre VLANS](#enrutamiento-entre-vlans)
    - [Enrutamiento VLANs Interfaces físicas](#enrutamiento-vlans-interfaces-físicas)
    - [Enrutamiento VLANs Interfaces Lógicas](#enrutamiento-vlans-interfaces-lógicas)
    - [Funcionamiento entre VLANs](#funcionamiento-entre-vlans)
    - [Configuración de Enrutamiento con y sin VLANs - Gestión remota del Switch](#configuración-de-enrutamiento-con-y-sin-vlans---gestión-remota-del-switch)
    - [Comparativa Métodos Enrutamiento de VLANs](#comparativa-métodos-enrutamiento-de-vlans)
    - [Consideraciones para el Enrutamiento entre VLANs](#consideraciones-para-el-enrutamiento-entre-vlans)
    - [Ejercicio PT Enrutamiento entre VLANs](#ejercicio-pt-enrutamiento-entre-vlans)
  - [Introducción a redes inalámbricas](#introducción-a-redes-inalámbricas)
    - [Estándares para Redes inalámbricas](#estándares-para-redes-inalámbricas)
    - [Componentes de las Redes inalámbricas](#componentes-de-las-redes-inalámbricas)
    - [Operación inalámbrica](#operación-inalámbrica)
    - [Planificación WLAN](#planificación-wlan)
    - [Amenazas para las Redes WLAN](#amenazas-para-las-redes-wlan)
    - [Protocolos de Seguridad WLAN](#protocolos-de-seguridad-wlan)
      - [Protocolo de Autenticación Extendida (EAP)](#protocolo-de-autenticación-extendida-eap)
    - [Protección de una LAN Inalámbrica](#protección-de-una-lan-inalámbrica)
    - [Práctica configuración red Inalámbrica](#práctica-configuración-red-inalámbrica)
  - [Redes y protocolos WAN](#redes-y-protocolos-wan)
    - [Operaciones de las Redes WAN](#operaciones-de-las-redes-wan)
    - [Capa Física de Redes WAN](#capa-física-de-redes-wan)
    - [Capa de Enlace de Datos de Redes WAN](#capa-de-enlace-de-datos-de-redes-wan)
    - [Conmutación de Circuitos y de Paquetes](#conmutación-de-circuitos-y-de-paquetes)
    - [Opciones de Conexión WAN](#opciones-de-conexión-wan)
    - [Conexión de Enlace Dedicado](#conexión-de-enlace-dedicado)
    - [Conexión por Conmutación de Circuitos](#conexión-por-conmutación-de-circuitos)
    - [Conexión por Conmutación de Paquetes](#conexión-por-conmutación-de-paquetes)
    - [Conexión por Internet](#conexión-por-internet)
      - [Seguridad en Conexión por Internet](#seguridad-en-conexión-por-internet)
  - [Protocolos PPP](#protocolos-ppp)
    - [Comunicación Serial](#comunicación-serial)
      - [Conector RS-232](#conector-rs-232)
    - [TDM](#tdm)
    - [STDM](#stdm)
    - [DTE \& DCE](#dte--dce)
    - [Encapsulación WAN](#encapsulación-wan)
    - [Verificación de Interfaces Seriales](#verificación-de-interfaces-seriales)
    - [Encapsulación PPP](#encapsulación-ppp)
    - [Arquitectura de Capas PPP](#arquitectura-de-capas-ppp)
    - [Tramas de PPP](#tramas-de-ppp)
    - [Inicio de sesión PPP](#inicio-de-sesión-ppp)
    - [Establecimiento de Enlace](#establecimiento-de-enlace)
    - [Proceso NCP](#proceso-ncp)
    - [Configuración Básica de PPP](#configuración-básica-de-ppp)
    - [Autenticación en PPP](#autenticación-en-ppp)
    - [Autenticación PAP](#autenticación-pap)
    - [Autenticación CHAP](#autenticación-chap)
    - [Cofiguración PAP](#cofiguración-pap)
    - [Configuración CHAP](#configuración-chap)
  - [Frame Relay](#frame-relay)
    - [Circuitos Virtuales](#circuitos-virtuales)
    - [Encapsulación Frame Relay](#encapsulación-frame-relay)
    - [Topologías de Frame Relay](#topologías-de-frame-relay)
    - [Tramas LMI](#tramas-lmi)
    - [Configuración Básica de Frame Relay](#configuración-básica-de-frame-relay)
    - [Problema del Horizonte Dividido](#problema-del-horizonte-dividido)
    - [Subinterfaz Frame Relay Punto a Punto](#subinterfaz-frame-relay-punto-a-punto)
    - [Subinterfaz Frame Relay Multipunto](#subinterfaz-frame-relay-multipunto)
    - [Ejercicio WAN](#ejercicio-wan)
  - [Introducción a seguridad de redes](#introducción-a-seguridad-de-redes)
    - [Definiciones](#definiciones)
    - [Acciones a realizar por un Hacker](#acciones-a-realizar-por-un-hacker)
    - [Tipos de delitos](#tipos-de-delitos)
    - [Políticas de Seguridad](#políticas-de-seguridad)
    - [Amenazas Comunes y Vulnerabilidades](#amenazas-comunes-y-vulnerabilidades)
    - [Tipos de Ataques](#tipos-de-ataques)
      - [Ataque de reconocimiento](#ataque-de-reconocimiento)
      - [Ataques de Acceso](#ataques-de-acceso)
      - [Ataques de Denegación de Servicios (DoS)](#ataques-de-denegación-de-servicios-dos)
      - [Ataques de Códigos Maliciosos](#ataques-de-códigos-maliciosos)
    - [Técnicas de Mitigación](#técnicas-de-mitigación)
    - [Rueda de Seguridad de la Red](#rueda-de-seguridad-de-la-red)
    - [Seguridad de los Routers](#seguridad-de-los-routers)
      - [Seguridad del IOS de Cisco](#seguridad-del-ios-de-cisco)
    - [Configuración Básica Seguridad de Routers](#configuración-básica-seguridad-de-routers)
    - [Servicios Vulnerables en Routers](#servicios-vulnerables-en-routers)
    - [Protección de Protocolos Dinámicos](#protección-de-protocolos-dinámicos)
    - [Configuración Autenticación del Protocolo RIP](#configuración-autenticación-del-protocolo-rip)
    - [Backup de Configuración y Flash](#backup-de-configuración-y-flash)
  - [Access Lists](#access-lists)
    - [Filtrado de Paquetes](#filtrado-de-paquetes)
    - [Listas de Acceso (ACL)](#listas-de-acceso-acl)
    - [Funcionamiento de las ACLs](#funcionamiento-de-las-acls)
    - [Tipos de ACLs](#tipos-de-acls)
    - [Numeración y Denominación de ACLs](#numeración-y-denominación-de-acls)
    - [Ubicación de las ACLs](#ubicación-de-las-acls)
    - [Configuración Básica de ACLs Estándar](#configuración-básica-de-acls-estándar)
    - [Ubicación de ACLs Estándar](#ubicación-de-acls-estándar)
    - [Cofiguración de ACL para Acceso Remoto](#cofiguración-de-acl-para-acceso-remoto)
    - [Configuración ACL Estándar Nombrada y Edición de ACLs Estándar](#configuración-acl-estándar-nombrada-y-edición-de-acls-estándar)
    - [ACLs Extendidas \& ACLs Estándar](#acls-extendidas--acls-estándar)
    - [Configuración Básica ACLs Extendida](#configuración-básica-acls-extendida)
    - [ACLs Complejas](#acls-complejas)
    - [ACLs Dinámicas](#acls-dinámicas)
      - [Configuración de ACLs Dinámicas](#configuración-de-acls-dinámicas)
    - [ACLs Reflexivas](#acls-reflexivas)
      - [Configuración de ACLs Reflexivas](#configuración-de-acls-reflexivas)
    - [ACLs Basadas en el Tiempo](#acls-basadas-en-el-tiempo)
      - [Configuración de ACLs Basadas en el Tiempo](#configuración-de-acls-basadas-en-el-tiempo)
    - [Ejercicio Práctico configura de ACLs](#ejercicio-práctico-configura-de-acls)
  - [Fundamentos sobre la comunicación remota](#fundamentos-sobre-la-comunicación-remota)
    - [Trabajo a Distancia](#trabajo-a-distancia)
    - [Componentes para el Trabajo a Distancia](#componentes-para-el-trabajo-a-distancia)
    - [Transmisión por Cable](#transmisión-por-cable)
    - [DSL](#dsl)
    - [Conexiones Inalámbricas](#conexiones-inalámbricas)
    - [Redes VPN](#redes-vpn)
    - [Tipos de VPN](#tipos-de-vpn)
    - [Componentes de VPN](#componentes-de-vpn)
    - [Características de una VPN](#características-de-una-vpn)
    - [Tunneling](#tunneling)
    - [Integridad de Datos en VPNs](#integridad-de-datos-en-vpns)
      - [HASH](#hash)
    - [IPSex](#ipsex)
  - [Servicios para redes LAN](#servicios-para-redes-lan)
  - [Redes desde cero](#redes-desde-cero)
    - [IP (Protocolo Internet)](#ip-protocolo-internet)
    - [IP Dinámicas y Estáticas](#ip-dinámicas-y-estáticas)
      - [Conocer la IP en Windows](#conocer-la-ip-en-windows)
      - [Conocer la IP en Linux](#conocer-la-ip-en-linux)
    - [NAT (Network Address Translation)](#nat-network-address-translation)
    - [Gateway o Puerta de Enlace](#gateway-o-puerta-de-enlace)
    - [Direcciones MAC](#direcciones-mac)
      - [Protocolo ARP (Address Resolution Protocol)](#protocolo-arp-address-resolution-protocol)
    - [TCP (Transmission Control Protocol)](#tcp-transmission-control-protocol)
      - [Three-Way Handshake](#three-way-handshake)
    - [UDP (User Datagram Protocol)](#udp-user-datagram-protocol)
    - [Protocolos y puertos más comunes en Ciberseguridad](#protocolos-y-puertos-más-comunes-en-ciberseguridad)
    - [Subneteo](#subneteo)
      - [Práctica Subneteo](#práctica-subneteo)

- - -

## Introducción a las comunicaciones y dispositivos de red

Reglas de comunicación, son los _protocolos_ de comunicación entre emisor y receptor. Requieren prioridades y confirmación.

_Factores externos_, hace referencia a la calidad de la ruta (cable, inalámbrica). Es el ancho de banda. También a la cantidad de veces que el mensaje debe ser reenviado. Por último, la cantidad de tiempo asignado a los mensajes.

_Factores internos_, hace referencia al tamaño y la complejidad del mensaje. La importancia del mensaje, es cuando se asigna cierta prioridad sobre otros.

Toda red tiene siempre cuatro elementos básicos: reglas, protocolos, mensajes, forma de conectar los dispositivos y los propios dispositivos de la red.

_Dispositivos terminales_, donde se recibe la información, como pueden ser: PC, Portátil, teléfonos IP, Servidores, impresoras.

_Dispositovos intermedios_: router, switch, firewall.

Las fibras ópticas con la cubierta de color amarillo, son _monomodos_, las fibras cuya cubierta es de color naranja, son _multimodo_.

## Modelo de capas

### Transmisión de Datos y Elementos de Red

- Segmentación: dividir el paquete de datos.
- Multiplexación: permite enviar datos distintos por el mismo medio.
- Ancho de banda: depende del medio de comunicación

| UTP CAT.5 | UTP CAT.6 |
| :--: | :--: |
| 1 Gbps | 10 Gbps a 35 m |
| 100 MHz | 250 MHz |
| > 100 m | 1 Gbps si d < 100 m |

Estándares en redes Wi-Fi

![alt text](image-3.png)

### Tipos de Redes

- **PAN**: red personal para un único usuario.
- **LAN**: red local de un o varios usuarios, tales como, oficinas, empresas.
- **CAN**: red de Campus o Universidades o Escuelas. Propias de entidades educativas.
- **SAN**: red de almacenamiento, conformada por servidores.
- **MAN**: red metropolitana, dispersa en una serie de edificios de una ciudad.
- **WAN**: red de área mundial.

En el ambiente corporativo se conoce lo que es **intranet**, no es una topología o un tipo de red. Otro concepto que se usa es **extranet**, viene siendo una acceso limitado a las personas en una empresa. Red interna en una corporación. El término más conocido es **internet** que es la red de redes.

### Suite de Protocolos y Modelos de Red

Los **protocolos** permiten que varios fabricantes distintos puedan comunicarse entre sí. Algunos protocolos que se utilizan son:

- **HTTP**. Protocolo de transferencia de hipertexto.
- **TCP**. Protocolo de control de transmisión.
- **IP**. Protocolo de Internet.

El modelo de capas explica el proceso de los protocolos y de cómo se relacionan entre sí.

![alt text](image-4.png)

- _Modelos de protocolo_. Representa toda la funcionalidad requerida para realizar la interconexión en la red de datos. El **modelo TCP/IP** es un modelo de protocolos.
- _Modelos de referencia_. Proporciona referencia común en todos los tipos de protocolos y servicios de red. El **modelo OSI** es el más conocido para sistemas abiertos en redes de datos.

#### Modelo TCP/IP

Modelo conformado en capas de estándar abierto.

![alt text](image-5.png)

**PDU** unidad de datos de protocolo. Cada capa encapsula la **PDU** que recibe de la capa anterior.

- **Capa Aplicación**: representa datos para el usuario más el control de codificación y diálogo.
- **Capa Transporte**: admite la comunicación entre dispositivos de distintas redes.
- **Capa Internet**: determina la mejor ruta a través de la red.
- **Capa Acceso a la red**: controla los dispositivos del Hardware y los medios que forman la red.

Ejemplo de un usuario enviando un email:

![alt text](image-6.png)

#### Funcionalidad del Modelo TCP/IP

Operación de protocolo de envío y recepción de un mensaje.

![alt text](image-7.png)

### Modelo OSI

| Capa | Utilidad |
| --- | --- |
| 7 | Proporciona los medios para la comunicación |
| 6 | Proporciona una representación de datos y encriptación |
| 5 | Proporciona servicios a la capa 6 para organizar los diálogos e intercambio de datos |
| 4 | Define los servicios para segmentar, transferir y ensamblar datos |
| 3 | Proporciona los servicios para intercambiar los datos |
| 2 | Describe los métodos para intercambiar los datos |
| 1 | Activa y desactiva las conexiones físicas |

![alt text](image-8.png)

#### Comparación del modelo OSI y del modelo TCP/IP

![alt text](image-9.png)

### Funcionalidad del Modelo OSI

El modelo OSI describe los procesos de codificación, segmentación y encapsulación de datos.

![alt text](image-10.png)

Formación de un encabezado PDU de datos del protocolo:

![alt text](image-11.png)

Cuando dos dispotivos se conectan intercambiando los datos, también se intercambian las direcciones MAC.

Las direcciones de capa 3 o lógicas son las IP.

![alt text](image-12.png)

## Protocolos de capas superiores

### Capas Superiores del Modelo OSI

![alt text](image-13.png)

Los protocolos principales que se pueden utilizar en las capas superiores son los servidores: **DNS, Telnet, E-mail, DHCP, HTTP/HTTPS, FTP**

### Modelo Cliente-Servidor y P2P

El modelo cliente-servidor, cuando un host accede a la información de un servidor se conoce como _descarga_. El proceso inverso, es _subida_ o carga de datos.

#### Redes P2P (Peer To Peer)

![alt text](image-14.png)

Una red P2P permite a un _cliente_ actuar como _servidor_ y viceversa. Es decir, un cliente es a la vez servidor y un cliente es a la vez servidor.

### DNS - Domain Name System

Un dominio es un acrónimo que identifica a un dispositivo asociado a una dirección IP. El sistema DNS es una base de datos que permite la resolución de nombres.

![alt text](image-15.png)

### HTTP - HTTPS & WWW

![alt text](image-16.png)

### SMTP & POP

![alt text](image-17.png)

- Enviar E-mail es con el protocolo SMTP. Controla los correos salientes desde el emisor.
- Obtener E-mail es con el protocolo POP3.
- Cliente MUA (agente de correo electrónico)
- Servidor de correo MTA (agente de transferencia de correo).
- MDA es el agente de entrega de correo.

![alt text](image-18.png)

Si el correo está en el servidor local, entonces se pasa al MDA.

![alt text](image-19.png)

### FTP

FTP es una aplicación que permite la transferencia de archivos entre cliente y servidor. El cliente es quien inicia la conexión con el servidor, usando el puerto 21. Posteriormente, una vez establecida la conexión, se transfieren los archivos por el puerto 20.

![alt text](image-20.png)

### DHCP

DHCP sirve para asignar direcciones IP/Mask/Gateway de forma automática. En lugar de asignar una dirección estática a cada dispositivo de la red, el servidor DHCP lo puede asignar de forma automática.

![alt text](image-21.png)

### Servicio Telnet

El servicio Telnet proporciona una conexión remota, también conocida como: sesión de conexión terminal virtual. La conexión Telnet se puede realizar desde un PC a los dispositivos o entre dispositivos. Los datos que se envian en Telnet no están encriptados, están en texto plano. Si la conexión es interceptada, los datos podrán ser visualizados. Se recomienda el uso de **SSH** en lugar de Telnet.

## Capa de Transporte

La capa de Transporte permite la segmentación de datos. Esta capa es el enlace entre las capas superiores (Aplicación) con las capas inferiores (Física) responsables de la transmisión de la red.

Las principales funciones son:

1. Seguimiento de conversaciones individuales.
2. Segmentación de datos, según los servicios (SMTP, VOIP, HTTP, STREAMING).
3. Reensamble de segmentos. Para entregarlo a las aplicaciones adecuadas.
4. Identificación de aplicaciones identificando el protocolo.
5. Es responsable de entregar el paquete de datos al dispositivo correcto.

### TCP & UDP

![alt text](image-22.png)

La capa de Transporte, realiza un seguimiento de los datos, con acuse de recibo y retransmisión de los datos que no hallan llegado a su destino. Para ello le asigna un número a cada segmento de datos.

En la capa de Transporte, pueden seleccionarse dos protocolos: **UDP** o el **TCP** orientado a la conexión.

| TCP | UDP |
| --- | --- |
| Confiable | Rápido, menor carga |
| Acuse de recibd de datos | Sin acuse de recibo |
| Reenvío de datos perdidos | No hay reenvío de datos perdidos |
| Entrega de datos en el orden en que fueron enviados | Entrega de datos a medida que se reciben |
| Usa segmentos | Usa datagramas |
| Más lento | Más rápido |

![alt text](image-23.png)

Ambos protocolos cuentan con un encabezado que distingue la aplicación.

![alt text](image-24.png)

En un Host su dirección IP y el puerto de comunicación (<65000) se denomina _socket_. Ejemplo: IP 192.168.1.5, Puerto 49152, entonces el Socket es 192.168.1.5:49152.

#### Tipos de puetos

| Rango de números de Puerto | Grupo de Puertos |
| --- | --- |
| de 0 a 1023 | Puertos bien conocidos (contacto) |
| de 1024 a 49151 | Puertos registrados para procesos |
| de 49152 a 65535 | Puertos privados y/o dinámicos |

![alt text](image-26.png)

![alt text](image-27.png)

![alt text](image-28.png)

### Funcionalidad de TCP

![alt text](image-29.png)

#### Partes del mensaje TCP

![alt text](image-30.png)

#### Otras Funciones de TCP

Reensamble de segmentos. Los diferentes segmentos pueden tomar diferentes rutas a través de los _Routers_.

Una vez que se reciben los _segmentos_ se ordenan y se pasan a la capa de _Aplicación_.

El **acuse de recibo TCP** se utiliza para confirmar la recepción de los datos.

![alt text](image-34.png)

El **control de flujo** es otra de las funcionalidades del TCP.

![alt text](image-36.png)

Otra funcionalidad es el **control de saturación de TCP**.

![alt text](image-37.png)

#### Servidor TCP

![alt text](image-31.png)

En la **retransmisión TCP** el Servidor web responde a una secuencia de paquetes de datos. El emisor reenviará la trama hasta que reciba la confirmación.

![alt text](image-35.png)

### Funcionalidad de UDP

**UDP** no establece ninguna conexión antes de enviar datos. **UDP** opera sin conexión.

La **PDU** de **UDP** se conoce como _datagrama_. **UDP** no puede reorganizar los datagramas. Lo que hace es ensamblar los datos según se reciben, antes de enviarlo a la capa de Aplicación.

![alt text](image-39.png)
![alt text](image-38.png)
![alt text](image-40.png)

## Capa de Red

![alt text](image-41.png)

- Protocolo de Internet versión 4 (IPv4)
- Protocolo de Internet versión 6 (IPv6)
- Intercambio Novell de paquetes de internetwork (IPX)
- AppleTalk
- Servicio de red sin conexión (CLNS/DECNet)

### Encabezado Paquete IPv4

La _capa de Transporte_ agrega un encabezado para organizar los _segmentos_ y puedan ordenarse en destino.

La _capa de Red_ agrega un encabezado para que los _paquetes_ puedan viajar por redes complejas y lleguen a su destino.

![alt text](image-43.png)

Encabezado de la IPv4 consta de las siguientes partes:

![alt text](image-44.png)

### Encabezado Paquete IPv6

![alt text](image-45.png)

### Separación de Redes

Es más práctico y manejable agrupar los hosts en subredes o redes específicas, según: el propósito, la ubicación geográfica o las propiedades.

Los problemas que se presentan en las redes, pueden ser:

- Degradación de rendimiento.
- Problemas de seguridad.
- Administración de direcciones.

### Conectividad fuera de la red local

Para conectar dos redes distintas, se utiliza el **Gateway** en los _Routers_.

![alt text](image-46.png)

## Direccionamiento IPv4

![alt text](image-42.png)

_Sin conexión_ significa que el emisor no sabe: si el receptor está presente, si llegó el paquete, si el receptor puede leer el paquete. Por otro lado, el receptor no sabe: cuando llegará el paquete.

_Mejor intento_. **IP** al ser un protocolo NO confiable de **Capa de Red**, **IP** no garantiza la recepción de todos los paquetes enviados, sin embargo otros protocolos administran el proceso de seguimiento de paquetes y garantiza su entrega.

_Medios independientes_. Unos dispositivos trabajan de forma independiente, es decir, no saben las configuraciones de los otros.

### Fundamentos de Enrutamiento

Con el comando _ipconfig_ en OS Windows, podemos ver:

| Descripción | Valores |
| --- | --- |
| IP Address | 192.168.0.70 |
| Subnet Mask | 255.255.255.0 |
| Default Gateway | 192.168.0.1 |

Ejemplo de tabla de enrutamiento en una red:

![alt text](image-47.png)

En una tabla de ruteo de un _Router_ se compruba: la dirección de destino, la dirección del siguiente salto y la métrica o dirección de red. Ejemplo de uso de la tabla de enrutamiento en un _Router_:

![alt text](image-48.png)

#### Protocolos de Enrutamiento

![alt text](image-49.png)

#### Enrutamiento Estático

Se introducen en cada _Router_ las rutas de forma manual.

![alt text](image-50.png)

#### Enrutamiento Dinámico

Requiere más capacidad de procesamiento y necesita de más ancho de banda, que el ruteo estático.

![alt text](image-51.png)

### Direcciones IPv4 y Prefijos de Red

El rango de IP está compuesto por: direcciones de red, direcciones de broadcast y direcciones de host.

La **dirección de red** es la más baja y termina en 0. Es la primera de las direcciones.
La **dirección de broadcast** es la más alta y termina en 255. Permite la comunicaicón a todos los Hosts de la red. Es la última de las direcciones.
Las **direcciones de hosts** son todas aquellas comprendidas entre la de red y la de broadcast.

#### Prefijos de Red

![alt text](image-52.png)

#### Otros Prefijos de Red

![alt text](image-53.png)

![alt text](image-54.png)

### Ejemplo encontrar las direcciones de: red, broadcast y hosts

1. Dirección de red.
2. Dirección de broadcast.
3. Cantidad de IPs de la red.
4. Cantidad de IPs utilizables por los Hosts.

Para la dirección _200.190.30.27/29_. Encontrar las direcciones. Usando el método matemático:

1.- Dirección de red calculada: 200.190.30.24

![alt text](image-55.png)

2.- Dirección de broadcast calculada: 200.190.30.31 Para ello se invierten los bits de la máscara de red.

![alt text](image-56.png)

3.- Para determinar la cantidad de IPs de la red, usamos la ecuación: $2^n$ donde n = 32 - prefijo_red (29) $n = 32 - 29 = 3$ => $2^3 = 8$. La cantidad de redes es de _8_.
4.- Para averiguar la cantidad de IPs utilizables se determinar restando 2 al valor anterior: 8 - 2 = _6_.
5.- Para averiguar las IPs de Host, son las comprendidas entre las direccones de red y de broadcast:

![alt text](image-57.png)

#### Ejemplo encontrar las direcciones de red, broadcast y host usando el método intuitivo

![alt text](image-58.png)

![alt text](image-59.png)

Para determinar la dirección de broadcast, se resta uno al valor máximo (en el ejemplo es 32)

![alt text](image-60.png)

### Unicast, Broadcast, Multicast

#### Unicast

Proceso por el cual se envía un paquete de un host a otro host de forma individual.

![alt text](image-61.png)

#### Broadcast

Es el proceso por el cual se envía un paquete de un host a todos los hosts de la red.

![alt text](image-62.png)

Existe el **broadcast dirigido** que se utiliza para enviar la información a todos los host de una red externa. En **broadcast típico** que conocemos.

#### Multicast

Es el proceso por el cual se envía un paquete de datos a un grupo seleccionado de hosts. De esta forma se reduce el consumo de ancho de banda.

![alt text](image-63.png)

### Rango de direcciones IPv4

Se clasifican en cinco clases desde la _A_ hasta la _E_:

![alt text](image-64.png)

![alt text](image-65.png)

Las direcciones privadas pueden duplicarse en diferentes redes. No pueden conocerse en Internet. Se requiere proceso NAT (proceso de traducción de las IP).

#### Direcciones IPv4 especiales

Clasificación de las direcciones de red y de broadcast:

- Ruta por defecto es la **0.0.0.0**.
- Loopback 127.0.0.1 (rango **127.0.0.0** - **127.255.255.255**). Sirven para realizar _ping_ sobre la propia tarjeta de red.
- Direcciones de enlace local (rango **169.254.0.0** - **169.254.255.255**). Cuando no se dispone de una configuración IP válida.

#### Planificación del direccionamiento

![alt text](image-66.png)

### Direccionamiento Estático, Dinámico e ISPs

Algunas ventajas del _direccionamiento estático_: es configurable manualmente, útil para servidores e impresoras.

En redes muy grandes se usa más el _direccionamiento dinámico_: no asigna de forma permanente las IP, necesita de un _pool_ de direcciones para asignar automáticamente.

El **ISP** suministra las IP como parte de los servicios a operadores. Las IP pueden ser prestadas o alquiladas a las compañías. Los **ISP** poseen los propios conjuntos de redes internas para datos.

La jerarquía de **ISP** es de tres niveles:

- _Nivel 1_: ofrecen servicios a empresas muy grandes e ISP a nivel 2, conexiones multiples y directas al _backbone_ brindan confiabilidad.
- _Nivel 2_: conexiones a Internet a través del ISP nivel 1, ofrecen servicios a empresas grandes e ISP nivel 3.
- _Nivel 3_: ofrecen servicios a pequeñas y medianas empresas, así como a hogares, conexiones a Internet a través de ISP nivel 2.

### Máscara de Subred

Indican la porción de red.

Ejemplo, para la IP 11.12.13.14/14, indica que el 14 es el número de unos del prefijo y el resto son ceros. El resultado sería: 255.252.0.0. Que es la máscara de subred.

### Generación de subredes

Para cada bit que se toma prestado, se generan subredes.

Ejemplo, para la IP 192.168.1.0/25 corresponde una máscara de subred 255.255.255.128. Las redes irán de 128 en 128 bits. Queda de la forma 192.168.1.128/25.

Ejemplo, crear tres subredes a partir de la IP 192.168.1.0/24:

![alt text](image-67.png)

### El patrón de las subredes

Ejemplo resuelto de la IP 172.16.0.0/16 creando las subredes: IP de red, máscara de subred, primera IP utilizable y broadcast.

![alt text](image-68.png)

En la siguiente tabla se muestran las posibles subredes a medida que se aumenta en un bit la máscara para la IP 10.0.0.0

![alt text](image-69.png)

### Método Saltos de Línea para crear Subredes

El método de saltos de red, también conocido como "subnetting" o "subredes", es una técnica fundamental en redes IPv4 que permite dividir una red en subredes más pequeñas y manejables. Esto ofrece varias ventajas, como una mejor organización, seguridad y eficiencia en el uso de direcciones IP.

¿Cómo funciona el método de saltos de red?

1. Comprensión de la máscara de red: La máscara de red es un conjunto de bits que define qué parte de una dirección IP corresponde a la red y cuál a los hosts (dispositivos conectados). Al modificar la máscara de red, podemos "tomar prestados" bits de la parte de hosts para crear subredes.

2. Cálculo de saltos: El "salto" es la diferencia entre las direcciones de red de dos subredes consecutivas. Se calcula restando la máscara de subred al valor máximo posible (256 en IPv4). Por ejemplo, si la máscara de subred es 255.255.255.192, el salto sería 256 - 192 = 64.

3. Creación de subredes: Las subredes se crean incrementando la dirección de red original en el valor del salto. Cada subred tiene su propio rango de direcciones IP utilizables para los hosts.

#### Ejemplo práctico

Supongamos que tenemos la dirección de red 192.168.1.0/24 (la máscara /24 indica que los primeros 24 bits identifican la red). Queremos crear 4 subredes.

- Cálculo de la nueva máscara: Necesitamos 2 bits adicionales para crear 4 subredes (2^2 = 4). La nueva máscara sería /26 (24 + 2 = 26).

- Cálculo del salto: El salto sería 256 - 192 (la parte de la máscara que cambió) = 64.

- Creación de las subredes:
  - Subred 1: 192.168.1.0/26 (rango: 192.168.1.1 a 192.168.1.62)
  - Subred 2: 192.168.1.64/26 (rango: 192.168.1.65 a 192.168.1.126)
  - Subred 3: 192.168.1.128/26 (rango: 192.168.1.129 a 192.168.1.190)
  - Subred 4: 192.168.1.192/26 (rango: 192.168.1.193 a 192.168.1.254)

#### Ventajas del método de saltos de red

- Mejor organización: Permite dividir una red grande en unidades más pequeñas y fáciles de administrar.
- Mayor seguridad: Facilita la creación de zonas de red aisladas para proteger información sensible.
- Uso eficiente de direcciones IP: Evita el desperdicio de direcciones IP al asignar solo las necesarias a cada subred.
- Mejor rendimiento: Reduce el tráfico de red al limitar el alcance de las transmisiones a cada subred.

#### Consideraciones importantes

- Es fundamental comprender el concepto de máscaras de red y cómo se utilizan para crear subredes.
- Se deben calcular cuidadosamente los saltos y las direcciones de red para evitar conflictos y errores.
- Existen herramientas y calculadoras en línea que pueden facilitar el proceso de subnetting.

### Comprender la notación CIDR

La notación /24 indica que los primeros 24 bits de la dirección IP (192.168.1) corresponden a la red, y los 8 bits restantes (el último octeto) se utilizan para identificar a los hosts dentro de esa red.

### Determinar la nueva máscara de subred

Para crear 4 subredes, necesitamos "tomar prestados" 2 bits más de la parte de hosts (ya que $2^2 = 4$). Esto significa que la nueva máscara de subred será /26 (24 + 2 = 26).

### Calcular el salto de red

El salto de red es la diferencia entre las direcciones de red de dos subredes consecutivas. Se calcula restando la parte variable de la nueva máscara (en este caso, los últimos 2 bits) al valor máximo posible (256).

En binario, la máscara /26 es 11111111.11111111.11111111.11000000. Los últimos 2 bits son 000000.
El valor máximo posible para los últimos 8 bits es 11111111 (255 en decimal).
El salto de red es 256 - 192 (el valor decimal de 11000000) = 64.

### Crear las subredes

Ahora, incrementamos la dirección de red original (192.168.1.0) en el valor del salto (64) para obtener la dirección de red de cada subred:

- Subred 1: 192.168.1.0/26
- Subred 2: 192.168.1.64/26
- Subred 3: 192.168.1.128/26
- Subred 4: 192.168.1.192/26

### Determinar el rango de direcciones IP utilizables en cada subred

Cada subred tiene un rango de direcciones IP que se pueden asignar a los hosts. La primera dirección en cada subred es la dirección de red, y la última dirección es la dirección de broadcast. Las direcciones intermedias se pueden asignar a los hosts.

- Subred 1: 192.168.1.1 a 192.168.1.62
- Subred 2: 192.168.1.65 a 192.168.1.126
- Subred 3: 192.168.1.129 a 192.168.1.190
- Subred 4: 192.168.1.193 a 192.168.1.254

![alt text](image-228.png)

### VLSM

Se pueden generar subredes a partir de una red mayor. Para determinar cuál es el tamaño adecuado de las subredes.

Como ejemplo: suponiendo la IP 192.168.0.0/24 podemos crear nuevas subredes que a su vez pueden volver a subdividirse en otras subredes.

![alt text](image-70.png)

### Pruebas Básicas de Capa de Red

El comando **Ping** es un protocolo de solicitudes de respuesta en la capa 3. El comando **Ping** envía respuesta desde una ubicación específica, usa el protocolo de mensajes de Internet llamado **ICMP**. Los paquetes **ICMP** son de ida y vuelte, entre el dispositvo que envía el **Ping** y el dispositivo que lo recibe.

Las funciones **ICMP** más usadas son:

- Confirmación de Host.
- Destino o servicio inalcanzable. Los códigos mostrados: 0 la red es inalcanzable, 1 el host es inalcanzable, 2 el protocolo es inalcanzable, 3 el puerto es inalcanzable.
- Tiempo excedido o superado.
- Redireccionamiento de ruta.
- Disminución de velocidad en origen.

El comando **Ping** también sirve para comprobar un host en una red remota.

El comando **Traceroute** se usa para comprobar la conectividad entre dos host. Permite observar la ruta entre los dispositivos. Proporciona una lista de saltos de red disponibles en la ruta. Al igual que con el comando _ping_ usa el protoclo **TTL** para limitar el tiempo de vida de la ejecución del comando.

- - -

### Método de Incremento de Subredes

El método de saltos de red, también conocido como **método de incremento de subredes**, es una técnica utilizada para crear subredes en una red IPv4. Este método se basa en calcular el tamaño de cada subred (el "salto") y determinar los rangos de direcciones IP asignables a cada una. A continuación, te explico paso a paso cómo funciona:

#### Concepto Clave

- **Dirección IP y máscara de subred**: Una dirección IPv4 tiene 32 bits y se divide en una parte de red y una parte de host. La máscara de subred define cuántos bits se usan para la red y cuántos para los hosts.
- **Subredes**: Dividir una red en subredes permite crear redes más pequeñas dentro de una red principal, optimizando el uso de direcciones IP.
- **Bloque de direcciones**: Cada subred tiene un rango específico de direcciones IP asignables.

#### Pasos para aplicar el método de saltos de red

##### _Paso 1_: Determinar el número de subredes necesarias

Decide cuántas subredes necesitas crear. Por ejemplo, si necesitas 5 subredes, debes calcular cuántos bits adicionales se requieren en la máscara de subred.

##### _Paso 2_: Calcular los bits de subred

Usa la fórmula: $2^n≥$ Número de subredes.

Donde _n_ es el número de bits adicionales necesarios para las subredes.

Por ejemplo, para 5 subredes: $2^3=8≥5$
Se necesitan 3 bits adicionales.

##### _Paso 3_: Calcular la nueva máscara de subred

Suma los bits adicionales a la máscara original.
Por ejemplo, si la máscara original es /24 (255.255.255.0) y añades 3 bits, la nueva máscara será /27 (255.255.255.224).

##### _Paso 4_: Calcular el tamaño del salto (tamaño de cada subred)

El tamaño del salto se calcula como: $Salto=2^{(32−NuevaMáscara)}$

Para una máscara /27: $Salto=2^{(32−27)}=2^5=32$

Cada subred tendrá 32 direcciones IP.

##### _Paso 5_: Determinar los rangos de direcciones de cada subred

Comienza con la dirección de red original y suma el tamaño del salto para obtener la siguiente subred. Por ejemplo, si la red original es 192.168.1.0/24 y el salto es 32:

- Subred 1: 192.168.1.0 - 192.168.1.31
- Subred 2: 192.168.1.32 - 192.168.1.63
- Subred 3: 192.168.1.64 - 192.168.1.95
- Y así sucesivamente.

##### _Paso 6_: Identificar direcciones asignables

En cada subred, la primera dirección es la dirección de red y la última es la dirección de broadcast. Las direcciones asignables son las intermedias.
Por ejemplo, en la subred 192.168.1.32/27:

- Dirección de red: 192.168.1.32
- Dirección de broadcast: 192.168.1.63
- Direcciones asignables: 192.168.1.33 - 192.168.1.62

### Ejemplo completo

![alt text](image-229.png){width=75% height=100px}

- - -

## Capa Enlace de Datos

Esta capa permite a las capas superiores, acceder a los medios, controlando los datos. Los términos usados en esta capa son: _PDU_ (trama), _Nodo_ o dispositivo, _Medio_, _Red_ (estructura).

Los protocolos de la _Capa 2_, especifican la encapsulaicón de un _paquete_ en una _trama_ y las técnicas para sacar y colocar los _paquetes_ encapsulados de cada medio. El _método de control de acceso al medio_, definen los procesos del acceso de los dispositivos acceden al medio de red.

![alt text](image-71.png)

### PDU de Capa 2

La _Trama_ **PDU** de Capa 2 incluye: los datos que vienen de la Capa 3, el encabezado de Capa 2 con información de control con direccionamiento y el trailer con información de control agregada al final de la _PDU_.

La estructura de la _Trama_ **PDU** contiene: inicio de trama, direccionamiento, tipo, control de calidad, detección de errores, fin de trama.

La _Capa 2_ se divide en dos subcapas: **LLC**, **MAC**.

![alt text](image-72.png)

### Control de Acceso al Medio

Regulan las entradas de las _Tramas_ a través de reglas; como son los turnos, sirven para evitar las colisiones. Si no existieran los turnos de envío, se producirán colisiones.

Un método, es el acceso controlado para medios compartidos. Cada dispositivo tiene un turno para enviar cuando está listo, en caso de que no necesita enviar datos, el turno pasa al siguiente. Hasta que la _trama_ enviado no llega al destino y es procesada, no se puede enviar otra _trama_ diferente. A este método se le conoce como _programado_ o _determinístico_.

Otro método es el _no determinista_ o _por contención_ para medios compartidos. Permite que cualquier dispositivo intente acceder al medio, siempre que haya datos. Para evitar las colisiones en la red, los dispositivos utilizan el _acceso múltiple con detección de portadora_ **CSMA** que sirve primero que nada, para dectar si existe una señal en el medio. En caso de que existe una portadora, el dispositivo espera hasta que esté libre el medio de comunicación, antes de volver a intentar enviar los datos.

- **CSMA/CD** para redes Ethernet.
- **CSMA/CAT** para redes inalámbricas 802.11

### Topologías Lógicas y físicas

La _topología lógica_ es la forma en que nuestra red transfiere tramas de un nodo al siguiente. Las conexiones son virtuales. Esta topología es la que influye en el tipo de _trama de red y control de acceso al medio_. Las _topologías lógicas_ y las _topologías físicas_ no siempre coinciden.

Tipos de topologías:

- Punto a punto.
- Bus. Requiere de control de acceso al medio.
- Anillo. Cuando un host recibe una trama, si no es para él, la reenvía al siguiente (Token-ring).

### Estructura de Trama

Cada Trama tiene tres partes básicas: _encabezado_, _datos_ y _trailer_.

El _encabezado_ contiene información específicca de la Trama:

![alt text](image-73.png)

Las _direcciones físicas_ no proporcionan información de en qué red se está. Estas _direcciones físicas_ son necesarias cuando existen múltiples destinos.

En enlaces _punto a punto_ o en el envío de paquete a otra red, no se requieren direcciones físicas.

El _trailer_ se utiliza para determinar si la Trama, llegó sin errores. Este proceso se denomina detección de errores. No es lo mismo que corrección de errores. La comprobación de redundancia cíclica o _FCS_ se añade en el _trailer_ para verificar, cuando en el destino llega la trama, calcula el _CRC_ y lo compara con el enviado _FCS_, si son iguales, se considera correcta ya que la Trama no se ha visto modificada.

### Protocolo Ethernet

Definen los estándares de Capa 2 y las tecnologías de Capa 1.

![alt text](image-74.png)

En el campo _Destino_ y en el _Origen_ están las **MAC** de destino y de origen de los dispositivos, de 6 bytes cada una de ellas.

En el campo _Tipo_ se especifica el valor que indica el protocolo de la capa superior recibirá los datos, es de 2 byes. El campo _Datos_ es la _PDU_ de la Capa 3.

### Protocolo PPP

Es un protocolo pensado para entregar Tramas entre dos nodos. Inicialmente se desarrolló como un protocolo WAN. Utiliza una arquitectura en capas, para establecer las comunicaciones lógicas o sesiones entre dos nodos.

![alt text](image-75.png)

### Protocolo WLAN

Se utilizan controles adicionales, ya que no existe una conexión física, pudiendo ser interferida. Se rige por el estándar 802.11 para comunicación inalámbrica.

![alt text](image-76.png)

## Capa Física

El objetivo de la _Capa Física_ es crear las señales, tanto: ópticas como eléctricas o inalámbricas. Que representa a los bit de la _trama_. Las cuatro propiedades a tener en cuenta:

- Propiedades físicas y eléctricas de los medios.
- Propiedades mecánicas de los conectores.
- Representación de los bits por medio de las señales.
- Definición de las señales de la información de control.

### Funciones de la Capa Física

Las tres funciones esenciales son: _codificación de datos_, la _señalización_ y los _componentes físicos_.

![alt text](image-77.png)

- **Codificación**: sirve para convertir los datos en un código predefinido con patrón predecible.
- **Señalización**: representan los 1 y los 0. Esta capa especifíca que valor de señal se corresponde con el 1 lógico y que valor de la señal es el 0 lógico.
- **Medios**: son los dispositivos electrónicos tales como los conectores y/o los medios.

#### Señalización

Las modulaciones usadas en la _señalización_ para el envío de la trama de datos es en _amplitud_ en _frecuencia_ o en _fase_.

![alt text](image-78.png)

##### Señalizaicón NRZ

Es un método en el que los bits se transmiten con una secuencia de valores de voltaje. La _señalización NRZ_ no utiliza el ancho de banda de manera eficiente y es susceptible a las interferencias electromagnéticas.

![alt text](image-79.png)

##### Señalización Manchester

Los bits se representan como transiciones de voltaje (saltos de 0V a 5V y viceversa).

![alt text](image-80.png)

#### Codificación: agrupación de bit

Al inicio y al final de una _trama_ se añaden grupos de bits con un valor específico que indican el estado.

![alt text](image-81.png)

La ventaja de la codificación, es la reducción del nivel de error en los bits. Limita la energía efectiva transmitida a los medios. Ayuda a distinguir los bits de datos de los bits de control.

Se puede usar la técnica _Código 4B_ de cuatro bits o el código denominado _Símbolo 5B_ formado por cinco bits.

### Transferencia de datos

Se puede medir de tres formas. El ancho de banda representa la capacidad que tiene un medio en transmitir datos en un tiempo determinado. Otra forma, es con el rendimiento al medir la transferencia de bits durante un periodo de tiempo. La tercera forma es, la capacidad de transferencia útil de datos en un periodo de tiempo determinado. A diferencia del rendimiento que mide la transferencia de bit no de datos, la capacidad de transferencia útil si que mide la cantidad de datos.

![alt text](image-82.png)

### Medios físicos de cobre - estructurados

Cable **UTP** no blindado:

![alt text](image-83.png)

Cable **STP** blindado:

![alt text](image-85.png)

#### Montaje cable estructurado en conector RJ-45

![alt text](image-84.png)

### Selección de cable directo o cruzado en dispositivos

![alt text](image-25.png)

### Cable de Fibra Óptica

![alt text](image-86.png)

Las _fibras ópticas_ **monomodo** permiten la transferencia de un único rayo de luz (longitud de onda). Diámetro del núcleo reducido, con pocas pérdidas y baja atenuación. Uso para largas distancias.

![alt text](image-87.png)

Las _fibras ópticas_ **multimodo** permite la transferencia de varias longitudes de onda, por lo que se puede usar varios colores de haz de luz.

![alt text](image-88.png)

### Medios Inalámbricos

Estándares en las comunicaciones inalámbricas, usadas en las redes de datos son: _WiFi_, _Bluetooth_, _WiMAX_.

![alt text](image-89.png)

### Conectores cableado en redes

Para cables estructurados:

![alt text](image-90.png)

En los cables de fibra óptica, los conectores más comunes: punta recta _ST_, conector suscriptor _SC_, _LC_

![alt text](image-91.png)

## Tecnología Ethernet

Las funciones de **Ethernet** es separa la _capa de enlace de datos_ en dos subcapas: **LLC**, **MAC**. Favoreciendo la compatibilidad entre dispositivos.

- **LLC**. Establece la conexión. Entrama el paquete. Identifica el protocolo.
- **MAC**. Delimitar. Direccionar. Detectar errores. Las funciones serían: control y recuperación.

### Trama Ethernet

- _Preambulo_.
- _Delimitador de inicio de trama_. Sirve para sincronización entre dispositivos.
- _Dirección de destino_. La dirección MAC del destino.
- _Dirección de origen_. Identifica la NIC de la trama.
- _Longitud/Tipo_. Tamaño campo de datos.
- _Encabezado y datos_. Contiene los datos de una capa superior.
- _Secuencia de verificación de trama_.Detecta errores. El calculado por el receptor debe coincidir con el enviado.

### MAC Address

El formato de la _dirección MAC_ asignado por cada Proveedor a los elementos de la red, siendo únicada para cada uno de ellos, tiene el formato establecido por el IEEE siguiente:

![alt text](image-92.png)

Las direcciones **MAC** utilizan un número en _hexadecimal_.

### Direccionamiento Ethernet

Las direcciones **IP** utilizadas para la comunicación entre redes. Las direcciones **MAC** utilizadas dentro de las redes entre los medios locales.

- Las _direcciones de capa de red_, permiten el envío de paquetes a su destino.
- Las _direcciones de enlace de datos_, permite el transporte del paquete utilizando los medios locales a través de cada segmento.

#### Unicast Ethernet

Envío desde un dispositivo único, hacia otro dispositivo único de la red.

![alt text](image-93.png)

#### Broadcast Ethernet

Se envía una trama a varios dispositivos de la red, usando una dirección IP de _broadcast_ junto con una dirección MAC de _broadcast_, formada esta última por 48 bits puestos a uno (representado por doce F).

![alt text](image-94.png)

#### Multicast Ethernet

Para enviar una trama de un dispositivo a un grupo dentro de la red. Las MAC Destino comienzan por _01-00-5E_

![alt text](image-95.png)

### Proceso CSMA/CD

El control de acceso al medio _CSMA/CD (acceso múltiple con detección de portadora con detección de colisiones)_ sirve para detectar y manejar colisiones. Determinando cuándo un dispositivo puede transmitir su trama de datos.

En la red, según se establece en el modelo _CSMA/CD_ los dispositivos lo primero que hacen es escuchar el medio antes de transmitir para comprobar si existe señal portadora.

![alt text](image-96.png)

### Velocidades Ethernet

![alt text](image-97.png)

### Switch

Cada una de las interfaces del _Switch_ envían selectivamente tramas individuales. Los _Switch_ almacenan y envían las tramas a los destinatarios específicos. El reenvío es selectivo.

![alt text](image-98.png)

#### Protocolo ARP

Los _Switch_ tienen una tabla de enrutamiento o tablas **ARP** con datos de la MAC y la IP de los usuarios. Estas tablas las utiliza para determinar quién es el destinatario y para rechazar tramas en caso que no estén en la lista.

El protocolo **ARP** realiza dos funciones: resolución de direcciones IPv4 y mantenimiento de una caché. El _Switch_ consulta esta tabla para encontrar los destinatarios de las tramas que le llegan.

En los dispositivos existe la posibilidad de borrar las direcciones en la tabla **ARP** que no se han utilizado en un tiempo. También se pueden usar comandos para borrar la caché **ARP**.

## Consideraciones para Conexiones

El _router_ si utiliza para interconectar redes diferentes, por ejemplo: una red LAN y una red WAN. También se pueden usar para interconectar redes que usen diferentes tecnologías.

El _switch_ se utiliza como elemento central en una topología en estrella. El _hub_ es similar y puede utilizarse también como elemento central. La diferencia está en que el _hub_ reenvía la información a todas las interfaces, mientras que el _switch_ únicamente la envía al destinatorio.

El coste económico de un _switch_ está en función de los puertos disponibles y de la velocidad de conmutación.

### Factores del cableado

Se tiene que tener en cuenta la longitud total del cable, se limita a **100 metros** por canal. Y se pueden usar hasta 5 m de latiguillo entre toma de pared y dispositivo.

![alt text](image-99.png)

En cuanto a la elección del tipo de cableado, se tiene en cuenta: el coste económico, el ancho de banda, la facilidad de instalación, interferencias electromagnéticas.

#### Conexión Directa y Cruzada

![alt text](image-100.png)

- Pines 1 y 2: se usan como transmisores.
- Pines 3 y 6: se usan como receptores.
- Cable _conexión directa_ entre: switch y router o switch a host.
- Cable _conexión cruzada_ entre: router y router o switch a switch.
- Dispositivos que dispongan de MDIX pueden autoconfigurarse de forma automática para el tipo de conexión directa o cruzada del cable.

### Conexiones WAN

Se pueden realizar conexiones muy extensas. Estas conexiones pueden ser sobre líneas telefónicas con conector RJ-11 en conexión DSL. Existen otras tipos de conexiones admitidas, tal como muestra la tabla siguiente:

![alt text](image-101.png)

- **DCE** es el equipo de comunicación de datos y se encuentra en el extremo WAN del proveedor de servicios.
- **DTE** es el equipo terminal de datos y se encuentra en el extremo WAN del usuario.

## IOS de CISCO

Para acceder al terminal CLI de los switch, es a través del purerto de consola que puede ser RJ-45. Otro acceso es por módem a través del puerto auxiliar. Los accesos por Telnet a través de las interfaces serial es otro de las opciones disponibles.

![alt text](image-102.png)

Modos de acceder a la configuración de un Switch permite configurar algunos de los parámetros, a continuación se muestran algunos de los comandos que se pueden utilizar dependiendo del modo de acceso:

```cisco
router>           (modo de configuración inicial)
router#           (modo de configuración básicp)
router(config)#   (modo de configuración avanzado)
```

![alt text](image-103.png)

### Comandos del IOS

El IOS tiene una sintaxis específico.

![alt text](image-104.png)

El IOS ofrece varias formas de ayuda. Una de las formas es escribir el signo de interrogación junto a la palabra:

![alt text](image-105.png)

Otra de las ayudas es con verificación de sintaxis de comandos: ambigüo, incompleto, incorrecto.

![alt text](image-106.png)

Los accesos rápidos del teclado:

- _Tab_: completa el resto del comando.
- _Ctrl+R_: muestra línea de comandos.
- _Ctrl+Z_: sale del modo de configuración.
- _Flecha abajo_: desplazarnos en los comandos anteriores.
- _Flecha arriba_:
- _Ctrl+Shift+6_: interrumpe un proceso.
- _Ctrl+C_: cancela el comando y sale del modo configuración.

### Comandos de análisis

![alt text](image-107.png)

### Comandos para añadir contraseñas y mensajes

![alt text](image-108.png)

El comando _enable_ limita el acceso al modo priviligiado:

![alt text](image-109.png)

### Administración de archivos de configuración

```cisco
Router# show running-configuration
router# copy running-configuration startup-configuration
```

### Configuración de interfaces

![alt text](image-110.png)

### Líneas de base & ARP

En el monitoreo y documentación de red, se puede establecer una línea de base de red, como un proceso para estudiar la red en intervalos regulares con el fin de determinar que la red funciona correctamente.

![alt text](image-111.png)

El comando _arp_ proporciona identificación a través de la **MAC** y la dirección IP conocidas.

![alt text](image-112.png)

En los _switch_ el comando útil para mostrar la tabla de información _arp_ es `show mac-address-table`.

### Prácticas. Configuraciones en Router

![alt text](image-113.png)

Configuración del Router para añadir contraseñas, texto de inicio, asignar IP/Mask y guardar las configuraciones en memoria flash.

```cisco
Rouer> enable
Rouer# configure terminal
Router(config)# 
Router(config)# hostname ROUTER_1
ROUTER_1(config)# 
ROUTER_1(config)# line console 0
ROUTER_1(config-line)#
ROUTER_1(config-line)# password cisco
ROUTER_1(config-line)# login
ROUTER_1(config-line)# exit
ROUTER_1(config)#
ROUTER_1(config)# line vty 0 4
ROUTER_1(config-line)#
ROUTER_1(config-line)# password cisco
ROUTER_1(config-line)# login
ROUTER_1(config-line)# exit
ROUTER_1(config)#
ROUTER_1(config)# enable password cisco
ROUTER_1(config)#
ROUTER_1(config)# interface fastEthernet 0/0
ROUTER_1(config-if)# description HACIA_ROUTER_2
ROUTER_1(config-if)# ip address 10.10.10.1 255.0.0.0
ROUTER_1(config-if)# no shutdown
ROUTER_1(config-if)# exit
ROUTER_1(config)#
ROUTER_1(config)# banner motd "MENSAJE DE INICIO - ACCESO RESTRINGIDO"
ROUTER_1(config)#
ROUTER_1(config)# exit
ROUTER_1# 
ROUTER_1# show running-config
ROUTER_1#
ROUTER_1# copy running-config startup-config
ROUTER_1# write (realiza lo mismo que el comando anterior)
ROUTER_1#
```

Configuración de un segundo Router conectado al anterior Router por medio de un cable cruzado.

```cisco
Rouer> enable
Rouer# configure terminal
Router(config)# 
Router(config)# hostname ROUTER_2
ROUTER_2(config)# 
ROUTER_2(config)# line console 0
ROUTER_2(config-line)#
ROUTER_2(config-line)# password cisco
ROUTER_2(config-line)# login
ROUTER_2(config-line)# exit
ROUTER_2(config-line)# line vty
ROUTER_2(config-line)# password cisco
ROUTER_2(config-line)# login
ROUTER_2(config-line)# exit
ROUTER_2(config)#
ROUTER_2(config)# enable password cisco
ROUTER_2(config)#
ROUTER_2(config)# interface fastEthernet 0/1
ROUTER_2(config-if)# description HACIA_ROUTER_2
ROUTER_2(config-if)# ip address 10.10.10.2 255.0.0.0
ROUTER_2(config-if)# no shutdown
ROUTER_2(config-if)# exit
ROUTER_2(config)# exit
ROUTER_2# write
```

Acceso al Router 1 desde el Router 2 mediante Telnet:

```cisco
Rouer> enable
ROUTER_2#
ROUTER_2# telnet 10.10.10.1 (conexión hacia el Router_1)
Password: (contraseña del Router_1)
ROUTER_1>
```

Configuración del Switc que está conectado al Router 2 anterior.

```cisco
Switch>
Switch> enable
Switch#
Switch# configure terminal
Switch(config)#
Switch(config)# hostname SWITCH
SWITCH(config)#
SWITCH(config)# line console 0
SWITCH(config-line)# password cisco
SWITCH(config-line)# login
SWITCH(config-line)# exit
SWITCH(config)#
SWITCH(config)# line vty 0 15
SWITCH(config-line)# password cisco
SWITCH(config-line)# login
SWITCH(config-line)# exit
SWITCH(config)#
SWITCH(config)# enable password cisco
SWITCH(config)# service password-encryption

//configuración de interfaces virtuales

SWITCH(config)# interface vlan 1
SWITCH(config-if)# ip address 172.16.0.1 255.255.0.0
SWITCH(config)# exit
SWITCH# copy running-config startup-config
```

## Direccionamiento IPv6

### Formato en IPv6

La **IPv4** puede direccionar a $2^{32}$ hosts (4E9 posibles direcciones), en cambio **IPv6** puede direccionar $2^{128}$ hosts (340E36 posibles direcciones) esto supone 5,6E28 direcciones IP por cada ser humano.

El formato de IPv6 es de ocho hextetos (16 bits) usando un formato hexadecimal, separados por dos puntos. Ejemplo:

![alt text](image-114.png)

La primera parte de la dirección es para _prefijos de Red_ y los cuatro últimos hextetos (16 bits x 4 bits) es para _identificador de Interfaz_.

![alt text](image-115.png)

#### Simplificación de las direcciones IPv6

- Los 0 de la izquierda pueden omitirse.
- Los grupos de 0 consecutivos, pueden escribirse como :: (solo se puede hacer una vez)

Ejemplos:

&sect; FF01:0000:0000:0000:0000:0000:0000:0101 => FF01::101

&sect; 0000:0000:0000:0000:0000:0000:0000:0001 => ::1

&sect; 0000:0000:0000:0000:0000:0000:0000:0000 => ::

#### Direcciones UNICAST - UNICAST GLOBAL (2000::/3)

Los paquetes de datos son entregados a una única interfaz.

![alt text](image-116.png)

Las direcciones _unicast globales_ se pueden enrutar públicamente. Básicamente son las direcciones públicas de las IPv6. Comienza por **2000:**.

![alt text](image-119.png)

Estas direcciones:

- 13% del total de direcciones posibles.
- Globalmente ruteables, como las direcciones públicas IPv4.

![alt text](image-120.png)

#### Direcciones MULTICAST (FF00::/8)

Los paquetes se entregan a todas las interfaces con la direción de multicast. Estas dirección siempre empiezan por **FF**:

![alt text](image-117.png)

#### Direcciones ANYCAST

Una dirección _anycast_ identifica múltiples interfaaces en varios dispositivos. Aunque el paquete solo es entregado a un único dispositivo, que coincide con el más cercano, en términos de enrutamiento.

![alt text](image-118.png)

Por lo general estas direcciones se configuran en _routers_ y no en _hosts_.

#### Direcciones LINK - LOCAL (FE80::/10)

Las direcciones **LINK-LOCAL** son como las direcciones locales y privadas que se requieran. Se configuran automáticamente.

![alt text](image-122.png)

Se utilizan en redes LAN pequeñas.

![alt text](image-121.png)

#### Direcciones LOCALES (FC00::/7)

Estan destinadas a usar en redes LAN, sin ruteo a través de Internet.

![alt text](image-123.png)

- Prefijo global único.
- No se espera ser ruteado en Internet.

![alt text](image-124.png)

#### Direcciones ESPECIALES

- **0:0:0:0:0:0:0:0** Es la dirección fuente de un host antes de recibir una dirección IP.
- **0:0:0:0:0:0:0:1** Es el equivalente de la dirección **loopback** de la tarjeta NIC en IPv4.
- **0:0:0:0:0:0:x.x.x.x** (0:0:0:0:0:0:0:192.168.100.1) Combinación de una dirección IPv6 con una IPv4. Escrita en formato mixto de red.
- **2000::/3** Las direcciones que comienzan por el _2000_ pertenecen al rango de las direcciones _globales unicast_.
- **FC00::/7** Estas direcciones pertenecen al rango _local unicast_.
- **FE80::/10** Pertenecen al rango _link local_.
- **FF00::/8** Pertenecen al rango _multicast_.
- **3FFF:FFFF::/32**
- **2001:0DB8::/32** Reservadas para ejemplos de documentación.
- **2002::/16** Se utilizan con túneles. Sistemas de transición de IPv4 a IPv6.

### Subnetting y Mecanismos de Transición

#### Subnettig IPv6

Para la dirección IPv6 siguiente: `2003:DB9:3000:: /37`. Escogemos los primeros tres sextetos, a saber: `2000`, `DB9`, `3000`. El `/37` indica que faltan 5 bits para completar los 32 bits que forman los dos primeros sextetos. Estos 5 bits se tomarán del tercer sexteto `3000`.

![alt text](image-125.png)

El valor máximo de la subred sería:

![alt text](image-126.png)

#### Mecanismos de Transición IPv6

- Dual Stack
- Túneles
  - Manuales, automáticos
  - Tunel Broker
  - 6 to 4
- NAT

## Configuraciones básicas

### Routers

Los _routers_ encaminan los paquetes hacia su destino hacia otras redes. Usando la tabla de enrutamientos con datos de las interfaces.

![alt text](image-127.png)

#### Proceso de arranque del Router

![alt text](image-128.png)

#### Interfaces de un Router

![alt text](image-129.png)

#### Routers y Capa de Red

El proceso de enrutamiento, envío de la IP de destino, es la Capa 3. Cuando un _router_ recibe un paquete, realiza una búsqueda en la tabla de enrutamiento para encontrar la coincidencia de la dirección IP de destino. Si existe la coincidencia, se empaqueta y se envía. Los _routers_ operan en las Capas 1, 2 y 3 del modelo OSI.

![alt text](image-130.png)

#### Configuraciones Básicas

![alt text](image-131.png)

![alt text](image-132.png)

#### Tabla de enrutamiento

Esta tabla contiene información de los dispositivos conectados a la red. Y se almacena en la memoria RAM del _Router_.

![alt text](image-133.png)

Cada _router_ dispone de su propia tabla de enrutamiento. Esto hace que las tramas de datos enviados puedan ir por una vía diferente al de recepcción, es lo que se conoce como enrutamiento asimétrico:

![alt text](image-135.png)

#### Enrutamiento Estático y Dinámico

El enrutamiento estático incluye la dirección de red la máscara de subred remota, además de la IP de _router_. El descubrimiento de redes, es la capacidad de compartir información con otros _routers_ que utilicen el mismo protocolo de enrutamiento. Los enrutamiento dinámicos, permiten a los _routers_ aprender automáticamente las mejores rutas para el envío de las tramas.

![alt text](image-134.png)

#### Mejor ruta

La mejor ruta, es la que tiene mejor métrica, es decir, menos saltos de __Routers_:

![alt text](image-136.png)

Otra métrica, es el ancho de banda (indica velocidad). Es una elección válida, en lugar de los saltos elegir la de mejor velocidad.

La función de conmutación:

![alt text](image-137.png)

El router revisa la tabla de enrutamientos, para comprobar si existen las direcciones IP de destino y la de origen. Se usa el **TTL** para asignarle un tiempo de vida a las tramas de datos.

### Prácticas configuración Router

```cisco
Router>enable
Router#configure terminal
Router(config)#hostname OFICINA
OFICINA(config)#line console 0
OFICINA(config-line)#password cisco
OFICINA(config-line)#login
```

```cisco
Router>enable
Router#show running-config
```

```cisco
Router(config)#banner motd "*********** 
******** mensaje de bienvenida ********
***************************************"
```

```cisco
Router(config)# interface fastEthernetwork 0/0
Router(config-if)# description HACIA_LAN_1
Router(config-if)# ip address 192.168.1.1 255.255.255.0
router(config-if)# no shutdown
```

```cisco
//Desde un Host en consola de terminal, realizamos conexión telnet a la IP del Router:
C:\> telnet 10.10.10.1 ...Open
User Access Verification

Password:
ROUTER> enable
ROUTER# configure terminal
ROUTER(config)# no enable password //sirve para eliminar la contraseña
ROUTER# exit
```

```cisco
//desde un Host en consola de terminal, realizamos tracert
C:\> ping 20.20.20.20
C:\> tracert 20.20.20.20
```

#### Práctica de conectividad entre Routers

Topología de la red de datos:

![alt text](image-138.png)

Resultados de ejecutar el comando _tracert_. La trama sigue una ruta a través de los _routers_:

![alt text](image-140.png)

Los paquetes de datos, siempre se van a ir por la que menor métrica tengan:

![alt text](image-141.png)

## Ruteo Estático

Comandos básicos de acceso y configuración de una red con tres routers conectados por el lado de la WAN,  en serie a través de cable serial conexión DCE en RS232 (Seeris/0/0/0). A su vez, cada router tiene conectado un PC con cable cruzado conectado a Ethernet:

```cisco
> enable
R1# configure terminal
R1(config)# interface fastEthernet 0/0
R1(config-if)# description LAN
R1(config-if)# IP Address 172.16.3.1 255.255.255.0
R1(config-if)# no shutdown
```

Configuración de las interfaces de los routers por el lado la WAN:

```cisco
> enable
R1# configure terminal
R1(config)# interface serial 0/0/0
R1(config-if)# description WAN
R1(config-if)# IP Address 172.16.2.1 255.255.255.0
R1(config-if)# clock rate 9600
R1(config-if)# no shutdown
```

Con el comando `R1# show ip route`ejecutado en un router, muestra la tabla de ruteos que tiene configuradas.

| Router | IP red LAN | IP red WAN |
| --- | --- | --- |
| R1 | 172.16.3.0/24 | 172.16.2.0/24 |
| R2 | 172.16.1.0/24 | 172.16.2.0/24 192.168.1.0/24 |
| R3 | 192.168.2.0/24 | 192.168.1.0/24 |

El comando `R1# debug ip routing` nos sirve para determinar qué está ocurriendo en la ejecución del _Router_. Para desactivar usamos el comando `R1# undebug all`.

### Rutas estáticas en el ejemplo

![alt text](image-142.png)

En el ejemplo existen un total de 5 redes. _R1_ conoce dos redes. _R2_ conoce tres redes. R3 conoce dos redes.

```cisco
R1> enable
R1# config terminal
R1(config)# ip route 172.16.1.0 255.255.255.0 172.16.2.2  //LAN 1
R1# config terminal
R1(config)# ip route 192.168.2.0 255.255.255.0 172.16.2.2 //LAN 2

R1# show ip route //muestra información del routeo

R1# show ip route 172.16.1.10 //muestra información de la conexión

R1# show cdp neighbors detail //muestra información de la red y otros
```

El _R2_ conoce tres redes que están conectadas a sus interfaces, para la configuración del resto de redes hacia las LAN de R1 y de R2 sería:

```cisco
R2> enable
R2# config terminal
R2(config)# ip route 172.16.3.10 255.255.255.255.0 172.16.2.1
R2(config)# ip route 192.168.2.0 255.255.255.255.0 serial 0/0/1

R2# show ip route //muestra las cinco rutas configuradas
```

El _R3_ solo conoce dos rutas. Se le tienen que indicar las demás redes existentes.

```cisco
R3> enable
R3# config terminal
R3(config)# ip route 172.16.1.0 255.255.255.0 serial 0/0/1
R3(config)# ip route 172.16.2.0 255.255.255.0 serial 0/0/2
```

Una configuración de ruta estática con dirección IP 0.0.0.0 envía todo el tráfico por la interfaz asignada:

```cisco
R3> enable
R3# config terminal
R3(config)# no ip route 172.16.1.0 255.255.255.0
R3(config)# no ip route 172.16.2.0 255.255.255.0
R3(config)# no ip route 172.16.3.0 255.255.255.0
R3(config)# ip route 0.0.0.0 0.0.0.0 serial 0/0/0 //configuración de ruta estática por defecto
```

### Protocolo CDP

Herramienta para control y solución de problemas en las redes. Recopila información de los dispositivos de la red. Es un protocolo creado por Cisco. El acrónimo **CDP** significa _Cisco Discovery Protocol_ e identifica dispositivos y varias propiedades de la red. Ejemplo de uso en un _Router_:

```cisco
> enable
R1# show cdp neighbors
R1# show cdp neighbors detail

//Para desactivar esta opción, por motivos de seguridad

R1# configure terminal
R1(config)# no cdp run
```

#### Práctica I - Ruteo estático en IPv6

[Rut-Est-IPv6](practicas/prRutEstIPv6.md)

#### Práctica II - Ruteo estático en IPv6

[Rut-Est-IPv6](practicas/prRutEstIPv6_II.md)

#### Práctica III - Ruteo estático en IPv6

[Rut-Est-IPv6](practicas/prRutEstIPvIP6_III.md)

- - -

## Introducción al ruteo dinámico

Los protocolos determinan laa mejor ruta en la red. El beneficio de los protocolos dinámicos, es que se intercambia información cuando se cambia la topología, permitiendo a los Routers aprender automáticamente.

![alt text](image.png)

### Función y Propósito de los Protocolos Dinámicos

Todo _protocolo de enrutamiento_ tiene el mismo propósito, y es el de conocer las nuevas redes remotas cuando se produce un cambio en la topología.

- **Tabla de ruteo**. Selección de la mejor ruta al destino.
- **Actualización**. Capacidad en encontrar una mejor ruta si al actual deja de funcionar.

Tabla comparativa entre _ruteo dinámico_ y _ruteo estático_:

![alt text](image-1.png)

### Tipos de Protocolos Dinámicos

- El **Gateway Interior** se usa para el enrutamiento dentro de un sistema autónomo. Se utilizan los protocolos_ **RIP**, **IGRP**.
- El **Gateway Exterior** se usa para sistemas interautónomos, enrutamiento entre sistemas autónomos. Se utilizan el protocolo **EGP**.

![alt text](image-2.png)
![alt text](image-139.png)

#### Protocolo Vector Distancia y Estado de Enlace

Los protocolos de enrutamiento se clasifican en: _vector distancia_ y _estado de enlace_. Los protocolos **vector distancia** usan: **RIP** e **IGRP**.

![alt text](image-143.png)

Los protocolos **Estado de Enlace** envía actualizaciones solo cuando se producen cambios en la topología. función mejor cuando el diseño de la red es jerarquico o en redes extensas.

### VLSM en Protocolos Dinámicos y Convergencia

- Protocolo de enrutamiento _con clase_: **RIP**, **IGRP**. No incluyen la máscara de subred.
- Protocolo de enrutamiento _sin clase_: **RIPv2**, **EIGRP**, **OSPFv2**, **IS-IS**. Incluyen máscara de subred.
- Protocolo de enrutamiento en IPv6: **RIPng**, **EIGRP IPv6**, **OSPFv3**, **IS-IS IPv6**. No hace uso de las clases.

![alt text](image-144.png)

#### Convergencia

La _convergencia_ ocurre cuando todas las tablas de enruramiento de los routers se encuentran en un estado de uniformidad. Una red _converge_ cuando todos los routers tienen la información de las redes interconectadas.

El _tiempo de convergencia_ es el tiempo que tardan los routers en compartir la información, calcular las mejores rutas.

Los protocolos se pueden clasificar en base al _tiempo de convergecia_; los más rápidos son EIGRP, OSPF. Siendo los más lentos RIP, IGRP.

### Métrica y Balance de Carga

Un protocolo "aprende" más de una ruta hacia el mismo destino. La **_métrica_** es un valor usado por los protocolos para diferenciar las rutas disponibles, asignándole _saltos_. De tal manera, que se determina qué ruta es mejor utilizar. Cada protocolo usa su propia métrica.

![alt text](image-145.png)

La elección de una ruta se base en las diferentes **métricas** en función de los siguientes parámetros:

- Conteo de saltos.
- Ancho de banda.
- Carga.
- Retardo.
- Confiabilidad.
- Costo.

### Distancia Administrativa

Define la preferencia de un origen de enrutamiento. A cada origen de enrutamiento se le asigna un orden de preferencia, usan el valor de _distancia administrativa_ **AD**.

La **AD** es un valor entre: 0 y 255. Mientras más pequeño sea el valor, mayor es la preferencia de la ruta.

![alt text](image-146.png)
![alt text](image-147.png)

Tabla de distancias administrativas predefinidas:

![alt text](image-148.png)

Las prioridades de la tabla de ruteo a la hora de elegir una ruta:

```mermaid
flowchart LR
    Métrica --> Costo
    Costo --> Coincidencia
```

La _Concidencia_ es el mayor número de bits en la máscara de subred.

- - -

## Protocolos dinámicos de Vector Distancia

Los _protocolos_ de enrutamiento _Vector Distancia_ incluyen **RIP**, **IGRP**, **EIGRP**.

- El protocolo **RIP** utiliza: conteo de saltos, límite máximo de 15, broadcast cada 30 s.
- El protocolo **IGRP** utiliza: BW, retardo, carga, confiabilidad para crear una métrica compuesta. Broadcast cada 90 segundos. Es el antecesor de EIGRP.
- El protocolo **EIGRP** utiliza: realiza balanceo de carga con diferente costo. Usa el algoritmo por difusión DUAL para calcular la ruta más corta. Actualiza cuando hay cambios en la topología.

### Vector Distancia

Las rutas son publicadas como vectores de distancia y dirección. Las distancias se definen en términos de una métrica, como el conteo de saltos.

![alt text](image-149.png)

Los protocolos _Vector Distancia_ comparten las siguientes características: las actualizaciones periódicas se envian en intervalos.

### Características Vector Distancia

| Ventajas | Desventajas |
| --- | --- |
| Implementación y mantenimineto simples | Convergencia lenta |
| Pocos requisitos de recursos | Escalabilidad limitada |
| | Routing loops |

Tabla con las características con los protocolos más comunes:

![alt text](image-150.png)

### Actualización de Enrutamiento y Convergencia

En el momento del arranque, las tablas de datos de los _Routers_ están vacías. Tras la carga de la configuración, verifica las direcciones de las redes directamente conectadas. Añade rutas estáticas y las de los vecinos más cercanos.

La cantidad de tiempo necesario, es directamente proporcional al tamaño de la red.

### Temporizadores y Envío de Actualizaciones

En las actualizaciones periódicas, se envía la tabla de direcciones de enrutamiento. Las actualizaciones se envían por medio de __broadcast_, es decir, todas sus interfaces activas.

En **RIP** existen tres temporizadores extras.

![alt text](image-151.png)

En **EIGRP** no se envían actualizaciones periódicas, más bien, se envía la tabla de direcciones cuando se cambia una ruta. Enviando la información solo aquellos _Routers_ que la necesiten.

### Actualizaciones periódicas

La actualización de la tabla de ruteo, se produce cuando hay un cambio en la topología de la red. Los problemas que puede tener los paquetes de actualizaciones es que: corrampan o que se pierdan en algún nodo de la red.

#### Fluctuación de fase aleatoria

Cuando varios _Router_ envían al mismo tiempo la tabla de actualizaciones, en un mismo segmento LAN, produce conflicto y pérdida de datos por _colisiones_. Para evitar este inconveniente se utiliza una variable aleatoria que resta una cantidad de tiempo variable al intervalo de actualización de cada _router_ de la red. Este intervalo varía del 0 al 15%.

### Routing Loops

En un segmento de red con varios _Routers_. Si un paquete de datagramas se envían entre los _Routers_ sin que alcance la red de destino. Si existe una ruta válida con un destino inalcanzable.

![alt text](image-152.png)

Existen _Loops_ de Capa 3 que no son tan problemáticos como los _Loops_ de Capa 2:

![alt text](image-153.png)

El protocolo IP utiliza el campo TTL que disminuye en 1 a cada reenvío del paquete. Previenen los _Loops_ de Capa 3.

### Cuenta a Infinito

Es una condición que se produce cuando las actualizaciones de enrutamiento inexactas aumentan el valor de la métrica a infinito (valor máximo de métrica) para una red que ya no se puede alcanzar.

>[!TIP]
>
>**RIP** define el valor infinito en 16.

### Prevención de Loops con Temporizadores

Los _temporizadores de espera_ se utilizan para evitar que en un mensaje de actualización regular, se reinstauren de manera inadecuada rutas que puede no ser válidas.

Si un _router_ recibe una actualización de una red que era accesible y ahora no lo és. El _router_ marca la red como deshabilitada, iniciando el temporizador de espera; si se recibe una actualización desde cualquier otro vecino durante el periodo de espera, con la misma o pero métrica, se ignora dicha actualización.

### Horizonte Dividido

La regla del _horizonte dividido_ sirve para evitar los _loops_ entre _routers_. Esta regla establece que una _router_ no debe publicar en la red a través de la interfaz, por la cual vino la actualización.

#### Envenenamiento en Reversa

El _envenenamiento de ruta_ o en _reversa_ es otro método para evitar los _loops_ entre _routers_. Marca la ruta como inalcanzable en una actualización de enrutamiento que se envía a otros _routers_. Esta técnica se puede combinar con la anterior.

#### TTL del protocolo IP

El TTL limita el número de saltos que un paquete puede tomar en cada uno de los _routers_ de un segmento de red, con el fin de evitar que esté circulando el paquete de datos indefinidamente. Un paquete de datos con el TTL = 0 queda descartado.

![alt text](image-154.png)

- - -

## Protocolos RIP versión 1

### Configuración RIPv1

![alt text](image-155.png)

Con el esquema de la red mostrada, realizaremos la configuración **RIP** (protocolo dinámico):

  ```R1
    /*_configuración del R1_*/
    > enable
    # config terminal
    (...)# router rip /*_esta oppción es para seleccionar rip version 1_*/
    (...)# network 192.168.1.0  /*_dirección de red LAN_*/
    (...)# network 192.168.2.0  /*_dirección de red WAN_*/
    (...)# network 192.168.6.0  /*_dirección de red WAN_*/
    (...)3 end

    # show ip protocols /*_muestra los protocolos del router_ */
  ```

El código para el router _R2_ y el router _R3_ es el mismo que el expuesto del router _R1_ usando las IP interconectadas a cada router.

>[!IMPORTANT]
>Los Routers de una red, se comunican si comparten protocolo.

### Interfaces Pasivas y Debug RIP

En el router _R1_, escribir el siguiente código:

  ```R1
    > enable
    # debug ip rip
  ```

Para evitar el tráfico de datos innecesario en los routers, podemos desactivar las interfaces para que dejen de recibir tramas de actualizaciones. Para ello tenemos el siguiente código:

  ```R1
    > enable
    # configure terminal
    (...)# router rip
    (...)# passive-interface fastEthernet 0/0
    (...)# end

    #show run           /*_sirve para verificar la configuración_*/
    #show ip protocols  /*_muestra protocolos_*/
    #debug ip rip       /*_verificar configuración. Actualizaciones solo por serial_*/
  ```

### Desventajas de RIPv1

Usaremos el esquema mostrado, para configurar el protocolo **RIP**.
![alt text](image-156.png)

Configuraciones en el router R1:

  ```R1
  /*En el R1*/
  > enable
  # configure terminal
  (...)# router rip
  (...)# network 172.20.1.0
  (...)# network 172.20.2.0
  ```

Configuraciones en el router R2:
  
  ```R2
  /*En el R2*/
  > enable
  # configure terminal
  (...)# router rip
  (...)# network 192.168.100.0
  (...)# network 200.160.100.0
  (...)# network 200.160.100.4
  ```

Configuraciones en el router R3:
  
  ```R3
  /*En el R3*/
  > enable
  # configure terminal
  (...)# router rip
  (...)# network 172.20.100.0
  (...)# network 172.20.200.0
  (...)# network 200.160.100.4
  ```

Para verificar las configuraciones en cada router ejecutaremos: _`show run`_. No muestra las rutas hacia las redes LAN. Si especificamos con el comando `show ip route x.x.x.x` en uno de los ruters, usando las IP de las LAN, no las mostrará.

El problema de **RIPv1** es que no envía las máscara de subred.

### Enruramiento Estático y RIP

![alt text](image-157.png)

En este ejemplo combinamos enrutamiento _dinámico_ y _estático_. La configuración del router _R1_:

  ```R1
    /*En el R1*/
    > enable
    # configure terminal
    (...)# router rip
    (...)# network 172.16.1.0
    (...)# network 172.16.2.0
  ```

  ```R2
    /*En el R2*/
    > enable
    # configure terminal
    (...)# router rip
    (...)# network 172.16.2.0
    (...)# network 172.16.3.0
    (...)# exit
    (config)# ip route 0.0.0.0 0.0.0.0 192.168.10.2 /*ruta estática*/

    /*para agregar rutas por defecto y porder comunicar RIP con STATIC*/
    > enable
    # configure terminal
    (...)# router rip
    (...)# default-information originate
    (...)# end
    # show running-config
  ```

  ```R3
    /*En el R3*/
    > enable
    # configure terminal
    (...)# ip route 172.16.0.0 255.255.0.0 192.168.10.1
  ```

Para verificar las conectividades ejecutar en cada uno de los router el siguiente comando: `show ip route`.

- - -

## VLSM & CIDR

### Clases de Direcciones IPv4

![alt text](image-158.png)
![alt text](image-159.png)

### Classless Inter-Domain Routing (CIDR)

El **CIDR** es una forma de resumir un bloque de rutas. Crear máscaras más grandes a partir de la máscara por defecto.
![alt text](image-160.png)

### Variable Length Subnet Mask (VLSM)

**VLSM** obtiene máscaras más pequeñas de una máscara dada.
![alt text](image-161.png)

[Practic-VLSM-I](practicas/pracvlsm1.md)

### Configuración Básica con IPv6

[Practic-IPv6](practicas/pracBasicIPv6.md)
- - -

## RIP versión 2 y otras configuraciones

### Interfaces Loopback

En la práctica, el uso frecuente de las _interfaces loopback_ es para el direccionamienot de los protocolos dinámicos.

![alt text](image-162.png)

```R3
    /*configuración del router R3 de la imagen*/
    R3> enable
    R3# configure terminal
    R3(config)# interface loopback 0 /*nº desde 0 a 2147483647*/
    R3(config-if)# ip address 172.30.110.1 255.255.255.0
    R3(config-if)# exit
    R3(config)# interface loopback 1
    R3(config-if)# ip address 172.39.200.129 255.255.255.0
    R3(config-if)# ip address 172.30.200.17 255.255.255.240
    R3(config-if)# exit
    R3(config)# interface loopback 2
    R3(config-if)# ip address 172.30.200.33 255.255.255.240
```

Si verificamos la configuración ejecutando `show running`; vemos que antes de las interfaces físicas, aparecen las interfaces lógicas.

Otro comando que podemos utilizar es `show ip interface brief`; mostrando las interfaces tando físicas como lógicas, activas y no conectadas.

### Rutas NULL y Redistribución de Ruta Estática

Siguiendo con el mismo esquema mostrado en el apartado anterior. En el R2 aparece una IP de Clase C (192.168.0.0/16) con una máscara /16 _Ruta Estática Resumida_.

```R2
    /*Configuración en el R2*/
    R2> enable
    R2# configure terminal
    R2(config)# ip route 192.168.0.0 255.255.0.0 null 0
```

La interface **Null** sirve para que; todo el tráfico que está busca esta red, pase a ser descartado. Las razones, son para verificar la redistribución de una ruta estática en el protocolo dinámico RIP. Y para comprobar que la versión 1 de RIP tampoco funcione
con CIDR igual que con VLSM. En cambio con RIPv2, la ruta si puede ser publicada.

Por tanto, se puede usar para enrutar el tráfico malicioso hacia una ruta nula (**null**) liberando el tráfico de la red.

```R2
    /*Habilitamos el protocolo RIP en el R2*/
    R2> enable
    R2# configure terminal
    R2(config)# router rip
    R2(config-router)# network 10.1.0.0
    R2(config-router)# network 1.1.1.0
    R2(config-router)# end

    /*para redistribuir las rutas estáticas en el protocolo RIP*/
    R2# configure terminal
    R2(config)# router rip
    R2(config-router)# redistribute static
```

```R1
    /*Habilitamos el protocolo RIP en el R1*/
    R1> enable
    R1# configure terminal
    R1(config)# router rip
    R1(config-router)# network 1.1.1.0
    R1(config-router)# network 172.30.0.0
```

Si verificamos la tabla de ruteo en el R1 con el comando `show ip route`, nos mostrará las redes atendidas por el protocolo RIP entre otros datos.

### RIPv2

En todos los Routers de una red, para que funcione bien **RIPv2** todos tienen que tener el mismo protocolo. En el ejemplo anterior, configuraremos **RIPv2** en el router R2:

```R1
    /*Habilitamos el protocolo RIPv2 en el R1*/
    R1> enable
    R1# configure terminal
    R1(config)# no router rip
    R1(config)# router rip
    R1(config-router)# version 2
    R1(config-router)# network 172.30.2.0
    R1(config-router)# network 172.30.1.0
```

### Ejercicio PT

[Ejercicio-PT](practicas/ejercicioPT.md)

### Ejercicio configuración RIPng para IPv6

[Ejercicio-RIPng-IPv6](practicas/ejercicioRIPngIPv6.md)

- - -

## Análisis de la tabla de enrutamiento

Existen _rutas de nivel uno_, cuyas máscaras es _igual_ o _menor_ a la máscara por defecto de la Clase. Un ejemplo, en una máscara de clase C la máscara /24 indicará que es de nivel uno.

Se denomina _ruta final_ aquella que, aparte de poseer su máscara de red correspondiente a la clase, además incluye la interfaz de salida con la IP del siguiente salto.

La _ruta principal_ es la IP de red con la máscara de red. Indica al Router rutas secundarias (subredes) finales con interfaz de salida asociada.

Las _rutas de nivel dos_ son aquellas rutas es una subred de una dirección de red.

- - -

## Protocolos dinámico EIGRP

### Protocolo EIGRP

- EIGRP es el protocolo mejorado.
- Utiliza el algoritmo por difusión DUAL.
- No conserva las entradas de enrutamiento ni utiliza actualizaciones periódicas.
- Mantiene una tabla de topología independiente de la tabla de enrutamiento.
- Cuando una ruta no está disponible, DUAL utilizará una ruta de respaldo.
- Convergencia más rápida debido a la ausencia de temporizadores de espera y un sistema coordinado de cálculo de rutas.

### Formato de mensajes EIGRP

![alt text](image-163.png)

- El campo _Código de operación_ especifica el tipo de paquete que puede ser: actualización, consulta, respuesta o saludo.
- El campo _Número de Sistema Autónomo_ especifica el proceso de enrutamiento.

![alt text](image-164.png)
![alt text](image-165.png)
![alt text](image-166.png)

### Tipos de paquetes EIGRP

EIGRP tiene la capacidad de realizar el enrutamiento de distintos protocolos, incluidos: IP, IPX mediante el uso de módulos dependientes de protocolos PDM.

EIGRP utiliza cinco paquetes distintos, algunos en pares:

- _Paquetes de Saludos_: para descubrir vecinos y formar adyacencias entre ellos.
- _Paquetes de Actualización_: sirven para dar actualización de enrutamiento. Se envían únicamente cuando son necesarios. Y se envían a aquellos Routers que lo precisen.
- _Paquetes de Acuse de Recibo_: se utilizan para informar a los vecinos que se ha recibido la información satisfactoriamente.
- _Paquetes de Consulta/Respuesta_: son utilizados por el algoritmo DUAL cuando busca redes y otras tareas.

#### Módulos Dependientes de Protocolo (PDM)

Los PDM son responsables de las tareas de enrutamiento específicas de cada protocolo de capa de red.

![alt text](image-167.png)

EIGRP no puede utilzar servicios UDP ni TCP.

EIGRP puede enviar paquetes como Unicast o Multicast. Los paquetes EIGRP Multicast utilizan la dirección reservada **224.0.0.10**

### Envíos de Saludos/Actualizaciones

El _paquete de saludos_ se envía cada 5 segundos.

![alt text](image-168.png)

El _tiempo de espera_ es aquel que le permite a un _Router_ determinar si un vecino se ha vuelto inalcanzable. Por lo general, es tres veces mayor al intervalo de saludo.

Las actualizaciones de EIRGP se consideran limitadas y parciales, porque solo envían información a cerca de los cambios de ruta, y son enviados a aquellos Router afectados por el cambio.

- _Parcial_ porque la actualización solo incluye la información sobre los cambios de la ruta.
- _Limitada_ porque solo recibirán la actualización aquellos Routers afectados por el cambio.

### DUAL, Distancia Administrativa y Autenticación

El algoritmo **DUAL** se utiliza para que no se produzcan _loops_ en una ruta. Ello, permite que cada Router se sincronicen en caso de cambio de topología.

La **Distancia Administrativa** es la confiabilidad del origen de una ruta. EIGRP tiene una DA de 90 para rutas internas.

![alt text](image-169.png)

Los protocolos se configuran para la **Autenticación**, para garantizar la seguridad.

### Configuración Básica de EIGRP

[Practic-EIGRP-I](practicas/eigrpBasic1.md)

### Práctica RIP - EIGRP - STATIC

[Practic-RIP-EIGRP-STATIC](practicas/ripeigrpstatic.md)

### Práctica Conexión en Topología

[Pract-Conex-topo](practicas/conextopo.md)

### Práctica configuración EIGRP para IPv6

[Pract-EIGRP-IPv6](practicas/preigrpipv6.md)

- - -

## Protocolos dinámicos de estado de enlace

### Protocolos de Estado de Enlace

![alt text](image-170.png)

### Algoritmo SPF (EDSGER DIJKSTRA)

El algoritmo SPF acumula _costos_ a lo largo de cada ruta, desde el origen hasta el destino. El _costo_ total, es la suma de cada ruta. La ruta más corta, no es necesariamente la ruta con la menor cantidad de saltos.

### Proceso de Enrutamiento

1. Cada router aprende de cada una de sus propias redes conectadas directamente.
2. Cada router tiene la responsabilidad de "saludar" a sus vecinos en redes conectadas directamente.
3. Cada router crea un Paquete de estado de enlace (LSP) que contiene el estado de cada enlace conectado directamente.
4. Cada router inunda el LSP hacia todos sus vecinos, quienes luego almacenan en una base de datos todos los LSP recibidos.
5. Cada router utiliza la base de datos para construir un mapa topológico completo y calcula la mejor ruta para cada red de destino.

### Conocimiento de Redes Directamente Conectadas

El Router registra las direcciones de red de las redes conectadas directamente, independientemente de los protocolos utilizados.

![alt text](image-171.png)

### Descubrimiento de vecinos

Se utiliza el protocolo OSPF para descibrir nuevos vecinos conectados. Los vecinos tienen que usar el mismo protocolo (OSPF) para que puedan ser descubiertos.

### Construcción de LSPs

Una vez que un router ha creado sus adyacencias, puede crear sus propias tablas de estado de enlaces LSP, los cuales incluyen: información del estado de enlace, de sus propias interfaces.

### Saturación de LSPs

Cada router inunda de información al resto de routers de la red. Los LSP no se envían siempre, únicamente cuando existe un cambio en la topología, arranque inicial del router o precesos de enrutamiento del protocolo. Un router conserva la información más actual en su base de datos.

### Base de Datos SPF

Cda router tendrá un LSP proveniente de otros routers de estado de enlace. Dicho LSP se almacena en tabla de estados de enlace del propio router. Estos datos sirven para construir los enlaces OSPF. Esta información, permite a los router calcular la ruta preferida o más corta para el envío de paquetes de datos en la red.

### Creación del árbol SPF

Un router puede comenzar a construir un árbol SPF, a partir, de los datos de estado de enlace almacenados en la base de datos.

Ejemplo del árbol SPF del router R1:
![alt text](image-172.png)

### Ventajas de los  protocolos SPF

- Cada router crea su propio mapa topológico de la red para determinar la ruta más corta.
- La saturación inmediate de los LSP logra una convergencia más rápida.
- Solo se envían LSP cuando se produce un cambio en la topología y éstos únicamente contienen la información relacionada con tal cambio.
- Diseño jerarquico utilizado cuando se implementan varias áreas.

### Requerimientos y Consideraciones

Los protocolos de enrutamiento de estado de enlace, están diseñados para minimizar los efectos en la memoria, CPU y el ancho de banda.

![alt text](image-173.png)

Los requerimientos del SPF:

- Memoria para la base de datos de estado de enlace.
- Procesamiento CPU del algoritmo SPF.
- Requerimientos de ancho de banda para la saturación de estado de enlace.

- - -

## Protocolos dinámicos OSPF

### Paquetes de OSPF

![alt text](image-174.png)
![alt text](image-175.png)

### Protocolo de Saludo

![alt text](image-176.png)

Antes de que un grupo de routers estén de acuerdo en la adyacencia, deben compartir tres valores: _intervalo de salud_, _intervalo muerto_ y _tipo de red_.

- Intervalo de saludo: se envían cada 10 segundos, en segmentos multiacceso y punto a punto. Y cada 30 segundos en segmentos multiacceso sin broadcast.
- Intervalo muerto: es el periodo expresado en segundos que el router esperará para recibir un paquete de saludo, antes de declarar un vecino como desactivado. De forma predeterminada, este tiempo es cuatro veces el intervalo de saludo.
- Tipo de red: es la comunicación de los vecinos.

### Actualización y Algoritmo OSPF

**LSU** son los paquetes utilizados para las actualizaciones del enrutamiento OSPF.
![alt text](image-177.png)

Un paquetes **LSU** puede incluir 10 tipos diferentes de notificaciones de estado de enlace.
![alt text](image-178.png)

### Distancia Administrativa de OSPF

La Distancia Administrativa (AD), es la confiabilidad o preferencia del origen de la ruta. **OSPF** tiene una AD de 110.
![alt text](image-179.png)

**OSPF** puede configurarse para _autenticación_. Así los routers se podrán comunicar únicamente si están identificados entre.

### Configuración Básica de OSPF

![alt text](image-180.png)

Configuración del router R1:

```R1
  (config)# router ospf 1
  (config-router)# network 172.16.1.16 0.0.0.15 area 0    //IPred + WildCard /28 + Dominio o área general de enrutamiento
  # network 192.168.10.0 0.0.0.3 area 0                   //WildCard /28
  # network 192.168.10.4 0.0.0.3 area 0                   //WildCard /28

  # show ip protocols
```

Configuración del router R2:

```R2
  (config)# router ospf 1
  (config-router)# network 10.10.10.0 0.0.0.255 area 0
  (config-router)# network 192.168.10.0 0.0.0.3 area 0
  (config-router)# network 192.168.10.8 0.0.0.3 area 0

  # show ip ospf neighbor
```

Configuración del router R3:

```R3
  (config)# router ospf 1
  (config-router)# network 172.16.1.32 0.0.0.7 area 0
  (config-router)# network 192.168.10.8 0.0.0.3 area 0
  (config-router)# network 192.168.10.4 0.0.0.3 area 0
  
  # show ip ospf neighbor
```

### ID del Router OSPF

La ID del router se determina en el siguiente orde:

- Utilice la dirección IP configurada con el comando router-id de OSPF.
- Si la id del router no está configurada, el router elige direcciones IP más altas de cualquiera de sus interfaces loopback.
- Si no está configurada ninguna interfaz loopback, el router elige la dirección IP activa más alta de alguna de sus interfaces físicas.

Con el comando `show ip brief` podemos ver si existen interfaces _loopback_.

### Configuración de Loopback y Cambio de Router ID

Con el comando `show ip protocols` podemos ver el Router ID (dirección IP más alta de las redes configuradas en las interfaces activas).

Ejemplo de configuración _loopback_:

```R1
  (config)# interface loopback 0
  (config-if)# ip address 10.1.1.1 255.255.255.255
  # show ip interface brief
```

Se puede configurar la misma dirección loopback en distintos routers.

#### Comando Router-ID

```R1
  ...
  (config)# router ospf 1
  (config-router)# router-id 1.1.1.1
  (config-router)# Reload or use "clear ip ospf process" command, for this to take effect
  (config-router)# exit
  # clear ip ospf process   //crea las adyacencias
  # show ip protocols       //muestra el nuevo ID
```

### Verificación de los procesos OSPF

1. Con el comando `show ip protocols` podemos ver: el ID de OSPF, el Router-ID, las redes que estamos publicadno y las fuentes de donde recibimos información entre otros datos.
2. Con el comando `show ip ospf` podemos ver nuestro propio ID, número de interfaces en el área, datos de los LSA, entre otros.
3. El comando `show ip ospf interface` nos muestra las interfaces en el proceso OSPF.
4. El comando `show ip route` muestra los enrutamientos usados.

### Métrica OSPF

La métrica en OSPF se llama _Costo_ (equivale a $10^8 / bps$). El _Costo_ está configurado por el Administrador del sistema. Cuanto más bajo sea el _Costo_ más probabilidad hay de que, la interfaz sea usada para enviar tráfico de datos.

En una red, el _Costo_ de ir de un punto a otro, es igual a la suma del _Costo_ de cada enlace.

### Análisis de la Métrica de OSPF

![alt text](image-181.png)
En la figura se muestra como se puede enviar información en el protocolo OSPF, por las _Serial0/0/1_ o _Serial0/0/0_ teniendo la misma métrica de 128.

Ejecutando el comando `show interfaces serial 0/0/0` podemos observar el ancho de banda (BW 1544 Kbit) utilizado.

Para ver la información de una interfaz, relativo al protocolo OSPF, ejecutar el comando `show ip ospf interface serial 0/0/0`. El valor del _Costo_ es de 64.
![alt text](image-182.png)

>[!NOTE]
> El valor del $Costo = 10^8 / bps$ </br>
> En el ejemplo anterior es 64.

### Modificar el Coste mediante el Ancho de Banda

![alt text](image-183.png)

La modificación del BW se debe realizar en ambos extremos del enlace.

```R1
  # show ip route
  (config)# interface serial 0/0/0
  (config-if)# bandwidth 64
  (config-if)# exit
  (config)# interface serial 0/0/1
  (config-if)# bandwidth 256 
  # show ip ospf interface serial 0/0/0
```

### Modificación Directa del Costo OSPF

Modificación del _Costo_ de forma manual:

```R1
  ...
  (config)# interface serial 0/0/0
  (config-if)# ip ospf cost 1562
  (config-if)# no bandwidth
  (config-if)# exit
  # show ip ospf interface serial 0/0/0
```

Comparativa entre el comando `bandwidth` y el comando `ip ospf cost nº`:
![alt text](image-184.png)

### Topología de Accesos Múltiples

![alt text](image-185.png)

Ejecutando el comando `show ip ospf neighbor`:
![alt text](image-186.png)

Ejecutando el comando `show ip ospf interface fastEthernet 0/0`:
![alt text](image-187.png)

### Elección de DR & BDR

El proceso **DR & BDR** se lleva a cabo, tan pronto como el primer router con una interfaz OSPF habilitada tiene acceso a una red de acceso múltiple.

![alt text](image-188.png)
![alt text](image-189.png)
![alt text](image-190.png)

### Modificación de Prioridad de OSPF

La prioridad la toman las _loopback_, por tanto, el router que tenga la IP más alta asume el rol **DDR**  que en este ejemplo es el router R3. El **BDR** sería el segundo más alto, el router R2.

Si no queremos depender del algoritmo OSPF para la asignación del **DDR** y **BDR**. Sino que designaremos que routers asumen el rol. Para ello forzaremos la prioridad del OSPF.

Ejemplo, designar al router R1 como nuestro **DDR**:

```R1
  ...
  (config)# interface fastEthernet 0/0
  (config-if)# ip ospf priority 200     //la más alta prioridad de los 3 routers
```

El router R2 será el **BDR**:

```R2
  ...
  (config)# interface fastEthernet 0/0
  (config-if)# ip ospf priority 100
```

### Redistribución de Rutas Estáticas y Rutas por Defecto

Utilizando el esquema de la red anterior. En el router R1, configuramos la distribución de rutas estáticas en OSPF:

```R1
  ...
  (config)# interface loopback 1
  (config-if)# ip address 8.8.8.8 255.255.255.255
  (config-if)# exit

  //crear ruta por defecto hacia la IP
  (config)# ip route 0.0.0.0 0.0.0.0 8.8.8.8
  (config)# ip route 0.0.0.0 0.0.0.0 loopback 1

  # show running

  (config)# router ospf 1
  (config-router)# default-information originate

  # show ip route
```

### Modificación del Ancho de Banda de Referencia

Seguimos con el mismo circuito usando anteriormente.

```R1
  ...
  (config)# router ospf 1
  (config-router)# auto-cost reference-bandwidth 1000
  //todos los router deben tener la misma referencia
```

### Modificación de Temporizadores

Modificación del intervalo entre routers (esquema anterior).

```R1
  ...
  (config)# interface serial 0/0/0
  //por defecto el tiempo es de 10" se modifica a 5"
  (config-if)# ip ospf hello-interval 5
  (config-if)# ip ospf dead-interval 20

  (config)# interface serial 0/0/0
  (config-if)# ip ospf hello-interval 5
  (config-if)# ip ospf dead-interval 20
```

### Ejercicio PT 1

[Ejercicio-PT1](practicas/ejercicioPT1.md)

### Ejercicio PT 2

[Ejercicio-PT2](practicas/ejercicioPT2.md)

### Ejercicio PT 3

[Ejercicio-PT3](practicas/ejercicioPT3.md)

- - -

## Introducción a redes conmutadas

### Redes Jerárquicas

Las redes jerárquicas permite la modularidad de la topología, siendo los beneficios: _escalabilidad_, _redundancia_, _rendimiento_, _seguridad_, _facilidad de administración_, _facilidad de mantenimiento_.
![alt text](image-191.png)

- _Capa de acceso_ con los dispositivos finales.
- _Capa de distribución_ agrega los datos recibidos de la _capa de acceso_.
- _Capa de núcleo_ es de alta velocidad, siendo esencial para la interconectividad de dispositivos de la _capa de distribución_.

### Principios de Diseño en Redes Jerárquicas

Los principios son:

1. Diámetro de la red. Medida de distancia del número de dispositivos del origen al destino.
2. Ancho de Banda. Se puede aumentar las conexiones entre _switch_ para aumentar el ancho de banda.
3. Redundancia. Para crear redes altamente disponibles. Se pueden duplicar las conexiones de red o el número de dispositivos.

### Consideraciones para Switch

Para seleccionar el _switch_ apropiado para una red jerárquica, es necesario contar con especificaciones que detallen los flujos de tráfico, los usuarios, los servidores de datos y almacenamiento.

![alt text](image-192.png)
![alt text](image-193.png)

Se debe considerar el tráfico tanto de: enlace cliente-servidor, como servidor-servidor.
![alt text](image-194.png)

Diagrama de topología, muestra como se interconectan todos los _switch_.
![alt text](image-195.png)

### Características de los Switch

_Switch_: apilable, modular.
![alt text](image-196.png)
![alt text](image-197.png)

Las _tasas de reenvío_ indican el nivel de procesamiento del _switch_ (capaz de conmutar Gbps de tráfico)

Algunas funcionalidades de los _switch_: **PoE** (suministro de energía por el cable Ethernet)
![alt text](image-199.png)

Por lo general, los _switch_ operan en la Capa 2 del modelo OSI (uso de direccioens MAC dispositivos conectados). Existen _switch_ de Capa 3 o multicapa, que ofrecen una funcionalidad avanzada operando como si fueran _routers_.
![alt text](image-200.png)

Características en la _Capa de Acceso_:
![alt text](image-201.png)

Características en la _Capa de Distribución_:
![alt text](image-202.png)

Características en la _Capa de Núcleo_:
![alt text](image-203.png)

- - -

## Funcionalidades de un Switch

### Elementos de Ethernet

Un dispositivo antes de transmitir, debe escuchar el medio, para evitar colisión de paquetes de datos.

La composición de una dirección **MAC**: 1 bit para Broadcast, 1 bit para Local, 22 bits para Número OUI, Número de fabricante (identifica de manera única al hardware).

Los puertos de _switch_ de la serie Cisco Catalyst 2960 pueden configurarse de tres maneras:

- **auto**: permite que los dos puertos se comuniquen para decidir el modo.
- **full**: establece el modo _full-duplex_.
- **half**: establece el modo _half-duplex_.

Los _switch_ utilizan direcciones MAC para enviar las tramas por la red a la interfaz correspondiente hasta el nuevo destino. El _switch_ crea una tabla de direcciones **MAC**.

### Dominios de Broadcast & Colisiones

Para evitar las colisiones, se pueden crear segmentos físicos de red individuales, llamados _dominios de colisión_. Los _switch_ evitan las colisiones, permiten una mejor utilización del ancho de banda, en los segmentos de red; ya que ofrecen un BW dedicado para cada segmento.

Los _switch_ no filtran las tramas de _broadcast_; al recibir dicha trama, la reenvía al resto de host que están conectados, excepto a la interfaz por la que recibió la trama.

#### Latencia

Es el tiempo que tarda una trama en hacer el recorrido desde el origen hasta llegar al final. La _latencia_ está formada de tres componentes: NIC de origen, NIC destino, retardo de la señal y cantidad de dispositivos.

![alt text](image-204.png)

#### Segmentar una LAN

Segmentar en partes una LAN, es útil para aislar el tráfico y lograr una mejor utilización del ancho de banda por usuario. Segmentar en dominios de colisión, evita el colapso de la red.
![alt text](image-205.png)

Todo _host_ conectado a un _switch_ pertenecen al mismo dominio de _broadcast_. Si se crean nuevos dominios de _broadcast_ se reduce el tráfico, mejorando el ancho de banda.
![alt text](image-206.png)
![alt text](image-207.png)

### Consideraciones en el Diseño de Redes LAN

Eliminar el "cuello de botella"; cuando la demanda del BW empieza a subir, se pueden agregar enlaces nuevos.
![alt text](image-208.png)

### Métodos de conmutación de un Switch

Un _switch_ recibe una trama, la almacena en los _buffer_ de datos hasta recibir la trama en su totalidad. El _switch_ analiza la trama para conocer el destinatario (CRC CHECK), también verifica si existen errores utilizando _tabla de conmutación_.

### Conmutación Simétrica y Asimétrica

>[!NOTE]
>La _conmutación asimétrica_ permite un mayor BW.</br>
>La _conmutación simétrica_ proporciona conexión entre puertos con el mismo BW.

### Buffer de memoria

El _buffer_ de memoria puede ser utilizado cuando el puerto de destino está ocupado. Existen dos tipos de almacenamiento de _buffer_:

|      |      |
| :--- | :--- |
| Memoria basada en puerto | Las tramas se almacenan en colas conectadas a puertos de entrada específicos |
| Memoria compartida | Se depositan todas las tramas en un _buffer_ común que comparten todos las interfaces del _switch_ |

### Switch de Capa 2 & Switch de Capa 3

#### Switch de Capa 2

LLevan a cabo los procesos de conmutación y filtrado basándose únicamente en las direcciones MAC. No soportan las configuraciones de IP en las interfaces físicas, pero si en las virtuales. No se configura ningún protocolo de enrutamiento.

![alt text](image-209.png)

#### Switch de Capa 3

Funcionan igual que un _switch_ de Capa 2, además de poder usar las direcciones IP para el envío de paquetes. Se puede prescindir del uso de _router_ en una red LAN.

![alt text](image-210.png)

El envío de paquetes se puede realizar a través de distintos segmentos de red, aunque no siempre cubre todas las posibilidades disponibles en un _router_.

| Características | Switch Capa 3 | Router |
| --- | --- | --- |
| Enrutamiento de Capa 3 | Con soporte | Con soporte |
| Administración del tráfico | Con soporte | Con soporte |
| Soporte de WIC |  | Con soporte |
| Protocolos de enrutamiento avanzados |  | Con soporte |
| Enrutamiento por velocidad de cable | Con soporte |   |

### IOS del Switch

```Switch
  Switch> enable
  Switch# show running-config
  Switch# show interface
  Switch# show ip interface brief
  Switch# show history
  
  Switch# configure terminal
  Switch(config)# interface fastEthernet 0/1
  Switch(config-if)#

  Switch# clock set 21:02:00 06 Jul 2024 //estable una hora y fecha
```

### Administración de un Switch por Interfaz Local

En _switch_ de Capa 2 no se usa el ruteo (direcciones IP).

```switch
  Switch> enable
  Switch# configure terminal
  Switch(config)# interface fastEthernet 0/1
  Switch(config-if)# ip address               //no se permite
```

La forma correcta de crear una interface de administración local, es creando una VLAN. Ello permite la conexión y configuración del _switch_ a través de la interface Ethernet asignando una IP.

```switch
  Switch> enable
  Switch# configure terminal
  Switch(config)# interface vlan 10
  Switch(config-if)# description ADMINISTRACION
  Switch(config-if)# ip address 10.10.10.10 255.255.255.0

  Switch# show run
  Switch# show interface brief

  //forma de crear una interface de administración local

  Switch# configure terminal
  Switch(config)# interface fastEthernet 0/20
  Switch(config-if)# description ADMINISTRACION
  Switch(config-if)# switchport mode access
  Switch(config-if)# switchport access vlan 10
```

![alt text](image-211.png)

### Secuencia de arranque de un Switch

- El _switch_ carga el software cargador de arranque de NVRAM.
- El cargador de arranque:
  - Realiza la inicializa de la CPU a bajo nivel.
  - Realiza el POST para el subsistema de la CPU.
  - Inicializa el sistema de archivos flash en la placa del sistema.
  - Carga una imagen predeterminada de software de sistema operativo en la memoria y arranca el switch.
- El SO se ejecuta utilizando el archivo _config.text_ guardado en el almacenamiento flash del switch.

### configuraciones Básicas de un Switch

```switch
  Switch> enable
  Switch# configure terminal
  Switch(config)# hostname SWITCH_LAN
  SWITCH_LAN(config)# line console 0
  SWITCH_LAN(config-line)# password cisco
  SWITCH_LAN(config-line)# login
  SWITCH_LAN(config-line)# exit
  SWITCH_LAN(config)# line vty 0 15                 //líneas de acceso remoto de la 0 a la 15
  SWITCH_LAN(config-line)# password cisco 
  SWITCH_LAN(config-line)# login
  SWITCH_LAN(config-line)# exit
  SWITCH_LAN(config)# enable secret cisco
  SWITCH_LAN(config)# service password-encryption   //para encriptar
  SWITCH_LAN(config)# banner motd "EQUIPO SWITCH LAN"
  SWITCH_LAN(config)# exit
  SWITCH_LAN# copy running-config startup-config
```

### Cambio de Velocidad en Interfaz

```switch
  Switch> enable
  Switch# show mac-address-table
  Switch# show ip interface brief
  Switch# show interface fastEthernet 0/20

  Switch# configure terminal
  Switch(config)# interface fastEthernet 0/20
  Switch(config-if)# speed    //se puede elegir: 10, 100, AUTO
  Switch(config-if)# duplex   //se puede elegir: AUTO, FULL, HALF
```

### Otros Comandos de Interés

```switch
  Switch> enable
  Switch# show startup-config
  Switch# show version

  Switch# write                                 //para guardar la configuración
  Switch# copy running-config startup-config    //igual al anterior comando
```

### Telnet & SSH

Conexiones remotas en _Switch_, también sirven para _Router_.

```switch_telnet
  SW> enable
  SW# configure terminal
  SW(config)# line vty 0 15
  SW(config-line)# transport input all
  SW(config-line)# transport output all
```

```switch_ssh
  SW> enable
  SW# configure terminal
  SW(config)# ip dominio-name DOMINIO.COM
  SW(config)# crypto key generate rsa
  How many bits in the modulus [512]: 500   //decimal number between 360 and 2048
  SW(config)# ip ssh version 2
  SW(config)# line vty 0 10                 //líneas de la 0 a la 10
  SW(config-line)# transport input ssh
```

### Ataques a una Red LAN

#### Saturación de MAC

Cuando un _host_ envía una trama y no conoce el puerto de destino, el _switch_ envía un paquete de _broadcast_ para recopilar toda la información.

![alt text](image-212.png)

Suponemos que el _host_ MAC C hay un atacante, que envía tramas y dado que la tabla ARP del _switch es limitada empieza a saturarse con nuevas direcciones MAC, todas en un mismo puerto, de tal forma que cuando el _host_ MAC A envíe un paquete, el _switch_ no va ha saber por donde enviarla, por lo que tendrá que reenviar un _broadcast_ nuevamente. El _host_ MAC C (hacker) podrá ver la información recibida en el _broadcast_.

#### Suplantación de Identidad

![alt text](image-213.png)

En este modo de ataque, un ususario realiza una consulta (Data) al servidor.

![alt text](image-214.png)
![alt text](image-215.png)

El atacante responde antes, que el servidor DHCP a la solicitud del usuario. Con lo que, el atacante ha desviado el tráfico de datos.

Para evitar este tipo de ataque, se usa el _snoopy dhcp_. Permite saber si los puertos son confiables o no (puertos confiables envía acuse de recibo).

![alt text](image-216.png)

#### Otros tipos de ataques

- _Ataques en CDP_ (se recomienda deshabilitar esta opción en los routers)
- _Ataques de Telnet_
- _Ataques de contraseña por fuerza bruta_
- _Ataque de DoS_

### Uso de Herramientas de Seguridad

- Las auditorías de seguridad de la red ayudan a:
  - Revelar qué tipo de información puede recopilar un ataque mediante un simple monitoreo del tráfico de la red.
  - Determinar la cantidad ideal de direcciones MAC falsas que deben eliminarse.
  - Determinar el periodo de expiración de la tabla de direcciones MAC.
- Las pruebas de penetración de red ayudan a:
  - Identificar debilidades dentro de la configuración de los dispositivos de red.
  - Iniciar varios ataques para probar la red.

- Entre las características comunes de una herramienta de seguridad se incluyen:
  - Identificación de servicios.
  - Soporte de servicios SSL.
  - Pruebas destructivas y no destructivas.
  - Base de datos de vulnerabilidades.
- Se pueden utilizar las herramientas de seguridad de red para:
  - Capturar mensajes de chat.
  - Capturar archivos de tráfico NFS.
  - Capturar solicitudes de HTTP en formato de registro común.
  - Capturar mensajes de correo en formato Berkeley mbox.
  - Capturar contraseñas.
  - Mostrar URL capturadas en Netscape en tiempo real.
  - Saturar una LAN con direcciones MAC aleatorias.
  - Falsificar las respuestas a direcciones DNS.
  - Interceptar paquetes en una LAN.

### Seguridad de Puertos

- Especificar un grupo de direcciones **MAC** válidas permitidas en el puerto.
- Permite que solo una dirección MAC acceda al puerto.
- Especificar que el puerto se desactive de manera automática si se detectan direcciones MAC no autorizadas.

Las direcciones **MAC** seguras son tres:

- Direcciones MAC seguras estáticas.
- Direcciones MAC seguras dinámicas.
- Direcciones MAC seguras sin modificación.

Tipos de accesos a los puertos:

![alt text](image-217.png)

### Seguridad de Puertos - MAC Estática y Dinámica

El comando `show mac-address-table` ejecutado en un _switch_ permite ver la tabla ARP.

La seguridad en puertos de un _switch_ con la MAC en exclusiva, permite aceptar únicamente los host con dicha MAC.

```switch
  ...
  SW(config)# interface fastEthernet 0/1
  SW(config-if)# switchport port-security               //activar seguridad
  SW(config-if)# switchport port-security mac-address 0002.173c.a35e

  SW# show mac-address-table
```

### Seguridad de Puertos - MAC sin Modificación

```switch
  ...
  SW(config)# interface fastEthernet 0/1
  SW(config-if)# switchport port-security maximum
  SW(config-if)# switchport port-security mac-address sticky
  
  SW# show mac-address-table
```

### Verificación Seguridad Puertos

Con el comando `show port-security address` muestra la seguridad del puerto.

### Ejercicio Funcionalidades de un Switch

[Ejercicio-Switch](practicas/ejercSwitch.md)

- - -

## VLANS

### Introducción a las VLAN

Las **VLAN** (_Virtual Local Area Network_) son grupos de dispositivos conectados de manera lógica, que actúan como si estuvieran en una red independiente, incluso si comparten una red común con otras **VLAN**.

#### Ventajas de Utilizar VLAN

![alt text](image-218.png)

- Seguridad
- Reducción de costes
- Mejor rendimiento
- Mitigación de la tormenta Broadcast
- Eficiencia de IT
- Administración más simple

#### División de las VLAN

Las **VLAN** se dividen en dos grupos:

- _Rango normal_. Se utilizan en redes pequeñas.
  - 1 - 1005
  - 1002 - 1005 reservadas para Token Ring y FDDI.
  - Guardadas en memoria Flash
- _Rango ampliado_. Permite una ampliación de muchas más redes LAN en una infraestructura de red.
  - 1006 - 4096
  - Guardadas en configuración en ejecución.

### Tipos de VLAN

- Las _VLAN de datos_, son aquellas configuradas para transmitir tráfico de datos.
- Otro tipo es, las _VLAN predeterminadas_; todos los puertos del _switch_ se convierten en miembros de la _VLAN predeterminada_, todos los puertos pertenecen al dominio del _router_. La _VLAN predeterminada_, por defectos es, la _VLAN uno_, la cual no puede eliminarse y volver a denominar.
- La _VLAN nativa_, está asociada a un puerto troncal. Adminte el tráfico que llega de muchas VLAN, también llamado, tráfico etiquetado, y el tráfico no etiquetado (el que llega de no VLAN)
- La _VLAN de administración_, es cualquier VLAN que se configura para acceder a las capacidades de administración de un _switch_

#### VLAN de Voz

Las **VLAN VoIP** son consideradas VLAN de datos. Sin embargo, por el tipo de datos se requiere de un ancho de banda garantizado para la voz, priorizando sobre otro tipo de datos y debe demorar menos de 150 ms.

![alt text](image-219.png)

### Puertos del Switch

>[!TIP]
>Los puertos de un _switch_ son interfaces de Capa 2.

- En el modo de _VLAN estática_, los puertos se asignan manualmente a una VLAN.
- En el modo de _VLAN dinámica_, se configura un servidor especial llamado **VMPS**. Este servidor asigna puertos del _switch_ a las VALN basado en la forma dinámica y la dirección MAC del dispositivo origen.
- En el modo de VoIP, el puerto se configura para que acepte a un teléfono IP. Se debe crear la VLAN de voz y una VLAN de datos.

![alt text](image-220.png)

### Dominios de Broadcast y Comunicación entre VLAN

En un _switch_ con VLAN, cuando un host envía una trama de _broadcast_, se reenvía por aquellos puertos pertenecientes al grupo de VLAN por el que ha sido recibido (excepto éste último). Así se reduce el tráfico _broadcast_ optimizando el ancho de banda.

### Configuración Básica de VLAN

![alt text](image-221.png)

[ConfigBasicVLAN](practicas/confBasicVLAN.md)

### Enlaces Troncales

![alt text](image-222.png)

Estos enlaces permiten extender las VLAN a toda la red. Si no existienran enlaces troncales los _switch_ necesitarían utilizar una interface física por cada VLAN que se utiliza.

![alt text](image-223.png)

#### Etiquetado de trama 802.1Q

![alt text](image-224.png)

En la trama se indica el tipo VLAN en el encapsulado **802.1Q**. Se agrega una etiqueta a la trama Ethernet original.

Relación entre la VLAN nativa y los enlaces troncales:

- Tramas con etiquetas en la VLAN nativa
  - Descargadas por el switch.
  - Los dispositivos no deben etiquetar el tráfico de control destinado a la VLAN nativa
- Tramas sin etiquetas en la VLAN nativa
  - Tienen su PVID modificado al valor de la VLAN nativa configurada
  - Permanecen sin etiquetar
  - Son reenviadas en la VLAN nativa configurada

### Modos de Configuración de Enlaces Troncales

![alt text](image-225.png){width=55%}

![alt text](image-226.png){width=45%}

![alt text](image-227.png)

### Configuración Básica en Enlaces Troncales

[ConfBasicEnlacTron](practicas/enlacTron.md)

### Modos de Enlace Troncal

Esquema en CPT con esquemas de _switch_ para comprobar diferentes configuraciones.

[ModoEnlaceTroncal](recursos/Modos%20de%20Enlace%20Troncal.pkt)

### Problemas Comunes Enlaces Troncales

| Problema | Resultados |
| --- | --- |
| Falta concordancia en VLAN nativa | Riesgo de seguridad, crea resultados no deseados |
| Falta concordancia en modo enlace troncal | Pérdida de conectividad en la red |
| VLAN y Subredes IP | Pérdida de conectividad de la red |
| VLAN permitidas en enlaces troncales | Tráfico no deseado o no envía tráfico por enlace troncal |

### Práctica Problemas Comunes

[PracProblComun](practicas/problemComun.md)

- - -

## VTP

### Introducción al Protocolo Enlace Local de VLAN (VTP)

Propagación de la VLAN hacia otros _switch_ (**clientes VTP**); se realiza desde el **servidor VTP**:
![alt text](image-230.png)

Los beneficios de **VTP**:

- Consistencia.
- Monitoreo de las VLAN.
- Informes.
- Configuración de enlaces troncales, que sean dinámicos.

Los modos de **VTP** son tres: _cliente_, _servidor_, _transparente_.
![alt text](image-231.png)

### Información de Estado de VTP

Con el comando `show vtp status` en consola, en modo privilegiado, podemos ver la información del protocolo VTP.

### Dominios de VTP

**VTP** permite separar redes LAN en redes de administración más pequeñas. Si los _switch_ están en diferentes dominios, no intercambian información.
![alt text](image-232.png)

El administrador configura el nombre de dominio, y los propaga al resto de _switch_, modificando el nombre del dominio:
![alt text](image-233.png)

### Número de Revisión de Configuración

Ejecutar el comando: `show vtp status` en consola CLI. Se observa _Configuration Revision: 0_. Por defecto, empieza en cero.

Si creamos una VLAN:

```switch
  ...
  S1(config)# vlan 10

  S1# show vtp status
  //muestra ==> Configuration Revision: 1

  S1(config)# vlan 20
  S1(config-if)# exit
  S1(config)# vlan 30
  S1(config-if)# exit

  S1# show vtp status
  //muestra ==> Configuration Revision: 3

  S1(config)# no vlan 10

  S1# show vtp status
  //muestra ==> Configuration Revision: 4
```

### Notificaciones de VTP

La Estructura  de Trama de VTP:
![alt text](image-234.png)

#### Publicaciones de VTP

Información resumida que se envían cada cierto tiempo inmediatamente después de restablecida la comunicación:
![alt text](image-235.png){width="20%" height="20%"}

Detalles de las publicaciones:
![alt text](image-236.png)

Subconjunto:
![alt text](image-237.png)

Publicación de petición:
![alt text](image-238.png)

### Modos de VTP

![alt text](image-239.png){width="20%" height="20%"}
![alt text](image-240.png){width="20%" height="20%"}

### Problemas Comunes en VTP

Detalles frecuentes en la configuración del VTP:

- Versiones incompatibles del VTP.
- Temas de la contraseña del VTP.
- Nombre de modo incorrecto del VTP.
- Todos los switches configurados al modo Cliente del VTP.

Versiones de VTP incompatibles:

- Las versiones de VTP 1 y 2 son incompatibles entre sí.
- Asegúrese de que todos los switches ejecutan la misma versión VTP.

Problemas con contraseñas VTP:

- Asegúrese de que las contraseñas sean todas iguales en todos los switches con VTP activando en el dominio VTP.
- De manera predeterminada un switch Cisco no usa una contraseña VTP.
- Cuando una publicación VTP se recibe, los switches Cisco no establecen de manera automática el parámetro de contraseña VTP.

El nombre de parámetro es clave en las VTP.

### Configuración Básica de VTP

[ConfBasicVTP](practicas/confBasiVTP.md)

[EjercicioVTP](practicas/ejercVTP.md)

- - -

## STP

### Redundancia en una Red Conmutada

La redundancia de Capa 2, mejora la disponibilidad de la red implementando rutas alternas, mediante el agregado de equipos y cables.

![alt text](image-241.png){width="10%"}

### Inconveniente de la Redundancia

![alt text](image-242.png){width=40%}
![alt text](image-243.png){width=40%}
![alt text](image-244.png){width=40%}

Una tormenta de _broadcast_ se produce cuando existen multitud de tramas atrapadas en un bucle, enviadas por diferentes dispositivos.

![alt text](image-245.png){width=40%}

Otros inconveniente son las _tramas duplicadas_, generadas por redundancia, cuando dos o más _switch_ poseen información con su tabla de direcciones MAC para un mismo destino.

![alt text](image-246.png){width=40%}

### Bucles de Infraestructura

Los bucles de Capa 2 se producen por un mal cableado de la red. O se realiza un bucle y no se configura de manera correcta los parámetros.

![alt text](image-247.png "Bucle Capa de Acceso")
![alt text](image-248.png "Redundancia hacia otra Capa")
![alt text](image-249.png "Tormenta de broadcast")

### Protocolo Spanning Tree (STP)

El **STP** es desarrollado para evitar bucles.

![alt text](image-250.png "STP. Evitar redundancias")

El **Algoritmo Spanning Tree** (STA):

![alt text](image-251.png "Algoritmo Spanning Tree")
![alt text](image-252.png "Puente Raíz")

El **STA** escoge la ruta de menor coste.

![alt text](image-253.png){width=40%}

Los _puertos raíz_ son los puertos del _switch_ más cercanos al puente raíz.

![alt text](image-254.png "Puertos Designados")

Los _puertos designados_ son todos los puertos del _switch_ que no son _puertos raíz_ y que aun, pueden enviar tráfico a la red.

![alt text](image-255.png "Puertos No Desginados")

Los _puertos no designados_ son todos los puertos configurados en estado de bloqueo para evitar bucles.

### BID & Costo de Ruta

Un _switch_ designado como _puente raíz_ sirve como referencia para todos los cálculos de **STA** en las rutas redundantes.

![alt text](image-256.png "Switch Puente Raíz")

### Coste de STP

Con el comando: `show spanning-tree` muestra la información Costo = 19 (estándar para fastEthernet).

Algunas configuraciones, para cambiar el _costo en **STP_** de una interfaz:

```switch
  ...
  switch(config)# interface fastEthernet 0/1
  switch(config-if)# spanning-tree cost 50
```

En caso de no querer manejar _costo en **STP**_, con el comando: `no spanning-tree cost`. La interface regresa a su modo normal.

### Proceso de BPDU

Tramas de **BPDU**:

![alt text](image-257.png)

El Proceso **BPDU** funciona de la siguiente manera:

![alt text](image-258.png)

Cada _switch_ tiene su ID, e inicialmente se identifica como puente raíz. El _switch_ S1 es el de menor prioridad (ID Raíz es menor).

### ID de Puente (BID)

El _ID de Puente_ (**BID**) es un valor que puede personalizarse. Y puede utilizarse para ejercer influencia sobre el _switch_ que debe convertirse en el puente raiz. El _switch_ con el menor **BID** se convierte en el _Puente Raíz_.

![alt text](image-259.png)

### Prioriddad de Puente Raíz

El _switch_ con la menor **MAC** será, el _puente raíz_.

Hay dos formas para asignar un puente raíz, para mostrarlo se utilizará el siguiente esquema:

![alt text](image-260.png)

- Método cambio de prioridad: asignar al _switch 2_ como el _puente raíz_.

```switch
  ...
  S2# show spanning-tree  //muestra Bridge ID Priority nº múltiplos de 4096

  S2(config)# spanning-tree vlan 1 priority 4096
```

- Método no usando números.

```switch
  ...
  S2(config)# no spanning-tree vlan 1 priority

  S2(config)# spanning-tree vlan 1 root secondary
```

### Funciones de los Puertos

Los puertos raíz, envían el tráfico a través del puente raíz. Sólo se permite un puente raíz por puente.

![alt text](image-261.png "Puertos Raíz")

Los puertos designados, no son puertos raíz. El switch que recibe tramas a través del puente raíz.

![alt text](image-262.png "Puerto Desginado")

El puerto no designado es aquel que está bloqueado. De manera nue no envía tramas de datos.

![alt text](image-263.png "Puerto No Desginado")
![alt text](image-264.png "Designación funciones en los puertos")

### Elección de Funciones de Puertos

El switch con prioridad más baja es el puente raíz. En el ejemplo es el S1. Configura los puertos, como puertos designados.

![alt text](image-265.png)

Los switch S2 y S3 configuran los puertos como pertos raíz, porque apuntan al puerto raíz. La conexión entre los switch S2 y S3 deben determinar que interfaz tiene el BID más bajo (basado en la MAC) para decidir cual es el puerto desginado.

![alt text](image-266.png)

### Estados de los Puertos

Existen 5 estados en los que puede estar un puerto: _bloque_, _escucha_, _aprender_, _enviar_, _deshabilitar_.

![alt text](image-267.png "Volver al bloqueo si no es la ruta de menor costo al Puente raíz")

La cantidad de tiempo en los que un pueto permanece deshabilitado, depende de los temporizadores **BPDU**.

![alt text](image-268.png "Tabla de temporizadores BPDU")

Estados de los puertos en un _switch_:

![alt text](image-269.png "Estados de las interfaces")

### Función PortFast

![alt text](image-270.png "Función PortFast")

El **PortFast** es una especie de interfaz hacia segmentos donde no nocesitamos el protocolo **STP**.

### Configuración de PortFast

[ConfigPortFast](practicas/confPortFast.md)

### Convergencia de STP

La convergencia es importante en _spanning-tree_ **STP**. Siendo el tiempo que toma la red, en determinar que switch asume la función de puente raíz, atavesando todos los otros estados de puerto y configurar todos los puertos.

Los pasos para alcanzar la convergencia: elegir un puente raíz, elegir los puertos raíz, elegir los puertos designados y no designados.

### Convergencia - Asignación de Puertos Raíz

![alt text](image-271.png "Elegir puente raíz")

Elegir el _puente raíz_ es el primer paso, identificando el menor ID, comparándose entre los _switch_ de la topología. Para ello envía tramas _BPDU_.

![alt text](image-272.png)

Para elegir los puertos raíz, el _switch_ compara los costes de ruta para cada uno de las interfaces:

![alt text](image-273.png "Costo de ruta")
![alt text](image-274.png "Puertos raíz")

### Verificación de STP

El comando `show spanning-tree` muestra la información del puerto.

### Cambio de Topología en STP

Un _switch_ detecta cambio en la topología cuando un puerto que envía se desactiva o cuando un puerto cambia de estado.

### Variantes de STP

![alt text](image-275.png "Variantes de STP")

### PVST+

**PVST+** es útil para ejecutar una instancia para cada **VLAN** de la red.
![alt text](image-276.png "VSTP+")

La configuración predeterminada de **VSTP+**:
![alt text](image-277.png "Configuración de VSTP+")

### Configuración de PVSTP

[ConfigPVSTP](practicas/confVSTP.md)

### RSTP

El **RSTP** es una evolución estándar del **STP**. Aumenta la velocidad de recálculo de _spanning-tree_ cuando cambia la topología de la red. No requiere de temporizadores.

### Puertos de Extremo

Un _puerto de extremos_ es un puerto de _switch_ que nunca se conecta con otros dispositivos de Capa 2.

![alt text](image-278.png "Puertos de Extremo")

### Tipos de Enlace

Proporciona una categorización para cada puerto.
![alt text](image-279.png "Enlace Punto a Punto")
![alt text](image-280.png "Enlace tipo Compartido")

### Estados y Funciones de Puertos en RSTP

![alt text](image-281.png "Estado de puerto")
![alt text](image-282.png "Estado de puerto operativo")
![alt text](image-283.png "Funciones de los puertos en los switch")

### Configuración de PVST

[ConfigPVST](practicas/configPVST.md)

### Configuración de Port-Channel

[ConfigPortChannel](practicas/configPortChannel.md)

- - -

## Enrutamiento entre VLANS

### Enrutamiento VLANs Interfaces físicas

El enrutamiento entre VLANS basado en routers. Proceso por el que se envía tráfico de una red hacia la otra red. Los dispositivos de las VLAN solo se pueden comunicar entre sí.
![alt text](image-284.png "Enrutamiento en VLANs")
![alt text](image-285.png "VLANs, Enlaces Locales y Router")

### Enrutamiento VLANs Interfaces Lógicas

Una sola interfaz física en el _router_ permite la interconexión entre las VLANs. Esta interfaz se configura como un enlace troncal.

![alt text](image-286.png "Enlace Troncal")

### Funcionamiento entre VLANs

![alt text](image-287.png "Petición ARP de PC2")
![alt text](image-288.png "Respuesta ARP de PC3")

Enrutamiento entre VLANs utiliza la encapsulación **dot1Q**.

![alt text](image-289.png "Una única conexión entre VLANs al Router")
![alt text](image-290.png "Petición ARP del PC2")
![alt text](image-291.png "Reenvío de petición ARP con etiqueta VLAN10")

### Configuración de Enrutamiento con y sin VLANs - Gestión remota del Switch

[ConfigEnrutsinVLAN](practicas/configEnrutSinVLAN.md)

### Comparativa Métodos Enrutamiento de VLANs

![alt text](image-292.png)

### Consideraciones para el Enrutamiento entre VLANs

- Configuración de _switch_:
  - Verificar que la VLAN sea configurada en la interfaz simple hacia el router.
  - Verificar que las VLANs sean agregadas al enlace troncal hacia el router.
  - Verificar el modo adecuado de operación de la interfaz hacia el router.
- Configuraciones del _router_:
  - Verificar que el direccionamiento sea el correcto en las interfaces simples.
  - Verificar que las subinterfaces coincidan con el formato **DOT1Q** adecuado.
- Configuraciones de IP_
  - Verificar que las subredes corresponden con las VLANs asociadas.
  - Verificar que los dispositivos finales poseen el direccionamiento adecuado.

### Ejercicio PT Enrutamiento entre VLANs

[EjercicioPT](practicas/ejercPTenrutVLAN.md)

- - -

## Introducción a redes inalámbricas

![alt text](image-293.png "Tabla comparativa redes inalámbricas")
![alt text](image-294.png "Tabla de características de redes inalámbricas")

### Estándares para Redes inalámbricas

![alt text](image-295.png "Estándares para Redes inalámbricas")

### Componentes de las Redes inalámbricas

1. Tarjetas de red inalámbricas, para montaje en placa base usando conector PCIe o para conexión en USB.
2. Puntos de acceso. Son dispositivos que conectan a los usuarios a la red de forma inalámbrica. Transforman la comunicación del estándar 802.11 al estándar 802.3 y viceversa. Usan el acceso múltiple por detección de portadora para evitar colisiones CSMA/CA.
3. Router inalámbricos. Cumplen la función de punto de acceso, switch y router al mismo tiempo.

### Operación inalámbrica

Es el estándar 802.11g, b/g, b o cualquier otro. Algunos dispositivos pueden operar con más de un estándar a la vez. Por lo que, es el usuario el que escoge el modo de funcionamiento.

![alt text](image-296.png "Tipos de estándares")

El **SSID** es el identificador asignado a la red, cuando buscamos con un dispositivos inalámbricos.

Los canales son anchos de banda dentro del rango de frecuencias. La superposición de canales causa interferencias.

![alt text](image-297.png "Canales en redes inalámbricas")

Las LAN inalámbricas pueden usar distintas topologías de red.

![alt text](image-298.png "LAN inalámbricas")
![alt text](image-299.png "Puntos de acceso en modo infraestructura")
![alt text](image-300.png "Varios puntos de acceso")

La forma en la que un cliente se asocia a un punto de acceso, consta de cuatro fases:
![alt text](image-301.png "Asociación de cliente a punto de acceso")

El sondeo es el mecanismo que utilizan los clientes para encontrar redes WiFi.
![alt text](image-302.png "Sondeo entre cliente y AP")

Proceso de autenticación entre cliente y AP:
![alt text](image-303.png "Autenticación de cliente y AP")

El último paso es la asociación entre el cliente y el AP permite el enlace de comunicación:
![alt text](image-304.png "Asociación de cliente y AP")

### Planificación WLAN

Alcance óptimo, cantidad máxima de usuarios y potenica del AP, son factores clave en el diseño de la red.

![alt text](image-305.png "Distribución de AP en edificio")

### Amenazas para las Redes WLAN

Existen seis amenazas:

- Buscadores de redes "_abiertas_". Con claves de autenticación débiles.
- Piratas informáticos.
- Empleados.
- MITM (_Man In The Middle_). Usando software capaz de interceptar las comunicaciones.

![alt text](image-306.png "Man In The Middle")

- Denegación de servicio.
- Atacante puede simular un AP y enviar una señal WiFi.

### Protocolos de Seguridad WLAN

![alt text](image-307.png "Evolución de los Protocolos de Seguridad WLAN")

![alt text](image-309.png "Encriptación TKIP, AES")

#### Protocolo de Autenticación Extendida (EAP)

![alt text](image-308.png "Protocolo de Autenticación Extendida EAP")

### Protección de una LAN Inalámbrica

1. La ocultación del SSID.
2. Filtrado de direcciones MAC.
3. Habilitar el cifrado (WPA, WPA2).

![alt text](image-310.png "Protección de redes inalámbricas")

### Práctica configuración red Inalámbrica

[RedWiFi](practicas/redWifi.md)

- - -

## Redes y protocolos WAN

Una **WAN** permite una comunicación geográfica extensa. Operan los proveedores de servicios de Internet.

El modelo de red jerárquico sigue siendo válido. Permite flexibilidad en la red.

![alt text](image-311.png "Modelo jerárquico de la red")

### Operaciones de las Redes WAN

![alt text](image-312.png "WAN en el modelo OSI")

### Capa Física de Redes WAN

Entre los dispositivos WAN están los MODEMS que modulan una señal, entre el cliente y el proveedor de los servicios.

![alt text](image-313.png "Topología de red WAN")

Los estándares para conectores físicos en una red WAN:

- EIA/TIA-232
- EIA/TIA-449/530
- EIA/TIA-612/613 (HSSI)
- V.35
- X.21

### Capa de Enlace de Datos de Redes WAN

Los protocolos más conocidos para acceso a WAN son:

![alt text](image-314.png "Protocolos de redes WAN")

Encapsulación de Capa 2 (enlace de datos) y de Capa 3 (red):

![alt text](image-315.png "Encapsulación de Capa 2")

### Conmutación de Circuitos y de Paquetes

La conmutación de paquetes es como se manejan las telecomunicaciones hoy en día. Divide los datos del tráfico en paquetes que se envían a través de una red compartida.

### Opciones de Conexión WAN

![alt text](image-316.png "Opciones de conexión WAN")

### Conexión de Enlace Dedicado

Enlace Punto a Punto, se utilizan para establecer conexión del cliente a través del proveedor de servicios de Internet.

![alt text](image-317.png "Conexío PPP en cliente a través de Proveedor")

### Conexión por Conmutación de Circuitos

La conexión conmutada y dedicada, sirve para transferencia de datos de bajo volumen. Se realiza a través de la red de telefonía.

![alt text](image-318.png "Conexión por conmutación PSTN")

La red digital de servicios integrados ISDN, es una tecnología de conmutación que permite transportar señales digitales.

![alt text](image-319.png "Red ISDN para señales digitales")

### Conexión por Conmutación de Paquetes

El protocolo X.25 de Capa de Red, proporciona una dirección de red a los suscriptores. Varios canales pueden estar activos en una sola conexión. Las aplicaciones típicas son los lectores de tarjetas de los puntos de venta. En la actualidad este protocolo no es utilizado.

![alt text](image-320.png "Protocolo X.25")

**FRAME RELAY** es similar a **X.25**, aunque es mucho más sencillo, reduce la _latencia_ ya que no realiza control de datos. Permite una conectividad permanente y compartida de ancho de banda mediano, usado para voz y datos.

![alt text](image-321.png "Protocolo Frame-Relay")

Conexión usando tecnología **ATM**:

![alt text](image-322.png "Protocolo ATM")

### Conexión por Internet

![alt text](image-323.png "Conexión por Internet")

La tecnología **DSL** utiliza líneas telefónicas y el par trenzado existente para el envío de datos con gran ancho de banda.

![alt text](image-324.png "Conexión inalámbrica")

La tecnología **WiMAX** utiliza el espectro de radiofrecuencias sin licencia, para enviar y recibir datos.

#### Seguridad en Conexión por Internet

La tecnología **VPN** son redes privadas virtuales que permiten una seguridad en la comunicación entre clientes a través de Internet.

![alt text](image-325.png "Seguridad en Conexión por Internet")

Las **VPN** establecen una comunicación encriptada entre redes privadas a través de una red pública (Internet). Se denominan _Túnel VPN_.

![alt text](image-326.png "Túnel VPN")

Las comunicaciones **VPN** son seguras ya que añaden _autenticación_ y _encriptación_. Los escalables por lo que, se pueden ampliar las conexiones sin invertir en los equipos. Y son compatibles con tecnología de banda ancha.

- - -

## Protocolos PPP

### Comunicación Serial

Las comunicaciones seriales se utilizan mucho, porque las comunicaciones paralelas son emuy efectivas, pero a corta distancia, lo contrario de las comunicaciones seriales.

Otro inconveniente de las comunicaciones paralelas, es que los bits no llegan todos al mismo instante de tiempo. Para lo cual, el transmisor y el receptor deben sincronizarse y comprobar que todos los bits hayan llegado. También se produce interferencias producidas por el conjunto de varios hilos en el cable, conocido como _Cross-Talk_; produciéndose cuando la frecuencia de transmisión es más alta.

#### Conector RS-232

El conector RS-232 existe en 9 pines DB9 y en 25 pines DB25.

![alt text](image-327.png "Conector RS-232")

### TDM

Multiplexación por división de tiempo **TDM** divide un ancho de banda de un solo enlace en canales separados de periodos de tiempo.

![alt text](image-328.png "Multiplexación por división de tiempo TDM")

**TDM** es un concepto de Capa Física, siendo independiente del protocolo de Capa 2.

Algunos ejemplos de **TDM** son: **ISDN** que utiliza TDM-5, **SDH**.

![alt text](image-330.png "Tabla informativa DS")

### STDM

![alt text](image-329.png "STDM")

El concepto **STDM** (División Estadística por División Temporal) corrige la deficiencia de **TDM** de ocupar periodos de tiempo para señales que no están transmitiendo. **STDM** utiliza un periodo de tiempo variable, lo que permite que los canales compitan para obtener el espacio del periodo.

### DTE & DCE

![alt text](image-331.png "DTE & DCE")

### Encapsulación WAN

En una comunicación WAN los datos se encapsulan dentro de las tramas. El protocolo más utilizado es **HDLC** y **PPP** a través de circuitos síncronos y asíncronos.

![alt text](image-332.png "Protocolos en enlaces WAN")

![alt text](image-333.png "Tramas del HDLC")

### Verificación de Interfaces Seriales

![alt text](image-334.png "DCE en conexión serial")

El cable serial con el símbolo del reloj es el DTE (transmite la información e inicia la sincronización), el otro extremo es el DCE, quien recibe la sincronía.

En los router Cisco la encapsulación por defecto es **HDLC**, no obstante, se puede cambiar a **Frame-Relay** o **PPP**.

```cisco
  ...
  R1(config)# interface serial 0/0/0
  R1(config-if)# encapsulation XXX    //donde xxx es: frame-relay, hdlc, ppp
```

Un comando interesenta que ofrece información muy útil es: `show controllers serial 0/0/0`.

### Encapsulación PPP

La encapsulación **PPP** se diseñó, para ser compatible con los hardware de soporte que más se usan.

PPP contiene tres componentes principales:

- **HDLC**. Para la encapsulación de datagramas.
- **LCP**. Es un protocolo de control de enlace extensible para: establecer, configurar y probar la conexión.
- **NCPs**. Familia de protocolos de control de red, para establecer y configurar distintos protocolos en la Capa de Red.

### Arquitectura de Capas PPP

![alt text](image-335.png "Arquitectura de Capas PPP")

La arquitectura de Capas PPP y el Modelo OSI comparten la Capa Física. Aunque el PPP puede usar medios _asíncronos_ y _síncronos_.

### Tramas de PPP

![alt text](image-336.png "Tramas de PPP")

### Inicio de sesión PPP

El inicio de sesión consta de varias fases:

1. Establecer el enlace y negociación de la configuración.
2. Determinación de la calidad del enlace.
3. Negociación de la configuración del protocolo de red.

### Establecimiento de Enlace

El establecimiento de enlace en PPP consta de varios pasos.

![alt text](image-337.png "Pasos en el establecimiento de enlace en PPP")
![alt text](image-338.png "Campos en el paquete LCP")
![alt text](image-339.png "PPP se configura para varias funciones")

### Proceso NCP

En **NCP** se realiza la configuración del protocolo que se está manejando y el envío de datos.

![alt text](image-340.png "Proceso NCP")

### Configuración Básica de PPP

![alt text](image-341.png "Topología para la configuración Básica de PPP")

```ROUTER-1
  ...
  R1# show interface serial 0/0/0

  R1(config)# interface serial 0/0/0
  R1(config-if)# encapsulation PPP
```

Comandos de interes en la configuración de PPP son: `compress predictor / stack`, `ppp quality 1 - 100`, `debug ppp`.

```ROUTER-2
  ...
  R2# show interface serial 0/0/0
  
  R2(config)# interface serial 0/0/0
  R2(config-if)# encapsulation PPP
```

### Autenticación en PPP

El protocolo de autenticación en PPP es **PAP**.

![alt text](image-342.png "Autenticación en PPP")

La autenticación **CHAP** es más segura que PAP.

![alt text](image-343.png "Autenticación en CHAP")

### Autenticación PAP

El nombre de usuario y la contraseña se envían como un paquete de datos LSP, en lugar de que el servidor envío un aviso de inicio de sesión.

![alt text](image-344.png "Autenticación PAP")

El router receptor compara los datos de usuario y clave recibidos con su base de datos.

### Autenticación CHAP

La autenticación CHAP realiza comprobaciones periódicas, para asegurarse que los equipos remotos, todavía poseen una contraseña válida.

![alt text](image-345.png "Autenticación CHAP")

### Cofiguración PAP

Si el router no puede negociar `pap` entonces se pasa a verificar con `chap`, tal como ponemos el orden de la línea  de comandos. Se puede poner una sola opción: `ppp authentication pap`.

```router
  ...
  R1(config)# interface serial 0/0/0
  R1(config-if)# ppp authentication pap chap

  /*------------ Para enviar credenciales ------------- */

  R1(config)# interface serial 0/0/0
  R1(config-if)# ppp pap sent-username R1 password cisco

  /*------------ Para crear la base de datos ------------- */
  
  R1(config)# username R1 password cisco
```

En el router 2 se deben configurar los parámetros tal como se han configurado en el router 2, incluido la creación de la base de datos en el router 2:

```router
  ...
  R2(config)# username R1 password cisco
```

### Configuración CHAP

**CHAO** funciona en tres vías.

```router-R1
  ...
  /*------------ habilitar CHAP ------------*/
  R1(config)# interface serial 0/0/0
  R1(config-if)# ppp authentication chap

  /*------------ crear la base de datos ------------*/
  R1(config)# username R2 password cisco
```

```router-R2
  ...
  /*------------ habilitar CHAP ------------*/
  R2(config)# interface serial 0/0/0
  R2(config-if)# ppp authentication chap

  /*------------ habilitar CHAP ------------*/
  R2(config)# username R2 password cisco
```

- - -

## Frame Relay

El **Frame Relay** hace uso de la red del proveedor para direccionar tráfico.

![alt text](image-346.png "Frame Relay")

Un **DTE** envía una trama al **DCE** a través de los dispositivos del proveedor.

![alt text](image-347.png "Trama entre DTE y DCE en Frame Relay a través de la WAN")

### Circuitos Virtuales

La conexión entre dos **DTE** con **Frame Relay**, se denomina _circuitos virtuales_. Ya que no hay una coneción eléctrica directa, de extremo a extremo.

![alt text](image-348.png "Circuitos virtuales")

Los _circuitos virtuales_, proporcionan una camino entre dispositivos, cuya trama enviada es identificado por un **DLCI** asignado por el proveedor de servicios.

![alt text](image-349.png "Valores DLCI en los dispositivos")

El valor numérico del **DLCI** cambia en la trama en cada uno de los _circuitos virtuales_ distintos.

El **Frame Relay** solo transmite una trama por vez.

![alt text](image-350.png "Distintos caminos virtuales para DLCI")

### Encapsulación Frame Relay

![alt text](image-351.png "Encapsulación Frame Relay")
![alt text](image-352.png "Byte en el segmento Dirección")

### Topologías de Frame Relay

![alt text](image-353.png "Topología en estrella")
![alt text](image-354.png "Topología con distintos circuitos virtuales")
![alt text](image-355.png "Topología de malla completa")

### Tramas LMI

![alt text](image-356.png "Trama FR estándar para una trama LMI")

El **LMI** es como, una trama de actualización utilizado en el Frame Relay.

![alt text](image-357.png "Trama LMI usada dentro del Frame Relay")

### Configuración Básica de Frame Relay

[ConfigBasicFR](practicas/configBasicFR.md)

### Problema del Horizonte Dividido

![alt text](image-358.png "Problema de horizonte dividido")

**Frame Relay** utiliza la técnica de _horizonte dividido_ para evitar _routing loops_. Si un router recibe una actualización por una interfaz, este router no vuelve a reenviar actualizaciones por la misma interfaz, por lo que, al tener una única interfaz física con la nube (Frama Relay) las actualizaciones no volverán a transmitirse a otros posibles routers.

La solución al _horizonte dividido_ es la creación de _subinterfaces_ o _interfaces lógicas_.

![alt text](image-359.png "Interfaces lógicas")

### Subinterfaz Frame Relay Punto a Punto

[SubinterfazFRPaP](practicas/configBasicFR.md)

### Subinterfaz Frame Relay Multipunto

[SubinterfazFRMultipunto](practicas/configBasicFR.md)

### Ejercicio WAN

[EjercicioWAN](practicas/ejercWAN.md)

- - -

## Introducción a seguridad de redes

### Definiciones

- Hacker de sombrero blanco. Encuentra vulnerabilidades e informa para su solución.
- Hacker de sombrero negro. Utiliza vulnerabilidades para beneficio personal.
- Cracker. Obtener permisos no autorizados.
- Phreaker. Manipulación de red de telefonía.
- Spammer. Envío masivo de correos electrónicos.

### Acciones a realizar por un Hacker

1. Realizar un reconocimiento.
2. Enumerar datos.
3. Manipular.
4. Aumentar privilegios.
5. Recopilar contraseñas y secretos.
6. Instalar virus.
7. Potenciar el Sistema comprometido.

### Tipos de delitos

- Abuso del acceso a la red por parte de personas que pertenecen a la organización.
- Virus.
- Robo de dispositivos portátiles.
- Suplantación de identidad.
- Uso indebido de la mensajería instantánea.
- Denegación de servicios.
- Acceso no autorizado a información clasificada.
- Bots dentro de la organización.
- Robo de información de cliente o empleados.
- Abuso de la red inalámbrica.
- Penetración en el sistema.
- Fraude financiero.
- Detección de contraseñas.
- Registro de claves.
- Alteración de sitios web.
- Uso indebido de una aplicación web pública.
- Robo de información patentada.
- Explotación del servidor DNS de una organización.
- Fraude en las telecomunicaciones.
- Sabotaje.

### Políticas de Seguridad

- Informar a los usuarios.
- Especificar mecanismos.
- Establecer una línea base.
- Proteger a las personas y a la información.
- Establecer normas de comportamiento.
- Realizar controles, sondeos e investigaciones.
- Definir y autorizar las consecuencias de violaciones.

Algunos componentes de la política de seguridad son:

- Declaración de autoridad y alcance.
- Políticas de uso aceptable.
- Políticas de identificación y auttenticación.
- Políticas de acceso a internet.
- Políticas de acceso al campus (cuando aplica)
- Políticas de acceso remoto.
- Procedimientos para el manejo de incidencias.
- Políticas de solicitud de acceso a las cuentas.
- Políticas de evaluación de adquisiciones.
- Políticas de auditorías.
- Políticas de confidencialidad de la información.
- Políticas de contraseñas.
- Políticas de evaluación de riesgos.
- Políticas global de servidores Web.
- Políticas del uso de correo electrónico.
- Políticas de Spam.
- Políticas de acceso telefónico.
- Políticas de seguridad de las VPN.

### Amenazas Comunes y Vulnerabilidades

1. Vulnerabilidades. Grado de debilidad inherente a cada red.
   1. Debilidad tecnológica.
   2. Debilidades en la configuración.
   3. Debilidades en la política de seguridad.
   4. Debilidades intrínsecos: protocolo TCP/IP, UDP, S.O.
2. Amenazas. Personas.
3. Ataques. Acciones que explotan las vulnerabilidades.

Amenazas comunes:

- Amenazas de Hardware.
- Amenazas condiciones ambientales.
- Amenazas eléctricas.
- Amenazas al mantenimiento.

Amenazas no estructuradas:

![alt text](image-360.png "Conjunto tipos de amenazas")

### Tipos de Ataques

- Ataque de reconocimiento. Recopila información. Busca brechas de seguridad.
- Ataque de Acceso.
- Ataque de denegación de servicios (DoS). Desactiva determinados servicios. Ejecutar múltiples servicios al mismo instante, consumiendo todo el ancho de banda, impidiendo a los usuarios acceder.
- Virus.
- Gusanos.
- Ransomware.
- Caballos de Troya.

#### Ataque de reconocimiento

Consultar datos, buscar en Internet. Barridos de `ping`. Escaneo de puertos en uso. Usar programas de detección de paquetes como _Wireshark_.

#### Ataques de Acceso

Ataque a la autenticación, a la contraseña. Se puede aplicar la _fuerza bruta_, comprobando gran cantidad de contraseñas en poco tiempo.

La _explotación de confianza_ es básicamente, atacar un dispositivo a través de otro dentro de la red. Por ejemplo, atacar un PC vulnerable, para con éste atacar otro dispositivo dentro de la red.

![alt text](image-361.png "Ataque de explotación de confianza")

La _redirección de puertos_ es un ataque de explotación de confianza, usando un host comprometido para alcanzar los datos a través del firewall.

![alt text](image-362.png "Ataque de redirección de puertos")

El ataque _hombre en el medio_ (_Man in the middle_) es aquel en el que un atacante, está situado entre el servidor y un cliente. Un método popular, es el uso de un Proxy transparente.

![alt text](image-363.png "Ataque Man in the middle")

#### Ataques de Denegación de Servicios (DoS)

El DoS principal es el _ping de la muerte_. Envío repetido del `ping dir-IP-víctima` de más de 65 kBytes.

Otro ataque _DoS_ es la saturación de sincronización. Envío de gran cantidad de peticiones de sincronización del protocolo de tres vías TCP/IP. El servidor, trata de responder a todas las peticiones hasta la saturación, no atendiendo a los usuarios legítimos o colapsar.

![alt text](image-364.png "Ataque DoS de tres vías TCP/IP")

Los ataques _DDoS_ son ataques distribuidos, haciendo uso de _Bots_ envían el ataque sincronizado: al mismo tiempo y al mismo dispositivo.

![alt text](image-365.png "Ataque DDoS")

#### Ataques de Códigos Maliciosos

- Los _gusanos_ ejecutan un código arbitrario e instalan copias de si mismos.
- Los _virus_ son códigos maliciosos que ejecuta una función particular no deseada.
- Los _troyanos_ tiene la apariencia de otra cosa, aunque es una herramienta de ataque.

### Técnicas de Mitigación

- Antivirus.
- Firewall.
- Parches y actualizaciones del IOS.

### Rueda de Seguridad de la Red

Herramienta eficaz, consta de cuatros pasos:

- _Asegurar_, la red mediante la aplicación de defensas y la inspección de paquetes.
- _Controlar_, la seguridad con métodos activos (auditorías) y pasivos (acciones tomodas por los dispositivos)
- _Probar_, las medidas de seguridad con actividades programas y verificar su eficacia.
- _Mejorar_. Se deben implementar las las mejoras teniendo en cuenta la evolución de la técnica.

### Seguridad de los Routers

Los routers conectados a Internet son más vulnerables que los conectados en redes LAN. Por lo que estos routers, deben tener unas características de seguridad aplicadas.

- Seguridad física.
- Actualización del IOS.
- Copia de seguridad.
- Aseguramiento de los puertos y servicios no utilizados.

#### Seguridad del IOS de Cisco

1. Administrar la seguridad del router.
2. Proteger el acceso administrativo remoto a los routers.
3. Registrar la actividad del router.
4. Proteger los servicios y las interfaces vulnerables del router.
5. Proteger los protocolos de enrutamiento.
6. Controlar y filtrar el tráfico de la red.

### Configuración Básica Seguridad de Routers

[ConfigBasicSeguridadRouter](practicas/confSegurRouter.md)

### Servicios Vulnerables en Routers

Los servicios que deben desactivarse en un Router:

![alt text](image-366.png "Lista de servicios que deben desactivarse en un Router")
![alt text](image-367.png "Vulnerabilidad de algunos protocolos")

### Protección de Protocolos Dinámicos

Interrupción de pares permite que los protocolos se reparen así mismos. Lo que cause que la interrupción dure poco tiempo. Hacer que los protocolos pierdan su comunicación de información. No es un fallo tan crítico.

![alt text](image-368.png "Protección protocolos dinámicos")

La falsificación de información de enrutamiento. Se lleva a cabo cuando un hacker penetra la red y logra difuminar información de enrutamiento, con el objetivo de provocar un ataque DoS. Por ejemplo, al crear rutas falsas, se pueden generar un _routing-loop_. Para mitigar esta amenaza, se recomienda habilitar la autenticación de protocolo, de forme que; cuando los routers reciben información de enrutamiento solo se acepta la información debidamente autenticada.

![alt text](image-369.png "Topología protección protocolos")

### Configuración Autenticación del Protocolo RIP

[ConfigAutenticProtolRIP](practicas/confAutenProtolRIP.md)

### Backup de Configuración y Flash

[ConfBackupFlas](practicas/confBackupFlash.md)

- - -

## Access Lists

### Filtrado de Paquetes

El filtrado de paquetes controla el acceso a la red, analizando los paquetes de entrada y de salida. Se realiza en la Capa 3 de Red.

![alt text](image-370.png "Filtrado de paquetes en Capa 3")

Diagrama del filtrado de paquetes que depende de la Capa de Red. Con el filtrado, se puede saber a que red pertenece y a que red va destinado el paquete.

![alt text](image-371.png "Diagrama del filtrado de paquetes")

### Listas de Acceso (ACL)

Un _Router_ permitirá o denegará los paquetes, según el criterio encontrado en el encabezado del paquete. Los _Routers_ no tienen configurado ninguna **ACL**. Las pautas para configurar las **ACL** son:

- Configurar ACL en routers firewall en la red interna y externa.
- Configurar ACL en routers en la red interna.
- Configurar ACL en routers situados en el extremo de la red.

![alt text](image-372.png "Ejemplo de ACL en routers según la topología")

| Las tres P para utilizar ACL |
| :--- |
| Sólo puede tener una ACL por protocolo, por interfaz y por dirección |
| Una ACL por protocolo, p. ej. IP o IPX |
| Una ACL por interfaz, p. ej. fastEthernet 0/0 |
| Una ACL por dirección, ENTRADA o SALIDA |

### Funcionamiento de las ACLs

Las ACL definen el conjunto de reglas de control adicional a los paquetes de datos que ingresan en las interfaces de entrada. Las ACL no actúan sobre los paquetes que se generan en el mismo router, sólo se aplican al tráfico entrante y saliente.

![alt text](image-373.png "Diagrama de bloques control del ACL sobre los paquetes de entrada")
![alt text](image-374.png "Diagrama de bloques control del ACL sobre los paquetes de salida")

Si un paquete no coincide con ninguna de las ACL se bloquea automáticamente.

![alt text](image-375.png "Diagrama de bloques ACL entrada/salida")

### Tipos de ACLs

Hay dos tipos de ACL:

- Las ACL estándar: permiten autorizar o denegar el tráfico desde las direcciones IP de origen. No importa el destino del paquete, ni los puertos involucrados. La sintaxis es de la forma `access-list 10 permit 192.168.30.0 0.0.0.255`.
- Las ACL extendidas: filtran los paquetes en función de varios atributos, como tipo de protocolo, dirección IP de origen o destino, TCP, UDP, etc. La sintaxis es de la forma `access-list 103 permit tcp 192.168.30.0 0.0.0.255 any eq 80`.

### Numeración y Denominación de ACLs

Las ACLs numeradas es eficaz para determinar el tipo de ACLs en las redes pequeñas. Es posible utilizar un nombre para identificar la ACLs.

![alt text](image-376.png "ACL numeradas y ACL denominada")

### Ubicación de las ACLs

Las ACLs pueden actuar como _firewall_ filtrando y eliminando paquetes no deseados. El lugar de ubicación de la ACL puede reducir el tráfico innecesario. Las reglas básicas para elegir el sitio idóneo en la red son:

- Las ACL extendidas, ubicarlas lo más cerca posible del tráfico denegado.
- Las ACL estándar, ubicarlas lo más cerca al destino.

![alt text](image-377.png "Ubicación de las ACL extendida y estándar")

### Configuración Básica de ACLs Estándar

[ConfigACLestandar](practicas/confBasACLestand.md)

### Ubicación de ACLs Estándar

[ConfigACLestandUbic](practicas/confACLestandUbi.md)

### Cofiguración de ACL para Acceso Remoto

[ConfACLaccessRemot](practicas/confACLaccesRemot.md)

### Configuración ACL Estándar Nombrada y Edición de ACLs Estándar

[ConfACLestNomEdicEst](practicas/confACLestNomEdiACLest.md)

### ACLs Extendidas & ACLs Estándar

Las **ACLs Extendidas** se utilizan más que las **ACLs Estándar**, ya que proporcionan mayor control y seguridad. Se pueden usar **ACLs Extendidas Numeradas** del 100 al 199 y del 2000 al 2699. Tanto las **ACL Extendidas** como las **ACL Estándar** verifican la dirección de origen del paquete, y también la dirección destino, los protocolos, número de puerto y servicio.

![alt text](image-378.png "Diagrama de bloques de ACLs Extendida")

### Configuración Básica ACLs Extendida

[ConfBasACLexte](practicas/confACLextendidas.md)

### ACLs Complejas

Existen tres tipos de **ACLs Complejas**:

- **ACL dinámicas** (de bloqueo). Los usuarios que deseen atravesar el router son bloqueados hasta que utilizan _Telnet_ para conectarse al router y son autenticados.
- **ACL reflexivas**. Permiten el tráfico saliente y limitan el tráfico entrante como respuesta a sesiones que se originan dentro del router.
- **ACL basadas en tiempo**. Permiten el control de acceso según la hora del día y la semana.

### ACLs Dinámicas

![alt text](image-379.png "ACLs Dinámicas")

#### Configuración de ACLs Dinámicas

![alt text](image-380.png "Topología de ACLs Dinámicas y 4 pasos para configurar")

### ACLs Reflexivas

Las **ACLs Reflexivas** se utilizan para permitir el tráfico IP en sesiones que se originan en subred, y al mismo tiempo denegar el tráfico IP en sesiones que se originan fuera de la red.

![alt text](image-381.png "ACLs Reflexivas")

Las ACLs Reflexivas, solo contienen entradas personales. Estas entradas personales se crean cuando se inicia una sesión IP. Estas entradas personales se eliminan automáticamente al finalizar la sesión.

#### Configuración de ACLs Reflexivas

![alt text](image-382.png "Topología de referencia para configurar ACLs Reflexivas")
![alt text](image-383.png "Pasos para configurar ACLs Reflexivas")

### ACLs Basadas en el Tiempo

![alt text](image-384.png "ACLs Basadas en el Tiempo")

#### Configuración de ACLs Basadas en el Tiempo

![alt text](image-385.png "Topología para configurar ACLs Basadas en el Tiempo e indicación de los 3 Pasos")

### Ejercicio Práctico configura de ACLs

[EjercPractACLs](practicas/)

- - -

## Fundamentos sobre la comunicación remota

### Trabajo a Distancia

El trabajo a distancia presenta los siguientes beneficios:

- Organizativos.
- Sociales.
- Ambientales.

### Componentes para el Trabajo a Distancia

![alt text](image-386.png "Distintas tecnologías para el trabajo remoto")
![alt text](image-387.png "Comunicación por VPN encriptada entre remoto y Empresa")

### Transmisión por Cable

![alt text](image-388.png "Servicio por cable")
![alt text](image-389.png "Módem por cable en Operador y en Cliente")

### DSL

Existen dos tipos de tecnología DSL: _asimétrica_ **ADSL** y _simétrica_ **SDSL**.

- ADSL: ofrece mayor BW de descarga.
- SDSL: ofrece la misma capacidad en subidas y en descargas.

DSLAM es el dispositivo final ubicado en el Proveedor, que conecta los distintos subcriptores DSL.

![alt text](image-390.png "DSLAM en el Proveedor del servicio")
![alt text](image-391.png "ADSL conexión de líneas analógicas para teléfonos")
![alt text](image-392.png "Voz y datos por el mismo cable de cobre")

### Conexiones Inalámbricas

- WiFi.
- WiMAX.
- Internet Satélite.

![alt text](image-393.png "Conexiones inalámbricas WAN en topología malla")

### Redes VPN

![alt text](image-394.png "Redes VPN")
![alt text](image-395.png "Redes VPN WAN Capa 2 tradicional")

### Tipos de VPN

![alt text](image-396.png "VPN Sitio-Sitio")
![alt text](image-397.png "VPN de Acceso Remoto")

### Componentes de VPN

Crean una red privada a través de una red pública. Utilizan túneles criptográficos para encriptar los paquetes.

![alt text](image-398.png "Componentes en las VPN")

### Características de una VPN

| Caracteristicas | Propósitos |
| --- | --- |
| Confidencialidad de datos | Protege los datos contra personas que pueden ver o escuchar subrepticiamente información confidencial _spoofing_ |
| Integridad de datos | Garantiza que no se realicen cambios indebidos ni alterados en los datos |
| Autenticación | Garantiza que sólo ingresen en la red emisores y dispositivos autorizados |

### Tunneling

Se utiliza la red pública de Internet para la comunicación de datos de forma segura.

![alt text](image-399.png "Tunneling de VPN")

El _tunneling_ encapsula un paquete entero, dentro de otro paquete. Usa tres tipos diferentes de protocolos para la transferencia de datos: protocolo de portador, de encapsulación y un protocolo pasajero.

### Integridad de Datos en VPNs

Para mantener la integridad de los datos, es necesario encriptarlos.

![alt text](image-400.png "Diagrama de bloques encriptación de datos")

El grado de seguridad que proporciona un algoritmo de encriptación depende de la longitud de la clave. Algunos algoritmos de cifrado: DES, 3DES, AES, RSA. Se pueden clasificar en dos tipos:

- Cifrado Simétrico.
- Cifrado Asimétrico.

![alt text](image-401.png "Criptografía de clave privada o cifrado simétrica")
![alt text](image-402.png "Encriptación de clave asimétrico o clave pública")

#### HASH

Los _Hash_ contribuyen a la autenticación y la integridad de los datos. El _Hash_ es un número generado a partir de una cadena de texto. El _Hash_ producido a partir de un texto, es únnico.

![alt text](image-403.png "Ejemplo de HASH enviado y recibido para determinar integridad")

En redes VPN se debe autenticar, el dispositivo que está al otro lado del extremo, antes que el túnel de comunicación se considere seguro.

![alt text](image-404.png "Esquema integridad de datos en túnel VPN con RSA o PSK")

### IPSex

Existen dos protocolos de estructura IPSec:

- Encabezado de autenticación.
- Contenido de seguridad encapsulado.

![alt text](image-405.png "Ejemplo de protocolos en IPsec")

El IPsec se basa en algoritmos existentes:

![alt text](image-406.png "Algoritmos de encriptación usados en IPsec")

- - -

## Servicios para redes LAN

- - -

## Redes desde cero

### IP (Protocolo Internet)

Asignado a cada dispositivo de la red. Existen IPv4 e IPv6.

- IPv4. Notación decimal conformado por 32 bits separados por cuatro octetos.
- IPv6. Notación hexadecimal conformado por 128 bits en ocho grupos, separados por dos puntos y cada grupo formado por cuatro dígitos hexadecimal o por 16 bits.

![alt text](image-32.png)

- Las IP públicas se asignan a equipos conectados directamente a Internet, siendo accesibles desde cualquier parte de la red WAN.
- Las IP públicas son únicas y no pueden repetirse.

### IP Dinámicas y Estáticas

Las **Dinámicas**:

- Es una IP que se asigna termporalmente a un dispositivo cada ves que se conecta a una red.
- Lo hace un servidor DHCP.
- Redes Domésticas, oficinas pequeñas e ISP.

Las **Estáticas**:

- Es una IP fija y permanente asignada a un dispositivo de la red.
- Puede ser asignada manualmente.
- Servidorres, Routers, Impresoras.

#### Conocer la IP en Windows

En la consola CMD escribir: _ipconfig /all_.

#### Conocer la IP en Linux

En el Terminal escribir: _ifconfig_.

### NAT (Network Address Translation)

Las IPv4 están agotadas, aunque se siguen usando gracias al NAT.

El **NAT**, es una técnica que se utiliza para traducir direcciones IP de un dominio de direcciones a otro. Su principal función es permitir que múltiples dispositivos en una red privada utilicen una única IP pública.

- Escasez de direcciones IPv4.
- Seguridad.
- Facilita de gestión de la red.
- Facilita la conexión de múltiples dispositivos en una red privada utilicen la conexión a Internet.
- Permite la compatibilidad con IPv6.

### Gateway o Puerta de Enlace

Es un dispositivo de red que actúa como un punto de acceso o puerta entre una red LAN y otra red WAN. Los Routers tienen la puerta de enlace integrada.

### Direcciones MAC

Es un identificador único asignado a cada interfaz de red que se conecta a una red de área local LAN. Las direcciones **MAC** están compuestas por números hexadecimales con un total de 48 bits.

#### Protocolo ARP (Address Resolution Protocol)

El **ARP** es el que vincula una dirección IP con una dirección **MAC** dentro de una red local. El dispositivo intermedio _Switch_ es el que lo realiza.

### TCP (Transmission Control Protocol)

Proporciona un servicio de comunicación orientado a la conexión y garantiza la entrega ordenada y confiable de paquetes.

Se utiliza en aplicaiones donde la precisión y la fiabilidad son críticas, como la navegación web, el correo electrónico y la transferencia de archivos.

#### Three-Way Handshake

Es un proceso utilizado por el protocolo TCP para establecer una conexión confiable entre un cliente y un servidor en una red.

![alt text](image-33.png)

Este protocolo asegura los paquetes y la confiabilidad.

### UDP (User Datagram Protocol)

Proporciona un servicio de comunicación no orientado a la conexión y sin garantía de entrega o orden de paquetes.

Se utiliza en aplicaciones donde la velocidad y la eficiencia son más importantes que la fiabilidad, como transmisiones de video en tiempo real y juegos en línea.

### Protocolos y puertos más comunes en Ciberseguridad

| TCP | UDP |
| --- | --- |
| FTP (21) | DNS (53) |
| SSH (22) | DHCP (67, 68) |
| Telnet (23) | TFTP (69) |
| SMTP (25) | SNMP (16) |
| DNS (53) | |
| HTTP (80) / HTTPS (443) | |
| POP3 (110) | |
| SMB (129, 445) | |
| IMAP (143) | |

### Subneteo

Es un proceso en la administración de redes que implica dividir una red IP más grande en subredes más pequeñas.

#### Práctica Subneteo

En Linux en Terminal, escribir el comando _ipconfig_. Vemos la IPv4 y la máscara de red. Esta marca indica que bit se destinan a los hosts y que parte se destinan a las redes. Los bits que tienen el valor de 1 lógico están destinados a la red, y los bits a 0 lógico están destinados a los hosts.
