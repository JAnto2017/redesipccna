# PRÁCTICA CON RIP - EIGRP - STATIC

![alt text](image-13.png)
![alt text](image-14.png)
![alt text](image-15.png)

## SECCIÓN RIP

Configuración del router WEST_1:

```WEST_1
    ...
    (configure)# router rip
    (config-router)# version 2
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# passive-interface serial 0/1/0
    (config-router)# passive-interface serial 0/1/1

    (config-router)# do show run

    (config-router)# network 172.16.0.1
    (config-router)# network 10.10.10.0
    (config-router)# network 30.30.30.4
```

Configuración del router WEST_2:

```WEST_2
    ...
    (configure)# router rip
    (config-router)# version 2
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0

    (config-router)# do show run

    (config-router)# network 172.16.1.0
    (config-router)# network 10.10.10.4
```

Configuración del router WEST_3:

```WEST_3
    ...
    (configure)# router rip
    (config-router)# version 2
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0

    (config-router)# do show run

    (config-router)# network 172.16.2.0
    (config-router)# network 10.10.10.8
```

Configuración del router WEST_4:

```WEST_4
    ...
    (configure)# router rip
    (config-router)# version 2
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0
    
    (config-router)# do show run

    (config-router)# network 172.16.3.1
    (config-router)# network 10.10.10.13

    (config-router)# do show run
```

## SECCION EIGRP

Configuración del router EAST_1:

```EAST_1
    ...
    (configure)# router EIGRP 25
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# passive-interface SERIAL 0/1/0
    (config-router)# passive-interface SERIAL 0/1/1

    (config-router)# do show run

    (config-router)# network 192.168.64.0   0.0.1.255   //WILD-CARD
    (config-router)# network 20.20.20.0     0.0.0.3     //WILD-CARD
    (config-router)# network 20.20.20.14    0.0.0.3     //WILD-CARD
    (config-router)# network 30.30.30.0     0.0.0.3     //WILD-CARD
    (config-router)# network 30.30.30.10    0.0.0.3     //WILD-CARD

    (config)# WRITE
```

Configuración del router EAST_2:

```EAST_2
    ...
    (configure)# router EIGRP 25
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# passive-interface fastEthernet 0/1

    (config-router)# do show run

    (config-router)# network 192.168.20.33  0.0.0.7   //WILD-CARD
    (config-router)# network 192.168.20.41  0.0.0.7   //WILD-CARD
    (config-router)# network 20.20.20.5     0.0.0.3   //WILD-CARD
    (config-router)# network 20.20.20.2     0.0.0.3   //WILD-CARD
    (config-router)# network 20.20.20.18    0.0.0.3   //WILD-CARD

    (configure)# show ip protocols
    (configure)# show ip eigrp topology
```

Configuración del router EAST_3:

```EAST_3
    ...
    (configure)# router EIGRP 25
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# passive-interface fastEthernet 0/1

    (config-router)# do show run

    (config-router)# network 192.168.30.1   0.0.0.15    //WILD-CARD
    (config-router)# network 192.168.30.17  0.0.0.15    //WILD-CARD
    (config-router)# network 20.20.20.9     0.0.0.3     //WILD-CARD
    (config-router)# network 20.20.20.6     0.0.0.3     //WILD-CARD

    (configure)# show ip protocols
    (configure)# show ip eigrp topology
```

Configuración del router EAST_4:

```EAST_4
    ...
    (configure)# router EIGRP 25
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# passive-interface fastEthernet 0/1

    (config-router)# do show run

    (config-router)# network 192.168.40.128     0.0.0.31    //WILD-CARD
    (config-router)# network 192.168.40.161     0.0.0.31    //WILD-CARD
    (config-router)# network 20.20.20.13        0.0.0.3     //WILD-CARD
    (config-router)# network 20.20.20.10        0.0.0.3     //WILD-CARD
    (config-router)# network 20.20.20.22        0.0.0.3     //WILD-CARD

    (configure)# show ip protocols
    (configure)# show ip eigrp topology
```

Configuración del router EAST_5:

```EAST_5
    ...
    (configure)# router EIGRP 25
    (config-router)# no auto-summary
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# passive-interface fastEthernet 0/1

    (config-router)# do show run

    (config-router)# network 192.168.50.128     0.0.0.63    //WILD-CARD
    (config-router)# network 192.168.50.192     0.0.0.63    //WILD-CARD
    (config-router)# network 20.20.20.16        0.0.0.3     //WILD-CARD
    (config-router)# network 20.20.20.20        0.0.0.3     //WILD-CARD
```

## OPTIMIZACIÓN DE EIGRP

Sumarización en las redes WAN del router EAST_2:

```EAST_2
    ...
    (configure)# interface serial 0/0/0
    (config-if)# summary-address eigrp 25 192.168.20.32 255.255.255.240
    (config-if)# exit

    (configure)# interface serial 0/0/1
    (config-if)# summary-address eigrp 25 192.168.20.32 255.255.255.240
    (config-if)# exit

    (configure)# interface serial 0/1/0
    (config-if)# summary-address eigrp 25 192.168.20.32 255.255.255.240
    (config-if)# exit

    (configure)# show ip eigrp topology
```

Sumarización en las redes WAN del router EAST_3:

```EAST_3
    ...
    (configure)# interface serial 0/0/0
    (config-if)# summary-address eigrp 25 192.168.30.0 255.255.255.224
    (config-if)# exit

    (configure)# interface serial 0/0/1
    (config-if)# summary-address eigrp 25 192.168.30.0 255.255.255.224
    (config-if)# exit

    (config)# write
```

Sumarización en las redes WAN del router EAST_4:

```EAST_4
    ...
    (configure)# interface serial 0/0/0
    (config-if)# summary-address eigrp 25 192.168.40.128 255.255.255.192
    (config-if)# exit

    (configure)# interface serial 0/0/1
    (config-if)# summary-address eigrp 25 192.168.40.128 255.255.255.192
    (config-if)# exit

    (configure)# interface serial 0/1/1
    (config-if)# summary-address eigrp 25 192.168.40.128 255.255.255.192
    (config-if)# exit
```

Sumarización en las redes WAN del router EAST_5:

```EAST_5
    ...
    (configure)# interface serial 0/1/0
    (config-if)# summary-address eigrp 25 192.168.50.128 255.255.255.128
    (config-if)# exit

    (configure)# interface serial 0/1/1
    (config-if)# summary-address eigrp 25 192.168.50.128 255.255.255.128
    (config-if)# exit
```

## CONFIGURACIÓN DEL ENRUTAMIENTO ESTÁTICO

Configuración del router WEST_1

```WEST_1
    ...
    (config)# configure terminal
    (config)# ip route 192.168.0.0 255.255.0.0 30.30.30.1

    (config)# ip route 0.0.0.0 0.0.0.0 serial 0/1/0

    (config)# ip route 20.20.20.0 255.255.255.224 30.30.30.1
```

Configuración del router EAST_1

```EAST_1
    ...
    (config)# configure terminal
    (config)# ip route 172.16.0.0 255.255.252.0 30.30.30.2

    (config)# ip route 10.10.10.0 255.255.255.240 30.30.30.2    //240=11110000 abarca 16 subredes

    (config)# ip route 0.0.0.0 0.0.0.0 serial 0/1/1

    //para publicar red EIGRP
    (config)# router eigrp 25
    (config-router)# redistribute static
```

Configuración del router ISP

```ISP
    ...
    (config)# configure terminal
    (config)# ip route 172.16.0.0 255.255.252.0 30.30.30.6

    (config)# ip route 10.10.10.0 255.255.255.240 30.30.30.6

    (config)# ip route 192.168.0.0 255.255.0.0 30.30.30.10

    (config)# ip route 20.20.20.0 255.255.255.224 30.30.30.10
```
