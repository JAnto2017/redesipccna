# PRÁCTICA CONFIGURACIÓN BÁSICA OSPFv3

![alt text](image-26.png)

Se utiliza IPv6 en los dispositivos.

## R1

```R1
    ...
    (config)# ipv6 router ospf 1
    (config-rtr)# router-id 1.1.1.1
    (config-rtr)# passive-interface fastEthernet 0/0
    (config-rtr)# exit

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit
```

## R2

```R2
    ...
    (config)# ipv6 router ospf 1
    (config-rtr)# router-id 2.2.2.2
    (config-rtr)# passive-interface fastEthernet 0/0
    (config-rtr)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    # show run
    # show ipv6 route

    (config)# ipv6 router ospf 1
    (config-rtr)# default-information originate

    # show run
    
```

## R3

```R3
    ...
    (config)# ipv6 router ospf 1
    (config-rtr)# router-id 3.3.3.3
    (config-rtr)# passive-interface fastEthernet 0/0
    (config-rtr)# exit

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 ospf 1 area 0
    (config-if)# exit

    # show ipv6 route
```
