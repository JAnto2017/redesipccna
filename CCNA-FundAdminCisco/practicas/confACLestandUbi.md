# CONFIGURAR ACL ESTÁNDAR - UBICACIÓN

![alt text](image-86.png "Topología para configurar ubicación de ACL estándar")

## Objetivos

1. Denegar red 192.168.10.0/25 en la sección azul.
2. Denegar host 192.168.10.200 a la sección verde.

## Respuesta

¿Dónde configurar el ACL? ¿En qué Router y cuál interfaz elegir?

```router
    ...
    R1(config)# access-list 50 deny 192.168.10.0 0.0.0.127      //denegar mitad sección amarilla
    R1(config)# access-list 50 permit any
    R1(config)# access-list 50 deny host 192.168.10.200         //denegar un host

    R1(config)# interface serial 0/0/0
    R1(config-if)# ip access-group 50 out
```

La solución planteada presenta problemas, ya que el ACL ejectura línea de código de forma consecutiva y descendente; en la segunda línea de código permite todo y en la tercera restringe, pero la trama ya ha pasado.

1. access-list 50 deny 192.168.10.0 0.0.0.127
2. access-list 50 permit any
3. access-list 50 deny host 192.168.10.200

```router
    ...
    R1(config)# no access-list 50
    
    R1(config)# access-list 50 deny 192.168.10.0 0.0.0.127
    R1(config)# access-list 50 deny host 192.168.10.200
    R1(config)# access-list 50 permit any

    R1(config)# interface serial 0/0/0
    R1(config-if)# ip access-group 50 out

    R1# show run        //muestra la configuración y el orden de las ACLs
```

Alterar la secuencia de comandos, tampoco funciona bien.

La ACL es mejor configurarla lo más cerca posible al destino. La mejor ubicación es colocarlo en el router R3, en las interfaces de sus LAN haci: sección azul y sección verde.

```router
    ...
    R1(config)# no access-list 50
    R1(config)# interface serial 0/0/0
    R1(config-if)# no ip access-group 50 out
```

```router
    ...

    /*** denegar red 192.168.10.0/25 a la sección azul ***/

    R3(config)# access-list 50
    R3(config)# access-list 50 deny 192.168.10.0 0.0.0.127
    R3(config)# access-list 50 permit any

    R3(config)# interface fastEthernet 0/1                  //interfaz hacia sección azul
    R3(config-if)# ip access-group 50 out

    /*** denegar host 192.168.10.200/24 a la sección verde ***/

    R3(config)# access-list 55 deny host 192.168.10.200
    R3(config)# access-list 55 permit any

    R3(config)# interface fastEthernet 0/0                  //interfaz hacia sección verde
    R3(config-if)# ip access-group 55 out
```
