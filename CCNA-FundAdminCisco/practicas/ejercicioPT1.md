# EJERCICIO PRÁCTICO PT 1 - PROTOCOLOS DINÁMICO OSPF

![alt text](image-22.png)
![alt text](image-23.png)

Ruta estática desde EDGE hacia ISP.

![alt text](image-24.png)

## Sección color amarillo

### R1

Configuración del OSPF básico.

```R1
    ...
    (config)# router ospf 10
    (config-router)# network 192.168.0.0 0.0.0.255 area 1
    (config-router)# network 172.20.20.0 0.0.0.15 area 1
    (config-router)# network 3.3.3.0 0.0.0.3 area 1
    (config)# write

    //colocar interfaces pasivas hacia las LAN externas
    (config)# router ospf 10
    (config-router)# passive-interface fastEthernet 0/0

    # show ip ospf neighbor
    (config)# router ospf 10
    (config-router)# router-id 1.0.0.1
    # clear ip ospf process
```

### R2

```R2
    ...
    (config)# router ospf 10
    (config-router)# network 192.168.1.0 0.0.0.255 area 1
    (config-router)# network 172.20.20.0 0.0.0.15 area 1
    (config-router)# network 3.3.3.0 0.0.0.3 area 1
    # write

    //colocar interfaces pasivas hacia las LAN externas
    (config)# router ospf 10
    (config-router)# passive-interface fastEthernet 0/0

    # show ip ospf neighbor
    (config)# router ospf 10
    (config-router)# router-id 2.0.0.2
    # clear ip ospf process

    //configuración de una prioridad
    (config)# interface fastEthernet 0/1
    (config-if)# ip ospf priority 200
    # write
```

### R3

```R3
    ...
    (config)# router ospf 10
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# network 192.168.2.0 0.0.0.255 area 1
    (config-router)# network 172.20.20.0 0.0.0.15 area 1

    # show ip ospf neighbor
    (config)# router ospf 10
    (config-router)# router-id 3.0.0.3
    # clear ip ospf process

    //configuración de una prioridad
    (config)# interface fastEthernet 0/1
    (config-if)# ip ospf priority 100
```

### R4

```R4
    ...
    (config)# router ospf 10
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# network 192.168.3.0 0.0.0.255 area 1
    (config-router)# network 172.20.20.0 0.0.0.15 area 1

    # show ip ospf neighbor
    (config)# router ospf 10
    (config-router)# router-id 4.0.0.4
    # clear ip ospf process
```

### Pruebas de conectividad

- [X] R4# `ping 3.3.3.2`. Desde R4 hacia R1.
- [X] R4# `ping 192.168.2.1`. Desde R4 hacia una LAN.

- - -

## Sección color celeste

### Router R-A

```R-A
    ...
    (config)# router ospf 20
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# network 172.30.64.0 0.0.0.127 area 2
    (config-router)# network 10.10.10.0 0.0.0.3 area 2
    (config-router)# network 10.10.10.8 0.0.0.3 area 2
    (config-router)# network 2.2.2.0 0.0.0.3 area 2
```

### Router R-B

```R-B
    ...
    (config)# router ospf 20
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# network 172.30.65.0 0.0.0.255 area 2
    (config-router)# network 10.10.10.4 0.0.0.3 area 2
    (config-router)# network 10.10.10.0 0.0.0.3 area 2

    # show ip ospf neighbor //para ver si se creo la adyacencia hacia el vecino
```

### Router R-C

```R-C
    ...
    (config)# router ospf 20
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# network 172.30.64.128 0.0.0.127 area 2
    (config-router)# network 10.10.10.8 0.0.0.3 area 2
    (config-router)# network 10.10.10.4 0.0.0.3 area 2

    # show ip ospf neighbor //para ver si se creo la adyacencia hacia el vecino
```

- - -

## Router EDGE

Este router está entre los dos bloques de color amarillo y celeste. Tendrá dos procesos OSPF.

```EDGE
    ...
    (config)# router ospf 10
    (config-router)# network 3.3.3.0 0.0.0.3 area 1    //adyacencia zona amarilla
    
    (config)# router ospf 20
    (config-router)# network 2.2.2.0 0.0.0.3 area 1    //adyacencia zona celeste

    (config)# ip route 0.0.0.0 0.0.0.0 serial 0/0/0     /*ruta por defecto*/

    (config)# router ospf 20
    (config-router)# default-information originate

    (config)# router ospf 10
    (config-router)# redistribute static subnets
```

## ISP

```ISP
    (config)# ip route 192.168.0.0 255.255.252.0 1.1.1.2
    (config)# ip route 172.20.20.0 255.255.255.240 1.1.1.2
    (config)# ip route 172.30.64.0 255.255.254.0 1.1.1.2
    (config)# ip route 10.10.10.0 255.255.255.240 1.1.1.2
    (config)# ip route 2.2.2.0 255.255.255.252 1.1.1.2
    (config)# ip route 3.3.3.0 255.255.255.252 1.1.1.2
```
