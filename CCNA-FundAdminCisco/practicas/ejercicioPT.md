# EJERCICIO PT

![alt text](image-7.png)

Configuración del Router OESTE3:

```OESTE_3
    # show ip router //para verificar tabla de ruteo

    //añadir dirección IP de red
    # configure terminal
    (config)# router rip
    (config-router)# network 192.168.40.0
    (config-router)# end

    //verificación de la configuración
    # show run

    //verificación de la tabla de ruteo
    # show ip route

    //para guardar configuración en el Router
    # write
```

Configuración del Router OESTE2:

```OESTE_2
    # show ip route

    # show ip protocols

    # show running-config 

    # config terminal
    (config)# router rip
    (config-router)# no passive-interface serial 0/0/0
    (config-router)# no passive-interface serial 0/0/1
    (config-router)# end

    # show ip protocols
```

Configuración del Router OESTE1:

```OESTE_1
    # config terminal
    (config)# router rip
    (config-config)# network 10.10.10.0     //WAN hacia otros Routers
    (config-router)# network 10.10.10.4
    (config-router)# end

    # show running-config 
```

Configuración del Router CENTRAL:

```CENTRAL
    # show ip protocols

    # config terminal
    (config)# router rip
    (config-router)# version 2
    (config-router)# end

    # show ip protocols

    //crear ruta por defecto al Router ISP (salida de Internet)
    # config terminal
    (config)# ip route 0.0.0.0 0.0.0.0 1.1.1.1
    (config)# end

    //No podemos crear una ruta por defecto al Router OESTE1 ya que está dentro de la red 
    # config terminal
    (config)# no ip route 0.0.0.0 0.0.0.0 serial 0/1/1
    (config)# end
```

Configuración de rutas estáticas desde el Router CENTRAL hacia los Routers OESTE3, OESTE1 y OESTE2:

```CENTRAL
    # config terminal
    (config)# ip route 192.168.0.0 255.255.255.0 serial 0/1/1

    //Las dos redes 192.168.1.125 /25 y 192.168.1.0 /25 se pueden agrupar en una sola red. Para ello: ip route 192.168.1.0 255.255.255.0 serial 0/1/1; en lugar de las dos líneas de código siguientes:

    (config)# ip route 192.168.1.0 255.255.255.128 serial 0/1/1
    (config)# ip route 192.168.1.128 255.255.255.128 serial 0/1/1
    
    //configuración hacia Router OESTE 3. Agrupamos ambas redes: 192.168.32.0/21 y 192.168.40.0/21 en una IP/20.

    # config terminal
    (config)# ip route 192.168.32.0 255.255.240.0 serial 0/1/1
    (config)# end

    //crear ruta a la red 10.10.10.0/28
    # config terminal
    (config)# ip route 10.10.10.0 255.255.240.0 serial 0/1/1
    (config)# end

    # show running-config

    //Publicar red LAN y la red WAN
    # config terminal
    (config)# router rip
    (config-router)# network 172.20.20.0    //LAN
    (config-router)# network 30.30.30.0    //WAN
    (config-router)# network 1.1.1.1    //WAN

    # show ip protocols

    //Añadir ruta pasiva hacia el OESTE
    # config terminal
    (config)# router rip
    (config-router)# passive-interface serial 0/1/1
    (config-router)# no auto-summary                //sirve para que puede enviar correctamente las máscaras de red
    (config-router)# default-information originate

    # show ip protocols
```

Configuración para el Router ESTE_2:

```ESTE_2
    # show ip protocols

    //configuración de la interfaz serial
    # config terminal
    (config)# interface serial 0/0/0
    (config-if)# ip address 20.20.20.6 255.255.255.252

    # show running
```

Configuración para el Router ESTE_3:

```ESTE_3
    # show ip route

    //Prueba de ping extendido. Es escribiendo el comando sin la IP. Posteriormente pedirá una serie de datos.
    # ping

    //para conocer una ruta en concreto
    # show ip route 172.20.23.0
```
