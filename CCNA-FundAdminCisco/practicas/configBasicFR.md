# CONFIGURACIÓN BÁSICA FRAME RELAY

![alt text](image-56.png "Topología ejemplo para configurar FR")

- La _nube_ es el proveedor de servicios.
- Los _routers_ son los clientes (dispositivos finales).

&sect; R1 --> 10.10.10.1/24</br>
&sect; R2 --> 10.10.10.2/24</br>
&sect; R3 --> 10.10.10.3/24</br>
&sect; Cloud-PT --> Se1 hacia R1, Se2 hacia R2, Se3 hacia R3. El reloj está en la nube.</br>

![alt text](image-57.png "Conexión serial en la nube")

## Router R1

```R1
    ...
    R1(config)# interface serial 0/0/0
    R1(config-if)# description WAN
    R1(config-if)# ip address 10.10.10.1 255.255.255.0
    R1(config-if)# no shutdown
    R1(config-if)# encapsulation frame-relay

    R1# show run
    R1# show interface brief

    /*** CONFIGURACIÓN MAPA DEL FRAME-RELAY ***/

    R1# show frame-relay map

    R1(config)# interface serial 0/0/0
    R1(config-if)# frame-relay map ip 10.10.10.2 102 broadcast  //IP vecino + DLCI = 102
    R1(config-if)# frame-relay map ip 10.10.10.3 103 broadcast  //IP vecino + DLCI = 103

    /*** CONFIGURACIÓN DE LAS TRAMAS LMI ***/

    R1(config)# interface serial 0/0/0
    R1(config-if)# frame-relay lmi-type ansi

    R1# show frame-relay map
    R1# show ip interface brief
```

## Router R2

```R2
    ...
    R2(config)# interface serial 0/0/0
    R2(config-if)# description WAN
    R2(config-if)# ip address 10.10.10.2 255.255.255.0
    R2(config-if)# no shutdown
    R2(config-if)# encapsulation frame-relay

    R2# show run
    R2# show interface brief

    /*** CONFIGURACIÓN MAPA DEL FRAME-RELAY ***/

    R2# show frame-relay map

    R2(config)# interface serial 0/0/0
    R2(config-if)# frame-relay map ip 10.10.10.1 201 broadcast  //IP vecino + DLCI = 201
    R2(config-if)# frame-relay map ip 10.10.10.3 203 broadcast  //IP vecino + DLCI = 203

    /*** CONFIGURACIÓN DE LAS TRAMAS LMI ***/

    R2(config)# interface serial 0/0/0
    R2(config-if)# frame-relay lmi-type ansi

    R2# show frame-relay map
    R2# show ip interface brief
```

## Router R3

```R3
    ...
    R3(config)# interface serial 0/0/0
    R3(config-if)# description WAN
    R3(config-if)# ip address 10.10.10.3 255.255.255.0
    R3(config-if)# no shutdown
    R3(config-if)# encapsulation frame-relay

    R3# show run
    R3# show interface brief

    /*** CONFIGURACIÓN MAPA DEL FRAME-RELAY ***/

    R3# show frame-relay map

    R3(config)# interface serial 0/0/0
    R3(config-if)# frame-relay map ip 10.10.10.1 301 broadcast  //IP vecino + DLCI = 301
    R3(config-if)# frame-relay map ip 10.10.10.2 302 broadcast  //IP vecino + DLCI = 302

    /*** CONFIGURACIÓN DE LAS TRAMAS LMI ***/

    R3(config)# interface serial 0/0/0
    R3(config-if)# frame-relay lmi-type ansi

    R3# show frame-relay map
    R3# show ip interface brief
```

Tras la configuración anterior, si realizamos `ping` a algún vecino, no vamos a obtener respuesta, ya que falta la configuración en la nube.

## Configuración CLOUD-PT

### Configurar Serial1 de Cloud-PT

![alt text](image-59.png "Configurar Serial1 de la nube")
![alt text](image-60.png "Añadir los DLC de los Routers")
![alt text](image-61.png "LMI ANSI")

### Configurar Serial2 de Cloud-PT

![alt text](image-62.png "Configuración Serial2 de la nube")

### Configurar Serial3 de Cloud-PT

![alt text](image-63.png "Configuración Serial3 de la nube")

### Configurar Frame-Relay

![alt text](image-58.png "Clic en la nube, seleccionar Frame Relay")
![alt text](image-64.png "Configuración Serial1 R1 a R2 y Serial2 R2 a R1")
![alt text](image-65.png "Configuración Serial1 R1 a R3 y Serial3 R3 a R1")
![alt text](image-66.png "Configuración Serial2 R2 a R3 y Serial3 R3 a R2")

- - -

## CONFIGURACIÓN SUBINTERFAZ PUNTO A PUNTO EN FRAME RELAY

### Configuración Subinterfaz en Router 1

```router1
    
    /*** borramos los mapas y la dirección IP ***/
    
    R1(config)# interface serial 0/0/0
    R1(config-if)# no frame-relay map ip 10.10.10.2 
    R1(config-if)# no frame-relay map ip 10.10.10.3
    R1(config-if)# no ip address 

    /*** CREAR LAS INTERFACES LÓGICAS ***/

    R1(config)# interface serial 0/0/0.102 point-to-point
    R1(config-subif)# description HACIA_R2
    R1(config-subif)# ip address 10.10.10.1 255.255.255.0
    R1(config-subif)# frame-relay interface-dlci 102

    R1(config)# interface serial 0/0/0.103 point-to-point
    R1(config-subif)# description HACIA_R3
    R1(config-subif)# ip address 192.168.1.1 255.255.255.0
    R1(config-subif)# frame-relay interface-dlci 103
```

### Configuración Subinterfaz en Router 3

```router3
    ...    
    R3(config)# interface serial 0/0/0
    R3(config-if)# ip address 192.168.1.3 255.255.255.0
    R3(config-if)# frame-relay map ip 192.168.1.1 301 broadcast
```

- - -

## CONFIGURACIÓN SUBINTERFAZ MULTIPUNTO

```R1
    ...
    /*** ELIMINAR MAPAS Y DIRECCIONES IP ***/
    
    R1(config)# interface serial 0/0/0
    R1(config-if)# no ip address
    R1(config-if)# no frame-relay map ip 10.10.10.2
    R1(config-if)# no frame-relay map ip 10.10.10.3

    /*** CREAR LA INTERFAZ LÓGICA MULTIPUNTO ***/

    R1(config)# interface serial 0/0/0.100 multipoint
    R1(config-subif)# ip address 10.10.10.1 255.255.255.0
    R1(config-subif)# frame-relay interface-dlci 102
    R1(config-subif)# frame-relay interface-dlci 103
    R1(config-subif)# description MULTIPUNTO_R2_R3
```
