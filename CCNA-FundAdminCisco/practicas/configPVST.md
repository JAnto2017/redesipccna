# Configuración de PVST Rapid

![alt text](image-37.png "Topología configuración PVST Rapid")

## Switch S1

```S1
    ...
    S1(config)# spanning-tree mode rapid-pvst
    S1(config)# interface range fastEthernet 0/1 - fastEthernet 0/2
    S1(config-if-range)# spanning-tree link-type point-to-point

    S1# show run
```

## Switch S2

```S2
    ...
    S2(config)# spanning-tree mode rapid-pvst
    S2(config)# interface range fastEthernet 0/1 - fastEthernet 0/2
    S2(config-if-range)# spanning-tree link-type point-to-point
```

## Switch S3

```S3
    ...
    S3(config)# spanning-tree mode rapid-pvst
    S3(config)# interface range fastEthernet 0/1 - fastEthernet 0/2
    S3(config-if-range)# spanning-tree link-type point-to-point
```
