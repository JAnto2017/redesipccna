# Práctica de laboratorio en CPT Enrutamiento VLAN con L3 Switch SVI

![alt text](image-182.png)

Pasos previos:

1. En el Switch de Capa 3 se programan las VLAN 10, 20 y 30.
2. Los PC deben tener las IPs y máscaras de red configuradas.

## Configuración del Switch

```switch
sw1> enable
sw1# configure terminal
sw1(config)# interface vlan 10
sw1(config-if)# ip address 10.1.10.1 255.255.255.0

sw1(config-if)# interface vlan 20
sw1(config-if)# ip address 10.1.20.1 255.255.255.0

sw1(config-if)# interface vlan 30
sw1(config-if)# ip address 10.1.30.1 255.255.255.0

sw1(config-if)# end

sw1# show running

//------------------------------------------------

sw1# configure terminal
sw1(config)# interface routing
sw1(config)# end

sw1# show ip route
```

## Configuración del Router

```router
R1> enable
R1# config terminal
R1(config)# interface gigabitethernet 0/0
R1(condif-if)# ip address 10.1.30.2 255.255.255.0
R1(condif-if)# no shutdown
R1(condif-if)# end

R1# show ip route

R1> enable
R1# config terminal
R1(config)# ip route 10.0.0.0 255.255.255.0 10.1.30.1
```

### Ruta predeterminada en el Switch

```switch
sw1> enable
sw1# configure terminal
sw1(config)# ip route 0.0.0.0 0.0.0.0 10.1.30.2
sw1(config)# exit
```

### Interfaz de bucle invertido en el Router

```router
R1> enable
R1# config terminal
R1(config)# interface loopback 0
R1(config-if)# ip add 8.8.8.8 255.255.255.255
R1(config-if)# exit
```

Ahora desde un PC de la VLAN-10 se puede hacer ping 8.8.8.8 y responde.