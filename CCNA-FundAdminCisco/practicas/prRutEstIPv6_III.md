# PRÁCTICA CONFIGURACIÓN BÁSICA Y RUTEO ESTATICO DE IPv6 (PARTE 3)

![alt text](image-2.png)

## Configuración ROUTER R2

```router R2
enable
configure terminal
ipv6 unicast-routing
interface serial 0/0/1
description HACIA_R3
ipv6 address 2001:bb:cc:dd::/64 eui-64  /*el eui-64 sirve para generar IP única usando MAC*/
no shutdown
end

show running

show ip interface serial 0/0/1

show ipv6 interface serial 0/0/1 (muestra link-locl)
```

```router R2
enable
configure terminal
interface serial 0/0/0
description HACIA_R1
ipv6 address 2000:abcd:1234::/64 eui-64
no shutdown
end

show running
```

Crear las rutas estáticas. Son las redes que no "ve" el router R2

```router R2
enable
configure terminal
ipv6 route FC10:D22D:40::/48 serial 0/0/1   (para una red)

ipv6 route FC00:AA:BB::/56 serial 0/0/0   (para otra red)
```

## Configuración ROUTER R1

El R1 tiene el serial 0/0/1 y el ethernet conectado.

```R1
enable
configure terminal
ipv6 unicast-routing
interface serial 0/0/1
description HACIA_R2
ipv6 address 2000:abcd:1234::/64 eui-64  /*el eui-64 sirve para generar IP única usando MAC*/
no shutdown
end
```

Configuración de las Ethernet del R1

```R1
enable
configure terminal
interface fastEthernet 0/0
description LAN
ipv6 address FC00:AA:BB::/56
no shutdown
exit
ipv6 route ::/0 serial 0/0/1
```

## Configuración PC1

- IPv6 Address          --> FC00:AA:BB:CC:10:: /56
- Link Local Address    --> FE80::200:FFFF:FEC6:C544
- IPv6 Gatewayy         --> FC00:AA:BB::

Prueba de conectividad:

`C:\>ping FC10:D22D:40:25::`
