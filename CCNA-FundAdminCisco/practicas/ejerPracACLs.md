# EJERCICIO PRÁTICO - CONFIGURACIÓN DE ACLs BASADOS EN TIEMPO

![alt text](image-90.png "Topología para configurar las ACLs")

## Conocimientos previos

La red está configurada y los dispositivos tienen conectividad.
Password = cisco

## Objeticos

1. R1, R2 y R3 deben poder transferir archivos de respaldo al servidor FTP. No es necesario que alcancen el servidor DNS. R4 tiene acceso completo a los servidores.
2. PC1 es el único host que tiene acceso completo a los servidores.
3. PC3 es el único host que puede hacer Telnet a los routers.
4. Bloquear cualquier tráfico de Internet dirigido a los servidores.
5. Bloquear el ICMP proveniente de Internet.
6. Bloquear el Telnet proveniente de Internet.
7. Las 4 PCs deben poder resolver sitios Web y no es necesario que hagan ping a los servidores ni que alcancen el servidor FTP.

## Configurar R1

```router
    ...
    /*** ACLs configurar el punto 3 ***/

    R1(config)# access-list 10 permit host 172.18.18.10
    R1(config)# line vty 0 4
    R1(config-line)# access-class 10 in

    /*** Configurar el punto 1 ***/

    R1# copy running-config tftp:
    Address or name remote host []? 172.20.20.20
    Destination filename [R1-config]? BACKUP_CONFIG_R1

```

## Configurar R2

```router
    ...
    /*** ACLs configurar el punto 3 ***/

    R2(config)# access-list 10 permit host 172.18.18.10
    R2(config)# line vty 0 4
    R2(config-line)# access-class 10 in

    /*** ACLs Extendida configuración punto 4 ***/

    R2(config)# ip access-list extended FILTRO_EXTERNO
    R2(config-ext-nacl)# deny ip any 172.20.20.0 0.0.0.255

    /*** ACLs Extendida configuración punto 5 ***/

    R2(config-ext-nacl)# deny icmp any any echo

    /*** ACLs Extendida configuración punto 6 ***/

    R2(config-ext-nacl)# deny tcp any any eq telnet
    R2(config-ext-nacl)# permit ip any any

    /*** mostrar lista de acceso ***/

    R2(config-ext-nacl)# do show run

    /*** interfaz a router ISP ***/

    R2(config)# interface fastEthernet 0/0
    R2(config-if)# ip access-group FILTRO_EXTERNO in

    R2(config)# show run

    /*** Configuración del punto 1 ***/

    R2# copy running-config tftp:
    Address or name of remote host []? 172.20.20.20
    Destination filename [R2-config]? BACKUP_CONFIG_R2
```

### Configurar R3

```router
    ...
    /*** ACLs configurar el punto 3 ***/

    R3(config)# access-list 10 permit host 172.18.18.10 
    R3(config)# line vty 0 4
    R3(config-line)# access-class 10 in

    /*** Configurar el punto 1 ***/

    R3# copy running-config tftp:
    Address or name remote host []? 172.20.20.20
    Destination filename [R1-config]? BACKUP_CONFIG_R3
```

### Configurar R4

```router
    ...
    /*** ACLs configurar el punto 3 ***/

    R4(config)# access-list 10 permit host 172.18.18.10
    R4(config)# line vty 0 4
    R4(config-line)# access-class 10 in

    /*** ACLs Extendida configuración del punto 7 ***/

    R4(config)# ip access-list extended FILTRO_SERVICIOS
    R4(config-ext-nacl)# permit udp 172.16.16.0 0.0.0.255 host 172.20.20.10 eq domain
    R4(config-ext-nacl)# permit udp 172.17.17.0 0.0.0.255 host 172.20.20.10 eq domain
    R4(config-ext-nacl)# permit udp 172.19.19.0 0.0.0.255 host 172.20.20.10 eq domain

    R4(config)# interface fastEthernet 0/1
    R4(config-if)# ip access-group FILTRO_SERVICIOS in

    R4(config)# ip access-list extended FILTRO_SERVICIOS
    R4(config-ext-nacl)# no permit udp 172.19.19.0.0 0.0.0.255 host 172.20.20.10 eq domain
    R4(config-ext-nacl)# permit udp 172.19.19.0.0 0.0.0.255 host 172.20.20.10 eq domain
    R4(config)# no ip access-list extended FILTRO_SERVICIO
    R4(config)# ip access-list extended FILTRO_SERVICIOS
    R4(config-ext-nacl)# permit udp 172.19.19.0.0 0.0.0.255 host 172.20.20.10 eq domain

    /*** corregir que lo único que se permite es tráfico TCP y Telnet ***/

    R4(config)# ip access-list extended FILTRO_SERVICIOS
    R4(config-ext-nacl)# permit tcp host 172.18.18.10 host 172.20.20.1 eq telnet
    R4(config-ext-nacl)# permit tcp host 172.18.18.10 host 192.168.0.10 eq telnet

    /*** configurar el punto 2 ***/

    R4(config)# ip access-list extended FILTRO_SERVICIOS
    R4(config-ext-nacl)# permit ip host 172.16.16.10 host 172.20.20.0 0.0.0.255

    /*** Configurar el punto 1 ***/

    R4# copy running-config tftp: 
    Address or name of remote host []? 172.20.20.20
    Destination filename [R4-config]? BACKUP_CONFIG_R4

    R4(config)# ip access-list extended FILTRO_SERVICIOS
    R4(config-ext-nacl)# permit ip any host 172.20.20.20

```
