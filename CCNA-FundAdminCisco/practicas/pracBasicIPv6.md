# Práctica Básica de IPv6

Topología para el ejemplo práctico:

![alt text](image-6.png)

## Configuraciones

### Configuración del router R1

```R1
    > enable
    # configure terminal
    (...)# hostname R1
    (...)# ipv6 unicast-routing
    (...)# interface fastethernet 0/0
    (...)# ipv6 address FC11:BB:1111:DE::/65
    (...)# no shutdown
    (...)# exit
    
    (...)# interface fastethernet 0/1
    (...)# ipv6 address FC11:BB:1111:DE:8000::/65
    (...)# no shutdown
    (...)# exit

    (...)# interface serial 0/0/0
    (...)# ipv6 address 2001:EE:EE:EE::/64 EUI-64
    (...)# no shutdown
    (...)# exit
```

EL `/65` o el `/64` es el PREFIJO DE RED (PREFIX).

La IP de red y Broadcast no se utilizan en IPv6.

El comando `EUI-64` sirve para modificar la dirección IPv6 ingresada, en función de la MAC de la tarjeta de red. Creando una dirección IPv6 única.

### Configuración del router R2

```R2
    > enable
    # configure terminal
    (...)# hostname R2
    (...)# ipv6 unicast-routing
    (...)# interface fastethernet 0/0
    (...)# ipv6 address FC00:AB12:F0::/48
    (...)# no shutdown
    (...)# exit

    (...)# interface serial 0/0/0
    (...)# ipv6 address 2001:EE:EE:EE::/64 EUI-64
    (...)# no shutdown
    (...)# exit

    (...)# interface serial 0/0/1
    (...)# ipv6 address 2001:FF:FF:FF::/64 EUI-64
    (...)# no shutdown
    (...)# exit
```

El comando `show ipv6 interface serial 0/0/0` muestra la direcciones IPv6 generadas con parte de la MAC. También observaremos el LINK LOCAL (FE80::) creado automáticamente.

### Configuración del router R3

```R3
    > enable
    # configure terminal
    (...)# hostname R3
    (...)# ipv6 unicast-routing
    (...)# interface fastethernet 0/0
    (...)# ipv6 address FC10:AA:2222:FF:0:1::/96
    (...)# no shutdown
    (...)# exit

    (...)# interface fastethernet 0/1
    (...)# ipv6 address FC10:AA:2222:FF::/96
    (...)# no shutdown
    (...)# exit

    (...)# interface serial 0/0/1
    (...)# ipv6 address 2001:FF:FF:FF::/64 EUI-64
    (...)# no shutdown
    (...)# exit
```

### Configuración ruteo estático

```R1
    > enable
    # configure terminal
    (...)# ipv6 route ::/0 serial 0/0/0
    (...)# exit
```

```R3
    > enable
    # configure terminal
    (...)# ipv6 route ::/0 serial 0/0/1
    (...)# exit
```

El R2 no puede crear rutas por defecto, se deben crear rutas estáticas hacia las LAN.
Las direcciones de la LAN utilizables van desde la:
    - FC11:BB:1111:DE::
    - FC12:BB:1111:DE::07FFF:FFFF:FFFF
Sumarizando las dos subredes de la LAN queda de la forma: FC11:BB:1111:DE::\64

```R2
    > enable
    # configure terminal
    (...)# ipv6 route FC11:BB:1111:DE::/64 serial 0/0/0
    (...)# exit
```

La red LAN en el R3 más pequeña es:
    - FC10:AA:2222:FF:0:1::/96

La otra red LAN en el R3 las IPv6 van desde:
    - FC10:AA:2222:FF:0:0::/96
    - FC10:AA:2222:FF:0:0:FFFF:FFFF

Sumarizando las IPv6: `FC10:AA:2222:FF::/95`.

```R3
    > enable
    # configure terminal
    (...)# ipv6 route FC10:AA:2222:FF::/95 serial 0/0/1
    (...)# exit
```
