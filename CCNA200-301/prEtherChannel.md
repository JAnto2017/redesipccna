# Práctica de laboratorio Ethernet Channel

![alt text](image-184.png)

## Configuraciones Previas

- IP/Mask/Gataway de los PC.
- Creadas las VLAN.

## Configuración del canal en Switch-1

```switch-1
sw1> enable
sw1# configure terminal
sw1(config)# interface range gigabitethernet 0/1-2
sw1(config-if-range)# no shutdown
sw1(config-if-range)# no switchport
sw1(config-if-range)# channel-group 12 mode desirable
sw1(config-if-range)# exit
sw1(config)#end
sw1# show running

sw1# configure terminal
sw1(config)# interface port 12
sw1(config-if)# ip address 10.1.12.1 255.255.255.0
sw1(config-if)# no shutdown
sw1(config-if)# exit
sw1(config-if)# end

<!- Corregir el error -->
sw1(config)# interface vlan 2
sw1(config-if)# no ip address 10.1.10.1 255.255.255.0   <!- borra -->
sw1(config-if)# ip address 10.1.2.1 255.255.255.0
sw1(config-if)# end
<!- Comprobación -->
sw1# show ip route

sw1# configure terminal
sw1(config)# ip route 10.1.3.0 255.255.255.0 10.1.12.2
sw1(config)# end
```

## Configuración del canal en Switch-2

```switch-2
sw2> enable
sw2# configure terminal
sw2(config)# interface range gigabitethernet0/1-2
sw2(config-if-range)# no shutdown
sw2(config-if-range)# no switchport
sw2(config-if-range)# channel-group 12 mode auto
sw2(config-if-range)# end
sw2(config)# end

sw2# show running

sw2# configure terminal
sw2(config)# interface port 12
sw2(config-if)# ip address 10.1.12.2 255.255.255.0
sw2(config-if)# end
sw2# 
sw2# show etherchannel summary
sw2# show ip route

sw2# configure terminal
sw2(config)# ip route 10.1.2.0 255.255.255.0 10.1.12.1
sw2(config)# exit
sw2(config)# show ip route
```

## Pruebas

- Desde PC0: `ping 10.1.2.1` puerta de enlace.
- Desde PC0 a PC1: `ping 10.1.3.2`.
