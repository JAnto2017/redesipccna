# CONFIGURACIÓN RIPng CON IPv6

![alt text](image-8.png)

Configuration de RIPng con IPv6. En la topología y direccionamiento mostrados.

Configuración del Router R1

```R1
    > enable
    # config terminal
    (config)# ipv6 unicast-routing
    (config)# interface fastEthernet 0/0
    (config-if)# description LAN
    (config-if)# IPv6 address FC00:AA:BB::/56
    (config-if)# no shutdown
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# clock rate 9600
    (config-if)# description HACIA_R2
    (config-if)# IPv6 address 2000:abcd:1234::/64 eui-64
    (config-if)# no shutdown
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# description HACIA_R3
    (config-if)# IPv6 address 2002:FF:AB:AB/64 eui-64
    (config-if)# no shutdown
    (config-if)# exit

    (config)# ipv6 router rip CISCO
    (config-rtr)# exit

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit
```

Configuración IP PC_1:

![alt text](image-9.png)

Configuración del Router R2:

```R2
    > enable
    # config terminal
    (config)# interface fastEthernet 0/0
    (config-if)# description LAN
    (config-if)# IPv6 address FC11:12FF:34:EE::/64
    (config-if)# no shutdown
    (config-if)# exit

    (config)# ipv6 unicast-routing
    (config)# interface serial 0/0/1
    (config-if)# description HACIA_R1
    (config-if)# IPv6 address 2000:ABCD:1234::/64 EUI-64
    (config-if)# no shutdown
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# clock rate 9600
    (config-if)# description HACIA_R3
    (config-if)# IPv6 address 2001:BB:CC:DD::/64 EUI-64
    (config-if)# no shutdown
    (config-if)# exit

    //Configuración de una interface LOOPBACK
    (config)# interface loopback 0
    (config-if)# IPv6 address EE:EE:EE:EE:15::/64
    (config-if)# exit

    //rura por defecto IPv6
    (config)# IPv6 route ::/0 loopback 0

    (config)# ipv6 router rip CISCO
    (config-rtr)# exit
    
    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 rip CISCO enable
    (config-if)# ipv6 rip CISCO default-information originate   //solo a las interfaces seriales
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 rip CISCO default-information originate
    (config-if)# exit
```

Configuración del Router R3:

```R3
    > enable
    # configure terminal
    (config)# ipv6 unicast-routing
    (config)# interface fastEthernet 0/0
    (config-if)# description LAN
    (config-if)# IPv6 address FC10:D22D:40::/48
    (config-if)# no shutdown
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# description HACI_R2
    (config-if)# IPv6 address 2001:BB:CC:DD::/64 EUI-64
    (config-if)# no shutdown
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# clock rate 9600
    (config-if)# description HACIA_R1
    (config-if)# IPv6 address 2002:FF:AB:AB::/64 EUI-64
    (config-if)# no shutdown
    (config-if)# exit

    (config)# ipv6 rip CISCO enable
    (config-rtr)# exit

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 rip CISCO enable
    (config-if)# exit
```

Configuración del PC_3:

![alt text](image-10.png)