# Práctica Ruteo Estático en IPv6

![alt text](image.png)

## PC2

- IP: 172.16.16.10
- Mask: 255.255.255.0
- Gateway: 172.16.16.1

## R2

```router
R2> enable
R2# configure terminal
R2(config)# interface fastEthernet 0/0
R2(config-if)# description LAN
R2(config-if)# ip address 172.16.16.1 255.255.255.0
R2(config-if)# no shutdown
R2(config-if)# exit

R2(config)# interface serial 0/0/1
R2(config-if)# description HACIA_R3
R2(config-if)# ip address 10.10.10.2 255.255.255.252
R2(config-if)# no shutdown
R2(config-if)# exit

R2(config)# ip route 192.168.1.0 255.255.255.0 serial 0/0/1
R2(config)# end

R2# show run <!- Sirve para ver la configuración realizada -!>
```

## R3

```router
R3> enable
R3# configure terminal
R3(config)# interface fastEthernet 0/0
R3(config-if)# description LAN
R3(config-if)# ip address 192.168.1.1 255.255.255.0
R3(config-if)# no shutdown
R3(config-if)# exit

R3(config)# interface serial 0/0/0
R3(config-if)# description HACIA_R2
R3(config-if)# ip address 10.10.10.1 255.255.255.252
R3(config-if)# no shutdown
R3(config-if)# exit

<!- Creamos ruta por defecto, para que conozca cualquier red -!>
R3(config)# ip route 0.0.0.0 0.0.0.0 serial 0/0/0
R3(config)# end

R3# show run
```

## PC4

- IP: 192.168.1.10
- Mask: 255.255.255.0
- Gateway: 192.168.1.1

`ping 172.16.16.10` => responde satisfactoriamente.
