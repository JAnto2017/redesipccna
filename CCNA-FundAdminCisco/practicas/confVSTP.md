# Configuración de PVST

![alt text](image-36.png "Topología para configurar PVST")

## Switch S1

Se precisa habilitar el servicio, y configurar las VLAN.

```S1
    ...
    S1# show vlan brief     //muestra las dos VLAN creadas (10 DATOS, 20 INTERNET)
    
    S1(config)# spanning-tree vlan 10 root primary

    S1(config)# spanning-tree vlan 10 root secondary
    S1(config)# spanning-tree vlan 20 root secondary

```

## Switch S3

```S3
    ...
    S3(config)# spanning-tree vlan 20 root primary

    S3# show spanning-tree
```

## Switch S2

```S2
    ...
    S2# show spanning-tree
```
