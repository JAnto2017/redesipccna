# EJERCICIO WAN

![alt text](image-67.png "Topología ejercicio WAN")

- En la sección celeste se debe configurar PPP con la autenticación CHAP en los extremos y PAP en la zona centro.
- El router SUR es el concentrador.
- En la nube se debe configurar los DLCI.
- Los enlaces WAN utilizar 10.10.10.0/27
- Configurar las fastEtherner de los routers.
- Se puede utilizar enrutamiento estático o dinámico, para que los PC tengan conectividad entre sí.
- El enrutamiento entre WANs no es necesario.

![alt text](image-68.png "Etiquetas con IP/Mask")

## Router SUR

```router
    ...
    SUR(config)# interface serial 0/1/0
    SUR(config-if)# description HACIA_SUR-1
    SUR(config-if)# ip address 10.10.10.17 255.255.255.252
    SUR(config-if)# clock rate 9600
    SUR(config-if)# encapsulation ppp
    SUR(config-if)# ppp authentication pap
    SUR(config-if)# ppp pap sent-username SUR password ejercicio
    SUR(config-if)# no shutdown
    SUR(config-if)# exit
    SUR(config)# username SUR-1 password ejercicio

    /*** RUTEO ESTÁTICO ***/

    SUR(config)# ip route 192.168.5.0 255.255.255.0 10.10.10.18
    SUR(config)# ip route 192.168.6.0 255.255.255.0 10.10.10.22

    /*** CONFIG SERIAL HACIA NUBE ***/

    SUR(config)# interface serial 0/0/0
    SUR(config-if)# description HACIA_FRAME_RELAY
    SUR(config-if)# encapsulation frame-relay
    SUR(config-if)# frame-relay lmi-type ansi
    SUR(config-if)# no shutdown
    SUR(config-if)# exit

    /*** CREACIÓN DE INTERFACE LÓGICA ***/

    SUR(config)# interface serial 0/0/0.100 multipoint
    SUR(config-subif)# description HACIA_OESTE_Y_ESTE
    SUR(config-subif)# ip address 10.10.10.1 255.255.255.240
    SUR(config-subif)# frame-relay interface-dlci 300
    SUR(config-subif)# frame-relay interface-dlci 500
```

## Router SUR-1

```router
    ...
    SUR-1(config)# interface serial 0/1/0
    SUR-1(config-if)# description HACIA_SUR
    SUR-1(config-if)# clock rate 9600
    SUR-1(config-if)# ip address 10.10.10.18 255.255.255.252
    SUR-1(config)# encapsulation PPP
    SUR-1(config-if)# ppp authentication PAP
    SUR-1(config-if)# ppp pap sent-username SUR-1 password ejercicio
    SUR-1(config-if)# no shutdown
    SUR-1(config-if)# exit
    SUR-1(config)# username SUR password ejercicio      //se añade a la base de datos para comparar con el dato envía del router SUR

    SUR-1(config)# interface serial 0/1/1
    SUR-1(config-if)# description HACIA_SUR-2
    SUR-1(config-if)# clock rate 9600
    SUR-1(config-if)# ip address 10.10.10.21 255.255.255.252
    SUR-1(config)# encapsulation PPP
    SUR-1(config-if)# ppp authentication PAP
    SUR-1(config-if)# ppp pap sent-username SUR password ejercicio2
    SUR-1(config-if)# no shutdown
    SUR-1(config-if)# exit
    SUR-1(config)# username SUR-2 password ejercicio2      //se añade a la base de datos para comparar con el dato envía del router SUR

    SUR-1# ping 10.10.10.17

    /*** ENRUTAMIENTO ESTÁTICO ***/

    SUR-1(config)# ip route 0.0.0.0 0.0.0.0 serial 0/1/0

    SUR-1# show run
```

## Router SUR-2

```router
    ...
    SUR-2(config)# interface serial 0/1/1
    SUR-2(config-if)# description HACIA_SUR
    SUR-2(config-if)# ip address 10.10.10.22 255.255.255.252
    SUR-2(config-if)# encapsulation ppp
    SUR-2(config-if)# ppp authentication pap
    SUR-2(config-if)# ppp pap sent-username SUR-2 password ejercicio2
    SUR-2(config-if)# no shutdown
    SUR-2(config-if)# exit
    SUR-2(config)# username SUR password ejercicio2

    SUR-2# ping 10.10.10.21

    /*** RUTAS POR DEFECTO ***/

    SUR-2(config)# ip route 0.0.0.0 0.0.0.0 serial 0/1/1
```

## Router OESTE

```router
    ...
    OESTE(config)# interface serial 0/0/1
    OESTE(config-if)# description HACIA_N-OESTE
    OESTE(config-if)# clock rate 9600
    OESTE(config-if)# ip address 10.10.10.9 255.255.255.252
    OESTE(config-if)# encapsulation ppp
    OESTE(config-if)# ppp authentication chap
    OESTE(config-if)# no shutdown

    OESTE# show run

    OESTE(config)# username N-OESTE password cisco123

    OESTE# ping 10.10.10.10

    /*** configuración hacia router SUROESTE ***/

    OESTE(config)# interface serial 0/0/0
    OESTE(config-if)# description HACIA_S-OESTE
    OESTE(config-if)# ip address 10.10.10.13 255.255.255.252
    OESTE(config-if)# clock rate 9600
    OESTE(config-if)# encapsulation ppp
    OESTE(config-if)# ppp authentication chap
    OESTE(config-if)# no shutdown
    OESTE(config-if)# exit
    OESTE(config)# username S-OESTE password 321cisco

    OESTE# ping 10.10.10.10
    OESTE# ping 10.10.10.14

    /*** ENRUTAMIENTO ESTÁTICO ***/
    
    OESTE(config)# ip route 192.168.1.0 255.255.255.0 10.10.10.10
    OESTE(config)# ip route 192.168.2.0 255.255.255.0 10.10.10.14

    /*** CONFIGURACIÓN HACIA CLOUD-PT ***/

    OESTE(config)# interface serial 0/1/0
    OESTE(config-if)# description HACIA_SUR
    OESTE(config-if)# encapsulation frame-relay
    OESTE(config-if)# frame-relay lmi-type ansi
    OESTE(config-if)# frame-relay map ip 10.10.10.1 300 broadcast
    OESTE(config-if)# ip address 10.10.10.2 255.255.255.248
    OESTE(config-if)# no shutdown
    
    /*** CONFIGURACIÓN DE OTRA MANERA ***/
    OESTE(config)# interface serial 0/1/0
    OESTE(config-if)# no frame-relay map ip 10.10.10.1
    OESTE(config-if)# frame-relay interface-dlci 300

    OESTE# ping 10.10.10.1

    OESTE# show ip route

    /*** CREAR RUTAS POR DEFECTO ***/
    
    OESTE(config)# ip route 0.0.0.0 0.0.0.0 10.10.10.1 

    OESTE# show ip route
```

## Router N-OESTE

```router
    ...
    N-OESTE(config)# interface serial 0/0/1
    N-OESTE(config-if)# description HACIA_OESTE
    N-OESTE(config-if)# encapsulation ppp
    N-OESTE(config-if)# ppp authentication chap
    N-OESTE(config-if)# no shutdown
    N-OESTE(config-if)# exit
    N-OESTE(config)# username OESTE password cisco123

    N-OESTE# ping 10.10.10.9

    N-OESTE(config)# ip route 0.0.0.0 0.0.0.0 serial 0/0/1

```

## Router S-OESTE

```router
    ...
    S-OESTE(config)# interface serial 0/0/0
    S-OESTE(config-if)# description HACIA_OESTE
    S-OESTE(config-if)# ip address 10.10.10.14 255.255.255.252
    S-OESTE(config-if)# encapsulation ppp
    S-OESTE(config-if)# ppp authentication chap
    S-OESTE(config-if)# no shutdown
    S-OESTE(config-if)# exit
    S-OESTE(config)# username OESTE password 321cisco

    S-OESTE# ping 10.10.10.13

    S-OESTE(config)# ip route 0.0.0.0 0.0.0.0 serial 0/0/0
```

## Router ESTE

```router
    ...
    ESTE(config)# interface serial 0/1/0
    ESTE(config-if)# description HACIA_N-ESTE
    ESTE(config-if)# clock rate 9600
    ESTE(config-if)# ip address 10.10.10.29 255.255.255.252
    ESTE(config-if)# encapsulation ppp
    ESTE(config-if)# ppp authentication chap
    ESTE(config-if)# no shutdown
    ESTE(config-if)# exit
    ESTE(config)# interface serial 0/1/1
    ESTE(config-if)# description HACIA_S-ESTE
    ESTE(config-if)# clock rate 9600
    ESTE(config-if)# ip address 10.10.10.25 255.255.255.252
    ESTE(config-if)# encapsulation ppp
    ESTE(config-if)# ppp authentication chap
    ESTE(config-if)# no shutdown
    ESTE(config-if)# exit
    ESTE(config)# username N-ESTE password CCNA
    ESTE(config)# username S-ESTE password cisco

    /*** CONFIGURACIÓN HACIA CLOUD-PT ***/

    ESTE(config)# interface serial 0/0/0
    ESTE(config-if)# description HACIA_FRAME_RELAY
    ESTE(config-if)# encapsulation frame-relay
    ESTE(config-if)# frame-relay lmi-type ansi
    ESTE(config-if)# no shutdown
    ESTE(config-if)# exit

    /*** CREAR INTERFACE LÓGICA ***/

    ESTE(config)# interface serial 0/0/0 point-to-point 
    ESTE(config-subif)# description HACIA_SUR
    ESTE(config-subif)# ip address 10.10.10.3 255.255.255.248 
    ESTE(config-subif)# frame-relay interface-dlci 500

    /*** CREAR RUTA POR DEFECTO ***/

    ESTE(config)# ip route 0.0.0.0 0.0.0.0 10.10.10.1
```

## Router N-ESTE

```router
    ...
    N-ESTE(config)# interface serial 0/1/0
    N-ESTE(config-if)# description HACIA_ESTE
    N-ESTE(config-if)# ip address 10.10.10.30 255.255.255.252
    N-ESTE(config-if)# encapsulation ppp
    N-ESTE(config-if)# ppp authentication chap
    N-ESTE(config-if)# no shutdown
    N-ESTE(config-if)# exit
    N-ESTE(config)# username ESTE password CCNA

    N-ESTE# ping 10.10.10.29

    /*** RUTAS POR DEFECTO ***/

    N-ESTE(config)# ip route 0.0.0.0 0.0.0.0 serial 0/1/0
```

## Router S-ESTE

```router
    ...
    S-ESTE(config)# interface serial 0/1/1
    S-ESTE(config-if)# description HACIA_ESTE
    S-ESTE(config-if)# ip address 10.10.10.26 255.255.255.252
    S-ESTE(config-if)# encapsulation ppp
    S-ESTE(config-if)# ppp authentication chap
    S-ESTE(config-if)# no shutdown
    S-ESTE(config-if)# exit
    S-ESTE(config)# username ESTE password cisco
    S-ESTE(config)# ip route 0.0.0.0 0.0.0.0 serial 0/1/1
    S-ESTE(config)# ip route 192.168.9.0 255.255.255.0 10.10.10.30
    S-ESTE(config)# ip route 192.168.10.0 255.255.255.0 10.10.10.26

    S-ESTE# ping 10.10.10.30
    S-ESTE# ping 10.10.10.26
```

## Configuración SERIAL de CLOUD-PT

![alt text](image-69.png "SERIAL-1 DLCI = 300 SUR-OESTE LMI = ANSI")
![alt text](image-70.png "SERIAL-1 DLCI = 500 SUR-ESTE LMI = ANSI")
![alt text](image-71.png "SERIAL-2 DLCI = 300 SUR-OESTE LMI = ANSI")
![alt text](image-72.png "SERIAL-3 DLCI = 500 SUR-ESTE LMI = ANDI")

## Configuración FRAME RELAY de CLOUD-PT

![alt text](image-73.png "Serial1 SUR-ESTE <-> Serial3 SUR-ESTE")
![alt text](image-74.png "Serial1 SUR-OESTE <-> Serial2 SUR-OESTE")
