# Ejercicio PT Funcionalidades del Switch

![alt text](image-27.png)

![alt text](image-28.png)

## Switch

Seguridad en todos los Puertos en uso.

```S1
    S1# show ip interface brief

    //crear una VLAN25
    S1# configure terminal
    S1(config)# interface fastEthernet 0/1
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 25
    S1(config-if)# switchport port-security
    S1(config-if)# exit
    S1(config)# interface fastEthernet 0/2
    S1(config-if)# switchport mode access
    S1(config-if)# switchport mode vlan 25
    S1(config-if)# switchport port-security
    S1(config-if)# exit
    S1(config)# interface fastEthernet 0/10
    S1(config-if)# switchport mode access
    S1(config-if)# switchport mode vlan 25
    S1(config-if)# switchport port-security
    S1(config-if)# exit
    S1(config)# interface fastEthernet 0/24
    S1(config-if)# switchport mode access
    S1(config-if)# switchport mode vlan 25
    S1(config-if)# switchport port-security
    S1(config-if)# exit
    S1(config)# interface fastEthernet 0/25
    S1(config-if)# switchport mode access
    S1(config-if)# switchport mode vlan 25
    S1(config-if)# switchport port-security
    S1(config-if)# exit
    S1(config-if)# exit

    S1# write
    S1# show run
```

Limitar la Fa0/1 del Switch a las primeras 25 MAC Address aprendidas

```S1
    ...
    S1(config)# interface fastEthernet 0/1
    S1(config-if)# switchport port-security maximum 25
    S1(config-if)# switchport port-security mac-address sticky
```

Limitar la Fa0/2 del switch a las primeras 5 MAC Address aprendidas.

```S1
    ...
    S1(config)# interface fastEthernet 0/2
    S1(config-if)# switchport port-security maximum 5
    S1(config-if)# switchport port-security max-address sticky
```

Los puertos hacia las PCs deben desactivarse al haber una violación de seguridad.

```S1
    ...
    S1(config)# interface rannge fastEthernet 0/1 - fastEthernet 0/2
    S1(config-if-range)# switchport port-security violation shutdown

    S1# show run
```

La interfaz hacia el servidor solo debe permitir la MAC Address del servidor.

```S1
    ...
    S1# show port-security address

    S1(config)# interface fastEthernet 0/10
    S1(config-if)# switchport port-security mac-address 0090.2B44.6C70
    
    S1# reload    
```

La interfaz hacia el servidor solo debe restringir el puerto en caso de una violación de seguridad.

```S1
    ...
    S1(config)# interface fastEthernet 0/10
    S1(config-if)# switchport port-security mac-address 0090.2B44.6C70

    S1# show port-security address

    S1(config)# interface fastEthernet 0/10
    S1(config-if)# switchport port-security violation restrict
```

La interface hacia el router debe permitir por defecto la MAC Address del router.

```S1
    ...
    S1(config)# interface fastEthernet 0/24
    S1(config-if)# switchport port-security mac-address sticky

    S1# show port-security address
```

La interfaz hacia el router debe proteger el puerto en caso de una violación de seguridad.

```S1
    ...
    S1(config)# interface fastEthernet 0/24
    S1(config-if)# switchport port-security violation protect

    S1# show run
```

El resto de interfaces del switch deben quedar deshabilitados.

```S1
    S1# show ip interface brief
    S1(config)# interface range fastEthernet 0/3 - fastEthernet 0/9
    S1(config-if-range)# shutdown

    S1# show ip brief

    S1# show ip interface brief
    S1(config)# interface range fastEthernet 0/11 - fastEthernet 0/23
    S1(config-if-range)# shutdown

    S1# show ip interface brief

    S1# show ip interface brief
    S1(config)# interface range gigabitEthernet 0/1 - gigabitEthernet 0/2
    S1(config-if-range)# shutdown
```
