# PRÁCTICA DE VLSM (parte 1)

![alt text](image-3.png)

En cada nube existe una red LAN:

![alt text](image-4.png)
![alt text](image-5.png)

## RED DE 6 LAN CON 400O HOST

### Número de IP según los bits asignados

| Prefijos | Nº de IPs |
| --- | --- |
| /20 | 4096 |
| /21 | 2048 |
| /22 | 1024 |
| /23 | 512 |
| /24 | 256 |
| /25 | 128 |
| /26 | 64 |

Escogeremos el valor _/20_ correspondiente a _4096_ IP direccionables para este ejercicio, ya que 4096 > 4000.

### Asignaciones de direcciones IP

172.16.0.0/20   --> Esta es la primera red.

Para saber de cuanto en cuanto irá cambiando (los saltos); sabiendo que estamos trabajando con el tercer octeto (8 bit desde el bit 17 al 24), los bit irán del 17 al 24:

17 18 19 20 21 22 23 24     --> bit del tercer octeto

El valor del bit más significativo es 128.

17  18  19  **20**  21  22  23  24      --> Prefijos del tercer octeto
128 64  32  **16**  8   4   2   1       --> Valor decimal del bit y el salto de red

Según la tabla anterior, si el prefijo del ejercicio es 20, quiere decir que la red irá cambiando de 16 en 16. Con este dato ya podemos definir las IP de las redes.

- 172.16.0.0/20
- 172.16.16.0/20
- 172.16.32.0/20
- 172.16.48.0/20
- 172.16.64.0/20
- 172.16.80.0/20

Son 6 redes con 4000 host cada una.

## RED DE 4 LAN CON 1000 HOST

Escogeremos el valor _/22_ correspondiente a _1024_ IP direccionables para este ejercicio, ya que 1024 > 1000.

172.16.80.0/20   --> Continuamos a partir de esta IP.

Para saber de cuanto en cuanto irá cambiando (los saltos); sabiendo que estamos trabajando con el tercer octeto (8 bit desde el bit 17 al 24), los bit irán del 17 al 24:

17  18  19  20  21  **22**  23  24      --> Prefijos del tercer octeto
128 64  32  16  8   **4**   2   1       --> Valor decimal del bit y el salto de red

Escogeremos el prefijo 22 que corresponde con el valor decimal 4 del tercer bit.

- 172.16.96.0/22
- 172.16.100.0/22
- 172.16.104.0/22
- 172.16.108.0/22

## RED DE 2 LAN CON 500 HOST

Escogeremos el valor _/23_ correspondiente a _512_ IP direccionables para este ejercicio, ya que 512 > 500.

Para saber de cuanto en cuanto irá cambiando (los saltos); sabiendo que estamos trabajando con el tercer octeto (8 bit desde el bit 17 al 24), los bit irán del 17 al 24:

17  18  19  20  21  22  **23**  24      --> Prefijos del tercer octeto
128 64  32  16  8   4   **2**   1       --> Valor decimal del bit y el salto de red

Escogeremos el prefijo 23 que corresponde con el valor decimal 2 del tercer bit.

172.16.108.0/22   --> Continuamos a partir de esta IP.

- 172.16.112.0/23
- 172.16.114.0/23

## RED DE 2 LAN CON 250 HOST

Escogeremos el valor _/24_ correspondiente a _256_ IP direccionables para este ejercicio, ya que 256 > 250.

Para saber de cuanto en cuanto irá cambiando (los saltos); sabiendo que estamos trabajando con el tercer octeto (8 bit, desde el bit 17 al 24), los bit irán del 17 al 24:

17  18  19  20  21  22  23  **24**      --> Prefijos del tercer octeto
128 64  32  16  8   4   2   **1**       --> Valor decimal del bit y el salto de red

Escogeremos el prefijo 24 que corresponde con el valor decimal 1 del tercer bit.

172.16.114.0/23   --> Continuamos a partir de esta IP.

- 172.16.116.0/24
- 172.16.117.0/24

## RED DE 2 LAN CON 200 HOST

Escogeremos el valor _/24_ correspondiente a _256_ IP direccionables para este ejercicio, ya que 256 > 200.

Para saber de cuanto en cuanto irá cambiando (los saltos); sabiendo que estamos trabajando con el tercer octeto (8 bit, desde el bit 17 al 24), los bit irán del 17 al 24:

17  18  19  20  21  22  23  **24**      --> Prefijos del tercer octeto
128 64  32  16  8   4   2   **1**       --> Valor decimal del bit y el salto de red

Escogeremos el prefijo 24 que corresponde con el valor decimal 1 del tercer bit.

172.16.117.0/24   --> Continuamos a partir de esta IP.

- 172.16.118.0/24
- 172.16.119.0/24

## RED DE 2 LAN CON 125 HOST

Escogeremos el valor _/25_ correspondiente a _128_ IP direccionables para este ejercicio, ya que 128 > 125.

17  18  19  20  21  22  23  24          --> Prefijos del tercer octeto
**25**  26  27  28  29  30  31  32      --> Prefijos del cuarto octeto
**128** 64  32  16  8   4   2   1       --> Valor decimal del bit y el salto de red

Escogeremos el prefijo **25**. Este bit ya pertenece al cuarto octeto y no al tercero. Con este valor de **25** las redes tendrán saltos de **128**.

172.16.119.0/24   --> Continuamos a partir de esta IP.

- 172.16.120.0/25
- 172.16.120.128/25

El siguiente salto sería _172.16.120.256/xx_ pero esta dirección no exite en Clase C. Por lo que se debe poner: _172.16.121.0/xx_.

## CONFIGURACIÓN DE LOS ENLACES WAN

Utilizaremos **/30** para las direcciones IP en las **13** WAN existentes en la topología de la red.

17  18  19  20  21  22  23  24          --> Prefijos del tercer octeto
25  26  27  28  29  **30**  31  32      --> Prefijos del cuarto octeto
128 64  32  16  8   **4**   2   1       --> Valor decimal del bit y el salto de red

Este bit ya pertenece al cuarto octeto y no al tercero. Con este valor de **30** las redes tendrán saltos de **4**.

172.16.121.0/30   --> Continuamos a partir de esta IP.

- 172.16.121.4/30
- 172.16.121.8/30
- 172.16.121.12/30
- 172.16.121.16/30
- 172.16.121.20/30
- 172.16.121.24/30
- 172.16.121.28/30
- 172.16.121.16/30
- 172.16.121.32/30
- 172.16.121.36/30
- 172.16.121.40/30
- 172.16.121.44/30
- 172.16.121.48/30

El siguiente salto sería _172.16.121.52/xx_.