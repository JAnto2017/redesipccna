# EJERCICIO PT ENRUTAMIENTO ENTRE VLANs, VTP y STP

![alt text](image-44.png "Topología VLANs")
![alt text](image-45.png "Descripción STP, VTP")

## Configuración de VTP ----------------------------------------------------------------

## SWITCH S3

```S3
    ...
    S3# show vtp status

    S3# show running-config

    S3(config)# vtp domain ORGANIZACION
    S3(config)# vtp password Redes
    S3(config)# vtp version 2
    S3(config)# vtp mode server
    
    S3(config)# vlan 10
    S3(config-vlan)# name ESTUDIANTES 

    S3(config)# vlan 20
    S3(config-vlan)# name PROFESORES 

    S3(config)# vlan 30
    S3(config-vlan)# name INVESTIGACION 

    S3(config)# vlan 500
    S3(config-vlan)# name NATIVA&GESTION

    S3# show vtp status

    S3# show vlan brief

    S3# write

    S3(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S3(config-if-range)# switchport mode trunk

    S3(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S3(config-if-range)# switchport trunk native vlan 500

    S3(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S3(config-if)# switchport mode access

    S3(config)# interface fastEthernet 0/1
    S3(config-if)# switchport access vlan 10

    S3(config)# interface fastEthernet 0/2
    S3(config-if)# switchport access vlan 20

    S3(config)# interface fastEthernet 0/3
    S3(config-if)# switchport access vlan 30  
```

## SWITCH S2

```S2
    ...
    S2# show vtp status
    S2(config)# vtp domain ORGANIZACION
    S2(config)# vtp databases password to Redes
    S2(config)# vtp mode client

    S2# show vtp status
    S2# show vlan brief

    S2(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S2(config-if-range)# switchport mode trunk

    S2# show vtp status
    S2# show vlan brief

    S2(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S2(config-if-range)# switchport trunk native vlan 500

    S2(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S2(config-if)# switchport mode access

    S2(config)# interface fastEthernet 0/1
    S2(config-if)# switchport access vlan 10

    S2(config)# interface fastEthernet 0/2
    S2(config-if)# switchport access vlan 20

    S2(config)# interface fastEthernet 0/3
    S2(config-if)# switchport access vlan 30
```

## SWITCH S1

```S1
    ...
    S1(config)# vtp domain ORGANIZACION
    S1(config)# vtp password Redes
    S1(config)# vtp version 2
    S1(config)# vtp mode client

    S1# show vtp status

    S1(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S1(config-if-range)# switchport mode trunk

    S1# show vtp status
    S1# show vlan brief

    S1(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S1(config-if-range)# switchport trunk native vlan 500

    S1(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S1(config-if)# switchport mode access

    S1(config)# interface fastEthernet 0/1
    S1(config-if)# switchport access vlan 10

    S1(config)# interface fastEthernet 0/2
    S1(config-if)# switchport access vlan 20

    S1(config)# interface fastEthernet 0/3
    S1(config-if)# switchport access vlan 30
```

## Configuración de STP ----------------------------------------------------------------

### Switch S1

```S1
    ...
    S1(config)# spanning-tree mode pvst
    S1(config)# spanning-tree rapid-pvst
    S1(config)# spanning-tree vlan 10 root primary
    S1(config)# spanning-tree vlan 30 root secondary
    S1(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S1(config-if-range)# spanning-tree link-type point-to-point

    S1# show spanning-tree

    S1(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S1(config-if-range)# spanning-tree portfast
```

### Switch S2

```S2
    ...
    S2(config)# spanning-tree mode rapid-pvst
    S2(config)# spanning-tree vlan 20 root primary
    S2(config)# spanning-tree vlan 10 root secondary
    S2(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S2(config-if-range)# spanning-tree link-type point-to-point

    S2# show spanning-tree

    S2(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S2(config-if-range)# spanning-tree portfast

    S2# write
```

### Switch S3

```S3
    ...
    S3(config)# spanning-tree mode rapid-pvst
    S3(config)# spanning-tree vlan 30 root primary
    S3(config)# spanning-tree vlan 500 root primary
    S3(config)# spanning-tree vlan 20 root secondary
    S3(config)# interface range fastEthernet 0/23 - fastEthernet 0/24
    S3(config-if-range)# spanning-tree link-type point-to-point

    S3# show spanning-tree

    S3(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S3(config-if-range)# spanning-tree portfast

    S3# show run
```

## Configuración de la gestión de los Switches

### El Switch S1

```S1
    ...
    S1# show vlan brief
    S1(config)# interface vlan 500
    S1(config-if)# description GESTION
    S1(config-if)# ip address 3.3.3.1 255.255.255.240

    S1(config)# ip default-gateway 3.3.3.14
```

### El Switch S2

```S2
    ...
    S2# show vlan brief
    S2(config)# interface vlan 500
    S2(config-if)# description GESTION
    S2(config-if)# ip address 3.3.3.2 255.255.255.240
    S2(config-if)# exit
    S2(config)# ip default-gateway 3.3.3.14
```

### El Switch S3

```S3
    ...
    S3(config)# interface vlan 500
    S3(config-if)# description GESTION
    S3(config-if)# ip address 3.3.3.3 255.255.255.240
    S3(config-if)# exit
    S3(config)# ip default-gateway 3.3.3.14
    S3(config)# exit
    S3# write
```

## Enrutamiento entre VLANs

### Configuración del Switch S3

```S3
    ...
    S3# config teminal
    S3(config)# interface fastEthernet 0/10
    S3(config-if)# description HACIA_ROUTER
    S3(config-if)# switchport mode trunk
    S3(config-if)# switchport trunk allowed vlan 10, 20, 30, 500
    S3(config-if)# switchport trunk native vlan 500

    S3# show running-config
    S3# write
```

### Configuración del Router

```R1
    ...
    R1(config)# interface fastEthernet 0/0
    R1(config-if)# description HACIA_RED
    R1(config-if)# exit
    R1(config)# interface fastEthernet 0/0.10
    R1(config-subif)# encapsulation dot1Q 10
    R1(config-subif)# description ESTUDIANTES
    R1(config-subif)# ip address 192.168.10.1 255.255.255.0
    R1(config-subif)# exit
    R1(config)# interface fastEthernet 0/0.20
    R1(config-subif)# encapsulation dot1Q 20
    R1(config-subif)# description PROFESORES
    R1(config-subif)# ip address 172.16.16.1 255.255.255.0
    R1(config-subif)# exit
    R1(config)# interface fastEthernet 0/0.30
    R1(config-subif)# encapsulation dot1Q 30
    R1(config-subif)# description INVESTIGACION
    R1(config-subif)# ip address 10.20.20.1 255.255.255.0
    R1(config-subif)# exit
    R1(config)# interface fastEthernet 0/0.500
    R1(config-subif)# encapsulation dot1Q 500 native
    R1(config-subif)# description GESTION
    R1(config-subif)# ip address 3.3.3.14 255.255.255.0
    R1(config-subif)# exit
    R1(config)# exit
    R1# show running-config

    R1(config)# interface fastEthernet 0/0
    R1(config-if)# no shutdown
```
