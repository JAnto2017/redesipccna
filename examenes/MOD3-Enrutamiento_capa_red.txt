Pregunta 1
Una computadora puede acceder a dispositivos en la misma red pero no a dispositivos en otras redes. ¿Cuál es la causa probable de este problema?

El cable no está conectada correctamente a la NIC.

La computadora tiene una dirección IP no válida.

NO > La computadora tiene una máscara de subred incorrecta.

done
SI > La computadora tiene una dirección de puerta de enlace predeterminada no válida.

Pregunta 2----------------------------------------------------------------
¿Qué información proporciona la prueba de bucle invertido?

SI > La pila TCP/IP del dispositivo funciona correctamente.

El dispositivo tiene conectividad integral.

DHCP funciona correctamente.

El cable Ethernet funciona correctamente.

close
NO > El dispositivo tiene la dirección IP correcta en la red.

Pregunta 3-----------------------------------------------------------------
¿Qué dos comandos se pueden usar en un host de Windows para mostrar la tabla de enrutamiento? (Elija dos opciones.)

netstat -s

done
SI > route print

show ip route

done
SI > netstat -r

tracert

Pregunta 4----------------------------------------------------
Durante el proceso de reenvío de tráfico, ¿qué hará el enrutador inmediatamente después de hacer coincidir la dirección IP de destino con una red en una enlace de tabla de enrutamiento conectada directamente?

descartar el tráfico después de consultar la tabla de rutas

busque la dirección del siguiente salto para el paquete

done
SI > enviar el paquete a la interfaz conectada directamente

analizar la dirección IP de destino

Pregunta 5-----------------------------------------------------------------
Un enrutador recibe un paquete de la interfaz Gigabit 0/0 y determina que el paquete necesita ser reenviado desde la interfaz Gigabit 0/1. ¿Qué hará el enrutador a continuación?

NO > enrutar el paquete hacia fuera de la interfaz Gigabit 0/1

SI > crear una nueva trama de Ethernet de Capa 2 para ser enviado al destino

NO > buscar en la memoria caché ARP para determinar la dirección IP de destino

close
NO > busque en la tabla de enrutamiento para determinar si la red de destino está en la tabla de enrutamiento

Pregunta 6------------------------------------------------------------------
¿Qué tipo de ruta indica el código C en una tabla de enrutamiento IPv4 en un enrutador Cisco?

ruta estática

ruta predeterminada

done
SI > ruta conectada directamente

ruta dinámica que se obtiene mediante EIGRP

Pregunta 7--------------------------------------------------------------------
Cuando un enrutador recibe un paquete, ¿qué información debe analizarse para que el paquete se envíe a un destino remoto?

dirección MAC de destino

dirección IP de origen

done
SI > dirección IP de destino

dirección MAC de origen

Pregunta 8-----------------------------------------------------------
¿Cuál tipo de ruta es creada cuando un administrador de red configura manualmente una ruta que tiene una interfaz de salida activa?

done
SI > estática

conectada directamente

local

dinámica

Pregunta 9-------------------------------------------------------------
Si una empresa decide no utilizar enrutamiento estática para los cuatro enrutadores dentro de la empresa, ¿cuál sería una solución alternativa?

Utilizar DHCP.

done
SI > Instalar un protocolo de enrutamiento.

Utilizar etiquetas de flujo automáticas.

Permitir que el proveedor de Internet realice el enrutamiento.

Pregunta 10-------------------------------------------------------------------
¿Cuáles dos instrucciones describen las características de una tabla de enrutamiento IPv4 en un enrutador? (Elija dos opciones.)

NO > El comando netstat -r se puede utilizar para mostrar la tabla de enrutamiento de un enrutador.

NO > Las interfaces conectadas directamente tienen dos códigos de origen de ruta en la tabla de enrutamiento: : C y S.

SI > La tabla de enrutamiento almacena información sobre las rutas derivadas de las interfaces del enrutador activo.

done
SI > Si se configura una ruta estática predeterminada en el enrutador, se incluirá una enlace en la tabla de enrutamiento con el código de origen S.

close
NO > La tabla de enrutamiento muestra las direcciones MAC de cada interfaz activa.

Pregunta 11-----------------------------------------------------------------
Un enrutador está recibiendo un flujo de paquetes entrantes que no contiene una ruta a la red de destino remoto. ¿Qué configuración puede emitir un administrador de red para reenviar correctamente los paquetes entrantes?

SI > Agregar una ruta predeterminada.

close
NO > Habilitar un protocolo de enrutamiento dinámico.

NO > SCompartir la tabla de enrutamiento con los hosts en la red local.

NO > Cambiar la dirección IP de la interfaz de ingreso para que coincida con la red de destino objetivo.

Pregunta 12--------------------------------------------------------------------
¿Qué dirección debe configurarse como la dirección de a puerta de enlace predeterminada de un dispositivo cliente?

la dirección de capa 2 de la interfaz de administración del conmutador

la dirección de capa 2 del puerto del conmutador que está conectada a la estación de trabajo

done
SI > la dirección IPv4 de la interfaz del enrutador que está conectada a la misma LAN

la dirección IP de la interfaz del enrutador que está conectada a internet
¡Ha enviado sus respuestas!
close
