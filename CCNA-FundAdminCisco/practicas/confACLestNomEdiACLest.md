# CONFIGURACIÓN DE ACL ESTÁNDAR NOMBRADA Y EDICIÓN DE ACLs ESTÁNDAR

![alt text](image-88.png "Topología para configurar ACL NOMBRADA")

## Objetivo

- Todos los PC puedan acceder excepto PC-ADMIN al router R2 vía Telnet.
- Utilizar un ACL estándar, pero nombrada.

## Configuración

```router
    ...
    R2(config)# ip access-list standard ACCESO_TELNET       //nombre en lugar de número
    R2(config-std-nacl)# deny host 172.20.20.20             //denegar PC
    R2(config-std-nacl)# permit 172.20.20.20 0.0.0.255      //permitir IP de red
    R2(config-std-nacl)# remark ACCESO_RESTRINGIDO          //añadimos una nota

    R2(config)# line vty 0 4
    R2(config-line)# access-class ACCESO_TELNET in          //permite tráfico entrada

    R2# show access-list        //listas las ACL y sus sentencias

    R2(config)# ip access-list standard ACCESO_TELENT
    R2(config-std-nacl)# deny host 172.20.20.3

    /*** cambiar el orden las líneas de comandos en las ACL ***/

    R2(config)# ip access-list standard ACCESO_TELNET
    R2(config-std-nacl)# NO deny host 172.20.20.3
    R2(config-std-nacl)# 15 deny host 172.20.20.3
```
