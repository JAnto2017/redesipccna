# CONFIGURAR ACL PARA ACCESO REMOTO

![alt text](image-87.png "Topología para configurar acceso remoto")

## Objetivo

El PC-PT ADMIN sea el único que tenga acceso remoto al Router R1.

El router R1 debe tener la configuración básica en la interfaz y creadas las líneas de comunicación _line vty_.

Desde los PC-1, PC-2 y PC-3 se puede hacer _Telnet_ e ingresar en el router.

## Configuración

Crear un ACL en el Router para restringir el acceso vía _telnet_ a los PC, excepto al PC-ADMIN (172.20.20.20/24).

```router
    ...
    R1(config)# access-list 21 remark SOLO_ACCESO_REMOTO
    R1(config)# access-list 21 permit host 172.20.20.20     //el deny es implícito al resto

    /*** APLICAMOS EL ACL A LA LÍNEAS VTY ***/

    R1(config)# line vty 0 4
    R1(config-line)# access-class 21 in             //el tráfico a restringir es de entrada
```

Tras la configuración los PCs pueden hacer _ping_ al router pero no pueden conectarse vía _telnet_
