# Práctica de laboratorio con CPT - Router On A Stick VLAN Routing

![alt text](image-181.png)

## Configuración en el Switch

```cisco
SW1> enable
SW1# configure terminal
SW1(config)# vlan 10
SW1(config-vlan)# vlan 20
SW1(config-vlan)# exit

SW1(config)# interface range fastethernet 0/1-2
SW1(config-if-range)# switchport mode access
SW1(config-if-range)# switchport access vlan 10
SW1(config-if-range)# interface range fastethernet 0/3-4
SW1(config-if-range)# switchport mode access
SW1(config-if-range)# switchport access vlan 20
SW1(config-if-range)# exit

SW1(config)# interface gigabitethernet 0/1
SW1(config-if)# switchport mode truck
SW1(config-if)# exit
SW1(config)# end

SW1# show running
```

## Configuración del Router

```cisco
R1> enable
R1# configure terminal
R1(config)# interface gigabitethernet 0/0
R1(config-if)# no shutdown
R1(config-if)# no ip address                    <!-se especifica no incluir dirección IP-!>
R1(config-if)# exit

R1(config)# interface gigabitethernet 0/0.10    <!--El número es referencia puede ser otro número--!>
R1(config-subif)# encapsulation dot1Q 10        <!--Es el mismo número de referencia--!>
R1(config-subif)# ip address 10.1.10.1 255.255.255.0
R1(config-subif)# exit

R1(config)# interface gigabitethernet 0/0.20
R1(config-subif)# encapsulation dot1Q 20        <!--Es el mismo número de referencia--!>
R1(config-subif)# ip address 10.1.20.1 255.255.255.0
R1(config-subif)# end

R1# show running
```
