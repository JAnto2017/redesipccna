# CONFIGURACIÓN BÁSICA SEGURIDAD EN ROUTERS

![alt text](image-75.png "Router 1841")

```router
    R> enable
    R# configure terminal
    R(config)# security password min-length 10          //mínimo de caracteres
    R(config)# enable secret abcdefgh1234               //débil a fuerza bruta
    R(config)# service password-encryption              //encripta todas las claves
    R(config)# line console 0                           //líneas de conexión 
    R(config-line)# password 123456789asdfg
    R(config-line)# login
    R(config-line)# exit

    R(config)# line aux 0                               //línea auxiliar no muy usada
    R(config-line)# no password
    R(config-line)# login                               //no se usa. Dejar desactivado
    R(config-line)# exit

    R(config)# line vty 0 4                             //líneas que permiten conexión remota
    R(config-line)# no transport input                  //deshabilita cualquier conexión
    R(config-line)# transport input ssh                 //acepta solo tráfico ssh
    R(config-line)# transport input telnet              //acepta solo tráfico telnet
    R(config-line)# password ccnacisco54321
    R(config-line)# login                               //clave queda encriptada
    R(config-line)# exec-timeout 5                      //cierra la sesión después de tiempo de inactividad en minutos
```

- - -

## CONFIGURACIÓN BÁSICA SSH

```router
    ...
    Router(config)# hostname R
    R(config)# ip domain-name SECURE_SERVER.COM         //asociado a dominio
    R(config)# crypto key generate rsa                  //cifrado RSA
    How Many bits in the modulus [512]: 1024            //recomendación de 1024 como mínimo
    R(config)# username ADMIN secret ADMIN_REMOTA
    R(config)# line vty 0 4
    R(config-line)# no transport input                  //deshabilitado
    R(config-line)# transport input ssh                 //habilitado ssh
    R(config-line)# login local                         //permitimos acceso local
    R(config-line)# exit
    R(config)# ip ssh time-out 5                        //añadir tiempo inactividad seg.
    R(config)# ip ssh authentication-retries 3          //nº máx. de intentos para acceder
```
