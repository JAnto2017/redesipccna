# PRÁCTICA CONEXIÓN EN TOPOLOGÍA

Realizar configuraciones para exista conexión en la presente topología. Existen problemas de configuración que deben solucionarse.

![alt text](image-16.png)

## EAST_1

```EAST_1
    ...
    # ping 8.8.8.8                  //hacia el ISP
    # ping 192.168.1.129            //hacia LAN
    # traceroute 192.168.1.129      //informa de error en 10.10.10.13

    # show ip route                 //sirve para ver interfaz 10.10.10.x

    # show running-config           //informa del nombre del router con interfaz

    # show ip route 192.168.1.129

    # ping 192.168.1.225
    # ping 192.168.1.193            //error
    # traceroute 192.168.1.194

    # ping 192.168.1.194
    # ping 192.168.1.249

    //una red LAN no está siendo publicada
    (config)# router eigrp 1
    (config-router)# network 192.168.1.0 0.0.0.127  //WILD CARD
```

EAST_4 parece que tiene un problema. Para ver que está publicando.

```EAST_4
    ...
    # show ip route
    
    //DUAL lanza un mensaje 10.10.10.21 está caída.

    # show running-config

    # ping 10.10.10.21

    # show ip protocols

    # show interface serial 0/1/1

    # show ip eigrp interfaces
    # show ip eigrp neighbors

    //error en sumarización de la red LAN. Se debe borrar
    (config)# interface serial 0/0/0
    (config-if)# no ip summary-address eigrp 1 192.168.1.0 255.255.255.0 5
    (config-if)# exit
    (config)# interface serial 0/0/1
    (config-if)# no ip summary-address eigrp 1 192.168.1.0 255.255.255.0 5
    (config-if)# exit
    (config)# interface serial 0/1/1
    (config-if)# no ip summary-address eigrp 1 192.168.1.0 255.255.255.0 5
    (config-if)# exit
```

```EAST_5
    ...
    # show run

    # show ip eigrp neighbors

    //corrección de saludo en la interfaz serial hacia EAST_4
    (config)# interface serial 0/1/1
    (config-if)# no ip hello-interval eigrp 1 70

    # show ip eigrp neighbors
```

```EAST_3
    ...
    # show ip protocols
    # show ip eigrp neighbors
    # show ip eigrp topology

    //Existe un problema con los router EAST_2 y EAST_3 usando sistemas distintos de _eigrp 2_. Otro problema, es que no está creando adyacencia hacia EAST_4.

    (config)# no router eigrp 2
    (config)# router eigrp 1
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# network 192.168.1.192 0.0.0.31
    (config-router)# network 10.10.10.4 0.0.0.3
    (config-router)# network 10.10.10.8 0.0.0.3
```

```EAST_2
    ...
    (config)# no router eigrp 2
    (config)# router eigrp 1
    (config-router)# passive-interface fastEthernet 0/0
    (config-router)# network 192.168.1.128 0.0.0.63
    (config-router)# network 10.10.10.4 0.0.0.3
    (config-router)# network 10.10.10.0 0.0.0.3
    (config-router)# network 10.10.10.16 0.0.0.3

    # show ip eigrp neighbors
    //se comprueba que se hayan creado 3 vecinos: 10.10.10.6; 10.10.10.1; 10.10.10.17; correspondientes a EAST_1, EAST_3 y EAST_5.

    //se detecta avería en serial 0/1/0 hacia EAST_5 tiempos en el EIGRP no ajustados
    (config)# interface serial 0/1/0
    (config-if)# no ip hello-interval eigrp 1 70
    (config-if)# end
```

```ISP
    ...
    //Falta agregar la ruta 10.10.10.0/24
    (config)# ip route 10.10.10.0 255.255.255.0 serial 0/1/1

```
