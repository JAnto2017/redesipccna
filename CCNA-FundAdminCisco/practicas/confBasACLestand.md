# CONFIGURACIÓN BÁSICA ACL ESTANDAR

![alt text](image-84.png "Topología para configurar ACL estándar")

- Configurar LAN amarilla para denegar comunicación a partir del router R1 hacia el router R3. Aplicar ACL estándar en el R1.

## Configuración ACL en R1

> NOTA</br>
> </br>
> Las ACL Estándar se numeran de 1-99</br>
> En las ACL se indica la _wildcard_ inverso de _subnet mask_

```router
    ...

    /*** permitir el tráfico de la red indicada únicamente, el resto no ***/

    R1(config)# access-list 10 permit 172.16.16.0 0.0.0.255

    /*** aplicar el filtro del ACL en la interfaz 0/0/0 al tráfico de salida ***/

    R1(config)# interface serial 0/0/0
    R1(config-if)# ip access-group 10 out

    /*** ACL para denegar el tráfico de la sección amarilla y permitir el resto ***/

    R1(config)# access-list 20 deny 192.168.10.0 0.0.0.255
    R1(config)# access-list 20 permit any

    /*** configuración de la interfaz serial 0/0/0 ***/

    R1(config)# interface serial 0/0/0
    R1(config-if)# no ip access-group 10 out
    R1(config-if)# ip access-group 20 out
```

> NOTA</br>
> </br>
> Estas dos ACLS (la 10 y la 20) hacen lo mismo</br>
> `access-list 10 permit 172.16.16.0 0.0.0.255`</br>
> `access-list 20 deny 192.168.10.0 0.0.0.255`</br>
> `access-list 20 permit any`.

Otra forma de ACLs implícitos. Si creamos un ACLs pero no lo asignamos a una interfaz no tendrán aplicación sobre las tramas de datos.

```router
    ...
    R1(config)# access-list 11 permit 172.16.16.0 0.0.0.255
    R1(config)# access-list 11 deny any

    R1(config)# no access-list 20

    /*** El ACL=10 es igual al ACL=11 ***/
```

Si dos ACLs son iguales comparten propiedades de restricción (denegación implícita).

- - -

## Configurar ACL parte 3

![alt text](image-85.png "Objetivo de la configuración parte 3")

De la sección color amarillo denegar el tráfico de dos PC permitiendo el resto.

- PC1-1 --> 192.168.10.10/24
- PC1-2 --> 192.168.10.20/24
- PC1-3 --> 192.168.10.200/24

Para ello, se debe denegar la IP 192.168.10.0/25

Subnet Mask: 255.255.255.128
Wildcard: 0.0.0.127

```router
    ...
    R1(config)# ip access-list 25 deny 192.168.10.0  0.0.0.127
    R1(config)# access-list 25 permit any

    /*** configurar interfaz serial R1 hacia router R2 ***/

    R1(config)# interface serial 0/0/0
    R1(config-if)# ip access-group 25 out

    R1# show access-lists
```

- - -

## Configurar ACL parte 4

El Objetivo es denegar el tráfico al PC1-3 (192.168.10.200/24) a la sección verde.

> NOTA</br>
> Se debe usar la __wildcard_.

```router
    ...
    R1(config)# access-list 30 deny 192.168.10.200 0.0.0.0 
    R1(config)# access-list 30 permit any

    R1(config)# interface serial 0/0/0
    R1(config-if)# ip access-group 30 out
```

> NOTA</br>
> Esta sentencia: `access-list 30 deny 192.168.10.200 0.0.0.0`</br>
> Es lo mismo que esta otra: `access-list 30 deny 192.168.10.200`.

Otra alternativa al ejemplo anterior es el mostrado a continuación:

```router
    ...
    R1(config)# access-list 31 deny host 192.168.10.200
    R1(config)# access-list 31 permit any

    R1# show access-list            //muestra los ACL configurados

    R1(config)# interface serial 0/0/0
    R1(config-if)# ip access-group 30 out
```

> NOTA</br>
> Cuando se utiliza la palabra `host` no es necesario poner la __wildcard_.</br>
> Sólo se permite una ACL por interfaz.
