# Práctica Problemas Comunes en VLAN

![alt text](image-31.png)

Los _host_ de la VLAN 10 deben configurar entre sí. Lo mismo para la VLAN 20.

## Comprobaciones en Switch S1

```S1
    ...
    S1# show interface gigabitEthernet 0/1 switchport

```

## Comprobaciones en Switch S2

```S2
    ...
    S2# show interface gigabitEthernet 0/1 switchport
    
    //muestra la VLAN 99 como nativa, se debe corregir

    S2# show vlan brief
    
    S2(config)# no vlan 99
    S2(config)# vlan 100
    S2(config-vlan)# name NATIVA

    S2# show run

    S2(config)# interface gigabitEthernet 0/1
    S2(config-if)# no switchport trunk native vlan
    S2(config-if)# switchport trunk native vlan 100

    S2# show interfaces gigabitEthernet 0/1 switchport

    S2(config)# interface gigabitEthernet 0/1
    S2(config-if)# switchport trunk allowed vlan remove 20
    S2(config-if)# switchport trunk allowed vlan 10

    S2# show interfaces gigabitEthernet 0/1 switchport
```

## Comprobaciones en Switch S3

```S3
    ...
    S3# show vlan brief
    S3# show interface gigabitEthernet 0/2 switchport       //interfaz troncal

    //no es recomendable tener un acceso en trunk y el otro en access

    S3(config)# interface gigabitEthernet 0/2
    S3(config-if)# no switchport access vlan
    S3(config-if)# switchport mode trunk
    S3(config-if)# switchport trunk native vlan 100

    //permitir la VLAN 10 y la VLAN 20
    S3(config-if)# switchport trunk allowed vlan 10, 20

    S3# show interface gigabitEthernet 0/2 switchport
```
