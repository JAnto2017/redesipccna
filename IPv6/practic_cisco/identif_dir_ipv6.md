Laboratorio - Identificar direcciones IPv6

Topología

![alt text](image-1.png)

Objetivos

Parte 1: Practicar con diferentes tipos de direcciones IPv6

Parte 2: Examinar una interfaz y una dirección IPv6 de red de host
Aspectos básicos / Escenario

Debido al agotamiento del espacio de direcciones de red del protocolo de Internet versión 4 (IPv4), la adopción de IPv6 y la transición a este nuevo protocolo, los profesionales de redes deben entender cómo funcionan las redes IPv4 e IPv6. Muchos dispositivos y aplicaciones ya admiten el protocolo IPv6. Esto incluye la compatibilidad extendida del Sistema operativo Internetwork (IOS) de los dispositivos Cisco y la compatibilidad de sistemas operativos de estaciones de trabajo y servidores, como Windows y Linux.

Esta práctica de laboratorio se centra en las direcciones IPv6 y los componentes de la dirección. En la Parte 1, identificará los tipos de direcciones IPv6 y la abreviatura de direcciones IPv6. En la Parte 2, verá la configuración de IPv6 en un PC.
Recursos necesarios
font-family:Webdings !important'>=

●   1 PC (Windows con acceso a internet)
Instrucciones
Parte 1: Práctica con diferentes tipos de direcciones IPv6

En esta parte, identificará los diferentes tipos de direcciones IPv6 y practicará la compresión y descompresión de direcciones IPv6.
Paso 1: Una la dirección IPv6 con su tipo.

Una las direcciones IPv6 con el tipo de dirección correspondiente. Observe que las direcciones se comprimieron a su notación abreviada y que no se muestra el número de prefijo de red con barra diagonal. Algunas opciones se deben utilizar más de una vez.

Opciones de respuesta:

a.     Dirección de loopback (bucle invertido)

b.     Dirección unidifusión global

c.     Dirección local de enlace

d.     Dirección local única

e.     Dirección de multidifusión

![alt text](image-2.png)

Respuestas:

![alt text](image-3.png)

Paso 2: Practique la compresión y descompresión de direcciones IPv6.
Preguntas:

Aplique las reglas para la abreviatura de direcciones IPv6 y comprima o descomprima las siguientes direcciones:

2002:0ec0:0200:0001:0000:04eb:44ce:08a2
Área de respuesta

2002:ec0:200:1::4eb:44ce:8a2

fe80:0000:0000:0001:0000:60bb:008e:7402
Área de respuesta

fe80::1:0:60bb:8e:7402

fe80::7042:b3d7:3dec:84b8
Área de respuesta

fe80:0000:0000:0000:7042:b3d7:3dec:84b8

ff00::
Área de respuesta

ff00:0000:0000:0000:0000:0000:0000:0000

2001:0030:0001:acad:0000:330e:10c2:32bf
Área de respuesta

2001:30:1:acad::330e:10c2:32bf

---

Parte 2: Examine una interfaz y una dirección de red de host IPv6

En la parte 2, revisará la configuración de red IPv6 de la PC para identificar la dirección IPv6 de la interfaz de red.
Paso 1: Revise la configuración de la dirección de red IPv6 de la PC.

Verifique que el protocolo IPv6 esté instalado y activo en la PC-A (revise la configuración de la conexión de área local).

a.     Navegue hasta el Control Panel (Panel de control).

b.     En la Vista de categoría, haga clic en el ícono Network and Sharing Center (Centro de redes y recursos compartidos). Haga clic en View network status and tasks (Ver el estado y las tareas de la red).

c.     En la ventana Centro de redes y recursos compartidos, verá las redes activas.

d.     En el lado izquierdo de la ventana, haga clic en Change adapter settings (Cambiar la configuración del adaptador). Ahora debería ver íconos que representan los adaptadores de red instalados. Haga clic con el botón derecho en su interfaz de red activa (puede ser una Ethernet o una WiFi), y luego haga clicen Properties (Propiedades).

e.     En la ventana Propiedades, desplácese por la lista de elementos para determinar si IPv6 está presente, lo que indica que está instalado, y si también está marcado, lo que indica que está activo.

f.      Seleccione el elemento Protocolo de Internet versión 6 (TCP / IPv6) y haga clic en Properties (Propiedades). Debería ver la configuración de IPv6 para la interfaz de red. Es probable que la ventana de propiedades de IPv6 esté establecida en Obtain an IPv6 address automatically (Obtener una dirección IPv6 automáticamente). Esto no significa que IPv6 dependa del protocolo de configuración dinámica de host (DHCP). En lugar de utilizar DHCP, IPv6 busca información de la red IPv6 en el router local y, luego, configura automáticamente sus propias direcciones IPv6. Para configurar IPv6 manualmente, debe proporcionar la dirección IPv6, la longitud del prefijo de subred y el gateway predeterminado. Haga clic en Cancel (Cancelar) para salir de las ventanas de propiedades.

Nota: El router local puede remitir las solicitudes de host de información IPv6, especialmente la información del Sistema de nombres de dominio (DNS), a un servidor DHCPv6 en la red.

g.     Después de verificar que IPv6 esté instalado y activo en la PC, debe revisar la información de dirección IPv6.
Paso 2: Verifique la configuración de la dirección IPv6 con el símbolo del sistema.

a.     Abra un símbolo del sistema e introduzca el comando ipconfig /all. El resultado debe ser similar al siguiente:

C:\Users\user> ipconfig /all

Windows IP Configuration

 

<output omitted>

 

Wireless LAN adapter Wireless Network Connection:

 

   Connection-specific DNS Suffix  . :

   Description . . . . . . . . . . . : Intel(R) Centrino(R) Advanced-N 6200 AGN

   Physical Address. . . . . . . . . : 02-37-10-41-FB-48

   DHCP Enabled. . . . . . . . . . . : Yes

   Autoconfiguration Enabled . . . . : Yes

   Link-local IPv6 Address . . . . . : fe80::8d4f:4f4d:3237:95e2%14(Preferred)

   IPv4 Address. . . . . . . . . . . : 192.168.2.106(Preferred)

   Subnet Mask . . . . . . . . . . . : 255.255.255.0

   Lease Obtained. . . . . . . . . . : Sunday, January 06, 2013 9:47:36 AM

   Lease Expires . . . . . . . . . . : Monday, January 07, 2013 9:47:38 AM

   Default Gateway . . . . . . . . . : 192.168.2.1

   DHCP Server . . . . . . . . . . . : 192.168.2.1

   DHCPv6 IAID . . . . . . . . . . . : 335554320

   DHCPv6 Client DUID. . . . . . . . : 00-01-00-01-14-57-84-B1-1C-C1-DE-91-C3-5D

 

   DNS Servers . . . . . . . . . . . : 192.168.1.1

                                       8.8.4.4

   <output omitted>

b.     Puede observar en el resultado que la PC cliente tiene una dirección IPv6 link-local con una identificación de interfaz generada en forma aleatoria.
Preguntas:

¿Qué indica sobre la red con respecto a la dirección de unidifusión global IPv6, la dirección local única de IPv6 o la dirección de puerta de enlace IPv6?

Indica que no hay un router de gateway con IPv6 habilitado que proporcione la dirección global, la dirección local o la información de subred en la red.

¿Qué tipo de direcciones IPv6 encontró al utilizar ipconfig /all?
Área de respuesta

Las respuestas varían, pero lo más probable es que también sean direcciones link-local.

Preguntas de reflexión

1.     ¿Cómo cree que debe dar soporte a IPv6 en el futuro?
Área de respuesta

Las respuestas pueden variar. Respuesta de ejemplo: los dispositivos futuros deben admitir IPv6 porque IPv6 eventualmente reemplazará a IPv4. IPv6 también simplifica y acelera la transmisión de datos. Además, IPv6 se creó teniendo en cuenta la seguridad. De manera predeterminada, IPv6 cifra el tráfico y verifica la integridad de los paquetes.

2.     ¿Considera que las redes IPv4 continuarán existiendo o que todos finalmente cambiarán a IPv6? ¿Cuánto tiempo cree que llevará esto?
Área de respuesta

Las respuestas pueden variar. Respuesta de ejemplo: la adopción de IPv6 continuará mientras la red IPv4 siga admitiendo dispositivos de usuario de IPv4. Todavía queda mucho tiempo antes de que todos pasen a IPv6 por completo. Este enlace proporciona información sobre IPv6 con capacidad por país: https://stats.labs.apnic.net/ipv6.
