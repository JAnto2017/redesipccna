Packet Tracer - Configuración de direccionamiento IPv6

Tabla de direccionamiento

![alt text](image.png)

Objetivos

Parte 1: Configurar el direccionamiento IPv6 en el router

Parte 2: Configurar el direccionamiento IPv6 en los servidores

Parte 3: Configurar el direccionamiento IPv6 en los clientes

Parte 4: Probar y verificar la conectividad de red

Aspectos básicos

En esta actividad, practicará la configuración de direcciones IPv6 en un router, en servidores y en clientes. También verificará la implementación del direccionamiento IPv6.
Parte 1: Configurar el direccionamiento IPv6 en el router
Paso 1: Habilitar el router para reenviar paquetes IPv6.

a.     Haga clic en R1 y, a continuación, en la ficha CLI. Presione Entrar.

Abrir una ventana de configuración

b.     Ingrese al modo EXEC con privilegios.

c.     Introduzca el comando de configuración global ipv6 unicast-routing. Este comando debe estar configurado para habilitar el router para que reenvíe paquetes IPv6.

R1(config)# ipv6 unicast-routing
Paso 2: Configurar el direccionamiento IPv6 en GigabitEthernet 0/0.

a.     Introduzca los comandos necesarios para pasar al modo de configuración de interfaz para GigabitEhernet0/0.

b.     Configure la dirección IPv6 con el siguiente comando:

R1(config-if)# ipv6 address 2001:db8:1:1::1/64

c.     Configure la dirección IPv6 link-local con el siguiente comando:

R1(config-if)# ipv6 address fe80::1 link-local

d.     Active la interfaz.

R1(config-if)# no shutdown
Paso 3: Configurar el direccionamiento IPv6 en GigabitEthernet 1/0.

a.     Introduzca los comandos necesarios para pasar al modo de configuración de interfaz para GigabitEhernet0/1.

b.     Consulte la tabla de direccionamiento para obtener la dirección IPv6 correcta.

c.     Configure la dirección IPv6 y la dirección link-local, y active la interfaz.
Paso 4: Configurar el direccionamiento IPv6 en Serial 0/0/0.

a.     Introduzca los comandos necesarios para pasar al modo de configuración de interfaz para Serial0/0/0.

b.     Consulte la tabla de direccionamiento para obtener la dirección IPv6 correcta.

c.     Configure la dirección IPv6 y la dirección link-local, y active la interfaz.
Paso 5: Verifique el direccionamiento IPv6 en R1.

Es una buena práctica verificar el direccionamiento cuando está completo comparando los valores configurados con los valores de la tabla de direccionamiento.

a.     Salga del modo de configuración en R1.

b.     Verifique el direccionamiento configurado ejecutando el siguiente comando:

R1# show ipv6 interface brief

c.     Si alguna dirección es incorrecta, repita los pasos anteriores según sea necesario para realizar las correcciones.

Nota: Para realizar un cambio en el direccionamiento con IPv6, debe eliminar la dirección incorrecta o bien tanto la dirección correcta como la incorrecta permanecerán configuradas en la interfaz.

Por ejemplo:

R1(config-if)# no ipv6 address 2001:db8:1:5::1/64

d.     Guarde la configuración en la NVRAM.

Cerrar la ventana de configuración
Parte 2: Configurar el direccionamiento IPv6 en los servidores
Paso 1: Configurar el direccionamiento IPv6 en el servidor de contabilidad.

a.     Haga clic en Accounting (Contabilidad) y, a continuación, en la ficha Desktop (Escritorio) > IP Configuration (Configuración de IP).

b.     Establezca 2001:db8:1:1::4 con el prefijo /64 como la dirección IPv6.

c.     Establezca la dirección link-local fe80::1 como el gateway IPv6.
Paso 2: Configurar el direccionamiento IPv6 en el servidor CAD.

Configure el servidor CAD con direcciones como se hizo en el paso 1. Consulte la tabla de direccionamiento para obtener la dirección Ia usar.
Parte 3: Configurar el direccionamiento IPv6 en los clientes
Paso 1: Configurar el direccionamiento IPv6 en los clientes de ventas y facturación.

a.     Haga clic en Billing (Facturación) y, a continuación, seleccione la ficha Desktop (Escritorio) > IP Configuration (Configuración de IP).

b.     Establezca 2001:DB8:1:1::3 con el prefijo /64 como la dirección IPv6.

c.     Establezca la dirección link-local fe80::1 como el gateway IPv6.

d.     Repita los pasos 1a a 1c para Sales (Ventas). Consulte la tabla de direccionamiento para obtener la dirección IPv6.
Paso 2: Configurar el direccionamiento IPv6 en los clientes de ingeniería y diseño.

a.     Haga clic en Engineering (Ingeniería) y, a continuación, seleccione la ficha Desktop (Escritorio) > IP Configuration (Configuración de IP).

b.     Establezca 2001:db8:1:2::3 con el prefijo /64 como la dirección IPv6.

c.     Establezca la dirección link-local fe80::1 como el gateway IPv6.

d.     Repita los pasos 2a a 2c para el diseño. Consulte la tabla de direccionamiento para obtener la dirección IPv6.
Parte 4: Probar y verificar la conectividad de red
Paso 1: Abrir las páginas web del servidor desde los clientes.

a.     Haga clic en Sales (Ventas) y, a continuación, en la ficha Desktop (Escritorio). Si es necesario, cierre la ventana IP Configuration (Configuración de IP).

b.     Haga clic en Web Browser (Navegador web). Introduzca 2001:db8:1:1::4 en el cuadro URL y haga clic en Go (Ir). Debería aparecer el sitio web de Accounting (Contabilidad).

c.     Introduzca 2001:db8:1:2::4 en el cuadro de URL y haga clic en Go (Ir). Debería aparecer el sitio web de CAD.

d.     Repita los pasos 1a a 1c para el resto de los clientes.
Paso 2: Hacer ping al ISP.

a.     Haga clic en cualquier cliente.

b.     Haga clic en la ficha Desktop (Escritorio) > Command Prompt (Símbolo del sistema).

c.     Pruebe la conectividad al ISP con el siguiente comando:

PC> ping 2001:db8:1:a001::1

d.     Repita el comando ping con otros clientes hasta que se haya verificado la plena conectividad.

---------------

Enrutador R1

---------------

ipv6 unicast-routing

interface GigabitEthernet0/0

 ipv6 address fe80::1 link-local

 ipv6 address 2001:db8:1:1::1/64

 no shutdown

interface GigabitEthernet0/1

 ipv6 address fe80::1 link-local

 ipv6 address 2001:db8:1:2::1/64

 no shutdown

interface Serial0/0/0

 ipv6 address fe80::1 link-local

 ipv6 address 2001:db8:1:a001::2/64

 no shutdown

Fin del documento

© 2017 - 2022 Cisco and/or its affiliates. All rights reserved. Cisco Public 