# Configuración Básica Enlaces Troncales

![alt text](image-30.png)

En el ejemplo, se observa como el direccionamiento IP es diferente, existiendo redes: 192.168.10.0/24, 172.16.16.0/24, 10.20.20.0/24.

- Color _celeste_     --> VLAN 10
- Color _amarillo_    --> VLAN 20
- Color _verde_       --> VLAN 30

## Configuración del Switch S1

```S1
    ...
    Switch(config)# hostname S1

    S1(config)# interface fastEthernet 0/1
    S1(config-if)# description L1
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 10

    S1(config)# interface fastEthernet 0/2
    S1(config-if)# description L2
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 20

    S1(config)# interface fastEthernet 0/3
    S1(config-if)# description L3
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 30
```

## Configuración del Switch S2

```S2
    ...
    Switch(config)# hostname S2

    S2(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S2(config-if-range)# switchport mode access
    S2(config-if-range)# exit
    S2(config)# interface fastEthernet 0/1
    S2(config-if)# switchport access vlan 10
    S2(config-if)# exit
    S2(config)# interface fastEthernet 0/2
    S2(config-if)# switchport access vlan 20
    S2(config-if)# exit
    S2(config)# interface fastEthernet 0/3
    S2(config-if)# switchport access vlan 30
```

## Configuración del Switch S3

```S3
    ...
    Switch(config)# hostname S3

    S3(config)# interface range fastEthernet 0/1 - fastEthernet 0/3
    S3(config-if-range)# switchport mode access
    S3(config-if-range)# exit
    S3(config)# interface fastEthernet 0/1
    S3(config-if)# switchport access vlan 10
    S3(config-if)# exit
    S3(config)# interface fastEthernet 0/2
    S3(config-if)# switchport access vlan 20
    S3(config-if)# exit
    S3(config)# interface fastEthernet 0/3
    S3(config-if)# switchport access vlan 30

    S3# write
```

## Configuración de los puertos troncales

### Configuración del switch S1

```S1
    ...
    S1(config)# interface gigabitEthernet 0/1
    S1(config-if)# switchport mode trunk
```

### Configuración del switch S2

```S2
    ...
    S2(config)# interface gigabitEthernet 0/1
    S2(config-if)# switchport mode trunk

    S2(config)# interface gigabitEthernet 0/2
    S2(config-if)# switchport mode trunk
```

### Configuración del switch S3

```S3
    ...
    S3(config)# interface gigabitEthernet 0/2
    S3(config-if)# switchport mode trunk
```

### Limitar las VLAN que pueden circular por los troncales

```S1
    ...
    S1(config)# interface gigabitEthernet 0/1
    S1(config-if)# switchport trunk allowed vlan 10
    S1(config-if)# switchport trunk allowed vlan 20

    S1(config)# interface gigabitEthernet 0/1
    S1(config-if)# switchport trunk allowed vlan add 10

    S1# show run
```

```S2
    ...
    S2(config)# interface range gigabitEthernet 0/1 - gigabitEthernet 0/2
    S2(config-if-range)# switchport trunk allowed vlan 10, 20

    S2# show run
```

```S3
    ...
    S3(config)# interface gigabitEthernet 0/2
    S3(config-if-range)# switchport trunk allowed vlan 10, 20

    S3# show run
```

## Nombreamientos de la VLANs

- {+ Nombramientos +}

```S1
    ...
    S1(config)# interface fastEthernet 0/4
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 50

    S1# show vlan

    //otra forma de crear VLAN --------------------
    S1(config)# vlan 60
    S1(config-vlan)# name ROJO
    S1(config-vlan)# exit

    //para eliminar VLAN --------------------------
    ...
    S1(config-if)# no switchport access vlan

    S1# show vlan

    S1(config)# no vlan 50
    S1(config)# no vlan 60
```

```S3
    ...
    S3(config)# vlan 10
    S3(config-vlan)# name CELESTE
    S3(config-vlan)# exit
    S3(config)# vlan 20
    S3(config-vlan)# name AMARILLO
    S3(config-vlan)# exit
    S3(config)# vlan 30
    S3(config-vlan)# name VERDE
    S3(config-vlan)# exit
```

## Configuración de VLAN Nativa

```S1
    ...
    S1(config)# vlan 100
    S1(config-vlan)# name NATIVA
    S1(config-vlan)# exit

    S1(config)# interface gigabitEthernet 0/1
    S1(config-if)# switchport trunk native vlan 100

    S1# show vlan brief
```

```S2
    ...
    S2(config)# vlan 100
    S2(config-vlan)# name NATIVA
    S2(config-vlan)# exit

    S2(config)# interface range gigabitEthernet 0/1 - gigabitEthernet 0/2
    S2(config-if-range)# switchport trunk native vlan 100
```

```S3
    ...
    S3(config)# vlan 100
    S3(config-vlan)# name NATIVA
    S3(config-vlan)# exit

    S3(config)# interface gigabitEthernet 0/2
    S3(config-if)# switchport trunk native vlan 100

    S3# show vlan brief
```
