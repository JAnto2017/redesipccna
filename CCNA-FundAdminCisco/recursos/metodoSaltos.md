# MÉTODO DE SALTOS EN IPv4

El método de saltos de red, también conocido como método de incremento de subredes, es una técnica utilizada para crear subredes en una red IPv4. Este método se basa en calcular el tamaño de cada subred (el "salto") y determinar los rangos de direcciones IP asignables a cada una. A continuación, te explico paso a paso cómo funciona:

>
> Una dirección IPv4 tiene 32 bits y se divide en una parte de red y una parte de host. La máscara de subred define cuántos bits se usan para la red y cuántos para los hosts.</br>
> Dividir una red en subredes permite crear redes más pequeñas dentro de una red principal, optimizando el uso de direcciones IP.</br>
> _Bloque de direcciones_: Cada subred tiene un rango específico de direcciones IP asignables.

- - -

## Pasos para aplicar el método de saltos de red

### Paso 1: Determinar el número de subredes necesarias

Decide cuántas subredes necesitas crear. Por ejemplo, si necesitas 5 subredes, debes calcular cuántos bits adicionales se requieren en la máscara de subred.

### Paso 2: Calcular los bits de subred

Usa la fórmula: $$ 2^n≥NumeroSubredes $$
Donde _n_ es el número de bits adicionales necesarios para las subredes.

Por ejemplo, para 5 subredes: $$ 2^3=8≥5 $$

Se necesitan 3 bits adicionales.

### Paso 3: Calcular la nueva máscara de subred

Suma los bits adicionales a la máscara original.
Por ejemplo, si la máscara original es **/24** (255.255.255.0) y añades 3 bits, la nueva máscara será **/27** (255.255.255.224).

### Paso 4: Calcular el tamaño del salto (tamaño de cada subred)

El tamaño del salto se calcula como: $$ Salto = 2^{32−NuevaMascara} $$

Para una máscara /27: $$ Salto = 2^{32−27} = 2^5 = 32 $$

Cada subred tendrá 32 direcciones IP.

### Paso 5: Determinar los rangos de direcciones de cada subred

Comienza con la dirección de red original y _suma el tamaño del salto_ para obtener la siguiente subred.
Por ejemplo, si la red original es 192.168.1.0/24 y el salto es 32:

- Subred 1: 192.168.1.0 - 192.168.1.31
- Subred 2: 192.168.1.32 - 192.168.1.63
- Subred 3: 192.168.1.64 - 192.168.1.95

Y así sucesivamente.

### Paso 6: Identificar direcciones asignables

En cada subred, la primera dirección es la _dirección de red_ y la última es la dirección de _broadcast_. Las direcciones asignables son las intermedias.
Por ejemplo, en la subred 192.168.1.32/27:

- Dirección de red: 192.168.1.32
- Dirección de broadcast: 192.168.1.63
- Direcciones asignables: 192.168.1.33 - 192.168.1.62

- - -

## Ejemplo completo

Supongamos que tienes la red 192.168.1.0/24 y necesitas crear 6 subredes.

### Número de bits adicionales

$2^3=8≥6$ → 3 bits adicionales.

### Nueva máscara de subred

/24 + 3 = /27 (255.255.255.224).

### Tamaño del salto

$2^{32−27}=32$

### Rangos de subredes

- Subred 1: 192.168.1.0 - 192.168.1.31
- Subred 2: 192.168.1.32 - 192.168.1.63
- Subred 3: 192.168.1.64 - 192.168.1.95
- Subred 4: 192.168.1.96 - 192.168.1.127
- Subred 5: 192.168.1.128 - 192.168.1.159
- Subred 6: 192.168.1.160 - 192.168.1.191

### Direcciones asignables

Para la subred 192.168.1.32/27:

Asignables: 192.168.1.33 - 192.168.1.62.

- - -

### Ventajas del método de saltos de red

- Es sistemático y fácil de aplicar.
- Permite calcular rápidamente los rangos de direcciones para cada subred.
- Facilita la planificación y el diseño de redes.

Este método es fundamental para la administración de redes IPv4 y es ampliamente utilizado en entornos profesionales.

- - -

## Pasos en la aplicación del método de saltos de red

Vamos a crear 4 subredes para la red IPv4 192.168.1.0/24 utilizando el método de saltos de red. Te explicaré paso a paso cómo hacerlo.

Datos iniciales

- Dirección de red: 192.168.1.0/24
- Máscara de subred original: 255.255.255.0 (/24)
- Número de subredes requeridas: 4

### Paso 1: Calcular los bits adicionales necesarios

Para crear 4 subredes, usamos la fórmula:

$$ 2^n ≥ NumeroSubredes $$

Donde _n_ es el número de bits adicionales necesarios.

$$ 2^2=4≥4 $$

Necesitamos 2 bits adicionales.

### Paso 2: Calcular la nueva máscara de subred

La máscara original es /24. Añadimos los 2 bits adicionales:

$$ NuevaMascara = 24+2=26 $$

La nueva máscara de subred es 255.255.255.192 (/26).

### Paso 3: Calcular el tamaño del salto (tamaño de cada subred)

El tamaño del salto se calcula como:

$$ Salto=2^{32−NuevaMascara} $$

Para una máscara /26:

$$ Salto=2^{32−26}=2^6=64 $$

Cada subred tendrá 64 direcciones IP.

### Paso 4: Determinar los rangos de direcciones de cada subred

Comenzamos con la dirección de red original (192.168.1.0) y sumamos el tamaño del salto (64) para obtener las siguientes subredes:

1. **Subred 1**:
    - Dirección de red: 192.168.1.0
    - Rango de direcciones: 192.168.1.0 - 192.168.1.63
    - Dirección de broadcast: 192.168.1.63
    - Direcciones asignables: 192.168.1.1 - 192.168.1.62
2. **Subred 2**:
    - Dirección de red: 192.168.1.64
    - Rango de direcciones: 192.168.1.64 - 192.168.1.127
    - Dirección de broadcast: 192.168.1.127
    - Direcciones asignables: 192.168.1.65 - 192.168.1.126
3. **Subred 3**:
    - Dirección de red: 192.168.1.128
    - Rango de direcciones: 192.168.1.128 - 192.168.1.191
    - Dirección de broadcast: 192.168.1.191
    - Direcciones asignables: 192.168.1.129 - 192.168.1.190
4. **Subred 4**:
    - Dirección de red: 192.168.1.192
    - Rango de direcciones: 192.168.1.192 - 192.168.1.255
    - Dirección de broadcast: 192.168.1.255
    - Direcciones asignables: 192.168.1.193 - 192.168.1.254

Tabla resumen de las subredes creadas.

![alt text](image.png "Tabla resumen subredes")

### Explicación de resultados

Cada subred tiene 64 direcciones IP, de las cuales:

La primera es la dirección de red.

La última es la dirección de broadcast.

Las direcciones intermedias son asignables a dispositivos.

El método de saltos de red permite dividir la red original en subredes de tamaño uniforme, lo que facilita la administración y el diseño de la red.

### Verificación

Total de direcciones utilizadas: 4 subredes × 64 direcciones = 256 direcciones (coincide con el tamaño de la red original /24).

Direcciones asignables por subred: 64 - 2 = 62 direcciones (se excluyen la dirección de red y la de broadcast).

- - -

## Algoritmo en Javascript

Aquí tienes un algoritmo en JavaScript que calcula las subredes a partir de una dirección IPv4 y una máscara de subred, utilizando el método de saltos de red. Este algoritmo te permitirá generar las subredes, sus rangos de direcciones, las direcciones de broadcast y las direcciones asignables.

```javascript
function calcularSubredes(ip, mascara, numSubredes) {
    // Convertir la IP a un número entero
    function ipToInt(ip) {
        return ip.split('.').reduce((acc, octeto) => (acc << 8) + parseInt(octeto), 0);
    }

    // Convertir un número entero a una IP
    function intToIp(int) {
        return [(int >>> 24) & 0xFF, (int >>> 16) & 0xFF, (int >>> 8) & 0xFF, int & 0xFF].join('.');
    }

    // Calcular el número de bits necesarios para las subredes
    const bitsSubred = Math.ceil(Math.log2(numSubredes));

    // Calcular la nueva máscara de subred
    const nuevaMascara = mascara + bitsSubred;

    // Calcular el tamaño del salto (tamaño de cada subred)
    const salto = Math.pow(2, 32 - nuevaMascara);

    // Convertir la IP base a un número entero
    const ipBaseInt = ipToInt(ip);

    // Generar las subredes
    const subredes = [];
    for (let i = 0; i < numSubredes; i++) {
        const dirRedInt = ipBaseInt + i * salto;
        const dirBroadcastInt = dirRedInt + salto - 1;

        const dirRed = intToIp(dirRedInt);
        const dirBroadcast = intToIp(dirBroadcastInt);
        const primeraAsignable = intToIp(dirRedInt + 1);
        const ultimaAsignable = intToIp(dirBroadcastInt - 1);

        subredes.push({
            subred: i + 1,
            direccionRed: dirRed,
            primeraAsignable: primeraAsignable,
            ultimaAsignable: ultimaAsignable,
            direccionBroadcast: dirBroadcast,
            mascara: `/${nuevaMascara} (${intToIp((0xFFFFFFFF << (32 - nuevaMascara)) >>> 0)})`
        });
    }

    return subredes;
}

// Ejemplo de uso
const ip = "192.168.1.0";
const mascara = 24; // Máscara original (/24)
const numSubredes = 4; // Número de subredes requeridas

const subredes = calcularSubredes(ip, mascara, numSubredes);

// Mostrar resultados
console.log("Subredes generadas:");
subredes.forEach(subred => {
    console.log(`Subred ${subred.subred}:`);
    console.log(`  Dirección de red: ${subred.direccionRed}`);
    console.log(`  Primera dirección asignable: ${subred.primeraAsignable}`);
    console.log(`  Última dirección asignable: ${subred.ultimaAsignable}`);
    console.log(`  Dirección de broadcast: ${subred.direccionBroadcast}`);
    console.log(`  Máscara de subred: ${subred.mascara}`);
    console.log("-----------------------------");
});
```

>
> Explicación del código

1. Conversión de IP a entero y viceversa:
    - ipToInt(ip): Convierte una dirección IP en un número entero para facilitar los cálculos.
    - intToIp(int): Convierte un número entero de nuevo a una dirección IP.
2. Cálculo de bits adicionales:
    - Se calcula el número de bits necesarios para las subredes usando Math.ceil(Math.log2(numSubredes)).
3. Nueva máscara de subred:
    - La nueva máscara se obtiene sumando los bits adicionales a la máscara original.
4. Tamaño del salto:
    - El tamaño de cada subred se calcula como $ 2^{(32−nuevaMascara)} $
5. Generación de subredes:
    - Se itera para generar cada subred, calculando la dirección de red, la dirección de broadcast y las direcciones asignables.
6. Resultados:
    - El algoritmo devuelve un array de objetos, donde cada objeto representa una subred con sus detalles.

>
> Ejemplos de salida

Para la red 192.168.1.0/24 y 4 subredes, la salida será:

```txt
Subredes generadas:
Subred 1:
  Dirección de red: 192.168.1.0
  Primera dirección asignable: 192.168.1.1
  Última dirección asignable: 192.168.1.62
  Dirección de broadcast: 192.168.1.63
  Máscara de subred: /26 (255.255.255.192)
-----------------------------
Subred 2:
  Dirección de red: 192.168.1.64
  Primera dirección asignable: 192.168.1.65
  Última dirección asignable: 192.168.1.126
  Dirección de broadcast: 192.168.1.127
  Máscara de subred: /26 (255.255.255.192)
-----------------------------
Subred 3:
  Dirección de red: 192.168.1.128
  Primera dirección asignable: 192.168.1.129
  Última dirección asignable: 192.168.1.190
  Dirección de broadcast: 192.168.1.191
  Máscara de subred: /26 (255.255.255.192)
-----------------------------
Subred 4:
  Dirección de red: 192.168.1.192
  Primera dirección asignable: 192.168.1.193
  Última dirección asignable: 192.168.1.254
  Dirección de broadcast: 192.168.1.255
  Máscara de subred: /26 (255.255.255.192)
-----------------------------
```

>
> Cómo utilizar el algoritmo

Cambia los valores de ip, mascara y numSubredes según tus necesidades.

Ejecuta el código en un entorno Node.js o en la consola del navegador.
