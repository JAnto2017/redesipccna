# CONFIGURACIÓN BÁSICA DE ACLs EXTENDIDAS

![alt text](image-89.png "Topología para la configuración ACL extendida")

## Conocimientos Previos

> ACLs Estándar se configuran en el punto más cercano al destino de los paquetes. Los rangos de numeración: 1 a 99.</br>
> ACLs Extendidas se configuran en puntos más cercanos al origen de los paquetes. Los rangos de numeración: 100 a 199.</br>

## Objetivos

1. Limitar el tráfico de la PC1, prohibiendo que:
   1. haga _telnet_ al router R2
   2. el servicio _web_ con el protocolo _http_. En el router se debe escoger _tcp_.

## Configuración

La ACLs debe estar en el router R1, por proximidad al PC1. El router R1 tiene la configuración básica, además de las líneas _vty_.

```router
    ...
    /*** limitar telnet (23) del host origen al host destino ***/

    R1(config)# access-list 101 deny tcp host 192.168.1.10 host 2.2.2.2 eq 23   
    R1(config)# access-list 101 permit ip any any

    /*** asignamos a una interfaz del router el ACL 101, tráfico es de entrada ***/

    R1(config)# interface fastEthernet 0/0
    R1(config)# ip access-group 101 in

    /*** denegar el servicio www al PC2 con ACL Extendida ***/

    R1(config)# no access-list 101
    R1(config)# access-list 101 deny tcp host 192.168.1.10 host 2.2.2.2 eq telnet
    R1(config)# access-list 101 deny tcp host 192.168.1.20 any eq www
    R1(config)# access-list 101 permit ip any any
```

- - -

## ACLs Nombrada

Crear reglas ACLs Extendidas para el PC2 en el router R2.

```router
    ...
    R2(config)# ip access-list extended FILTRO_PC2
    /*** deniega a 4 dir. IP (0.0.0.3) de la red 172. hacia la red 192. ***/
    R2(config-ext-nacl)# deny tcp 172.16.1.8 0.0.0.3 192.1.0.0 0.0.0.255 eq ftp established
    R2(config-ext-nacl)# remark OMITIR_PING
    R2(config-ext-nacl)# deny icmp host 172.16.1.10 any echo
    R2(config-ext-nacl)# permit ip any any

    R2(config)# interface fastEthernet 0/0
    R2(config-if)# ip access-group FILTRO_PC2 in 

    /*** Sentencias extras. Bloquea respuestas de ping ***/
    R2(config)# ip access-list extended FILTRO_PC2
    R2(config-ext-nacl)# 25 deny icmp host 172.16.1.10 any echo-replay
```
