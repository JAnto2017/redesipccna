# Ejercicio VTP

![alt text](image-33.png)
![alt text](image-34.png)

## Switch S1

```S1
    ...
    S1(config)# vtp domain EMPRESA
    S1(config)# vtp mode server
    S1(config)# vtp version 2
    S1(config)# vtp password topología

    S1(config)# vlan 11
    S1(config-vlan)# name AMARILLO

    S1(config)# vlan 22
    S1(config-vlan)# name CELESTE

    S1(config)# vlan 33
    S1(config-vlan)# name VERDE

    S1(config)# vlan 44
    S1(config-vlan)# name AZUL

    S1(config)# vlan 100
    S1(config-vlan)# name NATIVA

    S1# show vlan brief
    S1# show vtp status
    S1# show vtp password
```

```S1
    ...
    S1(config)# interface range fastEthernet 0/23 - fastEthernetwork 0/24
    S1(config-if-range)# switchport mode trunk
    S1(config-if-range)# switchport trunk native vlan

    S1(config)# interface fastEthernet 0/1
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 11
    S1(config-if)# exit
    S1(config)# interface fastEthernet 0/2
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 22
    S1(config-if)# exit
    S1(config)# interface fastEthernet 0/3
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 33
    S1(config-if)# exit
    S1(config)# interface fastEthernet 0/4
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 44
    S1(config-if)# exit

    S1# show vlan brief
```

## Switch S4

```S4
    ...
    S4(config)# vtp domain EMPRESA
    S4(config)# vtp password Topologia
    S4(config)# vtp version 2

    S4# show vtp status
    S4# show vlan brief     //se ven las VLAN ya creadas

    S4# write 
```

```S4
    ...
    S4(config)# interface fastEthernet 0/24
    S4(config-if)# switchport mode trunk 
    S4(config-if)# switchport trunk native vlan 100
```

```S4
    ...
    S4(config)# interface fastEthernet 0/1
    S4(config-if)# switchport mode access
    S4(config-if)# switchpor access vlan 11

    S4(config)# interface fastEthernet 0/2
    S4(config-if)# switchport mode access
    S4(config-if)# switchpor access vlan 22
```

## Switch S3

```S3
    ...
    S3(config)# vtp domain EMPRESA
    S3(config)# vtp password Topologia
    S3(config)# vtp version 2

    S3# show vtp status

    S3(config)# vtp mode transparent
```

```S3
    ...
    S3(config)# interface range fastEthernet 0/21 - fastEthernet 0/24
    S3(config-if-range)# switchport mode trunk
    S3(config-if-range)# switchport native vlan 100
    S3# write
```

```S3
    ...
    S3# show vtp status
    S3# show vlan brief

    S3(config)# vlan 555
    S3(config-vlan)# name PURPURA

    S3(config)# vlan 777
    S3(config-vlan)# name MARRON

    S3# show vlan brief
    S3# show vtp status

    S3(config)# interface fastEthernet 0/1
    S3(config-if)# switchport mode access
    S3(config-if)# switchport access vlan 555

    S3(config)# interface fastEthernet 0/2
    S3(config-if)# switchport mode access
    S3(config-if)# switchport access vlan 777
```

## Switch S2

```S2
    ...
    S2(config)# vtp domain EMPRESA
    S2(config)# vtp password Topologia
    S2(config)# vtp version 2

    S2# show vlan brief
    S2# show interface fastEthernet 0/21 switchport

    S2# show vlan brief     //muestra las VLAN aprendidas

    S2(config)# vtp mode transparent
```

```S2
    ...
    S2# show vlan brief     //muestra las VLAN aprendidas desde el servidor VTP

    //Configurar las VLAN locales 555 y 777

    S2(config)# vlan 555
    S2(config-vlan)# name PURPURA

    S2(config)# vlan 777
    S2(config-vlan)# name MARRON

    S2(config)# interface fastEthernet 0/1
    S2(config-if)# switchport mode access
    S2(config-if)# switchport access vlan 555

    S2(config)# interface fastEthernet 0/2
    S2(config-if)# switchport mode access
    S2(config-if)# switchport access vlan 777

    S2# show vlan brief     //muestra las VLAN aprendidas y las dos configuradas
    S2# show vtp status     //número de VLANs : 12
```

## Switch S5

```S5
    ...
    S5(config)# vtp domain EMPRESA
    S5(config)# vtp password Topologia
    S5(config)# vtp version 2

    S5# show vtp status
    S5# show vlan brief     //se observan las VLAN aprendidas

    S5# show interface fastEthernet 0/24 switchport

    S5(config)# interface fastEthernet 0/24
    S5(config-if)# switchport mode trunk
    S5(config-if)# switchport trunk native vlan 100

    S5(config)# vtp mode client

```

## Switch S6

```S6
    ...
    S6(config)# vtp domain EMPRESA
    S6(config)# vtp password Topologia
    S6(config)# vtp version 2

    S6# show vlan brief

    S6(config)# vtp mode client

    S6# show vtp status
```

```S6
    ...
    S6(config)# interface fastEthernet 0/22
    S6(config-if)# switchport trunk native vlan 100
```

```S6
    ...
    S6(config)# interface fastEthernet 0/24
    S6(config-if)# switchport mode trunk
    S6(config-if)# switchport trunk vlan 100
```

```S6
    ...
    S6(config)# interface fastEthernet 0/1
    S6(config-if)# switchport mode access 
    S6(config-if)# switchport access vlan 22
```

## Switch S7

```S7
    ...
    S7(config)# vtp domain EMPRESA
    S7(config)# vtp password Topologia
    S7(config)# vtp version 2
    S7(config)# vtp mode client

    S7# show vtp status
    S7# show vlan brief
```

```S7
    ...
    S7(config)# interface fastEthernet 0/24
    S7(config-if)# switchport mode trunk
    S7(config-if)# switchport trunk native vlan 100
```

```S7
    ...
    S7(config)# interface fastEthernet 0/1
    S7(config-if)# switchport mode access
    S7(config-if)# switchport access vlan 11

    S7(config)# interface fastEthernet 0/2
    S7(config-if)# switchport mode access
    S7(config-if)# switchport access vlan 33
```
