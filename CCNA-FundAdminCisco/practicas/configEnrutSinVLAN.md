# Configuración de Enrutamiento

## Configuración sin VLAN

![alt text](image-40.png)

## Configuración del Router

```router
    ...
    R(config)# interface fastEthernet 0/0
    R(config-if)# ip address 192.168.1.1 255.255.255.0
    R(config-if)# no shutdown

    R(config)# interface fastEthernet 0/1
    R(config-if)# ip address 192.168.3.1 255.255.255.0
    R(config-if)# no shutdown

    R# show running-config

    R# show ip route
```

- - -

## Configuración con VLAN

![alt text](image-41.png)

```router
    ...
    R# show ip route
```

```switch
    ...
    S(config)# interface fastEthernet 0/1
    S(config-if)# switchport mode access
    S(config-if)# switchport access vlan 10

    S(config)# interface fastEthernet 0/2
    S(config-if)# switchport mode access
    S(config-if)# switchport access vlan 30
```

Ahora si hacemos PING entre los PC no responde, ya que están en VLAN distintas.

```switch
    ...
    S(config)# interface fastEthernet 0/24      //interfaz Fa0/24 va hacia el router en Fa0/0
    S(config-if)# switchport mode access
    S(config-if)# switchport access vlan 10

    S(config)# interface fastEthernet 0/23      //interfaz Fa0/23 va hacia el router en Fa0/1
    S(config-if)# switchport mode access
    S(config-if)# switchport access vlan 30
```

## Configuración con VLAN y un único enlace entre Switch y Router

![alt text](image-42.png)

- Se utilizará la encapsulación **dot1Q**.
- La conexión en el _switch_ es en la interfaz Fa0/10.
- La conexión en el _router_ es en la interfaz Fa0/0.
- La interfaz en el _switch_ tiene que estar en modo **_troncal_**.

```switch
    ...
    S(config)# interface fastEthernet 0/10
    S(config-if)# switchport mode trunk
    S(config-if)# switchport trunk allowed vlan 10,30
```

```router
    ...
    R(config)# interface fastEthernet 0/1
    R(config-if)# no ip address

    R# show run

    R(config)# interface fastEthernet 0/0
    R(config-if)# no ip address
    
    R(config)# interface fastEthernet 0/0.10                //interfaz lógica + VLAN 10
    R(config-subif)# encapsulation dot1Q 10
    R(config-subif)# ip address 192.168.1.1 255.255.255.0

    R# show run

    R(config)# interface fastEthernet 0/0.30                //interfaz lógica + VLAN 30
    R(config-subif)# encapsulation dot1Q 30
    R(config-subif)# ip address 192.168.3.1 255.255.255.0

    R# show run
```

## Gestión remota del Switch

![alt text](image-43.png)

En el _switch_ se crean interfaces VLANs.

> RECUERDA!
> Es diferente la VLAN y la interface VLAN.
> La interface VLAN puede manejar IP.

```switch
    ...
    S1# configure terminal
    S1(config)# vlan 25
    S1(config-vlan)# name NATIVA&GESTION
    S1(config-vlan)# exit

    S1(config)# interface fastEthernet 0/10
    S1(config-if)# switchport trunk native vlan 25
    S1(config-if)# switchport trunk allowed vlan add 25

    S1(config)# interface vlan 25
    S1(config-if)# description NATIVA&GESTION
    S1(config-if)# ip address 10.10.10.10 255.255.255.0
    S1(config-if)# exit
    S1(config)# ip default-gateway 10.10.10.1

    S1(config)# line vty 0 15
    S1(config-line)# password cisco
    S1(config-line)# login
    S1(config)# enable password cisco
```

```router
    ...
    R(config)# interface fastEthernet 0/0.25
    R(config-subif)# encapsulation dot1Q 25
    R(config-subif)# description NATIVA&GESTION
    R(config-subif)# ip address 10.10.10.1 255.255.255.0

    R# show run

    R(config)# interface fastEthernet 0/0.25
    R(config-subif)# encapsulation dot1Q 25 native

    R# show run

    R# ping 10.10.10.10     //deberíamos obtener respuesta

    R# telnet 10.10.10.10   //acceso al switch con password
```
