# PRÁCTICA CONFIGURACIÓN BÁSICA DE EIGRP PARA IPV6

![alt text](image-17.png)

## PC1

![alt text](image-19.png)

## PC2

![alt text](image-18.png)

## PC3

![alt text](image-20.png)

Prueba desde PC3 a PC1:
![alt text](image-21.png)

## R1

- Hbilitado `ipv6 unicast-routing`.
- FastEthernet 0/0 --> IPv6: `FC00:AA:BB::/64`.
- Serial 0/0/0 --> IPv6: `2000:ABCD:1234::/64 eui-64`.
- Serial 0/0/1 --> IPv6: `2002:FF:AB:AB::/64 eui-64`.
- No direccionamiento estático o dinámico.

---

```R1
    ...
    (config)# ipv6 router eigrp 10
    (config-rtr)# passive-interface fastEthernet 0/0
    (config-rtr)# eigrp router-id 1.1.1.1
    (config-rtr)# no shutdown
    
    (config-rtr)# do show config
    (config-rtr)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 eigrp 10
    (config-if)# exit

    (config)# interface serial  0/0/1
    (config-if)# ipv6 eigrp 10
    (config-if)# exit

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 eigrp 10
    (config-if)# exit
```

## R2

- Posee una interfaz _LOOPBACK_ IPv6: `EE:EE:EE:EE:15::/64`.
- Hbilitado `ipv6 unicast-routing`.
- FastEthernet 0/0 --> IPv6: `FC11:12FF:34:EE::/64`.
- Serial 0/0/0 --> IPv6: `2001:BB:CC:DD::/64 eui-64`.
- Serial 0/0/1 --> IPv6: `2000:ABCD:1234::/64 eui-64`.

---

```R2
    ...
    (config)# ipv6 router eigrp 10
    (config-rtr)# eigrp router-id 2.2.2.2
    (config-rtr)# passive-interface fastEthernet 0/0
    (config-rtr)# no shutdown
    (config-rtr)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 eigrp 10
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 eigrp 10
    (config-if)# end

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 eigrp 10
    (config-if)# exit

    (config)# ipv6 router eigrp 10
    (config-rtr)# redistribute static
```

## R3

- Hbilitado `ipv6 unicast-routing`.
- FastEthernet 0/0 --> IPv6: `FC10:D22D:40::/40`.
- Serial 0/0/0 --> IPv6: `2002:FF:AB:AB::/64 eui-64`.
- Serial 0/0/1 --> IPv6: `2001:BB:CC:DD::/64 eui-64`.

---

```R3
    ...
    (config)# ipv6 router eigrp 10
    (config-rtr)# eigrp router-id 3.3.3.3
    (config-rtr)# passive-interface fastEthernet 0/0
    (config-rtr)# no shutdown
    (config-rtr)# exit

    (config)# interface fastEthernet 0/0
    (config-if)# ipv6 eigrp 10
    (config-if)# exit

    (config)# interface serial 0/0/0
    (config-if)# ipv6 eigrp 10
    (config-if)# exit

    (config)# interface serial 0/0/1
    (config-if)# ipv6 eigrp 10
    (config-if)# end

    # show ipv6 route
```
