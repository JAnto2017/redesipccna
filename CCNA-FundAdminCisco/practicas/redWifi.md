# PRÁCTICA RED WIFI

![alt text](image-46.png "Topología ejercicio práctico")

## Router WiFi

![alt text](image-47.png "Configuración GUI de la WAN del router WiFi")

![alt text](image-48.png "Configuración GUI de la LAN del router WiFi")

![alt text](image-49.png "Configuración GUI de la WiFi")

![alt text](image-50.png "Configuración GUI del cifrado WiFi")

## PC Wireless

![alt text](image-51.png "Icono de PC Wireless")
![alt text](image-52.png "Clic en Connect y en Refresh")
![alt text](image-53.png "Clic en Connect")
![alt text](image-54.png "Añadir clave y hacer clic en Connect")
![alt text](image-55.png "Uso del comando IPCONFIG en consola")