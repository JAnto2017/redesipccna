# Configuración del PortFast en Switch

## Topología

![alt text](image-35.png)

Los PC están conectados en las Fa0/24 y no participan en topologías de enlaces convergentes.

## Switch S1

Habilitar el _PortFast_ en la interfaz, hacia dispositivos finales.

```S1
    ...
    S1# configure terminal
    S2(config)# interface fastEthernet 0/24
    S2(config-if)# spanning-tree portfast
```

## Switch S2

```S2
    ...
    S2(config)# interface fastEthernet 0/24
    S2(config-if)# switchport mode access
    S2(config-if)# spanning-tree portfast
```

## Switch S3

```S3
    ...
    S3(config)# interface fastEthernet 0/24
    S3(config-if)# switchport mode access
    S3(config-if)# spanning-tree portfast
```
