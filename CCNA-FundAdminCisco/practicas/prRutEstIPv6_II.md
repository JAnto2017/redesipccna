# Práctica Configuración Básica y Ruteo Estático de IPv6 (Parte 2)

![alt text](image-1.png)

## PC3

- _IPv6 Address_: FC10:D22D:40:25:: / 48
- _Link Local Address_: FE80::201:42FF:FE28:17D8
- _IPv6 Gateway_: FC10:D22D:40::

## R3

configuración de la interface Ethernet:

```router
enable
configure terminal
ipv6 unicast-routing
interface fastEthernet 0/0
ipv6 address FC10:D22D:40::/48
end

show running-config

```

Configuración de la interface Serial:

```router
enable
configure terminal
interface serial 0/0/0
ipv6 address 2001:bb:cc:dd::/64 eui-64
end

show ip interface brief

show ipv6 interface fastEthernet 0/0
```

El _eui-64_ crea una dirección única usando la MAC.

Para crear una ruta por defecto:

```router
enable
configure terminal
ipv6 route ::/0 serial 0/0/0
end

show ip route

show ipv6 route
```
