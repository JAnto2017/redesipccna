# Configuración Básica de VLAN

![alt text](image-29.png)

## VLAN 50

La VLAN 50 zona de color amarillo.

## VLAN 100

La VLAN 100 zona de color verde.

## VLAN 200

La VLAN 200 zona de color celeste.

## Configuración de la VLAN 50 en el Switch

```switch
    ...
    S1(config)# interface fastEthernet 0/1
    S1(config-if)# description L1
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 50    //esta interface solo acepta VLAN 50
    
    S1(config)# interface fastEthernet 0/4
    S1(config-if)# description L4
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 50
```

## Configuración de la VLAN 100 en el Switch

```switch
    ...
    S1(config)# interface fastEthernet 0/2
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 100
    S1(config-if)# description L2

    S1(config)# interface fastEthernet 0/5
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 100
    S1(config-if)# description L5
```

## Configuración de la VLAN 200 en el Switch

```switch
    ...
    S1(config)# interface fastEthernet 0/3
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 200
    S1(config-if)# description L3

    S1(config)# interface fastEthernet 0/6
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 200
    S1(config-if)# description L6
```

## Configuración VLAN 150 para VoIP

```switch
    ...
    S1(config)# interface fastEthernet 0/3
    S1(config-if)# mls qos trust cos
    S1(config-if)# switchport voice vlan 150
    S1(config-if)# exit

    S1(config)# interface fastEthernet 0/6
    S1(config-if)# mls qos trust cos
    S1(config-if)# switchport voice vlan 150
    S1(config-if)# exit

    S1# show interface fastEthernet 0/3 switchport

    S1# show vlan
```
