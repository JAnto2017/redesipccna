# PRÁCTICAS BACKUP DE CONFIGURACIÓN Y FLASH

![alt text](image-77.png "Topología práctica configurar backup")

## Configurar ROUTER

Guardar copia en el servidor TFTP.

```router
    ROUTER# copy running-config startup-config
    ROUTER# copy startup-config tftp:
    Address or name of remote host []? 192.168.168.1
    Destination filename [ROUTER-config]? BACKUP_CONFIG_ROUTER

    ROUTER# show flash

    ROUTER# copy flash: tftp:
    Source filename []? <<pegar aquí el nombre del archivo antes visualizado>>
    Address or name of remote host []? 192.168.168.1
    Destination filename []? BACKUP_IOS_ROUTER
```

## Servidor TFTP

![alt text](image-78.png "Pestaña TFTP")
![alt text](image-79.png "Archivos enviados al servidor")

- - -

## RESTABLECIMIENTO DEL IOS Y LA CONFIGURACIÓN DEL ROUTER

```router
    ...
    ROUTER# write erase
    [confirm] yes

    ROUTER# show flash

    ROUTER# delete flash:
    Delete filename []? archivoxxxxx.bin
    [confirm] yes

    ROUTER# reload

    /*** AL BORRAR EL PROMPT CAMBIA AL MOSTRADO, EN MODO ROM. NO PERMITE ESCRIBIR COMANDOS ***/

    rommon 1 > 
```

Para solucionar el problema del ROUTER, se debe conectar un PC a la interfaz RS-232 (consola) para realizar las configuraciones desde el PC y no dentro del ROUTER.

![alt text](image-80.png "PC conectado por Consola y Fa0/0")
![alt text](image-81.png "PC conectado por Consola y Fa0/0")
![alt text](image-82.png "IP estática en PC de configuración")

```console
    rommon 1 > ?                                //muestra los comandos aceptados
    rommon 2 > tftpdnld
    rommon 3 > IP_ADDRESS=192.168.0.2
    rommon 4 > IP_SUBNET_MASK=255.255.255.0
    rommon 5 > DEFAULT_GATEWAY=192.168.0.1
    rommon 6 > TFTP_SERVER=192.168.0.1          //PC de config. hace de servidor
    rommon 7 > TFTP_FILE=nombredelarchivo.bin
    rommon 8 > tftpdnld
    Do you wish to continue? y/n: yes

    <<Ahora el router carga el archivo backup desde el PC de configuración por la interfaz fastEthernet>>

    rommon 9 > reset

    <<El router queda con la configuración de fábrica>>

    Router# configure terminal
    Router(config)# interface fastEthernet 0/0
    Router(config-if)# ip address 192.168.0.2 255.255.255.0
    Router(config-if)# no shutdown

    Router# copy tftp: running-config
    Address or name of remote host []? 192.168.0.1
    Source filename []? BACKUP_CONFIG_ROUTER           //recupera la configuración del router
```

- - -

## RESTABLECIENDO LA CONFIGURAIÓN IOS - OTRA FORMA

```router
    ROUTER# write erase     //borra la configuración
    ROUTER# reload          //reinicio 

    Router> enable
    Router# show flash
    Router# delete flash:   //para borra la flash
    Delete filename ? archivo-xxx-23-4-yyy.bin
    ROUTER# reload          //reinicio 

    rommon 1 >
```

Para restablecer la copia de seguridad, se debe conectar un PC por Consola (RS-232), pero en esta ocasión no llevamos los archivos de configuración ni de la flas en el PC.

![alt text](image-83.png "Topología con PC de configuración en interfaz RS-232 y Servidor")

```console
    rommon 1 > tftpdnld                 //muestra las instrucciones de configuración
    rommon 2 > IP_ADDRESS=20.20.20.2
    rommon 3 > IP_SUBNET_MASK=255.255.255.0
    rommon 4 > DEFAULT_GATEWAY=20.20.20.1
    rommon 5 > TFTP_SERVER=192.168.168.1
    rommon 6 > TFTP_FILE=BACK_IOS_ROUTER
    rommon 7 > tftpdnld

    /** una vez que termina la carga se debe reiniciar **/

    rommon 8 > reset

    /*** Restablecemos IP en la interfaz Fa0/1 ***/

    Router> enable
    Router# config terminal
    Router(config)# interface fastEthernet 0/1
    Router(config-if)# ip address 20.20.20.2 255.255.255.0
    Router(config-if)# no shutdown

    /*** prueba conectividad con router-servidores ***/

    Router# ping 20.20.20.1

    /*** Creamos ruteo para alcanzar el Servidor ***/

    Router(config)# ip route 192.168.168.0 255.255.255.0 20.20.20.1

    /*** Cargar imagen de la configuración ***/

    Router# copy tftp: running-config
    Address or name of remote host ? 192.168.168.1
    Source filename ? BACKUP_CONFIG_ROUTER
```

> NOTA
> Cuando se realiza este modo de configuración.
> El router debe tener conectado el cable en la fastEthernet 0/0.

- - -

## RECUPERACIÓN DE CONTRASEÑAS

Cuando perdemos las claves de acceso a los _routers_, una forma de recuperarlas es conectando un PC en local a través de la consola.

```consola
    Password:
```

Se debe apagar el _router_ y encenderlo; miestras se está reiniciando, se pulsa la tecla _pausa_ o las teclas _Ctrl+Supr_. Ésto produce, la interrupción del reinicio, quedando en modo ROM. Ahora se debe ingresar los siguientes comandos, que abren un puerto de acceso:

```consola
    rommon 1 > confreg 0x2142
    rommon 2 > reset
```

Tras el reinicio anterior:

```consola
    Router> enable
    Router# show run

    /*** copiar la configuración de inicio a la activa ***/

    Router# copy startup-config running-config
    [running-config]?

    ROUTER# show run        //muestra configuración original del router

    /** los password se muestran encriptados combinando números y letras **/
    /*** Reiniciar los password ***/

    ROUTER# config terminal
    ROUTER(config)# line vty 0 4
    ROUTER(config-line)# password cisco
    ROUTER(config-line)# login
    ROUTER(config-line)# exit
    ROUTER(config)# enable password cisco
    ROUTER(config)# exit
    ROUTER# show run

    /*** Encendemos todas las interfaces ***/

    ROUTER(config)# interface fastEthernet 0/0
    ROUTER(config-if)# no shutdown

    ROUTER(config)# interface fastEthernet 0/1
    ROUTER(config-if)# no shutdown

    ROUTER(config)# interface serial 0/0/0
    ROUTER(config-if)# no shutdown

    ROUTER(config)# interface serial 0/0/1
    ROUTER(config-if)# no shutdown

    /*** Ejercutar en modo configuración para cerrar el puerto ***/
    
    ROUTER(config)# config-register 0x2102

    /*** Copiamos la configuración activa a la configuración de inicio ***/

    ROUTER# copy running-config startup-config
```
