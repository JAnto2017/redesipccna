# Configuración Básica VTP

![alt text](image-32.png)

El switch S1 como servidor. Los otros switch como clientes.

## Configuración Switch S1

```S1
    ...
    S1(config)# vtp domain GRUPO
    S1(config)# vtp mode server
    S1(config)# vtp password cisco

    S1(config)# vlan 10
    S1(config-vlan)# name CELESTE
    S1(config-vlan)# exit

    S1(config)# vlan 20
    S1(config-vlan)# name AMARILLO
    S1(config-vlan)# exit

    S1(config)# vlan 30
    S1(config-vlan)# name VERDE
    S1(config-vlan)# exit

    S1(config)# vlan 100
    S1(config-vlan)# name NATIVA
    S1(config-vlan)# exit

    S1# show vtp status
```

## Configuración de las VLAN en Switch S1

```S1
    ...
    S1(config)# interface fastEthernet 0/1
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 10
    S1(config-if)# exit

    S1(config)# interface fastEthernet 0/2
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 20
    S1(config-if)# exit

    S1(config)# interface fastEthernet 0/3
    S1(config-if)# switchport mode access
    S1(config-if)# switchport access vlan 30
    S1(config-if)# exit

    S1(config)# interface gigabitEthernet 0/1
    S1(config-if)# switchport mode trunk
    S1(config-if)# switchport trunk native vlan 100

    S1# show interface gigabitEthernet 0/1 switchport
```

## Configuración Switch S2

```S2
    ...
    S2(config)# vtp domain GRUPO
    S2(config)# vtp password cisco

    S2# show vtp status

    S2(config)# vtp mode client
```

Tras configurar el Switch S1, si accedemos al Switch S2 y ejecutamos `show vlan brief` veremos como, ya ha aprendido las VLAN creadas en el switch S1.

```S2
    ...
    S2(config)# interface fastEthernet 0/1
    S2(config-if)# switchport mode access
    S2(config-if)# switchport access vlan 10

    S2(config)# interface fastEthernet 0/2
    S2(config-if)# switchport mode access
    S2(config-if)# switchport access vlan 20

    S2(config)# interface fastEthernet 0/3
    S2(config-if)# switchport mode access
    S2(config-if)# switchport access vlan 30
```

Esto es para que el S3 pueda aprender las VLAN:

```S2
    ...
    S2(config)# interface range gigabitEthernet 0/1 - gigabitEthernet 0/2
    S2(config-if-range)# switchport mode trunk
    S2(config-if-range)# switchport trunk native vlan 100
```

## Configuración Switch S3

```S3
    ...
    S3(config)# vtp mode client
    S3(config)# password cisco

    S3# show vtp status
```

El switch S3 no está aprendiendo nada, ya el enlace no es troncal (_trunk_).

```S3
    ...
    S3# show interface gigabitEthernet 0/2 switchport
    S3# show vtp status

    S3# show vlan brief     //ahora si que muestra las VLAN ya están aprendidas

    //asignar las VLAN aprendidas

    S3(config)# interface fastEthernet 0/1
    S3(config-if)# switchport mode access
    S3(config-if)# switchport access vlan 10

    S3(config)# interface fastEthernet 0/2
    S3(config-if)# switchport mode access
    S3(config-if)# switchport access vlan 20

    S3(config)# interface fastEthernet 0/3
    S3(config-if)# switchport mode access
    S3(config-if)# switchport access vlan 30
```
