# CURSO TEÓRICO PRÁCTICCO DE REDES IP - CCENT / CCNA / CISCO

- [CURSO TEÓRICO PRÁCTICCO DE REDES IP - CCENT / CCNA / CISCO](#curso-teórico-prácticco-de-redes-ip---ccent--ccna--cisco)
  - [Arquitectura TCP/IP y Modelo OSI](#arquitectura-tcpip-y-modelo-osi)
    - [Definición de Red de datos](#definición-de-red-de-datos)
      - [Protocolos](#protocolos)
      - [Estándares](#estándares)
    - [Modelos de Red TCP/IP y OSI](#modelos-de-red-tcpip-y-osi)
      - [Modelo TCP/IP](#modelo-tcpip)
      - [Modelo OSI](#modelo-osi)
      - [Modelo TCP/IP actual](#modelo-tcpip-actual)
    - [Capa Aplicación en el modelo TCP/IP](#capa-aplicación-en-el-modelo-tcpip)
      - [Arquitectura cliente/servidor](#arquitectura-clienteservidor)
    - [Capa Transporte en el modelo TCP/IP](#capa-transporte-en-el-modelo-tcpip)
      - [Recuperación de errores](#recuperación-de-errores)
    - [Capa de Red en el modelo TCP/IP](#capa-de-red-en-el-modelo-tcpip)
      - [Direccionamiento](#direccionamiento)
      - [Enrutamiento](#enrutamiento)
    - [Capa Enlace de Datos y Capa Física](#capa-enlace-de-datos-y-capa-física)
    - [Encapsulación / Desencapsulación](#encapsulación--desencapsulación)
      - [Transmisión de los datos](#transmisión-de-los-datos)
      - [Recepción de los datos](#recepción-de-los-datos)
    - [Modelo OSI de 7 capas](#modelo-osi-de-7-capas)
  - [Fundamentos de la capa 2 - Switch](#fundamentos-de-la-capa-2---switch)
    - [Introducción a las LAN Ethernet](#introducción-a-las-lan-ethernet)
      - [Direcciones MAC](#direcciones-mac)
      - [Direcciones UNICAST / MULTICAST / BRIOADCAST](#direcciones-unicast--multicast--brioadcast)
    - [Redes Half-Duplex / protocolo CSMACD](#redes-half-duplex--protocolo-csmacd)
    - [Dominio de colisión](#dominio-de-colisión)
    - [Funciones del Switch: Learning y Forwarding](#funciones-del-switch-learning-y-forwarding)
      - [Forwarding (reenvío de tramas)](#forwarding-reenvío-de-tramas)
      - [Learning (construcción de tabla)](#learning-construcción-de-tabla)
    - [Visión global del Learning y del Forwarding](#visión-global-del-learning-y-del-forwarding)
  - [Fundamentos de la capa 3 - Routing](#fundamentos-de-la-capa-3---routing)
    - [Introducción a la capa de Red y a los Routers](#introducción-a-la-capa-de-red-y-a-los-routers)
    - [Dirección IP y Máscara de Red](#dirección-ip-y-máscara-de-red)
    - [Cálculo de direcciones IP de Red, Host y Broadcast](#cálculo-de-direcciones-ip-de-red-host-y-broadcast)
    - [Funciones de los Routers: routing y forwarding](#funciones-de-los-routers-routing-y-forwarding)
    - [Tabla de rutas en Windows y Linux](#tabla-de-rutas-en-windows-y-linux)
      - [W10 / W11](#w10--w11)
      - [Linux](#linux)
  - [Gestión de equipos Cisco](#gestión-de-equipos-cisco)
    - [GNS3 / Cisco Packet Tracer](#gns3--cisco-packet-tracer)
    - [Introducción a la configuración de equipos Cisco usando CLI](#introducción-a-la-configuración-de-equipos-cisco-usando-cli)
    - [Puerto y cable de consola](#puerto-y-cable-de-consola)
    - [Modos de acceso SSH y Telnet](#modos-de-acceso-ssh-y-telnet)
    - [Tipos de memoria, secuencia de arranque y ficheros de configuración](#tipos-de-memoria-secuencia-de-arranque-y-ficheros-de-configuración)
  - [Redes IP: Cableado, ARP, Wireshark](#redes-ip-cableado-arp-wireshark)
    - [Cable estructurado: directo y cruzado](#cable-estructurado-directo-y-cruzado)
      - [Straiht Through Cable (Directo)](#straiht-through-cable-directo)
      - [Crossover Cable (Cruzado)](#crossover-cable-cruzado)
    - [Ancho de Banda, Delay, Autonegociación del Half/Full Dúplex](#ancho-de-banda-delay-autonegociación-del-halffull-dúplex)
    - [ICMP, PING](#icmp-ping)
    - [El protocolo ARP](#el-protocolo-arp)
    - [Introducción a Wireshark](#introducción-a-wireshark)
      - [Instalar Wireshark en Ubuntu](#instalar-wireshark-en-ubuntu)
    - [Primeros pasos con Wireshark](#primeros-pasos-con-wireshark)
    - [Frame Rewrite](#frame-rewrite)
  - [Rutas Estáticas y Rutas por defecto](#rutas-estáticas-y-rutas-por-defecto)
    - [Rutas Estáticas](#rutas-estáticas)
    - [Rutas por Defecto](#rutas-por-defecto)
  - [Fundamentos capa 4 - TCP y UDP](#fundamentos-capa-4---tcp-y-udp)
    - [Protocolo TCP y UDP - Multiplexación](#protocolo-tcp-y-udp---multiplexación)
      - [Transmission Control Protocol TCP](#transmission-control-protocol-tcp)
      - [User Datagram Protocol UDP](#user-datagram-protocol-udp)
    - [TCP - 3 Way Handshake](#tcp---3-way-handshake)
    - [TCP: Finalizado de la Conexión y Envío de Datos](#tcp-finalizado-de-la-conexión-y-envío-de-datos)
    - [TCP: Control de Flujo, Recuperación de Errores, Velocidad](#tcp-control-de-flujo-recuperación-de-errores-velocidad)
    - [TCP y UDP: Checksum](#tcp-y-udp-checksum)
      - [Checksum](#checksum)
  - [VLAN - Configuración](#vlan---configuración)
    - [Teoría de las VLAN](#teoría-de-las-vlan)
    - [Configuración de interfaces en modo Access](#configuración-de-interfaces-en-modo-access)
    - [Configuración de interfaces en modo Trunk](#configuración-de-interfaces-en-modo-trunk)
      - [DTP (Dynamic Trunking Protocol)](#dtp-dynamic-trunking-protocol)
  - [NAT \& PAT](#nat--pat)
    - [CLASSFUL / CLASSLESS (direcciones IP pública y privadas)](#classful--classless-direcciones-ip-pública-y-privadas)
    - [Introducción al NAT](#introducción-al-nat)
      - [Terminología NAT](#terminología-nat)
      - [Explicación del NAT Estático](#explicación-del-nat-estático)
      - [Expliecación del NAT Dinámico](#expliecación-del-nat-dinámico)
    - [NAT Estático](#nat-estático)
    - [Terminología Cisco para NAT](#terminología-cisco-para-nat)
    - [PAT Estático - Port Fordwarding](#pat-estático---port-fordwarding)
    - [PAT Dinámico](#pat-dinámico)
  - [Protocolo DNS](#protocolo-dns)
    - [DNS conceptos fundamentales](#dns-conceptos-fundamentales)
    - [Modos de acceso SSH / Telnet](#modos-de-acceso-ssh--telnet)

## Arquitectura TCP/IP y Modelo OSI

### Definición de Red de datos

**Equipos finales**, hacen uso de la red.
**Equipos de red**, permiten que la red funcione.
**Enlaces**, son los cables o las señales inalámbricas.
**Protocolos** y los **Estándares**, desarrollan y permiten el funcionamiento de la red de forma coordinada.

#### Protocolos

Conjunto de reglas, que permiten la comunicación entre dos dispositivos. Muchos protocolos, son *estándares*.

#### Estándares

Nos indican cómo deben establecerse las reglas o normas. Aplicado a la conexión de un RJ45 estable el órden los cables.

Un **estándar** se utiliza en muchos paises, es extendido a varios fabricantes.

### Modelos de Red TCP/IP y OSI

Conjunto de documentos que definen el fuuncionamiento de una red.

El modelo **TCP/IP** se impuso. Es un conjunto de **protocolos** que permite la comunicación entre los dispositivos.

El **RFC** (*Request For Comments*) estandarización, compatibilidad, modularidad.

#### Modelo TCP/IP

| Capa | Protocolo |
| --- | --- |
| Aplicación | POP3, SMTP, HTTP, SSH, FTP, DNS |
| Transporte | TCP, UDP |
| Internet | IP, ICMP, OSPF |
| Enlace | Ethernet, ATM, Frame Relay, PPP, FDDI |

#### Modelo OSI

| Capa modelo OSI | Capa modelo TCP/IP |
| --- | --- |
| Aplicación, Presentación, Sesión | Aplicación |
| Transporte | Transporte |
| Red | Internet |
| Enlace de datos, Física | Enlace |

#### Modelo TCP/IP actual

- Aplicación
- Transporte
- Red
- Enlace de datos
- Física

### Capa Aplicación en el modelo TCP/IP

La capa **aplicación** proporciona servicios a las aplicacones. Algunos protocolos que podemos encontrar en esta capa son: *POP3, SMTP, HTTP, SSH, FTP, DNS*

La *App* se adapta al **protocolo**, con ello, se consigue, **estandarización**.

#### Arquitectura cliente/servidor

Es el más común. El rol del **cliente** es el navegador web (hace las peticiones), y el rol del **servidor** es el que suministra la página web (suministra la información solicitada).

### Capa Transporte en el modelo TCP/IP

En esta capa encontramos dos protocolos:

- **TCP** (*Transmisión Contro Protocol*)
- **UDP** (*User Datagram Protocol*)

Proporcionan servicios a la capa superior (capa Aplicación), como son:

- Recuperación de errores.
- Control de flujo.
- Multiplexación.

#### Recuperación de errores

Si un paquete es enviado en fragmentos y uno de los mismos, se pierde. El **servicio de recuperación de errores** intentará recuperar este *fragmento* perdido para completar el *paquete* de datos.

### Capa de Red en el modelo TCP/IP

El protocolo que está en esta capa es **IP** (*Internet Protocol*). Ofrece funciones de:

- Direccionamiento.
- Enrutamiento.

Las direciones con la **IP origen** y la **IP destino** se colocan al inicio de la trama, en la cabecera.

Ejemplo de cabecera: *IP + TCP + HTTP + Datos*.

#### Direccionamiento

Cada equipo tiene una dirección única. Las direcciones son agrupables (números que designan una red).

#### Enrutamiento

Lo **Routers** designan los *caminos* por donde se enviarán los paquetes de datos o tramas.

- Reenvío / Forwarding.
- Routing.

### Capa Enlace de Datos y Capa Física

Estas dos capas **Enlace de datos** y **Física** del modelo TCP/IP, se encarga de la comunicación de entre *host*.

- **Enlace de datos**. Añade una *cabecera* y una *cola* a la trama de datos que se envía a la red. El protocolo de control de acceso al medio, se realiza en esta capa, como son: *Ethernet*, *PPP*, *Frame Relay*, *X.25*, *ATM*, etc.
- Capa **Física**. Determina los valores de tensión eléctrica (ethernet), luminosa (fibra óptica) o electromagnéticas (redes inalámbrica).

### Encapsulación / Desencapsulación

Se aplica la **pila** de protocolos de las diferentes capas.

1. **Segmento** (Packet Segment) es el conjunto de *datos + cabecera TCP* de la capa **Transporte**.
2. **Paquete** (Packet) es el conjunto de *datos + cabecera IP* de la capa de **Red**.
3. **Trama** (Frame) es el conjunto de *datos + cabecera Ethernet* de la capa **Enlace de datos**.
4. **Bits** es el conjunto de *bits* en la capa **Física**.

#### Transmisión de los datos

- La *cabecera* está formada por: *protocolo* más *datos*, en la capa **Aplicación**.
- En la capa **Transporte** se añade, al segmento de datos anterior, el protocolo **TCP** o el **UDP**.
- En la capa inferior, la capa de **Red**, se añade la cabecera **IP**.
- En la capa inferior, la capa de **Enlace de Datos**, se añaden *cabecera* y *cola* al segmento de datos, indicando *Ethernet* y añadiendo la **MAC**.
- En la capa **Física**, transmite el paquete de datos **encapsulado**, por el medio designado.

#### Recepción de los datos

El proceso es en orden inverso al anterior.

- Capa **Física**, recibe el paquete de datos, **encapsulado** y lo **desencapsula**.
- Capa **Enlace de datos**, determina la **MAC**, el tipo de medio usado *Ethernet*. Comprueba la dirección en la cabecera, para determinar si el paquete de datos es para él.
- Capa de **Red**, lee la cabecera de su protocolo, para determinar si el paquete es para él, determina la **IP** del remitente y del destinatario.
- Capa de **Transporte**, procesa que tipo de protocolo ha usado, **TCP** o **UDP**.
- Capa de **Aplicación**, procesa los datos a visualizar.

### Modelo OSI de 7 capas

Modelo de 7 capas. Son las siguientes:

- Capa 7 **Aplicación**: proporciona un formato de datos común.
- Capa 6 **Presentación**: responsable de la compresión y el cifrado.
- Capa 5 **Sessión**: administra el intercambio de datos, sincroniza los datos entre hosts.
- Capa 4 **Transporte**: establece, mantiene y finaliza las conexiones entre hosts. Recupera los errores. Control del flujo de la red.
- Capa 3 **Red**: establece la conectividad entre redes distintas. Permite el enrutamiento. Direccionamienot Lógico.
- Capa 2 **Enlace de datos**: detección de errores, direccionamiento físico.
- Capa 1 **Física**: especificaciones físicas (eléctricas, codificación, modulación) para utilizar el medio y enviar las señales.

El **CCNA** tiene como objetivo dominar las capas **Red** y **Enlace de datos**, corresponden con el número 3 y 2 respectivamente. Para estas capas, el equipo por excelencia, es el *Router* que trabaja con los protocolos **IP**, **ICMP**, **OSPF** entre otros. En cambio, en la capa **Enlace de datos** el equipo estrella es el *Switch* y el *Access Poit* utilizando los protocolos: **ETHERNET**, **PPP**, **HDLC** entre otros.

El **PDU** *Protocol Data Unit* es el empaqueta de: **Segmento**, **Paquete**, **Trama** y **Bits**. Siendo las unidades de datos de cada una de las capas del modelo OSI.

Las capas aportan:

- Menos complejidad.
- Modularidad, simplificando su aprendizaje y el desarrollo.
- Interfaces estándar. Diferentes fabricantes compatibles entre sí.
- Independecia entre capas.

## Fundamentos de la capa 2 - Switch

### Introducción a las LAN Ethernet

- PAN (*Personal Area Network*), es una red Bluetooth, áreas muy pequeñas.
- LAN. Redes de espacio acotado. Ethernet, WLAN.
- CAN. Redes de Campus (*Campus Area Network*)
- WAN. Redes de área amplia.

**Ethernet** define la **capa 1 Física** y la **capa 2 Enlace de datos**. Existe **Ethernet v2**, todos los equipos funcionan con este estándar, del mismo modo con las tramas y los frames. Las velocidades serán diferentes en función del enlace.

#### Direcciones MAC

A nivel de **capa 2 Enlace de datos**, existe otro tipo de direcciones, **MAC** (Media Access Control) con función específica. En la práctica, se utilizan direcciones de **capa 2** y de **capa 3**.

| Preámbulo | SFD | Destino | Origen | Tipo | Datos | FCS |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 7 byte | 1 byte | 6 byte | 6 byte | 2 byte | 46 - 1500 byte | 4 byte |

Direcciones de capa 2 enlace de datos **MAC** (Media Access Control) están formadas por **6 bytes = 48 bits = 12 dígitos hexadecimales**.

Las direcciones **MAC** identifican una interfaz de red, también llamadas **NIC** (Nic Network Card).

El organismo **IEEE** asigna direcciones **MAC** a los distintos fabricantes, **OUI** (Organizationally Unique Identifier). El fabricante que tiene **OUI** los pone en los primeros 24 bits de la dirección. Los siguientes 24 bits los asigna el fabricante.

Existen páginas web donde se le pone la dirección MAC, e indicará el fabricante, y para ello, compara los primeros 24 bits con su base de datos.

#### Direcciones UNICAST / MULTICAST / BRIOADCAST

Las direcciones **Unicast** son aquellas tramas, que se envían a un solo equipo. Las direcciones deben ser únicas.

Las direcciones **Multicast** son aquellas tramas, que se envían a un grupo de equipos. La trama se enviara a un grupo de hosts.

Las direcciones **Broadcast** son aquellas tramas, que se envían a *todos* los equipos de la red.

### Redes Half-Duplex / protocolo CSMACD

El **HUB** permite el acceso a la red e interconexión entre equipos. Cuando recibe una *trama* la reenvía a cada una de las interfaces, menos por la que lo ha recibido. Repite la señal que les entra por un puerto al resto de las interfaces.

El **HUB** no utiliza información de la **capa 2**, ni *header*, *tail*, *MAC*. Por lo que, se considera que trabaja en la **capa 1**.

Para que la información que colisione y se pierda, se utiliza la conexión **Half-Duplex**, siguiendo un protocolo de envío.

1. Escuchar antes de enviar.
2. Si alguien está enviando, esperar, así nunca se enviará y se recibirá a la vez.
3. Cuando está libre, enviar.
4. Si dos equipos envían al mismo instante de tiempo se produce colisión.

El algoritmo que garantiza la no colisión de tramas, es el algoritmo **CSMA/CD**.

1. Cuando un equipo tiene un *frame* para enviar, escucha antes y no inicia el envío hasta que el medio Ethernet no está liberado.
2. Cuando el medio está libre, el equipo empieza a enviar.
3. Se puede dar el caso que 2 equipos empiezan a enviar a la vez cuando el medio está libre, produciendo así una collisión. En este caso, los equipos la detectan y empiezan a enviar una señal que lo indica. De manera independiente, los equipos que estaban enviando eligen un tiempo de espera al azar, antes de volver a enviar petición de acceso al medio. Cuando se acaba el tiempo de espera, volvemos al inicio del algoritmo.

### Dominio de colisión

Está formado por todos los equipos conectados a un mismo *HUB* que tienen que aplicar el mismo algoritmo **CSMA/CD** para evitar colisiones.

A medida que la red crece, el dominio de colisión aumenta, y existe mayor probabilidad de colisión y menor rendimienot de la red.

El dispositivo **BRIDGE** segmenta dominios de colisión, permitiendo que la red mejore en rendimiento. Se instala entre los **HUBS**, permitiendo que dos equipos (uno por dominio de colisión) puedan enviar a la vez.

El  **Bridge** cuando recibe una *trama*, la pone en una cola de espera (memoria interna) de forma temporal. Comprueba si es necesario reenviarlo por el otro puerto o descartarlo, antes de reenviarlo: aplica el algoritmo **CSMA/CD**.

El **SWITCH** hace los mismo que el **Bridge**, pero con multitud de puertos (interfaces) que pueden ser de: 8, 16, 24, 48. Dependiendo del modelo del **Switch**.

Con la utilización de los **Switch** se reduce aun más el tamaño de los dominios de colisión. Si se sustituyen todos los **Hubs** por **Switch**, pasamos a tener una red **Full-Duplex**, donde todos los equipos pueden enviar y recibir datos a la vez, eliminándose las colisiones (libre de colisiones).

### Funciones del Switch: Learning y Forwarding

El **SWITCH** al recibir un *frame* lo almacena, hasta reenviarlo por la interfaz correcta. Para ello crea, una tabla de direcciones **MAC**. Estas direcciones identifican el puerto físico del **Switch**. Estas direcciones **MAC**, son de **capa 2**.

#### Forwarding (reenvío de tramas)

Es el envío de *tramas* de un puerto a otro. En la *trama* se incorpora el *destino*, que es la **dirección MAC** del host receptor del mensaje. El **Switch** realiza una comprobación con su tabla de direcciones, para consultar la **MAC** asociada a la dirección **IP** y en qué interfaz está conectado.

El reenvío de la *trama* por todos los puertos excepto por el que ha llegado, se conoce como **Flooding** y sirve para completar la **tabla de direccioens MAC** cuando no se conocen todos los dispositivos que están conectado.

#### Learning (construcción de tabla)

Es la acción de construcción de la **Tabla de Direcciones MAC**.

Cuando el **Switch** se inicia, no tiene la tabla de direcciones completada, al llegarle una *trama*, lee las direcciones **MAC** origen y destino. Completa los puertos para estas direcciones.

La **dirección MAC** destino de tipo **Broadcast** es de la forma: *ffff.ffff.ffff*. Reenviando la *trama* por todos los puertos excepto por el que le ha llegado. Esto permite completar la dirección de los puertos, en la **Tabla de Direcciones**.

### Visión global del Learning y del Forwarding

Al iniciar una red, el **Switch** lo primero que hace es, *aprender* completando las tablas de direcciones, con la primera *trama*. Utiliza el reenvío masivo *flooding* para ir conociendo las direcciones y los puertos utilizados.

## Fundamentos de la capa 3 - Routing

### Introducción a la capa de Red y a los Routers

**IP** Internet Protocol. Tiene el **direccionamiento** y el **enrutamiento**, donde este último incluye en **reenvío/forwarding** y el **routing**.

Para aquellas comunicaciones que estén en la misma red, es suficiente con la **capa 2 Red**. Para aquellos casos en los que las redes, estén formadas por conjunto de redes, no es suficiente, por ello es preciso el uso de la **capa 3** y el **router** para la comunicación entre las redes.

![Cabecera IPv4](imagenes/cabeceraipv4.png)

La **dirección origen** y la **dirección destino** es lo que usan los **Routers** para hacer la función del direccionamiento. ¡Recuerda! la **PDU** es el paquete de *datos* más la *cabecera*.

El **Router** cuando toma la decisión de enviar el paquete por un camino determinado, es lo que se conoce como **enrutamiento** o **routing**. Posteriormente realiza el **forwarding**, pasar un paquete de una interfaz a otra.

### Dirección IP y Máscara de Red

Una dirección IP y Máscara de Red identifica una interfaz de red y no un equipo. Por lo que, un equipo puede tener varias dirección IPs.

La **dirección IPv4** está formada por *4 octetos* de 8 bits separados por un punto. Los valores de cada uno de los *octetos* pueden ir desde, 0 a 255.

La **dirección IPv6** está formada por **dirección de red** más **dirección de host**. Para designar qué bits pertenecen a uno y otro direccionamiento, se utiliza la **máscara de red**.

La **máscara de red** define cuantos bits de una IP son de Host y cuandos son de Red. Está formado por 32 bits, separados en 4 octetos, separados por un punto. Los valores pueden ser números entre 0 y 255. El número binario no puede tener bits centrales del octeto, a cero.

En una **máscara de red** los bits a **1** indica el número de redes que puede direccionar, para los bits a **0** establece el número de Host que puede direccionar.

La notación **prefijo** o **CIDR** se indican de la forma: */24* (24 bits de red y el resto de host).

### Cálculo de direcciones IP de Red, Host y Broadcast

Una dirección IP estará compuesta de: direcciones de Red, varias direcciones de hots y una dirección de broadcast.

- IP de red. No se asigna a un dispositivo. La usan los Routers para calcular la red y determinar las dirección. Acaba en cero aquellos octetos asignados a la dirección de los host.
- IP de broadcast. Es la última de la dirección (acaba en 255 aquellos bits asignados al direccionamienot de host). No es asignable a ningún dispositivo.
- IP de Host. Es el resto de direcciones entre las dos anteriores. Si son asignables a los dispositivos.

*Ejemplo 1*. Cálculo, para la siguiente IP 172.16.5.11/16 que corresponde con 172.16.0.0/16 determinar:

| IP de red | IPs de Hosts | IP de broadcast |
| :---: | :---: | :---: |
| 172.16.0.0 | 172.16.0.1 a 172.16.255.254 | 172.16.255.255 |

Algunas de las IPs de Host son: 172.16.1.0, 172.16.1.255, 172.16.254.0, 172.16.254.255, 172.16.255.0

*Ejemplo 2*. Está incluida la IP de Host 20.200.7.12 en las siguientes redes? Para la respuesta, se debe mirar a los octetos correspondientes a la porción de red.

- 20.0.0.0/8    => si
- 20.0.0.0/16   => no
- 20.200.0.0/24 => no
- 20.201.7.0/24 => no
- 20.200.0.0/16 => si
- 20.200.7.0/24 => si

### Funciones de los Routers: routing y forwarding

Ejemplo de *tabla de rutas* para un Router A conectado a otros dos Routers:

| IP destino | Mask | Next-Hop | Interfaz de salida |
| :---: | :---: | :---: | :---: |
| 20.0.0.0 | 255.0.0.0 | 10.0.0.2 | g0/0 |
| 30.0.0.0 | 255.0.0.0 | directamente conectada | g0/1 |
| 10.0.0.0 | 255.255.255.0 | directamente conectada | g0/0 |
| 0.0.0.0 | 0.0.0.0 | 10.0.0.2 | g0/0 |
| 40.0.0.23 | 255.255.255.255 | 10.0.0.2 | g0/0 |

La dirección **0.0.0.0** es una dirección especial, que significa *todas las direcciones*. Cualquier IP hace *match* en esta ruta 0.0.0.0.

Las direcciones con *máscara 255.255.255.255* son **rutas de Hosts**.

Un Router cuando tiene varias rutas que puede elegir, selecciona la **ruta más específica** y es la que tiene la **máscara mayor** en la que hace *match*. En resumen:

1. Dónde hace MATCH?
2. Si hay más de un MATCH: elegir la ruta más específica.

Ejemplo de *tabla de rutas* para un Router doméstico, el cual no puede tener almacenado los millones de redes.

| IP destino | Mask | Next-Hop | Interfaz de salida |
| :---: | :---: | :---: | :---: |
| 192.168.1.0 | 255.255.255.0 | directly | LAN |
| 0.0.0.0 | 0.0.0.0 | 80.2.125.1 | WAN |

La **tabla de rutas** no es específico de los Routers, otros dispositivos tales como PC, Switch también las usan.

### Tabla de rutas en Windows y Linux

#### W10 / W11

1. **ipconfig /all** para ver la IPv4 de red y la Máscara de red.
2. **netstat -r** para ver la tabla de rutas, donde las dos primeras entradas indican *puerta de enlace*, *interfaz*, *máscara de red*, *IP de red*, *métrica*.

#### Linux

1. **ifconfig** para ver la IPv4 de red y la Máscara de red.
2. **netstat -r** para ver la tabla de rutas.

## Gestión de equipos Cisco

### GNS3 / Cisco Packet Tracer

**GNS3**, es un emulador, NO es un simulador. Entorno real en los dispositivos. Los inconvenientes son: requiere de la imagen del SO del dispositivo que queremos virtualizar. Otro inconveniente, es que ejecutar una simulación con varios equipos virtualizados requiere de muchos recursos del PC.

**Cisco Packet Tracer**, es un simulador. No es 100% real.

### Introducción a la configuración de equipos Cisco usando CLI

**CLI** (*Command Line Interface*). Herramienta para enviar instrucciones, en modo texto, al sistema operativo **IOS** y visualizar los resultados. Permite gestionar el equipo.

- **CLI** mejores opciones de configuración y más velocidad que con el uso de GUI.
- **GUI** más fácil de usar que CLI.

El acceso utilizando TCP/IP, se puede realizar mediante **SSH**. También está disponible la opción **OOB** (*Gestion Out of Band*). Si cae la red, cae la posibilidad de gestionar el equipo.

### Puerto y cable de consola

La **comunicación por consola**, no utiliza el protocolo **IP**. Utiliza un conector RJ45 por el lado del Switch y un conector USB por el lado del PC. Algunos fabricantes añaden conector uUSB en lugar de utilizar RJ45. Las tres opciones que podemos encontrar:

1. PC RS233 => Switch RJ-45.
2. PC USB => Switch RJ4-45.
3. PC USB => Switch uUSB.

![Tipos de conexión](imagenes/conexionconsola.png)

El cable *Cisco*, es un cable **ROLLOVER** con conector RJ-45. En un extremo monta la especificación **568-B** y en el otro extremo monta, justo al contrario que el otro extremo **M, BM, V, BA, A, BV, N,, BN** comenzando por el Pin-1 hasta el Pin-8.

![Rollover](imagenes/rollover.png)

### Modos de acceso SSH y Telnet

**SSH** *Secure Shell*, utiliza direcciones **IP** y el puerto **22** para establecer la conexión. La información se envía encriptada.

**Telnet** utiliza direcciones **IP** y el puerto **23**, para establecer la conexión. La información se envía en texto plano, no está encriptada.

### Tipos de memoria, secuencia de arranque y ficheros de configuración

| MEMORIAS RAM | MEMORIAS ROM |
| :---: | :---: |
| NVRAM, FLASH, RAM | ROM (sólo Lectura) |

- **ROM**. Sólo de lectura. Tiene el **BootStrap** comprueba hardware más instrucción de arranque inicializando IOS.
- **FLASH**. Tiene el IOS más ficheros adicionales. Copias de seguridad.
- **NVRAM**. Tiene la configuración de inicio del equipo (*Startup Configuration File*). Es una memoria RAM no volátil.
- **RAM**. Tiene la configuración activa del equipo (*Running Configuration File*). Imagen del IOS en ejecución. Tablas de Enrutamiento. Tablas de direcciones MAC. Paquetes en espera (*Buffers*)

Los pasos que ejecuta un Switch al iniciar son:

1. Encendido del equipo, ejecuta el **bootstrap**.
2. Busca la imagen del SO en Flash y lo carga en la memoria RAM.
3. La IOS en ejecución está en la RAM, y es copia de la IOS en memoria FLASH. Esta última se puede actualizar y cambiar mientras en Switch está ejecutando el IOS de la RAM.
4. Carga el fichero de configuración en la RAM y lo ejecuta línea a línea. Configuración activa del equipo (**Running configuration file**)

| NVRAM | RAM |
| --- | --- |
| Startup configuration file | Running configuration file |
| Archivo de configuracion de inicio | Configuración activa del equipo |
| Almacena los cambio aun cuando se halla apagado el equipo | Los cambios no se guarda si hay un corte de energía |

Los comandos para pasar configuraciones a la memoria NVRAM, es: **copy running-config startup-config**. Otra variante es **write**.

Para borrar toda la configuración del equipo, existen tres opciones:

1. **erase startup-config**
2. **erase nvram** (no funciona en CPT en uno real sí)
3. **write erase**

## Redes IP: Cableado, ARP, Wireshark

### Cable estructurado: directo y cruzado

| Nombre | Velocidad | IEEE | Tipo | Longitud máx. |
| :---: | :---: | :---: | :---: | :---: |
| Ethernet | 10 Mbps | 10BASE-T 802.3 | Cu | 100 m |
| FastEthernet | 100 Mbps | 100BASE-T 802.3u | Cu | 100 m |
| GigabitEthernet | 1000 Mbps | 1000BASE-T 802.3ab | Cu | 100 m |
| GigabitEthernet | 1000 Mbps | 1000BASE-X 802.32 | Fibra | Kms |
| 10G Ethernet | 10 Gbps | 10GBASE-T 802.3an | Cu | 100 m |

El estándar **IEE** definido para la **capa 1 Física** del modelo OSI. El trenzado de los cables reduce el **crosstalk** es la interferencia electromagnética **EMI** entre los pares de los cables.

**NIC** (*NETWORK INTERFACE CARD*)

Equipos que transmiten por los pines 1-2 (equipos de capa 3 excepto el AP), se les llama **MDI** Medium Dependent Interface:

- PC
- SERVER
- ROUTER
- FIREWALL
- ACCESS POINT

Equipos que transmiten por los pines 3-6 (equipos de capa 2, aunque Switch dedicado puede ser de capa 3), la interfaz es de tipo **MDI-X** Medium Dependent Interface Crossover:

- SWITCH
- HUB

La norma es que, para conectar equipos que pertenecen al mismo grupo, se utiliza cable cruzado y para conectar equipos que pertenecen a grupos distintos, se utiliza cable directo.

#### Straiht Through Cable (Directo)

El pineado del **cable directo** es el mismo en ambos extremos. Es decir, se monta la misma especificación (568-A ó 568-B) en ambos conectores RJ-45.

¿Por qué usar un cable directo? Depende de los equipos a conectar en los extremos.

- PC: transmiten por pines 1-2, reciben por pines 3-6.
- Switch: transmiten por pines 3-6, reciben por pines 1-2.

#### Crossover Cable (Cruzado)

El pineado del **cable cruzado** es diferente en cada extremo. En un conector RJ-45 se monta la especificación (568-A ó 568-Bb) y en el otro conector RJ-45 se instala la contraria.

La conexión entre dos PC requiere de un cable cruzado, ya que ambos equipos envían por pines 1-2 y reciben por pines 3-6. Lo mismo sucede si interconectamos dos Switch, como transmite y recibe por los mismos pines en ambos extremo require del cable cruzado. El Router es igual a un PC, por tanto, requiere de un cable cruzado para interconectar ambos.

### Ancho de Banda, Delay, Autonegociación del Half/Full Dúplex

- **BandWidth** = Ancho de Banda. Velocidad máxima de transferencia de datos. Se mide en `bits/Segundo`, también puede se representa en `Bytes/Segundo`.
- **Delay** = Latencia. Tiempo que tarda en llegar la información. Se mide en `ms`.
- **Interface** = Lógico. Es la representación software del puerto físico y es donde aplicamos la configuración.
- **Puerto** = Físico.
- **Dúplex**. Existen tres opciones: `Auto`, `Half` y `Full`.
- **Autonegociación**. Cuando configuramos los equipos en `Auto` (automático) la velocidad y el Dúplex, la negociación de los dispositivos, se establece con los valores más altos soportados por ambos participantes en los extremos. Es decir, un Switch puede tener diferentes velocidades en las insterfaces.

Si no se puede negociar:

- El criterio para la **Velocidad** (sólo en Switches CISCO), es  que, si falla la autodetección se utilizará la más lenta posible.
- El criterio para el **Dúplex** es: si la velocidad es 10 ó 100 entonces *Half-Dúplex* y si es superior, entonces *Full-Dúplex*.

Un comando que permite checkear la información del puerto, es: `show interface status`.

Un comando que permite establecer la velocidad de las interfaces y el modo dúplex, es: `speed 100 duplex full`.

### ICMP, PING

**ICMP**. Protocolo de control y notificación de errores. Es un protocolo de Red (algunos autores lo asignan a la capa de Transporte) y sus principales aplicaciones son: el diagnóstico y la notificación. Para ello utiliza la herramienta **ping** de la capa de aplicación.

Permite comprobar la conectividad ente dos equipos, al hacer un **ping** desde un PC hacia el otro.

URL con enlace a la IANA: [ICMP](https://iana.org/assignments/icmp-parameters/icmp-parameters.xml)

- ICMP - Echo Request.
- ICMP - Echo Reply.

### El protocolo ARP

Es un protocolo de resolución de direcciones **ARP** es de **capa 2**. Este protocolo permite almacenar en una tabla, las direcciones IP y las MAC correspondientes. Así un Host que únicamente conozca la IP, consultando la tabla puede saber la MAC.

Un PC al inicio envía una trama a la dirección **Broadcast** con la dirección **FFFF.FFFF.FFFF.FFFF** como dirección destino. El PC que responde a la trama, responde en una dirección **UNICAST** añadiendo la dirección **MAC** solicitada.

Fases del proceso:

1. Petición **ARP** con la pregunta: ¿Quién tiene esta dirección IP?. Enviada por **Broadcast** a todos los equipos.
2. Respuesta **ARP** del equipo que tiene la IP solicitada, añadiendo la dirección **MAC**. Esta respuesta la envía por **UNICAST**.

Tipos de **ARP**:

- ARP "tradicional".
- Reverse ARP.
- Proxy ARP.
- Gratuitous ARP.
- ARP Probe y ARP Announcement.

### Introducción a Wireshark

Captura la información que viaja por una red de datos, sin afectar a la comunicación realizando copias de los paquetes/tramas. Muestra la información de las distintas capas. Por tanto, con **WireShark** podemos:

1. Analizar los datos, para la resolución de problemas (*Troubleshooting*).
2. Validar el corrector funcionamiento de las aplicaciones (*Testing Apps*).
3. Uso Educativo/Aprendizaje del funcionamiento de las redes de datos.

#### Instalar Wireshark en Ubuntu

Añadir el repositorio de **Wireshark** para una versión estable: `sudo add-apt-repository ppa:wireshark-dev/stable`.

Actualizamos los paquetes: `sudo apt-get update`.

Lazamos la instalación: `sudo apt-get install wireshark`.

Si salen errores en la instalación, se deben matar los procesos que lo interrumpan: `ps aux | grep dpkg`, `ps aux | grep apt`, `sudo  kill -9 15724`.

Tras la instalación y reinicio del sistema, si ejecutamos **Wireshark** si no aparecen las interfaces del equipo, se debe a que el usuario no está en el grupo. Se debe añadir el usuario al grupo wireshark: `sudo gpasswd -a anto wireshark`.

### Primeros pasos con Wireshark

En la página de inicio, se pueden elegir: **filtros de visualización** y **filtros de captura**.

Los **filtros de captura** se configuran antes de lanzar la captura de tráfico. En cambio los **filtros de visualización** se aplican a posteriori, una vez lanzada la captura de tráfico.

En pantalla principal están el conjunto de *tramas* que ha ido capturanto, (en la mayotía de casos serán *paquetes* sin cabecera de *capa 3* o superiores). Al seleccionar una de las *tramas*, en la parte central se actualiza la información detallada de esta *trama* con las distintas capas que la componene. En el pie de la pantalla, está la misma información pero en hexadecimal/ASCII.

Para ver la **Tabla ARP** en W10, ejecutar `arp -a` en consola de comandos. Con eta tabla podemos saber las **MAC** de los Host que están conectados en la misma red. Para limpiar la **Tabla ARP**, ejecutar el comando `arp -d`.

¡Recuerda! La petición **ARP** requiere descubrir la **MAC** que corresponde a esa **IP**.

### Frame Rewrite

Se produce cuando una **trama** pasa de una red a otra, es decir, a través de los Routers.

A la hora de enviar una **trama** el PC origen, pondrá la **IP destino** en la trama, pero la **MAC** que pondrá, será la del Router, concretamente la **puerta de enlace**. De esta forma, se asegura que el Router pueda aceptar la **trama** y posteriormente reenviarla al destino a la otra interfaz que apunta hacia el destino. En esta nueva trama se cambia la **MAC origen** del Router y se añade la **MAC destino** que corresponde con el Host.

Los *saltos* que se producen en las redes virtuales, por parte de la **trama** hasta llegar al destino, se conoce como **Hop-To-Hop Delivery** en la **capa 2**. Para la **capa 3** la entrega es de extremo a extremo **End-To-End Delivery**.

**Tabla de direcciones MAC** no es lo mismo que la **Tabla ARP** (se aplica a equipos de nivel 3). El Switch tiene *Tabla de direcciones MAC*.

No se genera nuevo tráfico **ARP** cuando el Host o el Servidor ya conoce la **MAC** del Router o viceversa.

## Rutas Estáticas y Rutas por defecto

### Rutas Estáticas

Las **rutas estáticas** son aquellas que se introducen de forma manual por el Administrador de la red. En cambio, las **rutas dinámicas** se crean automáticamente, gracias a un protocolo de enrutamiento.

El comando para añadir una ruta estática (*en modo config*) es: `ip route IP-red-destino/mask IP-puerta-enlace`.

### Rutas por Defecto

Son rutas estáticas **Default Gateway** donde el destino no es una única red, sino todas las redes. La representación utiliza 0.0.0.0 para que acepte dodas las rutas:

| IP destino | Máscara | Next-Hop | Interfaz de salida |
| :---: | :---: | :---: | :---: |
| **0.0.0.0** | **0.0.0.0** | 10.0.0.2 | Gig 0/0 |

Cuando ninguna dirección de red, hace *match* usa **Gateway of Last Resort** (gateway de último recurso) Un ejemplo: `ip route 0.0.0.0 0.0.0.0 80.2.125.1`.

## Fundamentos capa 4 - TCP y UDP

### Protocolo TCP y UDP - Multiplexación

Estos protocolos proporcionan, servicios a los protocolos de la **capa de aplicación** (capa superior). Funciones que tienen cada uno de los protocolos:

#### Transmission Control Protocol TCP

Es un protocolo **Orientado a Conexión**.

- Multiplexación.
- Checksum.
- Conexión: establecimiento, finalización.
- Control de flujo.
- Recuperación de errores.
- Orden en los datos.
- PDU (cabecera + datos) se llama **Segmento**.
- Cabecera: IP origen, IP destino, SEQ, ACK, Offset, Flag bits, Checksum, ventana.

#### User Datagram Protocol UDP

Es un protocolo **No Orientado a Conexión**.

- Multiplexación.
- Checksum.
- PDU (cabecera + datos) se llama **Datagrama**.
- Cabecera: IP origen, IP destino, Longitud, Checksum.

---

La **Multiplexación** permite varias comunicaciones de manera simultánea. Utilizando sólo la **IP** podemos establecer varias comunicaciones simultáneas entre *Host* y *Server*. Para ello, utiliza un identificador en la cabecera de la capa de transporte, que es el **Puerto**.

El **SOCKET** es la asociación entre la dirección **IP**, el protocolo **TCP / UDP** y el **puerto** (número). Ejemplo de **Socket**: 10.0.0.1 / TCP / 1029.

El **número de puerto**, se define en el **RFC 1700** y se llama **Well Known Ports**:

| Nº Puerto | Protocolo | Aplicación |
| :---: | :---: | :---: |
| 7 | TCP ó UDP | Echo |
| 20 | TCP | FTP |
| 21 | TCP | FTP Control |
| 23 | TCP | Telnet |
| 25 | TCP | SMTP |
| 53 | TCP ó UDP | DNS |
| 66 | UDP | DHCP Server |
| 67 | UDP | DHCP Client |
| 69 | UDP | TFT |
| 80 | TCP | HTTP |
| 143 | TCP | IMSP |
| 161 | UDP | SNMP |
| 179 | TCP | BPG |
| 443 | TCP ó UDP | SSL |
| 514 | UDP | SYSLOG |

### TCP - 3 Way Handshake

La función que hace al protocolo **TCP** que sea **Orientado a la Conexión**. Si partimos, que cualquier conexión, tiene que: *Establecer* la conexión, *Enviar* los datos y *Finalizar* o cerrar la conexión.

El **Establecimiento** de la conexión se realiza aplicando el **3 Way Handshake**. Permite la sincronización entre ambos miembros. Es necesario antes de enviar los datos. Crea la conexión entre **Sockets** con la apertura de puertos, números SEQ (secuencia), números ACK y Flag de control.

El **número ACK** está formado por el **números SEQ** (aleatorio) más el **tamaño bytes** recibidos.

Conexión TCP establecida usando 3 Way Handshake:

1. Envía SYN (SEQ=300 CTL=SYN)
2. Recibe SYN. Envía SYN (SEQ=500 ACK=300+1 byte, CTL=SYN,ACK)
3. Recibe SYN,ACK. Envía ACK (SEQ=301 ACK=501 CTL=ACK)

Ejemplo de captura con Wireshark una trama con el uso del **3 Way Handshake** que se estable entre un navegador y la petición al servidor de una página web.

![3 Way Handshake](images/way3handshake.png)

### TCP: Finalizado de la Conexión y Envío de Datos

El **Finalizar** una conexión, es una negociación de 4 pasos. Se termina la conexión desde cada lado independientemente. Permite liberar el **Socket** asociado en cada extremo.

Pasos en la finalización de la conexión:

1. Envía el Host ACK, FIN (SEQ=400 ACK=800 CTL=ACK, FIN)
2. Recibe el Servidor ACK, FIN. Envía ACK (SEQ). Envía el Servidor ACK (SEQ=800 ACK=401 CTL=ACK).
3. Envía el Servidor ACK, FIN (SEQ=800 ACK=401 CTL=ACK, FIN)
4. Recibe el Host ACK, FIN. Envía el Host ACK (SEQ=401 ACK=801 CTL=ACK)
5. Recibe el Servidor ACK.

### TCP: Control de Flujo, Recuperación de Errores, Velocidad

El **Orden de los Datos** es una característica del protocolo **TCP**. Los datos pueden llegar desordenados al destinatario, y gracias al **SEQ** los puede ordenar y confirmar con un **ACK**.

La **Recuperación de Errores** es una característica del protocolo **TCP**. Cuando un Servidor confirma el dato recibido, envía al Host un **ACK** que permite determinar si se ha recibido todo la trama enviada. Si no es así, el Host reenvía, tan solo aquella que se perdió en la primera comunicación.

La función de **Control de flujo** hace referencia a permitir regular la velocidad de transferencia. Usa el mecanismo de *ventana deslizante* **Sliding Window**. La **Ventana** define el tamaño de bytes seguidos que se pueden enviar, antes de esperar confirmación.

### TCP y UDP: Checksum

En el protocolo **UDP** el **PDU** recibe el nombre de **Datagrama**. Este protocolo permite el envío de *datagramas* sin necesidad de establecer previamente una conexión. La *cabecera* está formada por una información mínima (**Puerto Origen, Puerto Destino, Longitud, Checksum**). Es un protocolo que no tiene confirmación, es no confiable, por lo que, el que lo envía nunca recibe confirmación. No permite el control de flujo. No ordena los *datagramas*. No segmenta.

El protocolo **UDP** es ideal en aplicaciones en tiempo real. Es un protocolo ligero, donde no interesa retransmitir ni reordenar.

#### Checksum

La *integridad de los datos* es la acción de encapsusar los datos para evitar los errores en la recepción de los mismos. El **Checksum** (suma de comprobación) tiene en cuenta la *integridad* de los datos que encapsulan, para mitigar el riesgo de la pérdida o modificación de la información. Si el **checksum** detecta que los datos se han alterado, los descarta. Por tanto, **checksum** valida los datos no los corrige.

Proceso de validación de un datagrama:

1. Host calcula el **checksum** como la suma de los campos y lo coloca en la cabecera. Se envía al receptor.
2. El receptor, en la capa 4, realiza el mismo cálculo del **checksum**. El resultado obtenido lo compara con el recibido, comparando ambos **checksum's**. Si es igual, entonces los datos son válidos, en caso contrario, son descartados.

## VLAN - Configuración

### Teoría de las VLAN

Aplicado a Switch de la capa 2, dominio de Broadcast (reenvío de la trama por todas las interfaces menos por la que lo recibió).

Segmentar los dominio de Broadcast, a nivel de rendimiento, es una mejora. Un Switch no segementa dominios de Broadcast, en cambio, un Router si segmenta dominios de Broadcast.

En un Switch podemos crer las **Virtual LAN** para **segmentar** el tráfico: **Broadcast**, **Multicast** y el **Unicast**.

Una **VLAN** permite:

- Menor coste, ya que evita comprar nuevos Switch y Routers para aislar el tráfico.
- Limitar el número de equipos que reciben los Broadcast.
- Limitar el número de equipos que reciben Flooding. El Flooding sigue haciéndose, sólo que, en su propia VLAN.
- Reduce el uso de CPU de los equipos, tanto los de red, como los equipos finales.
- Incrementa la seguridad.
- Permite agrupar a los equipos de forma lógica y no física. Si tengo dos o tres Switch y en cada uno de ellos, tengo creada una VLAN entoces los Host conectados a ellas, pueden comunicarse entre sí. Se conoce como **VLAN Extendida** a lo largo de la red en varios *Switchs*.
- Permite los enlaces de tipo **Trunking** entre *Switchs*, para extender las **VLAN**. El protocolo que lo define es el **802.1 Q = dot1Q**. Esta tecnología, añade una cabecera en la **trama** Ethernet, para diferenciar las tramas de cada **VLAN**.
- En el *Switch* se deben configurar, interfaces en **Modo Access** y otras en **Modo Trunk**.
- En las interfaces en **Modo Trunk** se permite el tráfico de más de una **VLAN**. Tráfico Etiquetado.
- En las interfaces en **Modo Access** nunca se utilizará el protocolo **802.1 Q = dot1Q**. Tráfico NO-etiquetado.
- Cada **Trunk** (enlace troncal) siempre tiene definica una **VLAN NATIVA**. Implica que, no se añade cabecera 802.1Q a los paquetes que se envían por esta **VLAN**.
- Cuando un Switch, recibe por un **Trunk** un paquete sin cabecera 802.1Q, considerará que el paquete forma parte de la **VLAN NATIVA**.
- Ambos Switch, que están conectados en la misma línea troncal, se deben configurar la misma **VLAN NATIVA** en sus interfaz.
- NO es posible la comunicación directa entre dos **VLAN** distintas. Si es posible la comunicación mediante *routing*, es decir, mediante un Router, siempre que sean redes diferentes.

### Configuración de interfaces en modo Access

Se pueden configurar las interfaces en **Modo Access** o en **Modo Trunk**. Los pasos a seguir son:

1. Creación de las VLAN a nivel configuración global (`configure terminal`). Para ello se usa el comando: `vlan número_de_vlan`. Posteriormente se asigna el nombre, aunque este paso es opcional, para ello usar el comando: `name nombre_vlan`.
2. Definir Modo de la interfaz (opcional). Para seleccionar la interfaz, `interface fastEthernet 0/0`, luego definir el *modo de acceso* usando el comando: `switchport mode access`
3. Asignar la VLAN a la interfaz. Para asignar la VLAN utilizar el comando: `switchport access vlan numero_de_vlan`. Para configurar un grupo de interfaces, `interface range fastEthernet 0/0 - 6`.

La **VLAN por defecto** no es lo mismo que una **VALN nativa**. Son independientes.

| VLAN por defecto | VLAN nativa |
| --- | ---|
| No se puede modificar en un Switch | Si se puede modificar |
| Tiene siempre el número 1 | Se define en ambos extremos de un troncal |
| Tramas no viajan etiquetadas | Las Tramas si están etiquetadas |

Existe otra forma (modo rápido). Sólo tiene un paso, que es: asignar VLAN a la interfaz. `interface fastEthernet 0/0`, `switchport access vlan numero_de_vlan`.

Con la forma rápida, estamos creando la VLAN automática y el modo del puerto por defecto es **dynamic auto**.

### Configuración de interfaces en modo Trunk

Existen diferentes modos de configuración de un **Switchport mode** para el uso de un troncal **Trunk**:

- **Dynamic Desirable**.
- **Dynamic Auto** (modo por defecto).
- **Trunk**.

Los comandos para configuración en modo **Trunk** (*Switchport Mode Trunk*) son:

1. Definir la encapsulación (802.1Q ya que ISL está en desuso en algunos Switch): `interface fastEthernet 0/1`, `switchport trunk encapsulation dot1q`.
2. Definir puerto en modo TRUNK: `switchport mode trunk`.
3. Definir VLANs permitidas (opcional): `switchport trunk allowed vlan 1,2,5,33`. Se puede poner un rango: `switchport trunk allowed vlan 1-33`.
4. Definir la VLAN nativa (opcional), por defecto es la 1: `switchport trunk native vlan 2`.

Los comandos para mostrar información en las VLAN:

- Muestra información de todas las interfaces: `show interfaces switchport`.
- Muestra información de la interface indicada: `show interfaces fa0/1 switchport`.
- Muestra información sobre los enlaces troncales: `show interfaces trunk`.
- Muestra las VLANs creadas y los puertos asignados: `show vlan`.
- Variantes del anterior comando: `show vlan id nº_vlan` y `show vlan brief`.

#### DTP (Dynamic Trunking Protocol)

El **Dynamic Trunking Protocol DTP** aplicable al *Dynamic Desirable* y al *Dynamic Auto* tiene las siguientes características:

- Protocolo propietario de Cisco.
- Automatiza la configuración del Trunk. Soporta dos tipos de encapsulaciones: **802.1Q**, **ISL**.
  
Es una forma de automatizar la configuración de los puertos troncales. Lo recomendable es no utilizarlo nunca.

**Modo Administrativo** de una interfaz, funcionamiento:

- Dynamic Auto. De forma pasiva, se mantiene a la espera de recibir mensajes de negocioción, para que dado el caso, responda y negocie la utilización del puerto como Trunk.
- Dynamic Desirable. Inicia de forma activa la negociación, enviando y respondiendo a los mensajes de negociación para determinar si el puerto puede funcionar como Trunk.
- Trunk. Funciona siempre como puerto Trunk.
- Access. Funciona siempre como puerto de acceso.

**Modo Administrativo** de una interfaz, tabla de combinaciones:

| Modos | Dynamic Auto | Dynamic Desirable | Trunk | Access |
| :---: | :---: | :---: | :---: | :---: |
| Dynamic Auto | Access | Trunk | Trunk | Access |
| Dynamic Desirable | Trunk | Trunk | Trunk | Access |
| Trunk | Trunk | Trunk | Trunk | error de configuración |
| Access | Access | Access | error de configuración | Access |

## NAT & PAT

### CLASSFUL / CLASSLESS (direcciones IP pública y privadas)

Direccionamiento IPv4 está formada por: direccionamieto Red + direccionamiento Host. Con la máscara de red, si los bits están a 1 indicamos *dirección de red* y si los bits están a 0 *dirección de host*. A su vez, las redes IPv4 las podemos dividir en clases, donde a esta clasificación la denominamos **Modelo CLASSFUL**.

El *modelo classful** consiste en que, el total de direcciones IP posibles se formaron grupos de clases, en total se crearon 5 clases.

Modelo **CLASSFUL**:

- Clase A. Red pensada para direcciones tipo **UNICAST**. Primer *octeto* para identificar la *red*, los otros tres *octetos* identifican los *host*.
- Clase B. Red pensada para direcciones tipo **UNICAST**. Los dos primeros *octetos* identifican la *red* y los otros dos restantes identifican los *hosts*.
- Clase C. Red pensada para direcciones tipo **UNICAST**. Los tres primeros *octetos* identifican la *red* y el último *octeto* identifica al *host*.
- Clase D. Red pensada para direcciones tipo **MULTICAST**.
- Clase E. Red pensada para direcciones tipo **MULTICAST**.

![Clases IPv4](imagenes/clasesipv4.png)

El **Modelo CLASSFUL** no es práctica, por ello se crea el **Modelo CLASSLESS** en el que hay más libertad en la asignación de bits para direccionar la red o los host según convenga.

Otra clasificación de redes IP es: **IP Públicas**, **IP Privadas**.

El organismo IETFF propuso, (por el escaso número de IP públicas disponibles):

- Utilizar direcciones IP privadas para la red interna.
- Utilizar **NAT** (*Network Address Translation*) Mecanismo de traducción de IPs Públicas/Privadas. Con ello, una empresa con IP privadas, puede conectarse a Internet "traduciendo" dichas IP privadas, en una IP Pública.

La clasificación de las IP privadas, según RFC 1918 (*Request For Comments*), sin necesidad de autorización por parte de la IANA:

| IP mínima | IP máxima | Máscara |
| :---: | :---: | :---: |
| 10.0.0.0 | 10.255.255.255 | /8 |
| 172.16.0.0 | 172.31.255.255 | /12 |
| 192.168.0.0 | 192.168.255.255 | /16 |

Estas redes privadas no son enrutables en Internet. Un Router que recibe una trama con una de estas direcciones, con destino hacia Internet, lo rechazará. Estas IPs NO están permitidas en Internet. Sólo tienen validez en la red privada. Por ello, pueden ser utilizadas simultáneamente por diferentes compañías.

### Introducción al NAT

**NAT** *Network Address Translation*, traducciones de direcciones de red, para acceso a Internet. Sirver para "traducir" **IP privadas** en **IP públicas** y viceversa. Y está definido en el estándar RFC 1918. La finalidad es ahorrar direcciones de **IP públicas** y pasar a utilizar direcciones de **IP privadas** que pueden ser reutilizadas por diferentes usuarios tantas veces como se quiera.

Utilizando **NAT** podríamos "traducir" de **IP privadas** a **IP privadas**, no es obligatorio que sea a **IP públicas**. O de **IP públicas** a **IP públicas**, es correcto y también es **NAT**.

#### Terminología NAT

Dentro de **NAT** existen diferentes modos de operación, cada uno de ellos recibe un nombre diferentes. El término **NAT** o el término **PAT**. Ambos modifican, parte de la cabecera de un paquete.

- **PAT** (*Port Address Translation*). Modifica de la cabecera, la IP Origen y la IP Destino (**capa 3**), además de modificar el puerto (**capa 4 TCP**)
- **NAT** (*Network Address Translation*). Modifica de la cabecera, la IP Origen y la IP Destino (**capa 3 IP**)

Los dispositivos que pueden realizar el **NAT** son: **Routers** y los **Firewalls**.

- **Valores Post Traducción**. Son aquellas IP ya modificadas, una vez aplicado el protocolo **NAT**.
- **Valores Pre-Traducción**. Son aquellas IP antes de que lleguen al Router encargado de aplicar el protocolo **NAT**.

#### Explicación del NAT Estático

Los define el administrador. Corresponde con *valores post-traducción* de IP, que han sido fijados por el Administrador de la red. Si es NAT sólo aplicable a la IP y si es PAT se fija el valor de IP y del Puerto.

Ejemplo de traducción de IP + Port: 192.168.1.2:4444 -> 85.10.4.11:5555. A este tipo de traducciones se le llama **One-to-One**.

#### Expliecación del NAT Dinámico

Los selecciona el equipo de red (Router) de modo aleatorio, de modo, que no podemos predecir. Los *valores post-traducción* son escogidos por el Router en el momento en que reciben el paquete y lo elige de una manera dinámica.

Ejemplo de traducción de IP + Port: 192.168.1.2:4444 -> rango(85.10.4.11 a 85.10.4.20):rango(1024 a 65535). A este tipo de traducciones se le llama **One-to-Many** ó **Many-to-One**.

### NAT Estático

**Network Address Translation Static**. Sólo se traduce direcciones IP y fijadas por el Administrador. Se puede *traducir* tanto la dirección IP origen como la dirección IP destino, es **bidireccional**.

**SRC** = Source (Origen).
**DST** = Destination (Destino).

Ejemplo de tramas enviadas desde un PC hasta un Servidor pasando por un Router con Gateway 83.1.4.9 por el lado del PC y con 10.1.1.8 por el lado del Servidor.

| PC => Router| Router => Servidor |
| :---: |:---: |
| SRC 40.4.5.5:4441 | SRC 40.4.5.5:4441 |
| DST 83.1.4.9:80 | DST 10.1.1.8:80 |

| PC <= Router | Router <= Servidor |
| :---: |:---: |
| SRC 10.1.1.8: 80 | SRC 10.1.1.8: 80 |
| DST 40.4.5.5:4441 | DST 40.4.5.5:4441 |

### Terminología Cisco para NAT

El **NAT Estático** en la práctica no se usa, ya que no ahorra direcciones IP.

Cisco introduce cuatro términos:

- **Inside Local**.
- **Inside Global**.
- **Outside Local**.
- **Outside Global**.

**Inside** y **Outside**, ambas hacen referencia a la ubicación física del equipo, que tiene esa dirección. **Inside** es una red interna desde el Router. **Outside** red externa al Router.

**Local** y **Global**. Perspectiva desde la que vemos esa dirección. **Local** es el direccionamiento visible desde la perspectiva de la red interna. **global** es el direccionamiento visible desde la perspectiva de la red externa.

![Sstatic NAT](imagenes/natestatico.png)

Para configurar un **NAT** estático en CPT se deben definir: **interfaz Inside** y la **Interfaz Outside**, además de aplicar el comando *NAT* específico.

### PAT Estático - Port Fordwarding

El **Static PAT** realiza la traducción de la **IP**, del **puerto** y del **protocolo**, ejemplo:

| IP red pública + Puerto | IP red privada + Puerto |
| :---: | :---: |
| 83.1.4.3:80 | 10.1.1.8:443 |
| 83.1.4.5:21 | 10.1.1.9:21 |

Diferencias y similitudes entre **Static PAT** y **Static NAT**:

| NAT static | One-to-One |
| PAT static | One-to-Many |

Redirección de puertos **Port Fordwarding** (reenvío de puertos). Se consigue cerrar el resto de puertos, con esto, la seguridad aumenta ya que, si el puerto indicado no es el correcto el paquete de datos no pasa por el Router.

### PAT Dinámico

Una red privada con varios Host se comunica a tavés de un Router con una única IP pública con Internet. El puerto se cambia por uno aleatorio dentro de un rango de números comprendido entre 0 al 65.535.

Ejemplo: una red privada con IP 10.1.1.0/24 se conecta a un Router con IP pública 83.1.4.5. Siendo esta IP pública la que se comunica con los servidores en Internet.

En la **tabla de traducciones** del Router, tendremos tantas entradas como conexiones (TCP o UDP) hayan iniciado los equipos pertenecientes a la red 10.1.1.0/24.

## Protocolo DNS

### DNS conceptos fundamentales

**DNS** (Domain Name System).El *sistema de nombre de dominios*, está en la capa de **aplicación**. En la capa de **red** va la dirección IP origen y la IP destino. Para saber esta dirección IP destino, el servidor **DNS** tiene una base de datos con los nombres y su IP asociada.

Algunas direcciones IP de servidores DNS libres 2024:

![Lista Servidores DNS](imagenes/listaservidns.png)

Con el comando `nslookup` en OS Windows podemos obtener la IP asociada a un dominio. Ejemplo: *c:\w10>nslookup <www.cisco.com>*, nos devuelve todo un conjunto de información entre la que está la IPv6, IPv4. Otra alternativa para conocer la IP asociada a un dominio, es hacer un `ping` al nombre. Ejemplo: *ping <www.cisco.ocm>*, nos devuelve la IPv4 asociada.

### Modos de acceso SSH / Telnet

Con **SSH** el comando `switch(config)#line vty 0 4` es un modo de acceso, de hasta dieciseis conexiónes (0 a 15). Así entramos en modo de configuración de línea: `switch(config-line)#`. Dentro de este modo de configuración podemos definir la contraseña `#password mi_contraseña_elegida` y el usuario `#login`.

Otra forma de configuración de acceso en SSH: `#line vty 0 15` y el usuario con `#login local`. Donde el Switch puede tener configurado `username pepito secret mi_password`.

Las gestión de la Seguridad, se puede realizar: **sin password**, **con password**, **con autenticación (usuario y password)**. La autenticación se puede realizar en local y externa.

En Cisco, con el comando `enable password 1234`, no se recomienda ya que usando el comando `show run` muestra la clave secrete en texto plano. Por esto Cisco ideó otra forma de configurar la password `enable secret 1234` se encripta siendo más segura.

Con el comando `crypto key generate rsa` se generan las claves. Previamente se deben configurar el dominio `ip domain-name nombre_dominio` y el nombre del equipo con `hostname nombre_equipo`. Por último, una vez generadas las claves, debemos crear un usuario local `username juan secret run1234`, `line vty 0 15`, `login local`.